#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:24:46
# @Updated Date: 2017-01-16 14:51:00

#   Level       Numeric value
#   CRITICAL    50
#   ERROR       40
#   WARNING     30
#   INFO        20
#   DEBUG       10
#   NOTSET      0
LOG_DEBUG = 'DEBUG'
LOG_INFO = 'INFO'
LOG_WARNING = 'WARNING'
LOG_ERROR = 'ERROR'
LOG_CRITICAL = 'CRITICAL'

TYPE_INT = 0
TYPE_FLOAT = 1
TYPE_STRING = 2
TYPE_BOOLEAN = 3
TYPE_DATETIME = 4
TYPE_TIMEDELTA = 5

# Error Code
E_OK = 0
E_FAILED = 1

# time constants
MILI_PER_SECOND = 1000
SECS_PER_MINUTE = 60
MINUTES_PER_HOUR = 60
HOURS_PER_DAY = 24
DAYS_PER_WEEK = 7
DAYS_PER_MONTH = 30
MONTHS_PER_YEAR = 12

################################################################################
TIME_FORMAT = '%Y-%m-%d %H:%M:%S'
DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'

###################################################################################
HTTP_METHODS = ["put", "post", "get", "patch", "delete", "head", "options"]

###################################################################################
# Time in milliseconds to wait for responses from the back end or elasticsearch.
# This must be > 0
REQUEST_TIMEOUT = 300000

# alert severity
SEVERITY_LEVEL = ["Critical", "Urgent", "High", "Normal", "Low"]

# global time format and timezone
TIME_FORMAT = '%Y-%m-%d %H:%M:%S'
CHINA_TIME_ZONE = 8

# expired time for session
SESSION_EXPIRED_MINUTES = MINUTES_PER_HOUR * HOURS_PER_DAY
SESSION_EXPIRED = SESSION_EXPIRED_MINUTES*SECS_PER_MINUTE

# default locale
DEFAULT_LOCALE = 'en'

# validate code range
_letter_cases = "abcdefghjkmnpqrstuvwxy"  # lower case chars，remove 'i'，'l'，'o'，'z'
_upper_cases = _letter_cases.upper()    # upper case chars
_numbers = ''.join(map(str, range(3, 10)))  # numbers, remove 0, 1, 2
VALIDATION_CODES = ''.join((_letter_cases, _upper_cases, _numbers))

# validation retry times threshold
VALIDATION_RETRY_TIMES = 3  # when input password VALIDATION_RETRY_TIMES times wrong, need additinal verification code
VALIDATION_RETRY_TIMES_THRESHOLD = 10  # retry times shreshold

# default id
DEFAULT_ID = '00000000-0000-0000-0000-000000000000'

# min length of required input string field
MIN_LENGTH = 1

# scheduler frequency threshold: 10 minutes
ALERT_INTERVAL_MIN_THRESHOLD = 10*SECS_PER_MINUTE

# default showed event number
DEFAULT_LIMIT_NUMBER = 30

SUPPORTED_AGENTS = ['zabbix']
SUPPORTED_EVENTS = ['trigger', 'resolve']
SUPPORTED_SEVERITIES = {
    5: {
        'level': 5,
        'severity': ['low', 'information', 'not classified'],
        'time-threshold': 48,   # hours
        'processing_time': 96,
        'description': 'You have a general development question or want to request a feature.'
    },
    4: {
        'level': 4,
        'severity': ['normal', 'average'],
        'time-threshold': 24,
        'processing_time': 48,
        'description': 'You can work around the problem. Non-critical functions of your application are behaving abnormally. You have a time-sensitive development question.'
    },
    3: {
        'level': 3,
        'severity': ['warning'],
        'time-threshold': 8,
        'processing_time': 24,
        'description': 'You can not work around the problem. Critical functions of your application are impaired or degraded.'
    },
    2: {
        'level': 2,
        'severity': ['urgent', 'high'],
        'time-threshold': 2,
        'processing_time': 12,
        'description': 'You can not work around the problem, and your organization is significantly impacted. Important functions of your application are unavailable.'
    },
    1: {
        'level': 1,
        'severity': ['critical', 'disaster'],
        'time-threshold': 0.5,
        'processing_time': 6,
        'description': 'You can not work around the problem, and your organization is at risk. Critical functions of your application are unavailable.'
    }
}

DEFAULT_ROOT_CAUSE = [
    {
        "name": "系统维护",
        "description": "计划内的人为事件，如计划内关机、重启、网站升级、网站维护等。"
    },
    {
        "name": "网络故障",
        "description": "网络原因导致的故障，如主干网络发生的故障。"
    },
    {
        "name": "硬件故障",
        "description": "服务器硬件导致的故障，如磁盘故障、内存故障、网卡故障等。"
    },
    {
        "name": "软件故障",
        "description": "软件原因导致的故障，如操作系统故障、Web服务器运行故障、应用程序BUG、软件服务容量超载等。"
    },
    {
        "name": "基础服务故障",
        "description": "基础服务平台故障，比如内部IPA、DNS、VPN、git、Jenkins、wiki、jira等。"
    }
]

DEFAULT_SEVERITY = 3
DEFAULT_RESPONSE_HOURS = 8
SUPPORTED_SEVERITIES_COUNT = 5
SUPPORTED_ACTIVITY_ACTIONS = ['trigger', 'open', 'acknowledge', 'unacknowledge', 'assign', 'notify', 'note', 'resolve', 'close']
SUPPORTED_NOTIFY_TYPES = ['email', 'weixin']
SUPPORTED_SCHEDULERS = ['weekly', 'daily']
DEFAULT_SUPPORT_USER = 'devops@rongcapital.cn'
DEFAULT_EMAIL_SUFFIX = '@rongcapital.cn'
DEFAULT_SUPPORT_DEPARTMENT = 'devops'
AUTO_TICKET_TRIGGER = 'API'
DEFAULT_WECHAT_SENDER = 'robot'

# Oncall
ONCALL_EMAIL = 'email'
ONCALL_SMS = 'sms'
ONCALL_WECHAT = 'wechat'
ONCALL_SLACK = 'slack'
ONCALL_FAILURE_FILE = '/rc/data/oncall_failure_message.log'

SMS_URL = "https://api.open.ruixuesoft.com:30005/ropapi"
SMS_METHOD = "ruixue.sms.note.send"
SMS_FORMAT = "json"
SMS_ACCESS_TOKEN = "3630B3C5-EFB3-4509-A15D-2872D7D4EE89"
SMS_CHANNEL_CODE = "BF175285-4821-4C41-9462-6EC00F84E57B"
SMS_RC_CODE = '0029'

SCHEDULE_REMINDER = 'SCHEDULE_REMINDER'

# ipa login shell
LOGIN_SHELL = ['/bin/bash', '/sbin/nologin']

DEFAULT_RETRY_TIMES = 3

# general category
GENERAL_CATEGORY = 'general'

# Object Storage API 
BUCKETNAME = {
    "ONDUTY": "onduty"
}

BUCKETDIRNAME = {
    "AVATAR": "avatar",
    "ATTACHMENT": "attachment"
}

SERVICETOKEN = {
    "AVATAR": {
        "access_key": "UCCOICB1EONUJ2BOZ7FW",
        "secret_key": "IvMh3W839EDNG0xA2dAQJAriUKlqaMzPEpKVOibN"
    },
    "ATTACHMENT": {
        "access_key": "UCCOICB1EONUJ2BOZ7FW",
        "secret_key": "IvMh3W839EDNG0xA2dAQJAriUKlqaMzPEpKVOibN"
    },
    "ONDUTY": {
        "access_key": "UCCOICB1EONUJ2BOZ7FW",
        "secret_key": "IvMh3W839EDNG0xA2dAQJAriUKlqaMzPEpKVOibN"
    }
}

IMGSIZE = (256, 256)

FILE_TYPE = {
    "DIR": "dir"
}

USER_INIT_STATUS = "initial"
