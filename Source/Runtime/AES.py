#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:31:19
# @Updated Date: 2015-12-21 17:14:50

import hmac
import hashlib
from Crypto.Cipher import AES as CryptoAES


class AESException(Exception):
    """A simple exception class used for AES exceptions"""
    pass


class AES(object):

    def __init__(self, secret):
        try:
            key = hmac.new(secret, secret, hashlib.sha1).hexdigest()[8:]
            self.__cipher = CryptoAES.new(key)
        except Exception as error:
            raise AESException(error)

    def encrypt(self, raw):
        try:
            data = [raw[i:i+16] for i in range(0, len(raw), 16)]
            msg = ""
            for line in data[:-1]:
                encoded = self.__cipher.encrypt(line)
                msg += encoded
            return "".join((msg, str(data[-1])))
        except Exception as error:
            raise AESException(error)

    def decrypt(self, encoded):
        try:
            data = [encoded[i:i+16] for i in range(0, len(encoded), 16)]
            msg = ""
            for line in data[:-1]:
                encoded = self.__cipher.decrypt(line)
                msg += encoded
            return "".join((msg, data[-1]))
        except Exception as error:
            raise AESException(error)

# -----------------------------------------------------
#               UT
# -----------------------------------------------------
if __name__ == '__main__':
    secret = 'karen loves jass,d dkfjkflewkrnmrnm1235erdskgmert,m3lddlgdkmr,g ser'
    aes = AES(secret)
    encoded = aes.encrypt(secret)
    decoded = aes.decrypt(encoded)
    print secret
    print decoded
