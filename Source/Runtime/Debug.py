#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:31:19
# @Updated Date: 2015-12-21 17:15:17

import pdb

class Debug(object):

    def _set(self, debug):
        if debug:
            pdb.set_trace()
