#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:31:19
# @Updated Date: 2015-12-21 17:15:55

class Singleton(object):
     """
     A singleton base class.
     Based on: http://code.activestate.com/recipes/52558/
     """
     _singleton = None
     def __new__(cls, *args, **kwargs):
         if cls._singleton == None:
             cls._singleton = object.__new__(cls)
         return cls._singleton

     @classmethod
     def get_singleton(cls):
         if '_singleton' not in vars(cls):
             return None
         else:
             return cls._singleton
