#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:31:19
# @Updated Date: 2015-12-21 17:16:24

import sys

def uuidgen(length=36, *args):
    if sys.version_info < (2, 5):
        """
        Generates a universally unique ID.
        Any arguments only create more randomness.
        """
        import socket
        import time
        import random
        import md5
        t = long( time.time() * 1000 )
        r = long( random.random()*100000000000000000L )
        try:
            a = socket.gethostbyname( socket.gethostname() )
        except:
            # if we can't get a network address, just imagine one
            a = random.random()*100000000000000000L
        data = str(t)+' '+str(r)+' '+str(a)+' '+str(args)
        data = md5.md5(data).hexdigest()
        uuid = data[:8] + '-' + data[8:12] + '-' + data[12:16] + '-' + data[16:20] + '-' + data[20:]
        return uuid[:length]
    else:
        import uuid
        return str(uuid.uuid4())[:length]
