#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-10 09:12:51
# @Updated Date: 2015-12-21 17:15:04

import ConfigParser
from datetime import timedelta
import textwrap

from Runtime.Constants import TYPE_INT, TYPE_FLOAT, TYPE_STRING, TYPE_BOOLEAN, TYPE_DATETIME, TYPE_TIMEDELTA

class ConfigException(Exception):
    """A simple exception class used for configuration exceptions"""
    pass

class ConfigSection(object):
    """ Section in the configuration file """
    def __init__(self, name, required, required_if=None, doc=None):
        self.name = name
        self.required = required
        self.required_if = required_if
        self.doc = doc
        self.options = {}

    def get_doc(self):
        return textwrap.dedent(self.doc).strip()


class ConfigOption(object):
    """ Option in the section """
    def __init__(self, name, getter, type, required, required_if=None, default=None, valid=None, doc=None):
        self.name = name
        self.getter = getter
        self.type = type
        self.required = required
        self.required_if = required_if
        self.default = default
        self.valid = valid
        self.doc = doc

    def get_doc(self):
        return textwrap.dedent(self.doc).strip()

class Config(object):
    def __init__(self, cfgfile, sections):
        self.config = ConfigParser.ConfigParser()
        self.config.read([cfgfile])
        self.sections = sections
        self._options = {}

        self.__load_all()

    def __load_all(self):
        required_sections = [s for s in self.sections if s.required]
        conditional_sections = [s for s in self.sections if not s.required and s.required_if != None]
        optional_sections = [s for s in self.sections if not s.required and s.required_if == None]

        sections = required_sections + conditional_sections + optional_sections

        for sec in sections:
            has_section = self.config.has_section(sec.name)

            # If the section is required, check if it exists
            if sec.required and not has_section:
                raise ConfigException("Required section [{0}] not found".format(sec.name))

            # If the section is conditionally required, check that
            # it meets the conditions
            if sec.required_if != None:
                for req in sec.required_if:
                    (condsec, condopt) = req[0]
                    condvalue = req[1]

                    if self.config.has_option(condsec, condopt) and self.config.get(condsec, condopt) == condvalue:
                        if not has_section:
                            raise ConfigException("Section '{0}' is required when {1}.{2}=={3}".format(sec.name, condsec, condopt, condvalue))

            # Load options
            if has_section:
                for opt in sec.options:
                    self.__load_option(sec, opt)


    def __load_option(self, sec, opt):
        # Load a single option
        secname = sec.name
        optname = opt.name

        has_option = self.config.has_option(secname, optname)

        if not has_option:
            if opt.required:
                raise ConfigException("Required option '{0}.{1}' not found".format(secname, optname))
            if opt.required_if != None:
                for req in opt.required_if:
                    (condsec,condopt) = req[0]
                    condvalue = req[1]

                    if self.config.has_option(condsec,condopt) and self.config.get(condsec,condopt) == condvalue:
                        raise ConfigException("Option '{0}.{1}' is required when {2}.{3}=={4}".format(secname, optname, condsec, condopt, condvalue))

            value = opt.default
        else:
            if opt.type == TYPE_INT:
                value = self.config.getint(secname, optname)
            elif opt.type == TYPE_FLOAT:
                value = self.config.getfloat(secname, optname)
            elif opt.type == TYPE_STRING:
                value = self.config.get(secname, optname)
            elif opt.type == TYPE_BOOLEAN:
                value = self.config.getboolean(secname, optname)
            elif opt.type == TYPE_DATETIME:
                value = self.config.get(secname, optname)
                #value = ISO.ParseDateTime(value)
            elif opt.type == TYPE_TIMEDELTA:
                value = self.config.getint(secname, optname)
                value = timedelta(seconds=value)

        if opt.valid != None:
            if not value in opt.valid:
                raise ConfigException("Invalid value specified for '{0}.{1}'. Valid values are {2}".format(secname, optname, opt.valid))

        self._options[opt.getter] = value

    def get(self, opt):
        return self._options[opt]

    @classmethod
    def from_file(cls, configfile):
        c = ConfigParser.ConfigParser()
        c.read(configfile)
        return c
