#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:31:19
# @Updated Date: 2015-12-21 17:14:58

from optparse import OptionParser
import sys
import xmlrpclib
import locale
locale.setlocale(locale.LC_NUMERIC, "")


class Command(object):
    """ Base class of all CLIs """
    def __init__(self, usage, version, argv):
        self.argv = argv
        self.optparser = OptionParser(usage="%s" % usage, version="%s" % version)

    def parse_options(self):
        opt, argv = self.optparser.parse_args(self.argv)
        self.opt = opt

    def error(self, msg):
        self.optparser.error(msg)

    def add_option(self, option):
        self.optparser.add_option(option)

    def console_display(self, format, values):
        print "\33[1m\33[4m",
        for (name,pname,width) in format:
            width = max(len(pname),width) + 1
            centered = pname.ljust(width)
            print centered,
        print "\33[0m"
        for v in values:
            for (name,pname,width) in format:
                value = v[name]
                width = max(len(name),width)
                print " %s" % str(value).ljust(width),
            print

    def console_display2(self, format):
        for row in format:
            print row
        print

    def __format_num(self, num):
        try:
            inum = int(num)
            return locale.format("%.*f", (0, inum), True)
        except (ValueError, TypeError):
            return str(num)

    def console_table(self, format):
        maxwidth = max([len(k) for k in format])
        for k in format:
            print  "\33[1m%s: \33[0m" % k[0].ljust(maxwidth + 1),
            print str(k[1]).rjust(maxwidth + 2)

class LocalCommand(Command):
    """ Local """

    def __init__(self, usage, version, argv):
        Command.__init__(self, usage, version, argv)

class RPCCommand(Command):  #TODO:
    """ Remote """

    def __init__(self, usage, version, argv):
        Command.__init__(self, usage, version, argv)

    def create_rpc_proxy(self, host, port):
        uri = "http://%s:%d" % (host, port)
        # use_datetime is introduced in Python 2.5, which means this code does NOT work with Python 2.4
        # Ref: http://docs.python.org/library/xmlrpclib.html
        #return xmlrpclib.ServerProxy(uri, allow_none=True, use_datetime=True)
        return xmlrpclib.ServerProxy(uri, allow_none=True)

    def login(self, username, passwd):
        self.server = self.__create_rpc_proxy(self.config.rpc_server, self.config.rpc_port)
        try:
            res = self.server.login(username, passwd)
            return res
        except Exception as error:
            print >> sys.stderr, "Error: {0}".format(error)
            exit(1)

# -----------------------------------------------------------------------------------
def test_console_display():
    usage = ''
    version = 1
    argv = ''
    c = Command(usage, version, argv)

    fields = [("id","ID", 3)]
    values = [
        {
            'id'    :   1
        }
    ]
    c.console_display(fields, values)

if __name__ == '__main__':
    test_console_display()
