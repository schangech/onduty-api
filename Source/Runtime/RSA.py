#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:31:19
# @Updated Date: 2015-12-21 17:15:38

from Crypto.PublicKey import RSA as CryptoRSA

#======================================================================================

class RSAException(Exception):
    """A simple exception class used for RSA exceptions"""
    pass

#======================================================================================

class RSA(object):

    def __init__(self, key):
        try:
            self.__key = CryptoRSA.importKey(key)
        except:
            raise RSAException

    def decrypt(self, encoded):
        try:
            return self.__unpad(self.__key.decrypt(encoded))
        except:
            raise RSAException

    def __unpad(self, text):
        if len(text) > 0 and text[0] == '\x02':
            # Find end of padding marked by nul
            pos = text.find('\x00')
            if pos > 0:
                return text[pos+1:]
        return None

# -----------------------------------------------------
#               UT
# -----------------------------------------------------
if __name__ == '__main__':
    import base64

    pem= """-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAwWmVcmjiqrpxn/TV9s74XCw6NGoWAOH/7VAdMUvCV0VjU0ApcleCIsBFYzXl
N+u9fXDe1vfg6olQTvR1Kae1/Y/7fqzDGqxLsu016O7BP+RO60+oVUfyeB+RKsStemB36f6r2dqr
YZizILXOZbKoWZp2mxVwwrrX7qjWX3bMWx3gKf9M02g9M7w556KSbJP41aTYF7s24pcxtg3YE7ow
DCXs9GWkIxy64Muqfj5Jd6/kAbyZSYJcrldfSdgeFqG+vQRRblvwsy6WQIygCdkKNqIFOFVW6Iqt
5gr3rQB8942PSzZP2Gmv6MM0lW3BaDY030h3FPtZZNlyqFQ10XO8cwIDAQABAoIBAFHv3PQfIweY
/NH68zof7G9//Rh/aNru43Ex42wi4C4Ao1d9cxGRqPv34G1rQ7QxFpGx43XGhW5WxcLtJUWEW0EU
YkMEBZOjMl6Wx6PqocvjeNuPX4zhcgMZvHIV6DhT7fkffsZIMRqjih9tZgcHbPKo0V5Z8u9MIHZm
6IqA1YoznSSPdDmp5DH23ceSjrZJN/RJ/XSlxjA4gdr28qe71mkSdRZjND/7Qu1NXMtouR0VZ1xd
jqBB6y2SRhYKy295HO0oINdW80oHiAtGVnWNw0ndVDkAfL//RpGlnB5P2yKrUwAu4SpizXOQNd4V
4Gyj8COnyny+RcYsE/71dx+ZYIECgYEA6a+uOey4uXgCN+o+i6eNkBn2dQtkW2Fgs4g7RW7GtBLm
0bu81N5rn/WXAGz21S1VI0O+pMuc4dls6S5sOek0Bj7P4B9fi7JtsoqfSgw0tpYqwe13wc7S0KAh
RIj+Ds6DXCfkopEeh/9FJfD7UCNO4KNKsO7FgoIo5NWYgr5mUoMCgYEA0+FvICsen7EAo8Ua8QnG
NpigBgsqiSDQh/Up+AvBhp/Mh68ySFLSfmKXThziD10kKbrPyaea8iaPVA/VEOUPe5RTEsi8fXvX
LyVdCVBBlcKc+WVNua7oKA/XgCIXg33rknJERrJ5TBJhKkhtlnXOY7EX3e70IPpZ3FFMvQXYC1EC
gYA+YsBIhoHqQbeV5ds8vUZ1qfi51oi8PIrsYvov1xsFB+bHJy+KRLbGxXHCk1RyJOANwhArPpBr
WvhQaCxMaY2R/ULRuo5vZPsUJx3PTuLju8M+cn7+JpDxUU6QVWI94bP/7kaDf+p+tA9pyaPOCR8N
qYpPIe9ooAb60GsevGXksQKBgQDMXfwezhiKtWLFlD/JUME0NgDb83dCrzkUel/R1h0Dai0Qjub0
JZbMuvHwMSHG6fMQoRT9D8H/YTwMO/0qf5Zwf3+ldaOBcar72ZEkg5nPBtj8IT9YI6Hfym/1Mmz1
ssY/CiJbApjGzE+WRigV4WmCdPVtZqdpC7ETNgPLqtBZEQKBgCwGJzOEohtwLbKggdFo+1xcL5ul
rQfZ00O8zGTBagH46v2xf1arG+8TpRm/0w0PBBET202G9KpiO0EWU1AvFXVK6xFsIZf3nybxIhPf
7S0BM74f0wGAs4HiVRxb5RgbWnDXl8nHtuOyVCp65Bgefk2boNAA1E8lKNNoDlB9Uc47
-----END RSA PRIVATE KEY-----"""

    passwd="""\r\nvmzZ8Orve7eF2P8/EzMvRwnk/Nhpl3DmDD+dKgV1qm5k/t3+5Q/93Ypt9IszRDyNPGZNaK9tz9QD/yp81laustYSSrFnEFDTa68dUpHQQVpETYdrzDHXpkyp2in5did09Rcnh6U2emPgBwBp98U3spFyTj7AED36hfiF/7B/8OYXUeVNdemAmfxcw+YNDMyGAN1srBMnKHGnGiKzRY/7y9qsjizWS1EL3PBiyveuCAWejVPYMwByatM6haU07DQqmRDMFkuspW+3fWBud7NO1zBoiDd33zCSewKZ2e2tBoulNrWv1tx7hPZkxMSqoPGk7Gi/K8m+NmOibOXp8/mkPg==\n\r"""

    pem = pem.replace('-----BEGIN RSA PRIVATE KEY-----\n', '')
    pem = pem.replace('-----END RSA PRIVATE KEY-----', '')
    rsa = RSA(base64.b64decode(pem))
    decoded = rsa.decrypt(base64.b64decode(passwd))
    print decoded
