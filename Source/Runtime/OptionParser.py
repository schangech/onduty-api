#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:31:19
# @Updated Date: 2015-12-21 17:15:32

import optparse as py_optparse

class OptionParser(py_optparse.OptionParser):
    def _init_parsing_state (self):
        py_optparse.OptionParser._init_parsing_state(self)
        self.option_seen = {}

    def check_values(self, values, args):
       	for option in self.option_list:
            if (isinstance(option, Option) and option.required and \
                not self.option_seen.has_key(option)):
                self.error("%s not supplied" % option)
		return (values, args)

class Option(py_optparse.Option):
    ATTRS = py_optparse.Option.ATTRS + ['required']

    def _check_required(self):
        if 'required' in vars(self) and not self.takes_value():
            raise py_optparse.OptionError(
                "required flag set for option that doesn't take a value",
                self)

    # Make sure _check_required() is called from the constructor!
    CHECK_METHODS = py_optparse.Option.CHECK_METHODS + [_check_required]

    def process(self, opt, value, values, parser):
        py_optparse.Option.process(self, opt, value, values, parser)
        parser.option_seen[self] = 1
