#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2015-11-24 11:49:57
# @Updated Date: 2016-09-01 17:41:56

import logging
from tornado import gen, web, escape

from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Result import Result
from Runtime.Constants import (
    SEVERITY_LEVEL)
from Forge.DutyScheduler.DutySchedulerGlobal import DutySchedulerGlobal


class SchedulerHandler(APIHandler):
    """
        endpoint /scheduler/
    """

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def get(self):
        """
            uri:
                /scheduler/?jobId=<job_id>
            Get all scheduler jobs.
        """

        job_ids = self.get_arguments('jobId')

        logging.info("{0}, begin action".format('|'.join(job_ids)))

        jobs = []
        for job_id in job_ids:
            job = DutySchedulerGlobal.scheduler.get_job(job_id)
            if not job:
                logging.error("Can not find the scheduler job {0}".format(job_id))
                continue

            logging.info("Get the job {0}".format(job_id))

            jobs.append(job)

        logging.info("{0}, action succes".format('|'.join(job_ids)))
        raise gen.Return({'data': jobs})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "job_id": {"type": "string"},
            },
            "required": ["job_id"]
        },
        output_schema={
            "type": "object",
            "properties": {
                "message": {"type": "string"}
            }
        }
    )
    @coroutine
    def post(self):
        """
            Add a new scheduler job.
        """

        job_id = self.body.get('job_id')
        logging.info("{0}, begin action".format(job_id))

        the_job = yield DutySchedulerGlobal.scheduler.add_job(job_id, 0.01)
        if not the_job:
            logging.error("{0}, add new scheduler job failed".format(job_id))
            raise APIError(400, job_id, flag='DUTY.HANDLER.SCHEDULER.ADD_FAILED')

        logging.info("{0}, add new scheduler job successfully".format(job_id))
        result = Result('DUTY.HANDLER.SCHEDULER.ADD_SUCCESS', job_id)
        raise gen.Return({"message": result.result})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "job_id": {"type": "string"},
                "interval": {"type": "number"}
            },
            "required": ["job_id"]
        },
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def put(self):
        """
            Update the scheduler job.
        """

        job_id = self.body.get('job_id')
        interval = self.body.get('interval')

        logging.info("{0}, begin action".format(job_id))

        # check job_id whether in job list. If not in raise exception.
        if not DutySchedulerGlobal.scheduler.get_job(job_id):
            logging.error("{0}, can not find the scheduler job".format(job_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

        job = yield DutySchedulerGlobal.scheduler.update_job(job_id, interval)
        if not job:
            logging.error("{0}, update the scheduler job interval to {1} failed".format(job_id, interval))
            raise APIError(400, job_id, flag='DUTY.HANDLER.SCHEDULER.UPDATE_FAILED')

        logging.info("{0}, update the scheduler job interval to {1} successfully".format(job_id, interval))
        result = Result('DUTY.HANDLER.SCHEDULER.UPDATE_SUCCESS', job_id)
        raise gen.Return({"message": result.result})

    @validate(
        output_schema={
            "type": "object",
            "properties": {
                "message": {"type": "string"}
            }
        }
    )
    @coroutine
    def delete(self):
        """
            uri:
                /scheduler/?jobId=<job_id>
            Delete the scheduler job.
        """

        job_ids = self.get_arguments('jobId')

        logging.info("{0}, begin action".format('|'.join(job_ids)))

        sucess_ids = []
        fail_ids = []
        for job_id in job_ids:
            # check job_id whether in job list. If not in raise exception.
            if not DutySchedulerGlobal.scheduler.get_job(job_id):
                logging.error("can not find the scheduler job {0}".format(job_id))
                fail_ids.append(job_id)
                continue

            flag = yield DutySchedulerGlobal.scheduler.delete_job(job_id)
            if not flag:
                logging.error("Delete the scheduler job {0} failed".format(job_id))
                fail_ids.append(job_id)
                continue

            sucess_ids.append(job_id)

        logging.info("{0}, action success".format('|'.join(job_ids)))

        result = Result('DUTY.HANDLER.SCHEDULER.DELETE_SUCCESS', job_id)
        raise gen.Return({"message": result.result})
