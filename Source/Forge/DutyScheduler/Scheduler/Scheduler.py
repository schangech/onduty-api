#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2015-11-18 18:07:17
# @Updated Date: 2016-09-12 11:21:37

from __future__ import division
import logging
import time
import pytz
import calendar
from datetime import datetime, timedelta
from tornado import gen, ioloop
# from selenium import webdriver

from apscheduler.triggers.interval import IntervalTrigger
from apscheduler.triggers.cron import CronTrigger
from apscheduler.schedulers.tornado import TornadoScheduler
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor
from apscheduler.events import EVENT_JOB_ERROR, EVENT_JOB_MISSED

from Runtime.Constants import (
    SECS_PER_MINUTE,
    MINUTES_PER_HOUR,
    HOURS_PER_DAY,
    TIME_FORMAT,
    DEFAULT_SUPPORT_USER,
    DEFAULT_RESPONSE_HOURS,
    SUPPORTED_SEVERITIES,
    DEFAULT_SEVERITY,
    ALERT_INTERVAL_MIN_THRESHOLD,
    DEFAULT_SUPPORT_DEPARTMENT,
    DEFAULT_RETRY_TIMES,
    SCHEDULE_REMINDER,
    CHINA_TIME_ZONE,
    ONCALL_EMAIL,
    ONCALL_WECHAT)
from Forge.Common.State import (
    TICKET_STATE_OPENED,
    TICKET_STATE_PROCESSING,
    SCHEDULE_STATE_ACTIVE)
from Forge.Common.Ops import (
    OPS_SCHEDULER_ALERT,
    OPS_ACTIVITY_NOTIFY,
    OPS_SCHEDULE_REMINDER)
from Forge.Common.Utils import is_email, ignored
# from Forge.Common.Global import Default
from Forge.Common.Email import Email
from Forge.Common.Wechat import Wechat
from Forge.DutyScheduler.DutySchedulerGlobal import DutySchedulerGlobal
from Forge.DutyScheduler.DutySchedulerUtil import DutySchedulerUtil

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


local_tz = pytz.timezone('Asia/Shanghai')


# -----------------------------------------------------
#  Exception
# -----------------------------------------------------
class SchedulerException(Exception):
    """A simple exception class used for Scheduler exceptions"""
    pass


class Scheduler(DutySchedulerUtil):
    # "Scheduler manager"

    def __init__(self, is_restore=False):
        """
            is_restore: whether restore the original schedulers.
        """

        self.executors = {
            'default': ThreadPoolExecutor(10),
            'processpool': ProcessPoolExecutor(2)
        }
        self.job_defaults = {
            'coalesce': False,
            'max_instances': 20
        }

        self.job_number = 0
        self.timedelta = 0.1 * SECS_PER_MINUTE  # start scheduler jobs after 5 minutes

        self.sched = TornadoScheduler()
        self.sched.configure(executors=self.executors, job_defaults=self.job_defaults)

        self.sched.start()
        # add listener
        self.sched.add_listener(self.__error_listener, EVENT_JOB_ERROR | EVENT_JOB_MISSED)

        if is_restore:
            self.init_interval_job()
        self.__add_schedule_reminder()

    @gen.coroutine
    def init_interval_job(self):

        # search all openned tickets and add scheduler task
        tickets = yield DutySchedulerGlobal.db.ticket.find(
            {
                # 'id': 'case-c9e9bfbb028b',
                'status': TICKET_STATE_OPENED,
                'type': 'bug'
            },
            {
                '_id': False
            }
        ).to_list(None)
        for ticket in tickets:
            logging.info("begin to add scheduler task {0}".format(ticket['id']))

            time_delta = self.__timedelta()

            # compute time interval
            duty_interval = abs(ticket['created_time'] - datetime.utcnow())
            # 获取到的是小时数，转换成timedelta需要转换成天数
            time_interval = yield self.__get_ticket_interval(ticket['id'])
            response_time_interval = timedelta(time_interval / HOURS_PER_DAY)
            if duty_interval < response_time_interval:
                duty_interval_hours = int(
                    (response_time_interval - duty_interval).total_seconds() / (SECS_PER_MINUTE * MINUTES_PER_HOUR))
            else:
                duty_interval_hours = yield self.__get_ticket_interval(ticket['id'])

            job = yield self.__add_job(ticket['id'], duty_interval_hours, time_delta)
            if job:
                self.job_number += 1
                logging.info("add scheduler task {0} successfully".format(ticket['id']))

    @gen.coroutine
    def add_job(self, job_id, scan_interval, exec_time=None):

        time_delta = self.timedelta

        interval = yield self.__get_ticket_interval(job_id)

        yield self.delete_job_history(job_id)

        job = yield self.__add_job(job_id, interval, time_delta, True, exec_time)
        if not job:
            raise gen.Return(False)

        self.job_number += 1

        raise gen.Return(job)

    @gen.coroutine
    def update_job(self, job_id, scan_interval):
        interval = yield self.__get_ticket_interval(job_id)
        try:
            trigger = IntervalTrigger(hours=interval)
            self.sched.modify_job(job_id, None, trigger=trigger)
        except Exception as error:
            logging.error("update scheduler job {0} exception {1}".format(job_id, error))
            raise gen.Return(False)
        # clear ticket oncall times
        res = yield DutySchedulerGlobal.db.scheduler_job.update(
            {'id': job_id},
            {'$set': {'times': 0}}
        )
        logging.info("update scheduler job {0} successfully".format(job_id))

        job = self.__get_job(job_id)
        # # 更新scheduler job，同时更新redis里的数据
        # if not DutySchedulerGlobal.redis_db.hmset(job_id, job):
        #     logging.info('Scheduler job write redis error')

        raise gen.Return(job)

    @gen.coroutine
    def delete_job(self, job_id):
        try:
            self.sched.remove_job(job_id)
            # DutySchedulerGlobal.redis_db.delete(job_id)
        except Exception as error:
            logging.error("delete scheduler job {0} exception {1}".format(job_id, error))
            raise gen.Return(False)

        self.job_number -= 1

        logging.info("delete scheduler job {0} successfully".format(job_id))
        raise gen.Return(job_id)

    # def get_job(self, job_id):
    #     """从redis里获取job，接口获取job状态时直接从redis里面取"""
    #     return DutySchedulerGlobal.redis_db.hgetall(job_id)

    def get_job(self, job_id):
        """从apscheduler里获取job"""
        job = self.sched.get_job(job_id)
        return self.__transfer(job)

    @gen.coroutine
    def shutdown(self):

        self.sched.shutdown()

    @gen.coroutine
    def __add_job(self, job_id, scan_interval, time_delta=0, replace_existing=False, exec_time=None):

        try:
            # alert job
            if isinstance(scan_interval, int) or isinstance(scan_interval, float):
                trigger = IntervalTrigger(hours=scan_interval)
            else:
                logging.error("invalid job interval {0}".format(scan_interval))
                raise SchedulerException("invalid job interval of job {0}".format(job_id))

            sched_job = self.sched.add_job(
                ioloop.IOLoop.instance().add_callback, trigger=trigger, id=job_id,
                args=[self.__scheduler_job, job_id], replace_existing=replace_existing)

            job = self.__get_job(sched_job.id)
            # # 添加任务，同时也要写到redis里
            # if not DutySchedulerGlobal.redis_db.hmset(job_id, job):
            #     logging.info('Scheduler job write redis error')
        except Exception as error:
            logging.error("add scheduler job for alert {0} exception {1}".format(job_id, error))
            raise gen.Return()

        logging.info("add scheduler job {0} successfully".format(job['id']))
        raise gen.Return(job)

    @gen.coroutine
    def __add_schedule_reminder(self, job_id=SCHEDULE_REMINDER, replace_existing=False):
        try:
            # meitian 12 dian
            trigger = CronTrigger(hour=12)

            sched_job = self.sched.add_job(
                ioloop.IOLoop.instance().add_callback, trigger=trigger, id=job_id,
                args=[self.__alert_job], replace_existing=replace_existing)

            job = self.__get_job(sched_job.id)
            # 添加任务，同时也要写到redis里
            # if not DutySchedulerGlobal.redis_db.hmset(job_id, job):
            #     logging.info('Scheduler job write redis error')
        except Exception as error:
            logging.error("add scheduler job for alert {0} exception {1}".format(job_id, error))
            raise gen.Return()

        logging.info("add scheduler job {0} successfully".format(job['id']))
        raise gen.Return(job)

    def __error_listener(self, event):
        if event.exception:  # error event
            logging.error("error job %s, scheduled job run time %s, \nexception %s, \ntraceback %s" % \
                (event.job.id, event.scheduled_run_time, event.exception, event.traceback))
        else:   # missend event
            logging.error("missed job {0}, scheduler job run time {1}".format(event.job_id, event.scheduled_run_time))

    def __transfer(self, job):
        if not job:
            return

        ret = {
            'name': job.name,
            'id': job.id,
            'interval': job.trigger.interval.seconds,
            'next_run_time_utc': calendar.timegm(job.next_run_time.utctimetuple()),
            'next_run_time_str': job.next_run_time.strftime(TIME_FORMAT)
        }
        return ret

    def filter_users(self, user_id_list):
        """过滤掉不是enabled状态的user"""
        users = self.get_user_info(user_id=user_id_list)
        if isinstance(users, list):
            for user in users:
                if user.get('status') != 'enabled':
                    user_id_list.remove(user.get('id'))

    def get_user_info(self, email=None, user_id=None):
        """获取用户信息"""
        if email is not None:
            filters = "?email={0}".format(email)
        else:
            _tmp = ''
            if isinstance(user_id, basestring):
                _tmp = "?id={0}".format(user_id)
            elif isinstance(user_id, list):
                for one_id in user_id:
                    _tmp = '{param}{key}={value}&'.format(param=_tmp or '?', key='id', value=one_id)
            filters = _tmp
        try:
            resp = self._request_metadata_api(method='GET', key='/user', filters=filters)
            users = eval(resp.json().get('data'))
            if isinstance(users, list) and (email is not None or isinstance(user_id, basestring)):
                user = users[0]
            else:
                user = users
        except Exception as e:
            user = None
            logging.warning('get user %s failed' % email if email else user_id)
        return user

    @gen.coroutine
    def __scheduler_job(self, job_id):
        """
        根据ticket找对应的值班人
        1.自动创建的ticket，可以根据ctid，找对应的department，然后找department对应的排班
        2.手工创建的ticket，根据cti找对应resolvegroup，然后找resolvegroup对应的排班
        """
        now = int(calendar.timegm(time.gmtime()))
        timestamp = datetime.utcfromtimestamp(now)
        logging.debug("job id {0}, timestamp {1}".format(job_id, timestamp))

        ticket = yield DutySchedulerGlobal.db.ticket.find_one(
            {'id': job_id},
            {'_id': False}
        )
        # can't find the ticket
        if not ticket:
            logging.error("ticket {0} is missing".format(job_id))
            self.delete_job(job_id)
            raise gen.Return(False)
        # ticket has been acknowledged

        if ticket['status'] not in [TICKET_STATE_OPENED, TICKET_STATE_PROCESSING]:
            logging.info("ticket {0} has been processed".format(job_id))
            self.delete_job(job_id)
            raise gen.Return(True)

        user_to = []
        cc_to = []

        rg_id = ticket['resolvegroupId']
        rg = yield DutySchedulerGlobal.db.resolvegroup.find_one({'id': rg_id})
        department = rg.get('name') if rg else None

        logging.debug('Ticket resolvegroup: %s' % department)

        # 获取下一个报警人员，这里的caller可能是leadergroup的成员
        next_caller_id_list, cc_id_list = yield self.get_next_caller(ticket)
        for user_id in next_caller_id_list:
            user = self.get_user_info(user_id=user_id)
            if not user:
                continue
            # 发送微信时是按照微信企业号里面的用户名，统一都是username，不是个人的微信号
            user_info = {
                'email': user['email'],
                'username': user['username'],
                'wechat': user['username']
            }
            user_to.append(user_info)

        for user_id in cc_id_list:
            user = self.get_user_info(user_id=user_id)
            if not user:
                continue
            user_info = {
                'email': user['email'],
                'username': user['username'],
                'wechat': user['username']
            }
            cc_to.append(user_info)

        # try to call on duty users
        flag = yield self.__on_call(ticket, user_to, cc_to, department=department)
        if not flag:
            logging.error("{0}, try to call user {1} failed".format(job_id, user_to))
            raise gen.Return(False)

        # update the duty scheduler
        try:
            time_interval = yield self.__get_ticket_interval(ticket['id'])
            # replace the original scheduler
            self.__add_job(job_id, time_interval, 0, True)
        except Exception as error:
            logging.error("{0}, update scheduler job failed, \n{1}".format(job_id, str(error)))
            raise gen.Return(False)

        job = self.__get_job(job_id)

        # 更新scheduler_job表，报警次数+1，更新已报警过的user
        res = yield DutySchedulerGlobal.db.scheduler_job.update(
            {'id': ticket['id']},
            {
                '$inc': {'times': 1},
                '$addToSet': {'caller': {'$each': next_caller_id_list}}
            },
            upsert=True
        )

        # if not DutySchedulerGlobal.redis_db.hmset(job_id, job):
        #     logging.info('Scheduler job write redis error')

        raise gen.Return(True)

    @gen.coroutine
    def delete_job_history(self, job_id):
        """
        删除定时任务执行记录
        """
        job = yield DutySchedulerGlobal.db.scheduler_job.find_one({'id': job_id})
        if job:
            res = yield DutySchedulerGlobal.db.scheduler_job.remove({'id': job_id})
            logging.info('delete shceduler_job result: {0}'.format(res))
            raise gen.Return(True if res else False)
        raise gen.Return(True)

    @gen.coroutine
    def get_next_caller(self, ticket):
        """
        获取下一个报警的人
        如果ticket状态是open状态：
            1. responser
            2. oncaller
            3. resolvegroup里的其他人
            ...
            如果都报警过了，向resolvegroup leader的leader报警

        如果是processing状态:
            1. responser & resolvegroup leader
            2. responser & resolvegroup leader & 上报列表的第一列
            3. responser & resolvegroup leader & 上报列表的第二列，抄送第一列
            ...
            如果都报警过了，向最后一列的第一个人的leader报警
        """
        logging.info('begin action')

        # 下一轮报警用户id_list
        next_caller_id_list = []
        # 下一轮报警抄送用户id_list
        cc_id_list = []

        responser = self.get_user_info(email=ticket['responser'])

        job = yield DutySchedulerGlobal.db.scheduler_job.find_one({'id': ticket['id']})
        if job:
            times = job.get('times', 0)  # 这里的次数是已经报警的次数
        else:
            times = 0
        logging.info('Ticket alert times: %s' % times)

        if ticket['status'] == TICKET_STATE_OPENED:
            # 状态为open
            logging.debug('ticket status: %s' % TICKET_STATE_OPENED)
            # responser --> oncaller --> rg中流转 --> rg_leader的leader

            # 抄送给当天值班人员，感觉这样比较合理
            oncaller_id = None
            oncaller_email = yield self._get_the_schedule(datetime.utcnow(), ticket['resolvegroupId'])
            if oncaller_email:
                oncaller = self.get_user_info(email=oncaller_email)
                oncaller_id = oncaller['id']
                cc_id_list.append(oncaller_id)

            # 如果不是第一次报警，则查找当天值班人员，或者rg中的下一个报警人
            if times == 0:
                if responser:
                    next_caller_id_list.append(responser['id'])
            else:
                # 不是第一次报警，发送给resolvegroup中的其他人
                # 向RG中的其他人报警
                rg = yield DutySchedulerGlobal.db.resolvegroup.find_one({'id': ticket['resolvegroupId']})
                rg_users = rg['users']
                rg_users_id = [rg_user['uid'] for rg_user in rg_users]
                self.filter_users(rg_users_id)
                rg_users = [rg_user for rg_user in rg_users if rg_user['uid'] in rg_users_id]

                # 取出RG中还没有报警过的user，并且按权重排序
                not_called_list = [user['uid'] for user in sorted(rg_users, key=lambda x: x.get('order')) if
                                   user['uid'] not in job.get('caller')]

                if not_called_list:
                    next_caller_id_list.append(not_called_list[0])
                else:
                    # 全部都已经报警过了
                    logging.info('all resolvegroup users has been alerted.')
                    next_caller_id_list.append(job.get('caller')[times % len(job.get('caller'))])

        elif ticket['status'] == TICKET_STATE_PROCESSING:
            logging.debug('ticket status: %s' % TICKET_STATE_PROCESSING)
            # 第一次：向responser + rg_leader报警
            # 第二次：向responser + rg_leader + 上报列表第一列报警
            # 第三次：向responser + rg_leader + 上报列表第二列报警，抄送第一列
            # ...

            # resolvegroup leader
            rg = yield DutySchedulerGlobal.db.resolvegroup.find_one({'id': ticket['resolvegroupId']})
            rg_master_list = rg.get('masters')
            logging.debug('rg_masters: %s' % rg_master_list)
            rg_master_id = rg_master_list[0]['uid']

            if responser:
                next_caller_id_list.append(responser['id'])
            next_caller_id_list.append(rg_master_id)

            # sla
            sla = yield DutySchedulerGlobal.db.sla.find_one({'id': ticket['resolvegroupId']})
            customize = sla.get('customize', False)
            escalation = sla.get('escalation', [])

            # 自定义报警
            if customize and escalation:
                if 1 <= times <= len(escalation):    # 最后一次，len()+1次
                    next_caller_id_list.extend(escalation[times-1])
                    if times >= 2:   # 第三次报警开始有抄送, 第三次抄送index为1的
                        for l in escalation[:times-1]:
                            cc_id_list.extend(l)
                elif times >= len(escalation)+1:
                    # 所有人员都报警过了
                    last_caller_id = escalation[-1][0]
                    last_caller = self.get_user_info(user_id=last_caller_id)
                    remain_deepth = times - len(escalation)
                    next_caller_email = yield self.__superior(last_caller['email'], remain_deepth)
                    user = self.get_user_info(email=next_caller_email)
                    if user:
                        next_caller_id_list.append(user['id'])
                    # 需要抄送所有escalation
                    for l in escalation:
                        cc_id_list.extend(l)
            else:
                rg_master = self.get_user_info(user_id=rg_master_id)
                if rg_master:
                    next_caller_email = yield self.__superior(rg_master['email'], times)
                    user = self.get_user_info(email=next_caller_email)
                    if user:
                        next_caller_id_list.append(user['id'])

        next_caller_id_list = list(set(next_caller_id_list))
        cc_id_list = list(set(cc_id_list))

        raise gen.Return((next_caller_id_list, cc_id_list))

    @gen.coroutine
    def _get_the_schedule(self, timepoint, resolveGroupId):
        """获取resolvegroup里当天值班的人员"""
        user = None
        schedule = yield DutySchedulerGlobal.db.schedule.find_one(
            {
                'resolvegroup': resolveGroupId,
                'status': SCHEDULE_STATE_ACTIVE
            }
        )
        # first get the special department schedule
        if schedule:
            user = [entry['user'] for entry in schedule['entries'] if entry['start'] <= timepoint <= entry['end']]
            if user:
                user = user[0]
                if not is_email(user):
                    user = '{0}@rongcapital.cn'.format(user)

        raise gen.Return(user)

    @gen.coroutine
    def __get_ticket_interval(self, job_id):
        """
        获取ticket的响应时间和处理时间
        """
        ticket = yield DutySchedulerGlobal.db.ticket.find_one({'id': job_id})
        rg_id = ticket['resolvegroupId']
        status = ticket['status']
        severity = ticket['severity']
        interval = yield self.__get_time_interval(rg_id, severity, status)
        raise gen.Return(interval)

    @staticmethod
    @gen.coroutine
    def __get_time_interval(rg_id, severity, status):
        """
        从sla里获取resolvegroup的interval
        """
        sla = yield DutySchedulerGlobal.db.sla.find_one({'id': rg_id})
        if not sla:
            logging.error('Resolvegroup {} not found sla'.format(rg_id))
            raise gen.Return(DEFAULT_RESPONSE_HOURS)
        sla = [s for s in sla['sla'] if s['severity'] == severity]

        response_minutes = sla[0]['response_time']
        processing_minutes = sla[0]['processing_time']
        response_hours = response_minutes / MINUTES_PER_HOUR
        processing_hours = processing_minutes / MINUTES_PER_HOUR

        logging.debug('SLA response time: %s minutes, processing time: %s minutes' % (response_minutes, processing_minutes))

        if status == TICKET_STATE_OPENED:
            result = response_hours
        else:
            result = processing_hours

        raise gen.Return(result)

    def __timedelta(self):

        time_delta = (self.job_number + self.timedelta) % ALERT_INTERVAL_MIN_THRESHOLD

        if time_delta < self.timedelta:
            time_delta = self.timedelta

        return time_delta

    @gen.coroutine
    def __superior(self, user_email, level=None):
        """
            Get the up `level` superior of `user_email` from database;
        """

        level = level or 1      # default the direct superior

        logging.debug("to find superior of user {0}/{1}".format(user_email, level))

        superior_email = user_email
        while level > 0:
            the_worker = self.get_user_info(email=str(user_email))
            if the_worker:
                the_superior_email = the_worker.get('superior')
                if the_superior_email and isinstance(the_superior_email, list):
                    the_superior_email = the_superior_email[0]

                if the_superior_email and not is_email(the_superior_email):
                    the_superior_email = '{0}@rongcapital.cn'.format(the_superior_email)
                    logging.debug("found user {0} superior {1}".format(superior_email, the_superior_email))

                if not the_superior_email or superior_email == the_superior_email:
                    logging.warning("can't find user {0} superior".format(superior_email))
                    break

                superior_email = the_superior_email

            level -= 1

        raise gen.Return(superior_email)

    @gen.coroutine
    def __backups(self, cti_id):
        """
            Get `department` on duty backups.
        """
        cti = yield DutySchedulerGlobal.db.cti.find_one({
            'id': cti_id
        })

        # 根据cti里的alias(部门名称)去找对应的排班
        schedule = yield DutySchedulerGlobal.db.schedule.find_one({
            'tag': cti.get('alias'),
            'status': SCHEDULE_STATE_ACTIVE
        })
        logging.debug('find schedule of cti: %s result: %s' % (cti_id, bool(schedule)))
        if not schedule:
            rg = yield DutySchedulerGlobal.db.resolvegroup.find_one({
                'ctis': {'$in': [cti_id]}
            })
            logging.debug('find resolvegroup of cti: %s result: %s' % (cti_id, rg))
            schedule = yield DutySchedulerGlobal.db.schedule.find_one({
                'resolvegroup': rg['id']
            })
            logging.debug('find schedule of resolvegroup: %s result: %s' % (rg['id'], bool(schedule)))

        if not schedule:
            logging.error("there is no schedule of cti {0}".format(cti.get('alias')))
            backups = None
        else:
            backups = schedule.get('backups', None)

        # if backups:
        #     backups = yield DutySchedulerGlobal.db.rc.aggregate(
        #         [
        #             {'$unwind': '$staffs'},
        #             {'$match': {'staffs.username': backups}}
        #         ]
        #     ).to_list(None)
        #     backups = []

        raise gen.Return(backups)

    @gen.coroutine
    def __on_duty(self, timepoint, cti_id):
        """
            Get the on duty user currently.
        """
        r_group = DutySchedulerGlobal.db.resolvegroup.find_one({
            'ctis': {'$in': list(cti_id)}
        })
        if not r_group:
            raise gen.Return(None)
        r_group_id = r_group.get('id')
        r_group_id_list = list(r_group_id)
        schedule = DutySchedulerGlobal.db.schedule.find_one(
            {
                'start': {
                    '$lte': timepoint
                },
                'end': {
                    '$gte': timepoint
                },
                'status': 'active',
                'resolvegroup': {'$in': r_group_id_list}
            }
        )

        entries = schedule.get('entries')
        users = [entry.get('user') for entry in entries if entry['start'] <= timepoint <= entry['end']]
        user_email = users[0] if users and len(users) > 0 else DEFAULT_SUPPORT_USER
        logging.debug("found on duty user {0}".format(user_email))
        raise gen.Return(user_email)

    @gen.coroutine
    def __on_call(self, ticket, user_to, cc_to=None, **kwargs):
        """
            Call on duty user with supported ways.
        """

        email_flag = False
        wechat_flag = False

        email_to_list = [user['email'] for user in user_to]
        cc_to_list = [user['email'] for user in cc_to]
        wechat_to_list = [user['wechat'] for user in user_to]

        ticket_info = {
            'ticket_id': ticket.get('id'),
            'ticket_severity': ticket.get('severity'),
            'ticket_user': ticket.get('responser'),
            'ticket_title': ticket.get('description'),
            'ticket_status': ticket.get('status'),
            'ticket_creater': ticket.get('requester'),
            'create_time': (ticket['created_time'] + timedelta(hours=CHINA_TIME_ZONE)).strftime(TIME_FORMAT),
            'ticket_url': 'http://{0}/tickets/{1}'.format(DutySchedulerGlobal.config.domain, ticket.get('id')),
            'ticket_details': ticket['details'].get('content')
        }

        # try to send emails
        email_format = Email.format(OPS_SCHEDULER_ALERT, **ticket_info)
        # 调用发送email的接口，发送邮件
        self._send_email(receiver=email_to_list, subject=email_format['Subject'],
                         content=email_format['Body'], cc=cc_to_list, format='html')

        email_flag = True
        job_id = ticket_info['ticket_id']
        ticket_url = ticket_info['ticket_url']
        logging.debug("Send email to {}".format(email_to_list))

        details = ticket['details']

        if ticket['type'] == 'account':
            # 账号申请
            mail_pre = details.get('mail').split('@')[0]                            # 邮箱名
            cn = details.get('cn')                                                  # 用户名
            telephonenumber = details.get('telephonenumber')                        # 电话
            mail = details.get('mail')                                              # 邮箱
            cc_list = ticket.get('cc_list')[0] if ticket.get('cc_list') else ''     # 全名
            loginshell = details.get('loginshell')                                  # 登录指令
            fullname = details.get('givenname') + details.get('sn')                 # 全名
            url = ticket_info['ticket_url']                                         # 链接

            wechat_format = Wechat.format('account', mail_pre=mail_pre, cn=cn,
                                          telephonenumber=telephonenumber, mail=mail, cc_list=cc_list,
                                          loginshell=loginshell, fullname=fullname, url=url)
        else:
            # 报障
            id = ticket.get('id')                                               # 报障id
            description = ticket.get('description')                             # 报障内容
            created_time = ticket.get('created_time') + timedelta(hours=CHINA_TIME_ZONE)
            created_time = created_time.strftime(TIME_FORMAT)                   # 创建时间
            status = ticket.get('status')                                       # 报障状态
            severity = ticket.get('severity')                                   # 严重等级
            hostname = details.get('hostname')                                  # 主机
            ip = details.get('ip')                                              # ip
            url = ticket_info.get('ticket_url')                                 # 链接

            wechat_format = Wechat.format('bug', id=id, description=description,
                                          created_time=created_time, status=status,
                                          severity=severity, hostname=hostname, ip=ip, url=url)

        # send to the wechat user
        self._send_wechat(receiver=wechat_to_list, type="single", content=wechat_format)
        logging.info("Send_wechat to {0}".format(wechat_to_list))
        # # send to wechat group
        # self._send_wechat(receiver=kwargs.get('department'), type="group", content=wechat_format)
        # logging.info("send_wechat {0} {1} {2}".format(kwargs.get('department', None), ticket_url, "group"))

        wechat_flag = True

        # Send message to slack
        self._send_slack(text='{}\n'.format(wechat_format))
        logging.info('Send slack to devops')

        # 发送成功，记录activity
        activity = {
            'ticket_id': job_id,
            'details': [{
                'action': OPS_ACTIVITY_NOTIFY,
                'users': user_to
            }],
            'created_time': datetime.utcnow(),
        }
        res = yield DutySchedulerGlobal.db.activity.insert(activity)
        if not res:
            logging.error("{0}, failed to create {1} "
                          "activity for ticket {2}".format('duty-scheduler', OPS_ACTIVITY_NOTIFY, job_id))

        raise gen.Return(email_flag)

    @gen.coroutine
    def __alert_job(self):
        """
        提醒排班中明天值班的人员
        :return:
        """
        schedules = yield DutySchedulerGlobal.db.schedule.find(
            {'status': SCHEDULE_STATE_ACTIVE},
            {'_id': False}
        ).to_list(None)
        logging.debug('find %s active schedules.' % len(schedules))

        for schedule in schedules:
            rg_id = schedule.get('resolvegroup')
            rg_name = rg_id
            rg = yield DutySchedulerGlobal.db.resolvegroup.find_one({'id': rg_id})
            if rg:
                rg_name = rg.get('name')

            current_time = datetime.utcnow()  # current time
            tomorrow_time = current_time + timedelta(days=1)  # tomorrow

            today_entry = filter(lambda e: e['start'] <= current_time <= e['end'], schedule['entries'])
            tomo_entry = filter(lambda e: e['start'] <= tomorrow_time <= e['end'], schedule['entries'])

            if not today_entry or not tomo_entry:
                logging.error('Resolvegroup {0} could not found entry!'.format(rg_name))
                raise gen.Return(False)

            today_entry = today_entry[0]
            tomo_entry = tomo_entry[0]
            curr_user = today_entry['user']
            tomo_user = tomo_entry['user']

            if curr_user != tomo_user:  # tomorrow is another user
                if not is_email(tomo_user):
                    tomo_user = '{0}@rongcapital.cn'.format(tomo_user)

                logging.debug('RG: %s Today: %s, Tomorrow: %s' % (rg_name, curr_user, tomo_user))

                remind_info = {
                    'user_name': tomo_user.split('@')[0],
                    'schedule_id': schedule['id'],
                    'rg_name': rg_name,
                    'start_time': tomo_entry['start'] + timedelta(hours=CHINA_TIME_ZONE),  # 计算时区
                    'end_time': tomo_entry['end'] + timedelta(hours=CHINA_TIME_ZONE),
                    'domain': DutySchedulerGlobal.config.domain,
                    'schedule_url': 'http://{0}/schedulers/{1}'.format(DutySchedulerGlobal.config.domain,
                                                                           schedule['id'])
                }
                # 给下一个值班的人发邮件&发微信
                email_format = Email.format(OPS_SCHEDULE_REMINDER, **remind_info)
                self._send_email(receiver=[tomo_user], subject=email_format['Subject'],
                                 content=email_format['Body'], format='html')
                wechat_content = "您所在的支持组 ({rg_name}) 在明天 ({start_time}) 将轮到您值班，请做好交接准备。"
                self._send_wechat(receiver=tomo_user.split('@')[0], type='single',
                                  content=wechat_content.format(**remind_info))


if __name__ == '__main__':

    sched = Scheduler()

