#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2015-11-18 18:07:17
# @Updated Date: 2016-06-24 11:23:44

import os
from Runtime.Constants import TYPE_STRING, TYPE_INT, TYPE_FLOAT, TYPE_BOOLEAN
from Runtime.Constants import LOG_DEBUG, LOG_INFO, LOG_WARNING, LOG_ERROR, LOG_CRITICAL
from Runtime.Config import Config, ConfigSection, ConfigOption, ConfigException
from Forge.Common.Global import Default


class DutySchedulerConfig(Config):
    """ Scheduler config class
    """

    #Scheduler Options#
    sched = ConfigSection(
        "DutyScheduler",
        required=True,
        doc="This section is used for general options affecting Scheduler as a whole."
    )
    sched.options = [
        ConfigOption(
            name="log_dir",
            getter="log_dir",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.Scheduler.log_dir
        ),
        ConfigOption(
            name="log_level",
            getter="log_level",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.Scheduler.log_level,
            valid=[LOG_DEBUG, LOG_INFO, LOG_WARNING, LOG_ERROR, LOG_CRITICAL]
        ),
        ConfigOption(
            name="log_rotate",
            getter="log_rotate",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.Scheduler.log_rotate
        ),
        ConfigOption(
            name="log_size",
            getter="log_size",
            type=TYPE_FLOAT,
            required=False,
            default=Default.Forge.Scheduler.log_size
        ),
        ConfigOption(
            name="port",
            getter="port",
            type=TYPE_INT,
            required=True,
            default=Default.Forge.Scheduler.port
        ),
        ConfigOption(
            name="remove_outdate_data",
            getter="remove_outdate_data",
            type=TYPE_BOOLEAN,
            required=False,
            default=Default.Forge.Scheduler.remove_outdate_data
        ),
        ConfigOption(
            name="alert_outdate_day_shreshold",
            getter="alert_outdate_day_shreshold",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.Scheduler.alert_outdate_day_shreshold
        ),
        ConfigOption(
            name="report_path",
            getter="report_path",
            type=TYPE_STRING,
            required=True,
        )
    ]

    # ------------------------------ General -----------------------------------
    general = ConfigSection(
        "General",
        required=True,
        doc=""
    )
    general.options = [
        ConfigOption(
            name="scribe_host",
            getter="scribe_host",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="scribe_port",
            getter="scribe_port",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.General.scribe_port
        ),
        ConfigOption(
            name="scribe_enable",
            getter="scribe_enable",
            type=TYPE_BOOLEAN,
            required=False,
            default=Default.Forge.General.scribe_enable
        ),
        ConfigOption(
            name="redis_host",
            getter="redis_host",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="redis_port",
            getter="redis_port",
            type=TYPE_INT,
            required=True,
            default=Default.Forge.General.redis_port
        ),
        ConfigOption(
            name="redis_db",
            getter="redis_db",
            type=TYPE_INT,
            required=True
        ),
        ConfigOption(
            name="redis_password",
            getter="redis_password",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.General.redis_password
        ),
        ConfigOption(
            name="mongodb_url",
            getter="mongodb_url",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="mongodb_timeout",
            getter="mongodb_timeout",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.General.mongodb_timeout
        ),
        ConfigOption(
            name="mongodb_db_copy",
            getter="mongodb_db_copy",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.General.mongodb_db_copy
        ),
        ConfigOption(
            name="mongodb_db",
            getter="mongodb_db",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.General.mongodb_db
        ),
        ConfigOption(
            name="domain",
            getter="domain",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.General.domain
        ),
        ConfigOption(
            name="oncall_url",
            getter="oncall_url",
            type=TYPE_STRING,
            required=False
        ),
        ConfigOption(
            name="metadata_url",
            getter="metadata_url",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="metadata_token",
            getter="metadata_token",
            type=TYPE_STRING,
            required=True
        )
    ]

    sections = [sched, general]

    def __init__(self, cfgfile):
        try:
            Config.__init__(self, cfgfile, self.sections)
            # log
            self.log_level = self.get("log_level")
            self.log_dir = self.get("log_dir")
            self.log_rotate = self.get("log_rotate")
            self.log_file = self.log_dir + '/scheduler.log'
            self.log_size = self.get("log_size")
            self.stdin = Default.Forge.Scheduler.stdin
            self.stdout = Default.Forge.Scheduler.stdout
            self.stderr = Default.Forge.Scheduler.stderr
            self.domain = self.get("domain")
            self.report_path = self.get("report_path")
            # service port
            self.service_port = self.get("port")
            # redis
            self.redis_host = self.get("redis_host")
            self.redis_port = self.get("redis_port")
            self.redis_db = self.get("redis_db")
            # remove outdate data flag
            self.remove_outdate_data = self.get("remove_outdate_data")
            self.alert_outdate_day_shreshold = self.get("alert_outdate_day_shreshold")
            # mongo
            self.mongo_url = self.get("mongodb_url")
            self.mongo_timeout = self.get("mongodb_timeout")
            self.mongo_db_copy = self.get("mongodb_db_copy")
            self.mongo_db = self.get("mongodb_db")
            # scribed
            self.scribe_port = self.get("scribe_port")
            self.scribe_host = self.get("scribe_host")
            self.scribe_enable = self.get("scribe_enable")
            # oncall
            self.oncall_url = self.get('oncall_url') or 'oncall.{0}'.format(self.domain)
            # metadata
            self.metadata_url = self.get('metadata_url')
            self.metadata_token = self.get('metadata_token')

            self.__check()
        except KeyError as msg:
            raise ConfigException("missing configuration parameter {0}".format(msg))
        except Exception as msg:
            raise ConfigException(msg)

    def __check(self):
        if not os.path.exists(self.log_dir):
            try:
                os.mkdirs(self.log_dir, 0755)
            except:
                raise ConfigException("the log directory {0} does not exist".format(self.log_dir))

    def get_attr(self, attr):
        return self.attrs[attr]

    def get_attrs(self):
        return self.attrs.keys()


class DutySchedulerGlobal(object):
    debug = False
    config = None
    db = None
    redis_db = None
    scheduler = None
    http_client = None
