#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2016-06-24 09:47:43
# @Updated Date: 2016-09-06 16:28:13


"""
DutyScheduler internal utils.
"""

import logging
import requests

from Runtime.Debug import Debug
from Runtime.Constants import ONCALL_EMAIL, ONCALL_WECHAT, ONCALL_SLACK
from Forge.DutyScheduler.DutySchedulerGlobal import DutySchedulerGlobal
from Forge.Common.RequestHandlers import APIError


class DutySchedulerException(Exception):
    """A simple exception class used for DutySchedulerUtil exceptions"""
    pass


class DutySchedulerUtil(Debug):

    USER_AGENT = 'OnDuty-Scheduler-client'

    def _debug(self):
        self._set(DutySchedulerGlobal.debug)

    # ----------------------------------------------------------------------------------------------
    # Email
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def _send_email(receiver, subject, content, sender=None, cc=None, format='text'):
        data = {
            'receiver': receiver,
            'subject': subject,
            'content': content,
            'format': format
        }
        if sender is not None:
            data.update({'sender': sender})
        if cc is not None:
            data.update({'cc': cc})

        DutySchedulerGlobal.redis_db.lpush(ONCALL_EMAIL, str(data))

    # ----------------------------------------------------------------------------------------------
    # Wechat
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def _send_wechat(receiver, type, content, sender=None):
        """
        type为single表示单发，group为群发
        群发时receiver为群聊的id，群聊id与resolvegroupname对应
        """
        data = {
            'receiver': receiver,
            'type': type,
            'content': content
        }
        if sender is not None:
            data.update({'sender': sender})

        DutySchedulerGlobal.redis_db.lpush(ONCALL_WECHAT, str(data))

    @staticmethod
    def _send_slack(text, at_list=None):
        """
        send message to slack
        """
        data = {
            'text': text
        }
        if at_list is not None:
            data['at'] = at_list

        DutySchedulerGlobal.redis_db.lpush(ONCALL_SLACK, str(data))

    def _request_metadata_api(self, key=None, method='GET', filters=None, data=None):
        '''
            @key: /url
            @method: GET/DELETE/POST/PUT
            @filters: parameters
            @data: body
        '''
        try:
            if method and (method.lower() in ['get', 'delete']):
                url = '{0}{1}{2}'.format(DutySchedulerGlobal.config.metadata_url, key, filters)
                resp = requests.request(method, url, headers=eval(DutySchedulerGlobal.config.metadata_token),
                                        timeout=30)
            elif method and (method.lower() in ['post', 'put', 'patch']):
                url = '{0}{1}'.format(DutySchedulerGlobal.config.metadata_url, key)
                resp = requests.request(method, url, headers=eval(DutySchedulerGlobal.config.metadata_token),
                                        data=data, timeout=30)
            else:
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        except Exception as err:
            logging.info('DutySchedulerGlobal._request_metadata_api() result {0}'.format(err))
            raise APIError(400, message="Metadata exception")
        return resp

