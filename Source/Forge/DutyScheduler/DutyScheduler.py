#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2015-11-18 18:07:17
# @Updated Date: 2016-06-22 14:20:17

import os
import sys
import signal
import errno
import redis
import logging
import logging.handlers
from optparse import Option
from motor import MotorClient
from tornado import web, ioloop, autoreload
from tornado.httpclient import AsyncHTTPClient

from Forge.Common.Routes import get_routes
from Forge.Common.Global import Default
from Forge.Common.Utils import ignored
from Runtime.Command import LocalCommand
from Runtime.Config import ConfigException
from Runtime.Log import LogFormatter
from Runtime.ScribeHandler import ScribeHandler
from Forge.Common.HandlerUtil import NotFoundHandler
from Forge.DutyScheduler.Scheduler.Scheduler import Scheduler
from Forge.DutyScheduler.DutySchedulerGlobal import DutySchedulerGlobal, DutySchedulerConfig


# -----------------------------------------------------
#  Exception
# -----------------------------------------------------
class DutySchedulerException(Exception):
    """A simple exception class used for DutyScheduler exceptions"""
    pass


class DutyScheduler():

    def __init__(self, foreground=True):

        self.__foreground = foreground

        # init logger
        try:
            level = logging.getLevelName(DutySchedulerGlobal.config.log_level)
            logging.getLogger().setLevel(level)
            logger = logging.getLogger()
            if not self.__foreground:
                pass
            else:
                # Set up color if we are in a tty and curses is installed
                fh = logging.StreamHandler()
                formatter = LogFormatter(console=True)
                fh.setFormatter(formatter)
                logger.addHandler(fh)
        except Exception as error:
            raise DutySchedulerException(error)

    def start(self):

        try:
            import Handlers
            handlers = get_routes(Handlers)
            handlers.append(('.*', NotFoundHandler))

            settings = {
                'debug': True,
                # 'xsrf_cookies': True,
                'gzip': True
            }
            self.application = web.Application(handlers, **settings)
            self.application.listen(int(DutySchedulerGlobal.config.service_port))

            logging.info("Scheduler is starting.")
            ioloop.IOLoop.instance().start()
        except Exception as error:
            raise DutySchedulerException(error)


if __name__ == '__main__':

    usage = "[-c] [-f] [-d] [-p]"
    version = "0.1"
    cmd = LocalCommand(usage, version, sys.argv)
    cmd.add_option(Option(
        "-c", "--conf", action="store", type="string", dest="conf",
        help="""
            The location of the Scheduler configuration file. If not
            specified, it will try to search under $INSTANT_HOME/conf/instant.cfg
        """))
    cmd.add_option(Option(
        "-f", "--fg", action="store_true",  dest="foreground",
        help="""
            Scheduler logs to foreground. Default is background
        """))
    cmd.add_option(Option(
        "-d", "--debug", action="store_true",  dest="debug",
        help="""
            Run Scheduler in debug mode. Default is Not
        """))
    cmd.add_option(Option(
        "-p", "--port", action="store",  dest="port",
        help="""
            Scheduler port
        """))
    cmd.parse_options()
    if not cmd.opt.foreground:
        cmd.opt.debug = False

    # Check if a daemon is already running
    if cmd.opt.foreground:
        pidfile = Default.Forge.Scheduler.pidfile
        if os.path.exists(pidfile):
            pf = file(pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()

            try:
                os.kill(pid, signal.SIG_DFL)
            except OSError, (err, msg):
                if err == errno.ESRCH:
                   # Pidfile is stale. Remove it.
                    os.remove(pidfile)
                else:
                    msg = "Unexpected error when checking pid file '%s'.\n%s\n" % (pidfile, msg)
                    sys.stderr.write(msg)
                    sys.exit(1)
            else:
                msg = "Scheduler is already running (pid {0})\n".format(pid)
                sys.stderr.write(msg)
                sys.exit(1)
        else:
            if not os.path.exists(os.path.dirname(Default.Forge.Scheduler.pidfile)):
                os.mkdir(os.path.dirname(Default.Forge.Scheduler.pidfile))
            file(Default.Forge.Scheduler.pidfile, 'w+').write("{0}\n".format(os.getpid()))

    # load the config and init
    try:
        if cmd.opt.conf is None:
            if "INSTANT_HOME" in os.environ:
                cmd.opt.conf = os.environ["INSTANT_HOME"] + "/conf/instant.cfg"
            else:
                cmd.opt.conf = "instant.cfg"
        if not os.path.exists(cmd.opt.conf):
            cmd.error("Missing configuration file %s" % cmd.opt.conf)

        DutySchedulerGlobal.debug = cmd.opt.debug if cmd.opt.debug is not None else False
        DutySchedulerGlobal.config = DutySchedulerConfig(cmd.opt.conf)

        DutySchedulerGlobal.db = MotorClient(DutySchedulerGlobal.config.mongo_url)[DutySchedulerGlobal.config.mongo_db]
        DutySchedulerGlobal.redis_db = redis.Redis(host=DutySchedulerGlobal.config.redis_host,
                                                   port=DutySchedulerGlobal.config.redis_port,
                                                   db=DutySchedulerGlobal.config.redis_db)

        # log
        for i in range(3):
            with ignored(Exception):
                level = logging.getLevelName(DutySchedulerGlobal.config.log_level)
                formatter = logging.Formatter(
                    "%(asctime)s-%(name)s-%(levelname)s-%(filename)s-%(funcName)s()-%(message)s")
                if DutySchedulerGlobal.config.scribe_enable:
                    handler = ScribeHandler(
                        host=DutySchedulerGlobal.config.scribe_host,
                        port=DutySchedulerGlobal.config.scribe_port,
                        category='%(hostname)s-OnDuty.Scheduler'
                    )
                else:
                    log_file = os.environ["INSTANT_HOME"] + '/log/OnDuty_scheduler.log'
                    handler = logging.handlers.RotatingFileHandler(log_file, mode='a', maxBytes=(1024*1024*10))
                handler.setFormatter(formatter)
                logger = logging.getLogger()
                logger.removeHandler(handler)
                logger.addHandler(handler)
                logger.setLevel(level)
                break
        else:
            logging.error('Scheduler: cannot connect to log service')
            sys.exit(255)

        DutySchedulerGlobal.http_client = AsyncHTTPClient(max_clients=1000)

        DutySchedulerGlobal.scheduler = Scheduler(is_restore=True)
    except KeyError as msg:
        print >> sys.stderr, "Missing environment setting:" + str(msg)
        sys.exit(1)
    except ConfigException as msg:
        print >> sys.stderr, "Cannot start Scheduler: {0}".format(msg)
        sys.exit(1)

    # start
    rs = DutyScheduler(cmd.opt.foreground)
    rs.start()
