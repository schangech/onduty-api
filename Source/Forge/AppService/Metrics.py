#!/usr/bin/env python
# -*- coding: utf-8 -*-


'''
@File: /home/sendal/workspace/bj-rc-devops-Onduty-api/Source/Forge/AppService/Handlers/Metrics.py
@Time: Jul 6, 2017
@Desc: Project Coding
@Author: sendal
@Email: <schangech@gmail.com>
@Version: 0.0.1
'''

from tornado import gen
from Forge.Common.Gen import coroutine
from Forge.Common.Schema import validate
from Forge.Common.RequestHandlers import APIHandler
# prometheus
from prometheus_client import multiprocess
from prometheus_client import generate_latest, REGISTRY, Gauge, Counter
from Forge.AppService.AppServiceGlobal import AppServiceGlobal


class MetricsHandler(APIHandler):

    def initialize(self):
        self.set_header("Content-Type", "text/plain")
        self.set_header("Access-Control-Allow-Origin", "*")

    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @validate()
    @coroutine
    def get(self):
        res = generate_latest(REGISTRY)
        raise gen.Return(res)
