#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:31:19
# @Updated Date: 2016-09-12 11:58:08

"""
AppService global variabs which from config file.
"""

import os

from Runtime.Constants import TYPE_STRING, TYPE_INT, TYPE_FLOAT, TYPE_BOOLEAN
from Runtime.Constants import LOG_DEBUG, LOG_INFO, LOG_WARNING, LOG_ERROR, LOG_CRITICAL
from Runtime.Config import Config, ConfigSection, ConfigOption, ConfigException
from Forge.Common.Global import Default


class AppServiceConfig(Config):
    """ AppService config class
    """

    # AppService Options#
    aps = ConfigSection(
        "AppService",
        required=True,
        doc="This section is used for general options affecting AppService as a whole.")
    aps.options = [
        ConfigOption(
            name="log_dir",
            getter="log_dir",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.AppService.log_dir
        ),
        ConfigOption(
            name="log_level",
            getter="log_level",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.AppService.log_level,
            valid=[LOG_DEBUG, LOG_INFO, LOG_WARNING, LOG_ERROR, LOG_CRITICAL]
        ),
        ConfigOption(
            name="log_rotate",
            getter="log_rotate",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.AppService.log_rotate
        ),
        ConfigOption(
            name="log_size",
            getter="log_size",
            type=TYPE_FLOAT,
            required=False,
            default=Default.Forge.AppService.log_size
        ),
        ConfigOption(
            name="port",
            getter="port",
            type=TYPE_INT,
            required=True
        ),
        ConfigOption(
            name="ipa_master",
            getter="ipa_master",
            type=TYPE_STRING,
            required=True,
        ),
        ConfigOption(
            name="ipa_cacert",
            getter="ipa_cacert",
            type=TYPE_STRING,
            required=True,
        ),
        ConfigOption(
            name="ipa_admin",
            getter="ipa_admin",
            type=TYPE_STRING,
            required=True,
        ),
        ConfigOption(
            name="ipa_password",
            getter="ipa_password",
            type=TYPE_STRING,
            required=True,
        )
    ]

    # ------------------------------ General -----------------------------------
    general = ConfigSection(
        "General",
        required=True,
        doc=""
    )
    general.options = [
        ConfigOption(
            name="scribe_host",
            getter="scribe_host",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="scribe_port",
            getter="scribe_port",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.General.scribe_port
        ),
        ConfigOption(
            name="scribe_enable",
            getter="scribe_enable",
            type=TYPE_BOOLEAN,
            required=False,
            default=Default.Forge.General.scribe_enable
        ),
        ConfigOption(
            name="redis_host",
            getter="redis_host",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="redis_port",
            getter="redis_port",
            type=TYPE_INT,
            required=True,
            default=Default.Forge.General.redis_port
        ),
        ConfigOption(
            name="redis_db",
            getter="redis_db",
            type=TYPE_INT,
            required=True
        ),
        ConfigOption(
            name="redis_password",
            getter="redis_password",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.General.redis_password
        ),
        ConfigOption(
            name="mongodb_url",
            getter="mongodb_url",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="mongodb_timeout",
            getter="mongodb_timeout",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.General.mongodb_timeout
        ),
        ConfigOption(
            name="mongodb_db_copy",
            getter="mongodb_db_copy",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.General.mongodb_db_copy
        ),
        ConfigOption(
            name="mongodb_db",
            getter="mongodb_db",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.General.mongodb_db
        ),
        ConfigOption(
            name="domain",
            getter="domain",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.General.domain
        ),
        ConfigOption(
            name="encrypt_key",
            getter="encrypt_key",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="encrypt_user",
            getter="encrypt_user",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="scheduler_host",
            getter="scheduler_host",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="scheduler_port",
            getter="scheduler_port",
            type=TYPE_INT,
            required=True
        ),
        ConfigOption(
            name="oncall_url",
            getter="oncall_url",
            type=TYPE_STRING,
            required=False
        ),
        ConfigOption(
            name="img_dir",
            getter="attachment_dir",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.General.attachment_dir
        ),
        ConfigOption(
            name="rcos_host",
            getter="rcos_host",
            type=TYPE_STRING,
            required=True,
            default=Default.Forge.General.rcos_host
        ),
        ConfigOption(
            name="rcos_port",
            getter="rcos_port",
            type=TYPE_INT, required=True,
            default=Default.Forge.General.rcos_port
        ),
        ConfigOption(
            name="metadata_url",
            getter="metadata_url",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="wechat_surl",
            getter="wechat_surl",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="wechat_scorpsecret",
            getter="wechat_scorpsecret",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="wechat_scorpid",
            getter="wechat_scorpid",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="wechat_qychatsecret",
            getter="wechat_qychatsecret",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="metadata_client_id",
            getter="metadata_client_id",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="metadata_secret",
            getter="metadata_secret",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="metadata_redirect_url",
            getter="metadata_redirect_url",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="metadata_oauth_auth",
            getter="metadata_oauth_auth",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="metadata_oauth_token",
            getter="metadata_oauth_token",
            type=TYPE_STRING,
            required=True
        )
    ]

    sections = [aps, general]

    def __init__(self, cfgfile):
        try:
            Config.__init__(self, cfgfile, self.sections)
            # log
            self.log_level = self.get("log_level")
            self.log_dir = self.get("log_dir")
            self.log_rotate = self.get("log_rotate")
            self.log_file = self.log_dir + '/AppService.log'
            self.log_size = self.get("log_size")
            self.stdin = Default.Forge.AppService.stdin
            self.stdout = Default.Forge.AppService.stdout
            self.stderr = Default.Forge.AppService.stderr
            self.ipa_master = self.get("ipa_master")
            self.ipa_cacert = self.get("ipa_cacert")
            self.ipa_admin = self.get("ipa_admin")
            self.ipa_password = self.get("ipa_password")
            self.domain = self.get("domain")
            self.attachment_dir = self.get("attachment_dir")
            # rcos
            self.rcos_host = self.get("rcos_host")
            self.rcos_port = self.get("rcos_port")
            # service port
            self.service_port = self.get("port")
            # scribed
            self.scribe_port = self.get("scribe_port")
            self.scribe_host = self.get("scribe_host")
            self.scribe_enable = self.get("scribe_enable")
            # redis
            self.redis_host = self.get("redis_host")
            self.redis_port = self.get("redis_port")
            self.redis_db = self.get("redis_db")
            # mongo
            self.mongo_url = self.get("mongodb_url")
            self.mongo_timeout = self.get("mongodb_timeout")
            self.mongo_db_copy = self.get("mongodb_db_copy")
            self.mongo_db = self.get("mongodb_db")
            # encrypt
            self.aes_key = self.get("encrypt_key")
            self.aes_user = self.get("encrypt_user")
            # scheduler server
            self.scheduler_host = self.get("scheduler_host")
            self.scheduler_port = self.get("scheduler_port")
            # oncall
            self.oncall_url = self.get('oncall_url') or 'oncall.{0}'.format(self.domain)
            # metadata
            self.metadata_url = self.get('metadata_url')
            self.md_client_id = self.get("metadata_client_id")
            self.md_secret = self.get("metadata_secret")
            self.md_redirect_url = self.get("metadata_redirect_url")
            self.md_oauth_auth = self.get("metadata_oauth_auth")
            self.md_oauth_token = self.get("metadata_oauth_token")
            # wechat
            self.wechat_surl = self.get('wechat_surl')
            self.wechat_scorpsecret = self.get('wechat_scorpsecret')
            self.wechat_scorpid = self.get('wechat_scorpid')
            self.wechat_qychatsecret = self.get('wechat_qychatsecret')

            self.__check()
        except KeyError as error:
            raise ConfigException("missing configuration parameter {0}".format(error))
        except Exception as error:
            raise ConfigException(error)

    def __check(self):
        if not os.path.exists(self.log_dir):
            try:
                os.mkdirs(self.log_dir, 0755)
            except Exception:
                raise ConfigException("the log directory <{0}> does not exist".format(self.log_dir))

    def get_attr(self, attr):
        return self.attrs[attr]

    def get_attrs(self):
        return self.attrs.keys()


class AppServiceGlobal(object):
    debug = False
    config = None
    db = None
    mdb = None
    redis_db = None
    mysql_db = None
    http_client = None
    es_client = None
    session_manager = None
    onduty_rcos = None
    IN_PROGRESS = None
    REQUESTS = None
    SUMMARY = None
    HISTONGRAM = None
    REQUESTSEXC = None
