#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2016-08-17 13:34:19
# @Updated Date: 2016-08-23 15:47:23

"""
OnDuty agent handler.
"""

import logging
import json
from tornado import gen
from datetime import datetime
from bson import json_util

from Forge.Common.State import (
    AGENT_STATE_ACTIVE)
from Runtime.Constants import (
    MIN_LENGTH)
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil


class AgentHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /agent/
    """

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "agent_id": {"type": "string", "minLength": MIN_LENGTH},
                "agent_version": {"type": "string", "minLength": MIN_LENGTH},
                "system_info": {
                    "type": "object"
                }
            },
            "required": ["agent_id", "agent_version"]
        },
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def post(self):
        """
            Handler agent heartbeat data.
        """

        agent = {
            'id': self.body.get('agent_id').strip().encode('utf-8'),
            'version': self.body.get('agent_version').strip().encode('utf-8'),
            'status': AGENT_STATE_ACTIVE,
            'platform': self.body.get('system_info') if 'system_info' in self.body else {}
        }

        the_agent = yield AppServiceGlobal.db.agent.find_one(
            {
                'id': agent['id']
            }
        )
        if the_agent:
            logging.info("Agent.AgentHandler.post(): handler agent heartbeat, {0} | {1} | {2}".format(agent['id'], agent['version'], agent['status']))
            agent['update_time'] = datetime.utcnow()
        else:
            logging.info("Agent.AgentHandler.post(): handler new agent heartbeat, {0} | {1} | {2}".format(agent['id'], agent['version'], agent['status']))
            agent['create_time'] = datetime.utcnow()
            agent['update_time'] = agent['create_time']

        res = yield AppServiceGlobal.db.agent.update(
            {
                'id': agent['id']
            },
            {
                '$set': agent
            },
            upsert=True,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("Agent.AgentHandler.post(): {0}, update agent status failed".format(agent['id']))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_DB")

        logging.info("Agent.AgentHandler.post(): {0}, action success".format(agent['id']))
        raise gen.Return({"message": "Handlered agent {0} heartbeat".format(agent['id'])})

    @validate(

    )
    @coroutine
    def get(self, agentId=None):
        """
            uri:
                /agent/?agentId=<agent_id>
            get special agent list;
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Agent.AgentHandler.get(): {0}, begin action".format(session.email))

        spec = {}

        agentId = self.get_argument('agentId', None)
        if agentId and isinstance(agentId, basestring):
            spec['id'] = agentId

        agents = yield AppServiceGlobal.db.agent.find(
            spec,
            {
                '_id': False
            }
        ).to_list(None)

        agents = json.loads(json_util.dumps(agents, default=json_util.default))
        logging.info("Agent.AgentHandler.get(): {0}, action success".format(session.email))
        raise gen.Return({"data": agents})
