#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
# File: Cti.py
# Time: 11:06:19 AM
# Desc:
# Author: sendal
# Email: <schangech@gmail.com>
# Version: 0.0.1
'''

import re
import uuid
import logging
import json
from tornado import gen
from Runtime.Constants import (
    MIN_LENGTH,
    LOGIN_SHELL,
    DEFAULT_LOCALE)
from Forge.Common.State import (
    TICKET_STATE_OPENED)
from Forge.Common.Result import Result
from Forge.Common.Ops import (
    OPS_ACCOUNT_RESET_PASSWORD,
    OPS_ACCOUNT_ACTIVE,
    OPS_ACCOUNT_UPDATE_PASSWORD)
from Forge.Common.Utils import is_email, validate_base64
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Email import Email
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil
from Forge.Common.Utils import ignored, parse_department, ipa_user_info_format_joint, is_phonenumbers
# prometheus
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


class CtiUtilsHandler(APIHandler, AppServiceUtil):

    @gen.coroutine
    def _process_node_tree(self, ctiIds, ctiTree=[]):
        # 将改节点下面的所有节点都取出，存放到一个列表中
        logging.debug('Cti.GroupHandler.get() _process_node_tree {0}'.format(ctiIds))
        if not ctiIds:
            res = yield self._query_cti_by_parent_node_id()
            ctiTree.append(res)
            tempIds = []
            for node in res:
                tempIds.append(node['id'])
            # logging.debug('_process_node_tree root node {0}'.format(tempIds))
            self._process_node_tree(tempIds)
        else:
            for cid in ctiIds:
                res = yield self._query_cti_by_parent_node_id(parentId=cid)
                if not res:
                    break
                ctiTree.append(res)
                # logging.debug('Cti.GroupHandler.get() find info by parent id {0}'.format(res))
                if res and isinstance(res, list):
                    tempIds = []
                    for node in res:
                        tempIds.append(node['id'])
                    self._process_node_tree(tempIds)
        raise gen.Return(False)


class CtiHandler(CtiUtilsHandler):
    #
    # endpoint: /cti/
    #
    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "name": {"type": "string", "minLength": MIN_LENGTH},
                "description": {"type": "string", "minLength": MIN_LENGTH},
                "type": {"type": "string", "minLength": MIN_LENGTH},
                "parent": {"type": "string", "minLength": MIN_LENGTH},
                "alias": {"type": "string", "minLength": MIN_LENGTH},
            },
            "required": ["name", "description"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def post(self):
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="cti").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Cti.CtiHandler.get(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        ctiName = self._ipa_input_except(body, input_str="name")
        ctiDescription = self._ipa_input_except(body, input_str="description")
        ctiParent = self._ipa_input_except(body, input_str="parent")
        ctiType = self._ipa_input_except(body, input_str="type")
        ctiAlias = self._ipa_input_except(body, input_str="alias")
        ctiId = str(uuid.uuid4()).split('-')[-1]

        # create a new node
        res = yield self._create_cti_node(ctiId=ctiId,
                                          name=ctiName,
                                          desc=ctiDescription,
                                          parent=ctiParent,
                                          ctiType=ctiType,
                                          ctiAlias=ctiAlias)
        logging.info('Cti.CtiHandler.post() create new node {0}'.format(res))

        try:
            returnRes = res if res else 'failed'
        except Exception:
            returnRes = 'failed'
        raise gen.Return({"data": returnRes})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        #
        # endpoint: /cti?id=<ctiId>&filter=true/false
        #
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="cti").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Cti.CtiHandler.get(): begin action")

        ctiIds = self.get_arguments('id')
        res = []
        if not ctiIds:
            res = yield self._query_cti_node()
        else:
            doc = {'id': {'$in': ctiIds}}
            res = yield self._query_cti_by_doc(doc)
        logging.debug('Cti.CtiHandler.get() action successfully')
        returnRes = res if res else []
        raise gen.Return({"data": returnRes})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "id": {"type": "string", "minLength": MIN_LENGTH},
                "name": {"type": "string", "minLength": MIN_LENGTH},
                "description": {"type": "string", "minLength": MIN_LENGTH},
                "type": {"type": "string", "minLength": MIN_LENGTH},
                "parent": {"type": "string", "minLength": MIN_LENGTH},
                "alias": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["id"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def put(self):
        # endpoint: /cti?id=<ctiId>
        #
        AppServiceGlobal.REQUESTS.labels(method='PUT', endpoint="cti").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Cti.CtiHandler.get(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        ctiId = self._ipa_input_except(body, input_str="id")
        ctiName = self._ipa_input_except(body, input_str="name")
        ctiDescription = self._ipa_input_except(body, input_str="description")
        ctiParent = self._ipa_input_except(body, input_str="parent")
        ctiType = self._ipa_input_except(body, input_str="type")
        ctiAlias = self._ipa_input_except(body, input_str="alias")

        setting = {}
        if ctiName:
            setting['name'] = ctiName
        if ctiDescription:
            setting['description'] = ctiDescription
        if ctiParent:
            setting['parent'] = ctiParent
        if ctiType:
            setting['type'] = ctiType
        if ctiAlias:
            setting['alias'] = ctiAlias
        # 如果cti的type为 org,region,department则仅仅允许devops人员操作
        checkCtiIsExist = yield self._query_cti_node(ctiId=ctiId)
        if checkCtiIsExist[0]['type'] in ['org', 'region', 'department', 'organization'] and 'devops' not in session.department:
            # logging.debug("Cti.CtiHandler.put() {0} permission forbidden".format(ctiId))
            raise APIError(400, flag='COMMON.ACCOUNT.LIMIT_AUTHORITY')
        # update the node info
        res = yield self._update_cti_node(ctiId=ctiId, setting=setting)
        returnRes = res if res else 'failed'
        logging.info("Cti.CtiHandler.put() action successfully {0}".format(returnRes))
        raise gen.Return({"ctiId": ctiId})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def delete(self):
        #
        # endpoint: /cti?id=<ctiId>
        #
        AppServiceGlobal.REQUESTS.labels(method='DELETE', endpoint="cti").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Cti.CtiHandler.get(): {0}, begin action".format(session.email))

        ctiIds = self.get_arguments('id')
        if not ctiIds:
            logging.error("Cti.CtiHandler.delete() is forbidden")
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        res = []
        for cid in ctiIds:
            # 先判断id是否存在
            checkCtiIsExist = yield self._query_cti_node(ctiId=cid)
            if not checkCtiIsExist:
                raise APIError(404, flag='COMMON.GLOBAL.ERROR_PARAM')
            dtemp = yield self._delete_cti_node(ctiId=cid)

            res.append(dtemp)
        logging.info("Cti.CtiHandler.delete() action successfully {0}".format(res))
        raise gen.Return({'ctiId': ctiIds})


class GroupHandler(CtiUtilsHandler):
    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        #
        # endpoint: /cti/group?id=<cti_id>&direction=<direction>&filter=true/false
        #
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="cti/group").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Cti.GroupHandler.get(): begin action")

        # 根据ctiId查询时，仅仅支持单个
        # down 查询该节点下面所有的子节点
        # up 查询该节点上面所有的父节点
        ctiIds = self.get_arguments('id')
        direction = self.get_argument('direction', "down")
        filter = self.get_argument('filter', False)
        if direction and direction.lower() == "up":
            if not ctiIds:
                raise gen.Return({"data": []})
            res = yield self._one_by_one_child_id(ctiIds=ctiIds, filter=filter)
        else:
            # 查询所有关联了支持组的cti,并以树状的结构显示
            res = yield self._one_by_one_parent_id(parentId=ctiIds, filter=filter)
        logging.info('Cti.GroupHandler.get() action successfully')
        returnRes = res if res else 'failed'
        raise gen.Return({"data": returnRes})


class CTIBatchHandler(CtiUtilsHandler):
    '''
    批量插入多个cti(Category,type,item)
    输入的为id或者name，
    @param id: string 如果为name则表示新增
    @param name: string 如果为id，表示选择以前的
    '''
    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "cti": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "category": {"type": "string", "minLength": MIN_LENGTH},
                            "type": {"type": "string", "minLength": MIN_LENGTH},
                            "item": {"type": "string", "minLength": MIN_LENGTH}
                        },
                        "required": ["category", "type", "item"]
                    }
                }
            }
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def post(self):
        '''
            Check the CTI message. if no -> create it, yes link it
        '''
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="cti/ctibatch").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Cti.CTIBatchHandler.post(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        ctiLists = self._ipa_input_except(body, input_str="cti")

        # 校验此cti是否已经存在
        # 根据当前ctiParent判断是否为null，如果为Null,表示该节点为根节点,创建根节点
        # 如果有ctiParent,表示为叶子节点，将该节点加入到ctiParent节点之下
        returnRes = {
            "category": [],
            "type": [],
            "item": []
        }
        for ctiDict in ctiLists:
            for c in xrange(3):
                if c == 0:
                    # res = yield self._query_cti_by_doc({"name": ctiDict.get("category")},
                    #                                    deepth=c)
                    res = yield self._query_cti_category(ctiDict.get("category"))
                    if res:
                        # parentId = res[0].get("id")
                        parentId = res.get("id")
                    else:
                        ctiId = str(uuid.uuid4()).split('-')[-1]
                        res = yield self._create_cti_node(ctiId=ctiId,
                                                          name=ctiDict.get("category"),
                                                          desc=ctiDict.get("category"),
                                                          parent=None,
                                                          ctiType=None,
                                                          ctiAlias=None)
                        parentId = ctiId
                    returnRes["category"].append(parentId)
                elif c == 1:
                    # res = yield self._query_cti_by_doc({"name": ctiDict.get("type")},
                    #                                    deepth=c)
                    res = yield self._query_cti_type(ctiDict.get("category"),
                                                     ctiDict.get("type"))
                    if res:
                        # parentId = res[0].get("id")
                        parentId = res.get("id")
                    else:
                        ctiId = str(uuid.uuid4()).split('-')[-1]
                        res = yield self._create_cti_node(ctiId=ctiId,
                                                          name=ctiDict.get("type"),
                                                          desc=ctiDict.get("type"),
                                                          parent=parentId,
                                                          ctiType=None,
                                                          ctiAlias=None)
                        parentId = ctiId
                    returnRes["type"].append(parentId)
                elif c == 2:
                    # res = yield self._query_cti_by_doc({"name": ctiDict.get("item")},
                    #                                    deepth=c)
                    res = yield self._query_cti_item(ctiDict.get("category"),
                                                     ctiDict.get("type"),
                                                     ctiDict.get("item"))
                    if res:
                        # parentId = res[0].get("id")
                        parentId = res.get("id")
                    else:
                        ctiId = str(uuid.uuid4()).split('-')[-1]
                        res = yield self._create_cti_node(ctiId=ctiId,
                                                          name=ctiDict.get("item"),
                                                          desc=ctiDict.get("item"),
                                                          parent=parentId,
                                                          ctiType=None,
                                                          ctiAlias=None)
                        parentId = ctiId
                    returnRes["item"].append(parentId)
                else:
                    raise APIError(400, message="CtiBatch is Error")

                logging.info('Cti.CtiHandler.post() create cti id succeeded ')

        logging.info("Cti.CTIBatchHandler.post(): {0}, action succeeded".format(session.email))
        raise gen.Return({"data": returnRes})


class SearchHandler(CtiUtilsHandler):
    """
    endpoint: /cti/search
    """

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        # 根据parent_id和关键字搜索CTI
        # 如果没有p_id,则搜索catagory
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="cti/search").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Cti.SearchHandler.get(): begin action")

        pid = self.get_argument('pid', None)
        key = self.get_argument('key', '')

        if pid is not None:
            spec = {'parent': pid}
        else:
            spec = {'parent': None}

        spec['name'] = {'$regex': key, '$options': 'i'}
        res = yield self._query_cti_by_doc(spec)

        result = res if res else []
        raise gen.Return({'data': result})


class GlobalSearchHandler(CtiUtilsHandler):
    """
    endpoint: /cti/globalsearch
    """

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        """按关键字进行搜索，返回一个完整的cti对象"""
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="cti/globalsearch").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Cti.GlobalSearchHandler.get(): {0}, begin action".format(session.email))

        key = self.get_argument('key', None)

        result = []

        cti_items = yield self.format_cti()
        if key:
            reg = re.compile(key, re.IGNORECASE)
            for item in cti_items:
                if reg.search(item['category']['name']) or reg.search(item['type']['name']) \
                    or reg.search(
                        item['item']['name']):
                    result.append(item)
        else:
            result = cti_items

        raise gen.Return({'data': result})
