#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2016-09-09 14:53:39
# @Updated Date: 2016-09-12 16:31:37

"""
Attachment handler interfaces.
"""

import logging
import os
import uuid
import jsonschema
import hashlib
from tornado import gen

from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Utils import get_cls_name
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil
from Forge.Common.RcosConnection import RcosException
from Runtime.Constants import (BUCKETDIRNAME, MIN_LENGTH)


rcos_conn = AppServiceGlobal.onduty_rcos


class AttachmentHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /attachment
    """

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def post(self):
        """
            Upload attachment images;
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info(
            "Attachment.AttachmentHandler.post(): {0}, begin action".format(
                session.email))

        files = []
        if self.request.files:
            if not self.request.files.get('attachment'):
                raise APIError(400, message="Invalid Parameters Error, attachment is needed")
            for attachment in self.request.files.get('attachment'):
                raw_filename = attachment['filename']
                raw_ext = os.path.splitext(raw_filename)

                # compute the attachment md5
                atta_md5 = self.__md5(attachment['body'])
                # check attachment whether existed
                target_filename = '{0}{1}'.format(atta_md5, raw_ext[1])
                target_filepath = '{0}{1}'.format(
                    AppServiceGlobal.config.attachment_dir, target_filename)
                if not os.path.isfile(target_filepath):
                    with open(target_filepath, 'w') as fh:
                        fh.write(attachment['body'])
                        logging.debug("Attachment.AttachmentHandler.post(): {0}, save a new attachment {1}".format(session.email, target_filepath))
                        files.append((raw_filename, target_filename))
                else:
                    logging.debug("Attachment.AttachmentHandler.post(): {0}, save the existed attachment {1}".format(session.email, target_filepath))
                    files.append((raw_filename, target_filename))

        logging.info("Attachment.AttachmentHandler.post(): {0}, action success".format(session.email))
        raise gen.Return({"data": files})

    @validate(output_schema={
        "type": "object"
    })
    @gen.coroutine
    def post(self):
        """
            Upload attachment resource.
        """

        session = self.check_authorization(
            AppServiceGlobal.session_manager, self.request)
        logging.info(get_cls_name(
            self, "%s, upload object begin action." % session.email))

        bucket_dir_name = BUCKETDIRNAME.get("ATTACHMENT")

        attachment_ids = []
        try:
            if self.request.files:
                update_body = yield rcos_conn.upload_request_files(
                    request=self.request,
                    dir_name=bucket_dir_name,
                    reset_size=False)
                for file_body in update_body:
                    rcos_filename = file_body.get("file_name")
                    rcos_key = file_body.get("key")
                    doc = {
                        "filename": rcos_filename,
                        "key": rcos_key,
                        "id": 'attach-{0}'.format(str(uuid.uuid4())[:8]),
                        "description": self.get_argument('description')
                    }
                    res = yield AppServiceGlobal.db.attachment.insert(doc)
                    if not res:
                        logging.error(get_cls_name(self, "insert attachment fail"))
                    else:
                        file_body['attachment_id'] = doc['id']
                        attachment_ids.append(doc['id'])
            else:
                logging.error(get_cls_name(
                    self, "%s, can't files." % session.email))
                raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")
        except APIError as e:
            logging.error(get_cls_name(
                self, "upload_request_files APIError error %s" % e))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_RCOS")
        except RcosException as e:
            logging.error(get_cls_name(
                self, "upload_request_files RCOSException error %s" % e))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_RCOS")

        # 如果是在报障中添加附件，传ticket_id,如果是新创建报障是添加附件，还没有ticket_id，不会去更新ticket
        ticket_id = self.get_argument('ticket_id', None)
        if ticket_id:
            spec = {'id': ticket_id}
            if attachment_ids:
                setting = {'$addToSet': {'attachment': {'$each': attachment_ids}}}
                res = yield AppServiceGlobal.db.ticket.update(spec, setting)

        logging.info(get_cls_name(
            self, "%s, upload object action success." % session.email))
        raise gen.Return({"email": session.email, "body": update_body})

    @gen.coroutine
    def get(self):
        """
            Download attachment resource.
        """

        # session = self.check_authorization(
        #    AppServiceGlobal.session_manager, self.request)
        logging.info(get_cls_name(self, "download object begin action."))

        try:
            object_name = self.get_argument('key')
            file_name = self.get_argument('file_name', None)

            body = yield rcos_conn.download_object(key=object_name)
            content_type = self.get_buffer_mime(body)

            if content_type:
                self.set_header("Content-type", content_type)

            if file_name:
                self.add_header('Content-Disposition',
                                'attachment; filename="%s"' % file_name)

            logging.info(get_cls_name(self, "download object action success."))
            self.write(body)
        except APIError as e:
            logging.error(get_cls_name(
                self, "download_object APIError error %s " % e))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_RCOS")
        except RcosException as e:
            logging.error(get_cls_name(
                self, "download_object RCOSException error %s " % e))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_RCOS")

    def options(self):
        self.add_header("Access-Control-Allow-Credentials", "true")
        self.add_header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,\
                        Authorization, If-None-Match, If-Match, Cache-Control")
        self.add_header("Access-Control-Allow-Methods", "GET, PUT, POST, PATCH, DELETE, HEAD, OPTIONS")
        # self.add_header("Access-Control-Allow-Origin", "*")

        self.set_status(204)

    def __md5(self, file_content):
        hash_md5 = hashlib.md5()
        # for chunk in iter(lambda: file_handler.read(DEFAULT_FILE_CHUNK_SIZE), b""):
        #     hash_md5.update(chunk)

        hash_md5.update(file_content)

        return hash_md5.hexdigest()


class AttachmentURLAnnotationsHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /attachment/{object}
    """

    __url_names__ = ["[a-zA-Z0-9\.\-\/]+"]

    @gen.coroutine
    def delete(self):
        """
            Delete attachment resource.
        """

        session = self.check_authorization(
            AppServiceGlobal.session_manager, self.request)
        logging.info(get_cls_name(
            self, "%s, delete object begin action." % session.email))

        try:
            object_name = self._get_object_name(self.request.uri)
            yield rcos_conn.delete_object(key=object_name)
            logging.info(get_cls_name(
                self, "%s, delete object action success." % session.email))
        except APIError as e:
            logging.error(get_cls_name(
                self, "delete_object APIError error %s " % e))
            raise APIError(400, flag="APS.HANDLER.RCOS.ERROR_RCOS")
        except RcosException as e:
            logging.error(get_cls_name(
                self, "delete_object RCOSException error %s " % e))
            raise APIError(400, flag="APS.HANDLER.RCOS.ERROR_RCOS")

    def _get_object_name(self, uri):
        if uri:
            # return uri.split('/')[-1]
            return '/'.join(uri.split('/')[3:])
        logging.error(get_cls_name(
            self, "get object name error %s " % uri))
        raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")


class DescriptionHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /attachment/description
    """

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "attachment_id": {"type": "string"},
                "description": {"type": "string"}
            },
            "required": ["attachment_id", "description"]
        },
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @coroutine
    def put(self):
        """

        """
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info(get_cls_name(self, "%s, upload object begin action." % session.email))

        attachment_id = self.body.get('attachment_id')
        description = self.body.get('description')

        spec = {'id': attachment_id}
        setting = {'$set': {'description': description}}

        res = yield AppServiceGlobal.db.attachment.update(spec, setting)

        logging.info("Attachment.DescriptionHandler.put(): {0}, action success".format(session.email))
        raise gen.Return({"message": 'success' if res.get('updatedExisting') else "fail"})
