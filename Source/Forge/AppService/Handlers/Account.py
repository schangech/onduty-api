#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2016-08-22 18:19:04
# @Updated Date: 2016-11-14 11:47:30

import uuid
import os.path
import logging
import json
import jsonschema
from tornado import gen
from bson import json_util
from Forge.Common.Utils import get_cls_name

from Runtime.Constants import (
    MIN_LENGTH,
    USER_INIT_STATUS,
    LOGIN_SHELL)
from Forge.Common.State import (
    TICKET_STATE_OPENED)
from Forge.Common.Ops import (
    OPS_ACCOUNT_RESET_PASSWORD,
    OPS_ACCOUNT_ACTIVE,
    OPS_ACCOUNT_REJECT,
    OPS_ACCOUNT_UPDATE_PASSWORD)
from Forge.Common.Utils import is_email, validate_base64, ignored
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Email import Email
from Forge.Common.WechatManager import WechatManager
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil
from Forge.Common.Utils import (
    generate_user_info,
    is_phonenumbers,
    encrypt_password)
from Forge.Common.RcosConnection import RcosException
from Runtime.Constants import (BUCKETDIRNAME, MIN_LENGTH)


import sys
reload(sys)
sys.setdefaultencoding('utf-8')

__UPLOADS__ = "/rc/data/static/icons/"

rcos_conn = AppServiceGlobal.onduty_rcos


class AccountConnection(APIHandler, AppServiceUtil):

    @gen.coroutine
    def _find_user_from_mongodb(self, username):
        # 登录验证之前，先检测用户是否已经存在
        account = yield AppServiceGlobal.db.rc.aggregate(
            [
                {'$unwind': '$staffs'},
                {'$match': {'staffs.username': username}}
            ]
        ).to_list(None)
        tickets = json.loads(json_util.dumps(
            account, default=json_util.default))
        raise gen.Return(tickets)


class AccountHandler(AccountConnection):
    """
        endpoint: /account/
    """
    @validate(
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @coroutine
    def get(self):
        """
            User select.
            /account/?status=[enabled|disabled|removed|initial]&email=<username>&id=<id>&title=[CEO|CTO|leader]
        """
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Account.AccountHandler.get(): {0}, begin action".format(session.email))

        limit = self.get_argument('limit', '')
        page = self.get_argument('page', '')
        email = self.get_argument('email', '')
        username = self.get_argument('name', '')
        uid = self.get_argument('id', '')
        status = self.get_argument('status', 'enabled')
        search = self.get_argument('search', '')
        q = self.get_argument('q', '')

        if q == "API":
            raise gen.Return({"data": None})

        body = {}
        with ignored(Exception):
            # 如果没有指定id或者邮箱，默认获取状态为enabled用户
            if email:
                url = "/user?q={}&status={}".format(email, status)
            elif username:
                url = "/user?q={}&status={}".format(username, status)
            elif uid:
                url = "/user/{}".format(uid)
            elif q:
                url = "/user?q={}&status={}".format(q, status)
            elif search:
                url = "/user?q={}&status={}".format(search, status)
            else:
                url = "/user?status={}".format(status)

            if limit != '':
                url = '{}&limit={}'.format(url, limit) if '?' in url else '{}?limit={}'.format(url, limit)
            if page != '':
                url = '{}&page={}'.format(url, page) if '?' in url else '{}?page={}'.format(url, page)

            resp = yield self._request_metadata_api(
                method='GET', key=url, session=session)

            body = self._request_return_format(resp)
            body = json.loads(body)

        logging.info("Account.AccountHandler.get(): {0}, action success".format(session.email))
        raise gen.Return(json.loads(json_util.dumps(body, default=json_util.default)))

    '''
    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "email": {"type": "string", "minLength": MIN_LENGTH},
                "password": {"type": "string", "minLength": MIN_LENGTH},
                "firstname": {"type": "string", "minLength": MIN_LENGTH},
                "lastname": {"type": "string", "minLength": MIN_LENGTH},
                "mobile": {"type": "string", "minLength": MIN_LENGTH},
                "departmentId": {"type": "string"}
            },
            "required": ["email", "password", "mobile", "firstname", "lastname", "departmentId"]
        },
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def post(self):
        # User register.

        body = json.loads(self.request.body)
        email = self._ipa_input_except(body, input_str="email")
        username = email.split('@')[0]
        password = self._ipa_input_except(body, input_str="password")
        email = email.strip()
        if len(password) < 8:
            raise APIError(
                400, flag='APS.HANDLER.ACCOUNT_PASSWORD.PASSWORD_SHORT')
        if not is_email(email):
            raise APIError(400, flag='APS.HANDLER.ACCOUNT.INVALID_EMAIL')

        params = dict()
        mobile = self._ipa_input_except(body, input_str="mobile")
        if mobile:
            params['mobile'] = mobile

        telephonenumber = self._ipa_input_except(body, input_str="telephone")
        if not telephonenumber and mobile:
            mobile = is_phonenumbers(mobile)
            if not mobile:
                raise APIError(400, flag='APS.HANDLER.ACCOUNT.INVALID_PHONE')
            telephonenumber = mobile
            params['telephone'] = telephonenumber

        departmentId = self._ipa_input_except(body, input_str="departmentId")

        params['username'] = username
        params['firstname'] = body.get('firstname') or username
        params['lastname'] = body.get('lastname') or username
        params['email'] = email
        params['password'] = password
        params['title'] = body.get('title') or None
        params['status'] = body.get('status') or None
        params['is_super'] = body.get('is_super') or None
        params['wechat'] = body.get('wechat') or None
        params['company'] = body.get('company') or None
        params['department'] = departmentId

        resp = yield self._request_metadata_api(
            method='GET', key='/v1/user', filters='?q={0}'.format(username))
        if resp.code != 200:
            raise APIError(400, message=eval(resp.text).get('message'))
        if eval(resp.json().get('data')):
            raise APIError(400, message="username/email had exist")

        # Add metadata
        resp = yield self._request_metadata_api(
            method='POST', key='/v1/user', data=params)
        if resp.code != 200:
            logging.info('Metadata.add_user.result {0}'.format(resp))
            raise APIError(400, message=resp.text)

        # Create a ticket
        current_time = datetime.utcnow()
        key = '/v1/department/{}'.format(departmentId)
        resp = yield self._request_metadata_api(method='GET', key=key)
        mdReturn = eval(resp.json().get('data'))
        responser = [leader.get('leader')
                     for leader in mdReturn if leader.get('leader')] or None
        params.pop("password")
        ticket = {
            'type': "account",
            'severity': 4,
            'description': "{0} 账号申请".format(email),
            'requester': email,
            'responser': responser[0] if responser else None,
            'details': params,
            'ctiId': departmentId,
            'category': "account",
            'status': TICKET_STATE_OPENED,
            'cc_list': responser or None,
            'created_time': current_time,
            'updated_time': current_time,
            "flag": USER_INIT_STATUS
        }
        ticked_id = yield self._create_rc_ticket(email, ticket)
        if not ticked_id:
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        logging.info("Account.AccountHandler.POST() action successfully ")
        raise gen.Return({"ticked_id": ticked_id})

    @validate(
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @coroutine
    def delete(self):
        # User delete.
        #   /account/?email=<email name>
        session = self.check_authorization(
            AppServiceGlobal.session_manager, self.request)
        logging.info(
            "Account.AccountHandler.delete(): {0}, begin action".format(session.email))

        ids = self.get_argument('id', '')
        is_devops = yield self._is_devops_group(session.department)
        if not is_devops:
            logging.info(
                "Account.AccountHandler.delete(): {0} not in devops".format(session.email))
            raise APIError(400, flag='COMMON.ACCOUNT.LIMIT_AUTHORITY')

        res = yield self._request_metadata_api(
            key='/v1/user/{}'.format(ids=ids), method='DELETE')
        logging.info(
            "Account.AccountHandler.delete(), action success {res}".format(res=res))

        message = 'succeeded' if is_devops else 'failed'
        raise gen.Return({"message": message})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "givenname": {"type": "string", "minLength": MIN_LENGTH},
                "sn": {"type": "string", "minLength": MIN_LENGTH},
                "cn": {"type": "string", "minLength": MIN_LENGTH},
                "loginshell": {"type": "string", "enum": LOGIN_SHELL}
            }
        },
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @coroutine
    def put(self):
        """
            User modify.
            endpoint: /account/ put
        """
        session = self.check_authorization(
            AppServiceGlobal.session_manager, self.request)
        logging.info(
            "Account.AccountHandler.put(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        setting = dict()
        setting['id'] = session.uid
        current_time = int(time.time())
        email = self._ipa_input_except(body, "email", 'POST') or session.email
        givenname = self._ipa_input_except(body, "firstname", 'POST')
        if givenname:
            setting['firstname'] = givenname
        sn = self._ipa_input_except(body, "lastname", 'POST')
        if sn:
            setting['lastname'] = sn
        mobile = self._ipa_input_except(body, "mobile", 'POST')
        if mobile:
            telephone = is_phonenumbers(mobile)
        telephone = self._ipa_input_except(body, "telephone", 'POST')
        if telephone:
            telephone = is_phonenumbers(telephone)
            if not telephone:
                raise APIError(400, flag='APS.HANDLER.ACCOUNT.INVALID_PHONE')
            setting['telephone'] = telephone
        wechatId = self._ipa_input_except(body, "wechatId", 'POST')
        if wechatId:
            setting['wechat'] = wechatId
            # 把微信添加到通讯录，这里的微信号是微信id,不能是手机号或者昵称
            wechat_mg = WechatManager(AppServiceGlobal.config.mongo_url, AppServiceGlobal.config.mongo_db,
                                      AppServiceGlobal.config.wechat_surl, AppServiceGlobal.config.wechat_scorpsecret,
                                      AppServiceGlobal.config.wechat_scorpid, AppServiceGlobal.config.wechat_qychatsecret)
            username = email.split('@')[0]
            doc = {'users.uid': {'$in': [session.uid]}}
            resolvegroup_list = yield self._query_resolvegroup_info(doc)
            rg_name_list = [rg.get('name') for rg in resolvegroup_list]
            logging.debug('User {0} resolvegroup: {1}'.format(
                username, rg_name_list))
            # 在企业通讯录中创建用户，并加到部门所在的群聊中
            if wechat_mg.check_user_exits(username):
                old_wechatid = wechat_mg.get_user(username).get('weixinid', '')
                if wechatId.lower() != old_wechatid.lower():
                    logging.info('Update wechatId: old: {0}, new: {1}'.format(
                        old_wechatid, wechatId))
                    result = wechat_mg.update_user(username, wechatId)
                    # result = yield self.add_user_to_wechat(wechat_mg, username, wechatId, email, rg_name_list)
                    logging.info(
                        'Update wechat user {0} result {1}'.format(username, result))
            else:
                result = yield self.add_user_to_wechat(wechat_mg, username, wechatId, email, rg_name_list)
                logging.info(
                    'Add user {0} to wechat {1}'.format(username, result))

        title = self._ipa_input_except(body, "title", 'POST')
        if title:
            setting['title'] = title
        setting['update_time'] = current_time

        oldPass = self._ipa_input_except(body, input_str="oldPass")
        newPass = self._ipa_input_except(body, input_str="newPass")
        newPassConfirm = self._ipa_input_except(
            body, input_str="newPassConfirm")

        # 只有当登录session中的email等于更新email时，才能操作,或者管理员
        flag = yield self._check_user_equal_session_name(session.email, email, session.department)
        if not flag:
            logging.debug(
                "Account.AccountHandler.put(): {0} Not allow".format(session.email))
            raise APIError(400, flag='COMMON.ACCOUNT.LIMIT_AUTHORITY')

        if oldPass and newPass and newPassConfirm:
            if len(newPass) < 8 or len(newPassConfirm) < 8:
                logging.debug(
                    "Account.PasswordHandler.put() password is too short")
                raise APIError(
                    400, flag='APS.HANDLER.ACCOUNT_PASSWORD.PASSWORD_SHORT')

            if not (newPass.strip() == newPassConfirm.strip()):
                logging.debug(
                    "Account.PasswordHandler.put() two password not match")
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

            # 验证旧密码是否有效
            resp = yield self._request_metadata_api(
                key='/authentication/user', method='GET', filters='?email={name}'.format(name=email))
            if resp.code != 200:
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
            mdReturn = eval(resp.json().get('data'))
            encryptPassword = encrypt_password(
                oldPass, salt=mdReturn.get('salt'))
            if mdReturn.get('password') != encryptPassword:
                raise APIError(400, message='Old password not match')
            data = {"id": session.uid, "oldPassword": oldPass,
                    "newPassword": newPass, "confirmPassword": newPassConfirm}
            resp = yield self._request_metadata_api(
                key='/update/password', method='PUT', data=data)
            if resp.code != 200:
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
            raise gen.Return({'message': 'update password success'})

        res = yield self._request_metadata_api(
            key='/v1/user', method='PUT', data=setting)
        if res.code != 200:
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        logging.info(
            "Account.AccountHandler.put(): {0}, action success".format(email))
        raise gen.Return({"message": "success"})

    @gen.coroutine
    def add_user_to_wechat(self, wechat_mg, username, wechatId, email, department_list):
        """添加用户到微信企业号"""
        for dp in department_list:
            if not wechat_mg.get_department_by_name(dp):
                wechat_mg.create_department(dp)
        result = wechat_mg.create_user(
            userid=username,
            name=username,
            department=department_list,
            weixinid=wechatId,
            email=email
        )
        raise gen.Return(result)

    @gen.coroutine
    def _check_user_equal_session_name(self, loginEmail, updateEmail, department):
        # 判断当前登录用户的session是否等于更新账号 或者 为超级管理员
        is_devops = yield self._is_devops_group(department)
        flag = True if loginEmail == updateEmail or is_devops else False
        raise gen.Return(flag)
        '''


'''
class Wechat(AccountConnection):
    """获取微信是否关注"""

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def get(self):
        """uri: /account/wechat?email=<email>"""
        email = self.get_argument('email')
        username = email.split('@')[0]

        wechat_mg = WechatManager(
            AppServiceGlobal.config.mongo_url, AppServiceGlobal.config.mongo_db)
        is_followed = wechat_mg.check_user_follow(username)
        raise gen.Return({'data': is_followed})


class GroupHandler(AccountConnection):
    # endpoint: /account/group/

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def get(self):
        # uri:
        #     /account/group/?groupName=<group_name>
        # Get all or special groups infomation.
        # session = None
        # with ignored(Exception):
        try:
            session = self.check_authorization(
                AppServiceGlobal.session_manager, self.request)
        except Exception:
            session = None

        # logined user
        if session:
            logging.info(
                "Account.GroupHandler.get(): {0}, begin action".format(session.email))

            # 找出所有type为org，并且parent为null的所有数据
            doc = {}
            doc['parent'] = None
            doc['type'] = 'org'
            res = yield self._query_cti_by_doc(doc)
            topOrgId = []
            try:
                for orgCti in res:
                    topOrgId.append(orgCti.get('id'))
            except Exception as e:
                logging.debug(
                    'Account.GroupHandler.get() except {0}'.format(e))
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

            ctiTree = yield self._one_by_one_parent_id(parentId=topOrgId)
            # 循环查找，将每个节点上应该有的用户都加上
            TempAll = []
            for cti in ctiTree:
                # 进入每一个parent为null的根节点
                res = yield self._insert_fileds_to_cti(cti)
                TempAll.append(res)
            result = {
                'data': TempAll
            }

        else:  # unlogined user
            # logging.info("Account.GroupHandler.get(): begin action")
            # 找出所有type为org，并且parent为null的所有数据
            doc = {}
            doc['parent'] = None
            doc['type'] = 'org'
            res = yield self._query_cti_by_doc(doc)
            topOrgId = []
            try:
                for orgCti in res:
                    topOrgId.append(orgCti.get('id'))
            except Exception as e:
                logging.debug(
                    'Account.GroupHandler.get() except {0}'.format(e))
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

            departments = yield self._one_by_one_parent_id(parentId=topOrgId)
            result = {
                'data': departments
            }

            logging.info("Account.GroupHandler.get(): action success")

        raise gen.Return(result)

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "cn": {"format": "string", "minLength": MIN_LENGTH},
                "user": {"format": "string", "minLength": MIN_LENGTH},
                "group": {"format": "string", "minLength": MIN_LENGTH}
            },
            "required": ["cn"]
        },
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @coroutine
    def post(self):
        # endpoint: /account/group/
        # Create a new group and add the user
        # Waring: The user must be exist
        session = self.check_authorization(
            AppServiceGlobal.session_manager, self.request)
        logging.info(
            "Account.GroupHandler.post(): {0}, begin action".format(session.email))

        params = dict()
        body = json.loads(self.request.body)
        cn = body.get('cn').lower().strip().encode('utf-8')
        params['cn'] = cn
        user = self._ipa_input_except(body, input_str="user")
        params['user'] = user if user is not None else None
        group = self._ipa_input_except(body, input_str="group")
        params['group'] = group if group is not None else None

        # only devops can do it
        flag = yield self._is_devops_group(session.department)
        if not flag:
            raise APIError(400, flag='COMMON.ACCOUNT.LIMIT_AUTHORITY')

        resp = self._update_user_group(user, params)
        logging.info(
            "Account.GroupHandler.post(): {0}, action success".format(cn))
        raise gen.Return({"message": resp})
        '''


class IconHandler(AccountConnection):
    """
        Upload the user icon.
        endpoint: /ipa/account/icon/
    """
    @validate(
        output_schema={
            "type": "object"
        },
    )
    @coroutine
    def post(self):
        '''
            现阶段先简单的实现图片上传，不针对图片进行裁剪，修改，变换大小等操作.
        '''
        session = self.check_authorization(
            AppServiceGlobal.session_manager, self.request)
        logging.info(
            "Account.IconHandler.post(): {0}, begin action".format(session.email))

        email = session.email
        # 用户上传原图存放
        fileinfo = self.request.files['filearg'][0]
        fname = fileinfo['filename']
        extn = os.path.splitext(fname)[1]
        cname = str(uuid.uuid4()) + extn
        fh = open(__UPLOADS__ + cname, 'wb')
        fh.write(fileinfo['body'])
        fh.close()
        # 更新用户头像路径信息，即用户的缩率图
        thumbnail = os.path.splitext(cname)[0] + extn
        self.generate_thumbnail(cname, extn)
        setting = {}
        setting['id'] = session.uid
        setting['avatar'] = cname

        res = yield self._request_metadata_api(
            key='/user', method='PUT', data=setting)
        if res.code != 200:
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

        logging.info("Account.IconHandler.POST() success {0}".format(res))
        raise gen.Return({"message": "success", "data": cname,
                          "thumbnail": "thumbnail/" + thumbnail})

    @validate(output_schema={
        "type": "object"
    })
    @coroutine
    def put(self):
        """
            Upload avatar resource.
        """

        session = self.check_authorization(
            AppServiceGlobal.session_manager, self.request)
        logging.info(get_cls_name(
            self, "%s, upload object begin action." % session.email))

        uid = session.uid
        bucket_dir_name = BUCKETDIRNAME.get("AVATAR")
        setting = {}
        setting['id'] = uid

        try:
            if self.request.files:
                body = yield rcos_conn.upload_request_files(
                    request=self.request,
                    dir_name=bucket_dir_name,
                    reset_size=True)
                if body and len(body) == 1:
                    setting["avatar"] = body[0]["key"]

                    res = yield self._request_metadata_api(
                        key='/user', method='PUT', data=setting)
                    if res.code != 200:
                        raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
            else:
                logging.error(get_cls_name(
                    self, "%s, can't files." % session.email))
                raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")
        except APIError as e:
            logging.error(get_cls_name(
                self, "upload_request_files APIError error %s " % e))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_RCOS")
        except RcosException as e:
            logging.error(get_cls_name(
                self, "upload_request_files RCOSException error %s " % e))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_RCOS")

        logging.info(get_cls_name(
            self, "%s, upload object action success." % session.email))

        raise gen.Return({"email": session.email, "body": body})

    @coroutine
    def get(self):
        """
            Download avatar resource.
        """

        logging.info(get_cls_name(
            self, "%s, download object begin action." % "avatar"))

        object_name = self.get_argument('key')

        try:
            body = yield rcos_conn.download_object(key=object_name)
            content_type = self.get_buffer_mime(body)

            if content_type:
                self.set_header("Content-type", content_type)

            logging.info(get_cls_name(
                self, "%s, download object action success." % avatar))
            self.write(body)
        except APIError as e:
            logging.error(get_cls_name(
                self, "download_object APIError error %s " % e))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_RCOS")
        except RcosException as e:
            logging.error(get_cls_name(
                self, "download_object RCOSException error %s " % e))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_RCOS")


class IconURLAnnotationsHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /IPA/account/icon/{object}
    """

    __url_names__ = ["icon/[a-zA-Z0-9\.\-\/]+"]

    @coroutine
    def get(self):
        """
            Download avatar resource.
        """
        logging.info(get_cls_name(
            self, "%s, download object begin action." % "avatar"))

        try:
            object_name = self._get_object_name(self.request.uri)
            body = yield rcos_conn.download_object(key=object_name)
            content_type = self.get_buffer_mime(body)

            if content_type:
                self.set_header("Content-type", content_type)

            logging.info(get_cls_name(
                self, "%s, download object action success." % "avatar"))
            self.write(body)
        except APIError as e:
            logging.error(get_cls_name(
                self, "download_object APIError error %s " % e))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_RCOS")
        except RcosException as e:
            logging.error(get_cls_name(
                self, "download_object RCOSException error %s " % e))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_RCOS")

    @gen.coroutine
    def delete(self):
        """
            Delete avatar resource.
        """

        session = self.check_authorization(
            AppServiceGlobal.session_manager, self.request)
        logging.info(get_cls_name(
            self, "%s, delete object begin action." % session.email))

        try:
            object_name = self._get_object_name(self.request.uri)
            yield rcos_conn.delete_object(key=object_name)
            logging.info(get_cls_name(
                self, "%s, delete object action success." % session.email))
        except APIError as e:
            logging.error(get_cls_name(
                self, "delete_object APIError error %s " % e))
            raise APIError(400, flag="APS.HANDLER.RCOS.ERROR_RCOS")
        except RcosException as e:
            logging.error(get_cls_name(
                self, "delete_object RCOSException error %s " % e))
            raise APIError(400, flag="APS.HANDLER.RCOS.ERROR_RCOS")

    def _get_object_name(self, uri):
        if uri:
            # return uri.split('/')[-1]
            return '/'.join(uri.split('/')[4:])
        logging.error(get_cls_name(
            self, "get object name error %s " % uri))
        raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

    def generate_thumbnail(self, filename, extn):
        from PIL import Image

        size = (256, 256)
        outfile = os.path.splitext(filename)[0] + extn
        try:
            im = Image.open(__UPLOADS__ + filename)
            im.thumbnail(size, Image.ANTIALIAS)
            im.save(__UPLOADS__ + 'thumbnail/' + outfile, 'JPEG')
        except IOError:
            logging.error(
                "Account.IconHandler.POST() generate thumbnail failed {0}", filename)
