#!/usr/bin/env python
# -*- coding: utf-8 -*-

# __author__ = 'liuzhenwu'


"""
Root cause management interfaces.
"""

import logging
import json
import jsonschema
import uuid
from tornado import gen
from bson import json_util

from Runtime.Constants import MIN_LENGTH, DEFAULT_ROOT_CAUSE
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil
# prometheus
from prometheus_client import multiprocess
from prometheus_client import generate_latest, REGISTRY, Gauge, Counter


class RootcauseHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /rootcause/
    """

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        """
            uri:
                /rootcause/?resolvegroupId=<resolvegroupId>&rootcauseId=<rootcauseId>
            Get all rootcauses information or get a special rootcause information
        """
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="rootcause").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("{0}, begin action".format(session.email))

        rg_id = self.get_argument('resolvegroupId', None)
        rc_id = self.get_argument('rootcauseId', None)

        spec = {}

        if rg_id:
            # 先判断resolvegroup是不是存在
            rg = yield AppServiceGlobal.db.resolvegroup.find_one(
                {
                    'id': rg_id
                }
            )
            if not rg:
                logging.warning("{0}, invalid resolvegroup {1}".format(session.email, rg_id))
                raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

            spec['resolvegroup'] = rg_id
        if rc_id:
            spec['id'] = rc_id

        rc_list = yield AppServiceGlobal.db.rootcause.find(spec).to_list(None)

        # 如果按照resolvegroup查找没有找到，则添加默认的root cause到该resolvegroup
        if rg_id and len(rc_list) == 0:
            rc_list = yield self._insert_default(rg_id)

        result = json.loads(json_util.dumps(rc_list, default=json_util.default))

        logging.info("{0}, action success".format(session.email))
        raise gen.Return({"data": result})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "name": {"type": "string", "minLength": MIN_LENGTH},
                "description": {"type": "string", "minLength": MIN_LENGTH},
                "resolvegroup_id": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["name", "resolvegroup_id"]
        },
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def post(self):
        """
            Add a new root cause.
        """
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="rootcause").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("{0}, begin action".format(session.email))

        name = self.body.get('name')
        description = self.body.get('description')
        rg_id = self.body.get('resolvegroup_id')

        # 先判断resolvegroup是不是存在
        rg = yield AppServiceGlobal.db.resolvegroup.find_one(
            {
                'id': rg_id
            }
        )
        if not rg:
            logging.warning("{0}, invalid resolvegroup {1}".format(session.email, rg_id))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

        rootcause = {
            'id': 'rc-{0}'.format(str(uuid.uuid4())[:8]),
            'name': name,
            'resolvegroup': rg_id
        }
        if description is not None:
            rootcause['description'] = description

        res = yield AppServiceGlobal.db.rootcause.insert(rootcause)
        if not res:
            logging.error("{0}, add new rootcause {0} failed".format(session.email, rootcause['id']))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        else:
            rootcause.pop('_id')

        result = json.loads(json_util.dumps(rootcause, default=json_util.default))
        logging.info("{0}, action success".format(session.email))
        raise gen.Return({"data": result})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "rootcause_id": {"type": "string", "minLength": MIN_LENGTH},
                "name": {"type": "string", "minLength": MIN_LENGTH},
                "description": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["rootcause_id"]
        },
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def put(self):
        """
            uri:
                /rootcause/
            Update special rootcause.
        """
        AppServiceGlobal.REQUESTS.labels(method='PUT', endpoint="rootcause").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("{0}, begin action".format(session.email))

        rc_id = self.body.get('rootcause_id')
        name = self.body.get('name')
        description = self.body.get('description')

        setting = {}

        if name is not None:
            setting['name'] = name
        if description is not None:
            setting['description'] = description

        res = yield AppServiceGlobal.db.rootcause.update(
            {'id': rc_id},
            {'$set': setting},
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("{0}, update root cause info failed".format(session.email))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        raise gen.Return({"message": "update root cause {0} successfully".format(rc_id)})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def delete(self):
        """
            uri:
                /rootcause/?rootcauseId=<rootcauseId>
            Delete special rootcause from resolvegroup
        """
        AppServiceGlobal.REQUESTS.labels(method='DELETE', endpoint="rootcause").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("{0}, begin action".format(session.email))

        spec = {}
        rootcause_ids = self.get_arguments('rootcauseId')
        if rootcause_ids and isinstance(rootcause_ids, list):
            spec['id'] = {
                '$in': rootcause_ids
            }

        # 解除与resolvegroup的关联
        res = yield AppServiceGlobal.db.rootcause.update(
            spec,
            {
                '$set': {
                    'resolvegroup': None
                }
            },
            multi=True,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("{0}, remove rootcause {0} failed".format(session.email, rootcause_ids))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        logging.info("{0}, action success".format(session.email))
        raise gen.Return({"message": "remove rootcause {0} successfully".format(rootcause_ids)})

    @coroutine
    def _insert_default(self, resolvegroup_id):
        """
        Insert default root cause for resolvegroup
        """
        logging.info('Insert default root cause for resolvegroup {0}'.format(resolvegroup_id))

        insert_result = []

        for default in DEFAULT_ROOT_CAUSE:
            rootcause = {
                'id': 'rc-{0}'.format(str(uuid.uuid4())[:8]),
                'name': default.get('name'),
                'description': default.get('description'),
                'resolvegroup': resolvegroup_id
            }
            res = yield AppServiceGlobal.db.rootcause.insert(rootcause)
            if not res:
                logging.error("add default rootcause {0} failed".format(rootcause['name']))
            else:
                insert_result.append(rootcause)

        raise gen.Return(insert_result)


class DefaultHandler(APIHandler, AppServiceUtil):

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        """
            uri:
                /rootcause/default
            Get default rootcause information
        """
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="rootcause/default").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("{0}, begin action".format(session.email))

        raise gen.Return({"data": DEFAULT_ROOT_CAUSE})
