#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
# File: ResloveGroup.py
# Time: 4:30:52 PM
# Desc:
# Author: sendal
# Email: <schangech@gmail.com>
# Version: 0.0.1
'''

import uuid
import time
import logging
import json
import jsonschema
from datetime import datetime, timedelta
from bson import json_util
from tornado import gen
from tornado import web
from Runtime.Constants import (
    MIN_LENGTH,
    LOGIN_SHELL,
    DEFAULT_LOCALE)
from Forge.Common.State import (
    TICKET_STATE_OPENED)
from Forge.Common.Result import Result
from Forge.Common.Ops import (
    OPS_ACCOUNT_RESET_PASSWORD,
    OPS_ACCOUNT_ACTIVE,
    OPS_ACCOUNT_UPDATE_PASSWORD)
from Forge.Common.State import SCHEDULE_STATE_ACTIVE
from Forge.Common.Utils import is_email, validate_base64
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Email import Email
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil
from Forge.Common.Utils import ignored, parse_department, ipa_user_info_format_joint, is_phonenumbers

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


class ResolveGroupUtilsHandler(APIHandler, AppServiceUtil):

    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    def _distinct_element(self, elements):
        import itertools
        elements.sort()
        eles = itertools.groupby(elements)
        new_elements = [ele for ele, group in eles]
        return new_elements

    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    def _check_uid_is_in_master(self, uid, master_list):
        uid = uid.get('uid')
        master_id = [mid for mid in master_list if mid.get('uid')]
        flag = True if uid in master_id else False
        return flag

    def _check_uid_is_in_user(self, uid, user_list):
        uid = uid.get('uid')
        user_id = [mid for mid in user_list if mid.get('uid')]
        flag = True if uid in user_id else False
        return flag

    def _update_uid_order(self, uid, order=10):
        pass

    def _merge_new_data_for_resolvegroup_user(self, old_data, new_data):
        # 两个列表，都包含有字典，当Ａ中存在，则更新Ａ中信息，否则将Ｂ中新字典加入到Ａ中
        temp_su = [{s.get('uid'): s.get('order')} for s in old_data]
        temp_fu = [{s.get('uid'): s.get('order')} for s in new_data]
        for f in temp_fu:
            for s in temp_su:
                if s.keys()[0] == f.keys()[0]:
                    temp_su.remove(s)
                    temp_su.append(f)
                    break
            else:
                temp_su.append(f)
        return [{'uid': t.keys()[0], 'order': t.values()[0]} for t in temp_su]

    def _format_data_struct(self, datas):
        struct_data = []
        if isinstance(datas, list):
            for data in datas:
                struct_data.append(self._format_data(data))
        else:
            struct_data.append(self._format_data(datas))
        return struct_data

    def _format_data(self, data):
        if isinstance(data, dict) and 'order' not in data:
            data['order'] = 10
        return data

    @gen.coroutine
    def _query_resolvegroup_master(self, rid):
        # 查询用户信息
        doc = {
            'masters': {
                '$in': [rid]
            }
        }
        res = yield AppServiceGlobal.db.resolvegroup.find(
            doc,
            {
                '_id': False
            }
        ).to_list(None)
        if not res:
            raise gen.Return(False)

        result = json.loads(json_util.dumps(res, default=json_util.default))
        raise gen.Return(result)

    @gen.coroutine
    def _update_resolvegroup_info(self, rg_id=None, setting=None):
        doc = {
            '$set': setting
        }
        res = yield AppServiceGlobal.db.resolvegroup.update(
            {
                'id': rg_id
            },
            doc,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("ResolveGroup.ResolveGroupUtilsHandler._update_resolvegroup_info(): {0}, update failed".format(rg_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        info = json.loads(json_util.dumps(setting, default=json_util.default))
        raise gen.Return(info)

    @gen.coroutine
    def _delete_resolvegroup_info(self, rid):
        """不再物理删除，而是把状态设置为removed"""
        if rid:
            doc = {'id': {'$in': rid}}
        res = yield AppServiceGlobal.db.resolvegroup.update(
            doc,
            {'$set': {'status': 'removed'}},
            multi=True
        )
        # 如果有对应的排班，需要把排班也置为不可用状态
        rg_doc = {"resolvegroup": rid[0] if isinstance(rid, list) else rid, "status": SCHEDULE_STATE_ACTIVE}
        is_link_schedu = yield self._check_resolvegroup_is_in_scheduler(doc=rg_doc)
        if not is_link_schedu:
            yield AppServiceGlobal.db.schedule.update(
                rg_doc,
                {'$set': {'status': 'removed'}}
            )
        if not res:
            logging.error("ResolveGroup.ResolveGroupUtilsHandler._delete_resolvegroup_info(): {0} delete cti failed".format(rid))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        cti = json.loads(json_util.dumps(res, default=json_util.default))
        raise gen.Return(cti)

    @gen.coroutine
    def _multi_to_one(self, ids, uid, flag, field, session=None):
        status = []
        if ids and uid and isinstance(ids, list):
            for id in ids:
                ret = self._one_to_one(id, uid, flag, field, session=session)
                status.append(ret)
        raise gen.Return(status)

    @gen.coroutine
    def _one_to_multi(self, id, uids, flag, field, session=None):
        status = []
        if id and uids and isinstance(uids, list):
            for uid in uids:
                ret = self._one_to_one(id, uid, flag, field, session=session)
                status.append(ret)
        raise gen.Return(status)

    @gen.coroutine
    def _one_to_one(self, id, uid, flag, field, session=None):
        res = yield self._query_resolvegroup_info({'id': id}, session=session)
        # 将用户加入到RG中或者RG中删除
        if res and isinstance(res, list) and isinstance(uid, dict):
            setting = {}
            if flag and flag.lower() == "add":
                user = self._format_data_struct(uid)
                res[0][field] = self._merge_new_data_for_resolvegroup_user(res[0][field], user)
                setting[field] = self._distinct_element(res[0][field])
            elif flag and flag.lower() == "remove":
                uids = map(lambda x: x.get('uid'), res[0][field])
                if (uid.get('uid') in uids) and len(uids) < 2:
                    logging.warning('ResolveGroup._one_to_one {0} {1} operation forbidden'.format(id, field))
                    raise APIError(400, flag="APS.HANDLER.RESOLVEGROUP.USER")
                for removeUid in [tmp for tmp in res[0][field] if tmp.get('uid') == uid.get('uid')]:
                    res[0][field].remove(removeUid)
                setting[field] = self._distinct_element(res[0][field])
            updateRes = yield self._update_resolvegroup_info(rg_id=id, setting=setting)
        else:
            updateRes = None
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")
        result = 'successfully' if updateRes else 'failed'
        raise gen.Return(result)

    @gen.coroutine
    def _resolveGroup_multi_to_one(self, ids, uid, flag, field):
        status = []
        if ids and uid and isinstance(ids, list):
            for id in ids:
                ret = self._resolveGroup_one_to_one(id, uid, flag, field)
                status.append(ret)
        raise gen.Return(status)

    @gen.coroutine
    def _resolveGroup_one_to_multi(self, id, uids, flag, field):
        status = []
        if id and uids and isinstance(uids, list):
            for uid in uids:
                ret = self._resolveGroup_one_to_one(id, uid, flag, field)
                status.append(ret)
        raise gen.Return(status)

    @gen.coroutine
    def _resolveGroup_one_to_one(self, id, ctiId, flag, field, session=None):
        res = yield self._query_resolvegroup_info({'id': id}, session=session)

        if res and isinstance(res, list) and isinstance(ctiId, basestring):
            setting = {}
            if flag and flag.lower() == "add":
                res[0][field].append(ctiId)
                setting[field] = list(set(res[0][field]))
            elif flag and flag.lower() == "remove":
                res[0][field].remove(ctiId)
                setting[field] = list(set(res[0][field]))
            updateRes = yield self._update_resolvegroup_info(rg_id=id, setting=setting)
        else:
            updateRes = None
            raise APIError(404, flag="COMMON.GLOBAL.ERROR_PARAM")
        result = 'successfully' if updateRes else 'failed'
        raise gen.Return(result)


class ResolveGroupHandler(ResolveGroupUtilsHandler):

    @web.asynchronous
    @validate(
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @AppServiceGlobal.SUMMARY.time()
    @AppServiceGlobal.REQUESTSEXC.count_exceptions()
    @AppServiceGlobal.HISTONGRAM.time()
    @coroutine
    def get(self):
        """
            获取支持组信息
            User select.
            /resolvegroup?id=<ID>&uid=<ID>&mine=true&limit=10&page=2
        """
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="resolvegroup").inc()
        logging.info("ResolveGroup.ResolveGroupHandler.get(): begin action")

        limit = self.get_argument("limit", None)
        page = self.get_argument("page", None)
        resolveGroupIds = self.get_arguments('id')
        uids = self.get_arguments('uid')
        is_mine = self.get_argument('mine', 'false')
        # 获取所有或者根据用户获取时，过滤掉状态为removed的
        if limit and int(limit) > 1000:
            raise APIError(400, message="Page limit over max 1000")
        total_spec = [{'status': {'$ne': 'removed'}}]
        res = []
        if resolveGroupIds:
            total_spec.append({'id': {'$in': [str(rid) for rid in resolveGroupIds]}})
            doc = {'id': {'$in': [str(rid) for rid in resolveGroupIds]}}
        if uids:
            total_spec.append({'users.uid': {'$in': uids}})
            doc = {"$and": [{'users.uid': {'$in': uids}}, {'status': {'$ne': 'removed'}}]}
        if not resolveGroupIds and not uids:
            doc = {'status': {'$ne': 'removed'}}
        if is_mine.lower() == 'true':
            # 如果要获取我所书的支持组，需要从session里获取用户id， 其他查询方式可以不用session正常查询
            session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
            total_spec.append({'users.uid': {'$in': [session.uid]}})
            doc = {"$and": [{'users.uid': {'$in': [session.uid]}}, {'status': {'$ne': 'removed'}}]}
        if limit and page:
            try:
                res = yield self._query_resolvegroup_info(
                    doc, limit=int(limit), page=int(page))
            except ValueError as err:
                logging.error("ResolveGroup.ResolveGroupHandler.get Exception {}".format(err))
                raise APIError(400, message="ValueError Invalid literal for int")
        else:
            res = yield self._query_resolvegroup_info(doc)
        total_count = yield self._query_resolvegroup_totalcount({"$and": total_spec})
        returnData = res if res else []
        allUserInfo = yield self._query_account()

        for tmpData in returnData:
            tmpMasters = []
            tmpUsers = []
            for user in allUserInfo:
                for mId in tmpData.get("masters"):
                    if user.get("status") == "enabled" and user.get("id") == mId.get("uid"):
                        user['order'] = mId['order']
                        tmpMasters.append(user)
                        break
                for uId in tmpData.get("users"):
                    if user.get("status") == "enabled" and user.get("id") == uId.get("uid"):
                        user['order'] = uId['order']
                        tmpUsers.append(user)
                        break
            tmpData['masters'] = tmpMasters
            tmpData['users'] = tmpUsers

        raise gen.Return({"data": returnData, "count": len(returnData),
                          "total_count": total_count})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "name": {"type": "string", "minLength": MIN_LENGTH},
                "description": {"type": "string", "minLength": MIN_LENGTH},
                "masters": {"type": "array"},
                "rootcause": {"type": "array"},
                "sla": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "severity": {"type": "number", "enum": [1, 2, 3, 4, 5]},
                            "response_time": {"type": "integer"},  # 响应时间
                            "processing_time": {"type": "integer"},  # 处理时间
                            "description": {"type": "string", "minLength": MIN_LENGTH}
                        }
                    }
                },
                "escalation": {
                    "type": "array",
                    "items": {
                        "type": "array",
                        "items": {
                            "type": "string", "minLength": MIN_LENGTH
                        }
                    }
                },
                "customize": {"type": "boolean"}
            },
            "required": ["name", "description", "masters"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def post(self):
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="resolvegroup").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.ResolveGroupHandler.post(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        name = self._ipa_input_except(body, input_str="name")
        description = self._ipa_input_except(body, input_str="description")
        users = self._ipa_input_except(body, input_str="users")
        masters = self._ipa_input_except(body, input_str="masters")
        ctis = self._ipa_input_except(body, input_str="ctis")
        rootcause = self._ipa_input_except(body, input_str="rootcause")
        sla = self._ipa_input_except(body, input_str="sla")
        escalation = self._ipa_input_except(body, input_str="escalation")
        customize = self._ipa_input_except(body, input_str="customize")
        rid = str(uuid.uuid4()).split('-')[-1]
        currentTime = int(time.time())
        doc = {}
        doc['id'] = 'rg-' + rid
        doc['add_time'] = currentTime
        doc['update_time'] = currentTime
        doc['name'] = name
        doc['description'] = description
        doc['users'] = self._format_data_struct(users) or []
        doc['masters'] = self._format_data_struct(masters) or []
        doc['ctis'] = ctis or []
        returnRes = yield AppServiceGlobal.db.resolvegroup.insert(doc)

        if returnRes:
            # insert rootcause
            if isinstance(rootcause, list):
                rc_list = [{
                    'id': 'rc-{0}'.format(str(uuid.uuid4())[:8]),
                    'name': r.get('name'),
                    'description': r.get('description'),
                    'resolvegroup': doc['id']
                } for r in rootcause]
                res = yield AppServiceGlobal.db.rootcause.insert(rc_list)
                if not res:
                    logging.error("insert rootcause failed: {}".format(doc['id']))
                    raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

            # insert sla
            if isinstance(sla, list):
                sla_setting = {
                    'id': doc['id'],
                    'sla': sla
                }
                if escalation is not None:
                    sla_setting['escalation'] = escalation
                if customize is not None:
                    sla_setting['customize'] = customize

                res = yield AppServiceGlobal.db.sla.insert(sla_setting)

                if not res:
                    logging.error("insert sla failed: {}".format(doc['id']))
                    raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        # insert return object
        logging.info("ResolveGroup.ResolveGroupHandler.post(): {0} action success \n {1}".format(session.email, returnRes))
        returnRes = 'successfully' if returnRes else 'failed'
        raise gen.Return({"message": returnRes})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "id": {"type": "string", "minLength": MIN_LENGTH},
                "name": {"type": "string", "minLength": MIN_LENGTH},
                "description": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["id"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def put(self):
        # endpoint: /resolvegroup
        AppServiceGlobal.REQUESTS.labels(method='PUT', endpoint="resolvegroup").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.ResolveGroupHandler.put(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        rg_id = self._ipa_input_except(body, input_str="id")
        name = self._ipa_input_except(body, input_str="name")
        description = self._ipa_input_except(body, input_str="description")
        users = self._ipa_input_except(body, input_str="users")
        masters = self._ipa_input_except(body, input_str="masters")
        ctis = self._ipa_input_except(body, input_str="ctis")
        # 支持支持组中支持修改
        rootcause = self._ipa_input_except(body, input_str="rootcause")
        sla = self._ipa_input_except(body, input_str="sla")
        escalation = self._ipa_input_except(body, input_str="escalation")
        customize = self._ipa_input_except(body, input_str="customize")
        currentTime = int(time.time())
        # 校验支持组是否存在
        findOut = yield self._query_resolvegroup_info({'id': rg_id}, session=session)
        if not findOut:
            logging.warning('ResolveGroup.ResolveGroupHandler.put() {0} not exist'.format(rg_id))
            raise APIError(404, flag='COMMON.GLOBAL.ERROR_PARAM')

        # 修改支持组
        # 当存在,则进行更新[如果有更新],不存在,则进行创建
        if rootcause:
            # 删除那些这次没有传过来的根因
            rg_rc = yield AppServiceGlobal.db.rootcause.find(
                {"resolvegroup": rg_id}
                ).to_list(None)
            old_rc_ids = [rc.get("id") for rc in rg_rc if rc.get("id")]
            new_rc_ids = [rc.get("id") for rc in rootcause if rc.get("id")]
            remove_rc_id = list(set(old_rc_ids) ^ set(new_rc_ids))
            yield AppServiceGlobal.db.rootcause.remove(
                {"resolvegroup": rg_id, "id": {"$in": remove_rc_id}}
                )
            for rc in rootcause:

                is_exist = yield AppServiceGlobal.db.rootcause.find_one(
                    {"resolvegroup": rc.get("resolvegroup"), "id": rc.get("id")})
                if is_exist:
                    yield AppServiceGlobal.db.rootcause.update(
                        {"resolvegroup": rc.get("resolvegroup"), "id": rc.get("id")},
                        {"$set": {
                            'name': rc.get('name'),
                            'description': rc.get('description')
                            }
                         })
                else:
                    insert_rootcase = {
                        'id': 'rc-{0}'.format(str(uuid.uuid4())[:8]),
                        'name': rc.get('name'),
                        'description': rc.get('description'),
                        'resolvegroup': rg_id
                    }
                    yield AppServiceGlobal.db.rootcause.insert(insert_rootcase)

        # sla,自定义和escalation
        sla_is_exist = yield AppServiceGlobal.db.sla.find_one({"id": rg_id})
        sla_setting = {}
        if sla is not None:
            sla_setting['sla'] = sla
        if escalation is not None:
            sla_setting['escalation'] = [esc for esc in escalation if esc]
        if customize is not None:
            sla_setting['customize'] = customize
        if sla_is_exist:
            yield AppServiceGlobal.db.sla.update({"id": rg_id},
                                                 {"$set": sla_setting})
        else:
            sla_setting["id"] = rg_id
            sla_doc = sla_setting
            yield AppServiceGlobal.db.sla.insert(sla_doc)
        setting = {}
        if name:
            setting['name'] = name
        if description is not None:
            setting['description'] = description
        if isinstance(users, list):
            # user_list = findOut[0].get('users') + self._format_data_struct(users)
            # setting['users'] = self._distinct_element(user_list)
            setting['users'] = users
        if isinstance(masters, list):
            # master_list = findOut[0].get('masters') + self._format_data_struct(masters)
            # setting['masters'] = self._distinct_element(master_list)
            setting['masters'] = masters
        if isinstance(ctis, list):
            setting['ctis'] = list(set(ctis))
        setting['update_time'] = currentTime

        # update the resolveGroup info
        res = yield self._update_resolvegroup_info(rg_id=rg_id, setting=setting)
        returnRes = 'success' if res else 'failed'
        logging.info("ResolveGroup.ResolveGroupHandler.post(): {0}, action success".format(session.email))
        raise gen.Return({"message": returnRes})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def delete(self):
        #
        # endpoint: /resolvegroup?id=<ID>
        #
        AppServiceGlobal.REQUESTS.labels(method='DELETE', endpoint="resolvegroup").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.ResolveGroupHandler.delete(): {0}, begin action".format(session.email))

        rg_ids = self.get_arguments('id')
        if not rg_ids:
            logging.error("ResolveGroup.ResolveGroupHandler.delete() is forbidden")
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        rids = [str(rid) for rid in rg_ids if rid]
        for rid in rids:
            # 禁止删除那些带tag的rg
            doc = {'id': rid}
            checkRG = yield self._query_resolvegroup_info(doc, session=session)
            if checkRG and isinstance(checkRG, list) and checkRG[0].get('tag'):
                logging.warning("ResolveGroup.ResolveGroupHandler.delete() is forbidden")
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
            # # 如果删除的resolvegroup中有scheduler,则禁止删除
            # rg_doc = {"resolvegroup": rid, "status": SCHEDULE_STATE_ACTIVE}
            # is_link_schedu = yield self._check_resolvegroup_is_in_scheduler(doc=rg_doc)
            # if not is_link_schedu:
            #     logging.warning("ResolveGroup.ResolveGroupHandler.delete() {0} resolvegroup can find the schedu".format(rid))
            #     raise APIError(400, rid, flag='APS.HANDLER.RESOLVEGROUP.RGSCHEDU')

        deleteRes = yield self._delete_resolvegroup_info(rid=rids)
        logging.info("ResolveGroup.ResolveGroupHandler.delete() {0} action successfully".format(session.email))
        result = 'successfully' if deleteRes else 'failed'
        raise gen.Return({'message': result})


class AdminHandler(ResolveGroupUtilsHandler):
    @validate(
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        # /resolvegroup/admin?rgId=<resolvegroup_id>
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="resolvegroup/admin").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.UserHandler.get(): {0}, begin action".format(session.email))

        rgId = self.get_argument('rgId', None)
        resolvegroup_info = yield self._query_resolvegroup_info(
            {'id': {'$in': [rgId]}}, session=session)
        # 测试当resolvegroup为空时的结果
        masters = resolvegroup_info[0].get('masters') if resolvegroup_info else []
        master_ids = [master['uid'] for master in masters]
        masters = yield self.filter_users(master_ids)
        users = [user for user in masters if user.get('status') == "enabled"]

        logging.info("ResolveGroup.UserHandler.get() {0} action successfully".format(users))
        raise gen.Return({'data': users})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "id": {"type": "string", "minLength": MIN_LENGTH},
                "user": {
                    "type": "object",
                    "properties": {
                        "uid": {"type": "string", "minLength": MIN_LENGTH},
                        "order": {"type": "integer"}
                    },
                    "required": ["uid"]
                },
                "flag": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["id", "user", "flag"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def post(self):
        # endpoint: /resolvegroup/admin
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="resolvegroup/admin").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.AdminHandler.post(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        id = self._ipa_input_except(body, input_str="id")
        uid = self._ipa_input_except(body, input_str="user")
        flag = self._ipa_input_except(body, input_str="flag")

        if not id or not uid:
            logging.warning('ResolveGroup.AdminHandler.post() resovegroup and uid is null')
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        if isinstance(id, list) and isinstance(uid, list):
            logging.warning('ResolveGroup.AdminHander.post() resovegroup and uid param err')
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")
        elif isinstance(id, list):
            res = yield self._multi_to_one(id, uid, flag, field='masters', session=session)
            # if flag.lower() == "add":
            #     res = yield self._multi_to_one(id, uid, flag, field='users')
        elif isinstance(uid, list):
            res = yield self._one_to_multi(id, uid, flag, field='masters', session=session)
            # if flag.lower() == "add":
            #     res = yield self._one_to_multi(id, uid, flag, field='users')
        else:
            res = yield self._one_to_one(id, uid, flag, field='masters', session=session)
            # if flag.lower() == "add":
            #     res = yield self._one_to_one(id, uid, flag, field='users')

        result = 'successfully' if 'failed' not in res else 'failed'
        raise gen.Return({"message": result})


class UserHandler(ResolveGroupUtilsHandler):
    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def post(self):
        # endpoint: /resolvegroup/user
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="resolvegroup/user").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.UserHandler.get(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        id = self._ipa_input_except(body, input_str="id")
        uid = self._ipa_input_except(body, input_str="user")
        flag = self._ipa_input_except(body, input_str="flag")

        if not id or not uid:
            logging.warning('ResolveGroup.UserHandler.post() empty...')
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        if isinstance(id, list) and isinstance(uid, list):
            logging.warning('ResolveGroup.UserHandler.post() param is error')
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")
        elif isinstance(id, list):
            # resAdmin = yield self._query_resolvegroup_info({'id': {'$in': [id]}})
            # if resAdmin and isinstance(resAdmin, list) and flag.lower() == "remove":
            #     is_group = self._check_uid_is_in_master(uid, resAdmin[0].get('masters'))
            #     if resAdmin[0].get('masters') and is_group:
            #         resMaster = yield self._multi_to_one(id, uid, flag, field='masters')
            #         logging.warning('ResolveGroup.UserHandler.post() {0}'.format(resMaster))
            res = yield self._multi_to_one(id, uid, flag, field='users')
        elif isinstance(uid, list):
            # resAdmin = yield self._query_resolvegroup_info({'id': {'$in': [id]}})
            # if resAdmin and isinstance(resAdmin, list) and flag.lower() == "remove":
            #     is_group = self._check_uid_is_in_master(uid, resAdmin[0].get('masters'))
            #     if resAdmin[0].get('masters') and is_group:
            #         resMaster = yield self._one_to_multi(id, uid, flag, field='masters')
            #         logging.debug('ResolveGroup.UserHandler.post() {0}'.format(resMaster))
            res = yield self._one_to_multi(id, uid, flag, field='users')
        else:
            # resAdmin = yield self._query_resolvegroup_info({'id': {'$in': [id]}})
            # if resAdmin and isinstance(resAdmin, list) and flag.lower() == "remove":
            #     is_group = self._check_uid_is_in_master(uid, resAdmin[0].get('masters'))
            #     if resAdmin[0].get('masters') and is_group:
            #         resMaster = yield self._one_to_one(id, uid, flag, field='masters')
            #         logging.debug('ResolveGroup.UserHandler.post() {0}'.format(resMaster))
            res = yield self._one_to_one(id, uid, flag, field='users')

        result = 'successfully' if 'failed' not in res else 'failed'
        raise gen.Return({"message": result})

    @validate(
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        # /resolvegroup/user?rgId=<resolvegroup_id>
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="resolvegroup/user").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.UserHandler.get(): {0}, begin action".format(session.email))

        rgId = self.get_argument('rgId', None)
        resolvegroup_info = yield self._query_resolvegroup_info(
            {'id': {'$in': [rgId]}}, session=session)
        # 测试当resolvegroup为空时的结果
        users = resolvegroup_info[0].get('users') if resolvegroup_info else []

        user_ids = [user['uid'] for user in users]
        print(user_ids, "------------------------")
        users = yield self.filter_users(user_ids)
        users = [user for user in users if user.get('status') == "enabled"]

        logging.info("ResolveGroup.UserHandler.get() {0} action successfully".format(users))
        raise gen.Return({'data': users})


class ScheduleHandler(ResolveGroupUtilsHandler):

    @validate(
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        """
            获取没有关联排班的resolvegroup.
            /resolvegroup/schedule?flag=false
        """
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="resolvegroup/schedule").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.ScheduleHanlder.get(): {0}, begin action".format(session.email))

        flag = self.get_argument('flag', 'false')

        # 获取所有或者根据用户获取时，过滤掉状态为removed的
        if flag and flag.lower() == 'false':
            spec = {'status': 'active'}
            schedules = yield AppServiceGlobal.db.schedule.find(
                spec
            ).to_list(None)
            resolvegroupIds = [schedule['resolvegroup'] for schedule in schedules]

            doc = {
                'id': {'$nin': resolvegroupIds},
                'status': {'$ne': 'removed'}
            }
        else:
            doc = {'status': {'$ne': 'removed'}}

        resolvegroups = yield AppServiceGlobal.db.resolvegroup.find(
            doc,
            {
                '_id': False
            }
        ).to_list(None)

        returnData = resolvegroups if resolvegroups else []

        # logging.info("ResolveGroup.ResolveGroupHandler.get(): {0}, action success".format(session.email))
        raise gen.Return({"data": returnData})


class CtiHandler(ResolveGroupUtilsHandler):
    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def post(self):
        # endpoint: /resolvegroup/cti
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="resolvegroup/cti").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.CtiHandler.post(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        id = self._ipa_input_except(body, input_str="id")
        ctiId = self._ipa_input_except(body, input_str="ctiId")
        flag = self._ipa_input_except(body, input_str="flag")

        if not id or not ctiId:
            logging.debug('ResolveGroup.CtiHandler.post() empty...')
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        if isinstance(id, list) and isinstance(ctiId, list):
            logging.debug('ResolveGroup.UserHandler.post() param is error')
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")
        elif isinstance(id, list):
            res = yield self._resolveGroup_multi_to_one(id, ctiId, flag, field='ctis')
        elif isinstance(ctiId, list):
            res = yield self._resolveGroup_one_to_multi(id, ctiId, flag, field='ctis')
        else:
            res = yield self._resolveGroup_one_to_one(id, ctiId, flag, field='ctis')

        result = 'successfully' if 'failed' not in res else 'failed'

        raise gen.Return({"message": result})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        # /resolvegroup/cti?ctiId=<id>
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="resolvegroup/cti").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.CtiHandler.get(): {0}, begin action".format(session.email))

        ctiId = self.get_argument('ctiId', None)
        res = []

        if ctiId:
            # 根据该id查找所有cti
            doc = {
                'ctis': {
                    '$in': [ctiId]
                }
            }
            res = yield self._query_resolvegroup_info(doc, session=session)

        result = res if res else []
        raise gen.Return({'data': result})


class OrderHandler(ResolveGroupUtilsHandler):
    @validate(
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        # /resolvegroup/order?rgId=<resolvegroupId>
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="resolvegroup/order").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.OrderHandler.get(): {0}, begin action".format(session.email))

        rgId = self.get_argument('rgId', None)
        if not rgId:
            logging.info('ResolveGroup.OrderHandler.get() params err')
            raise gen.Return([])
        order_user = []
        # 找出schedule中当前值班人员
        current_time = datetime.utcnow()
        current_oncall_user = yield self._get_the_schedule(current_time, rgId)
        current_oncall_id = yield self._query_account(email=current_oncall_user)
        # 如果order表中没有存用户信息，则到resolvegroup中取默认用户信息列表
        # 找出resolvegroup中对应的成员列表
        # 如果今天有值班，则将其order权重置为０
        doc = {
            'id': rgId
        }
        resolvegroup_res = yield self._query_resolvegroup_info(doc, session=session)
        if not resolvegroup_res:
            logging.info('ResolveGroup.OrderHandler.get() resolvegroup params error')
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        user_list = resolvegroup_res[0].get('users')

        for user in user_list:
            if user.get('uid') == current_oncall_id[0].get('id'):
                user['order'] = 0
            order_user.append(user)
        order_user = sorted(order_user, key=lambda user: user.get('order'))
        logging.info('ResolveGroup.OrderHandler.get() {0} , action success'.format(session.email))
        raise gen.Return({'data': order_user})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "id": {"type": "string", "minLength": MIN_LENGTH},
                "uid": {"type": "string", "minLength": MIN_LENGTH},
                "order": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["id", "uid", "order"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def put(self):
        # endpoint: /resolvegroup/order
        AppServiceGlobal.REQUESTS.labels(method='PUT', endpoint="resolvegroup/order").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.OrderHandler.put(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        rg_id = self._ipa_input_except(body, input_str="id")
        uid = self._ipa_input_except(body, input_str="uid")
        order = self._ipa_input_except(body, input_str="order")
        spec = {'id': rg_id, 'users.uid': uid}
        setting = {'users.$.order': order}

        res = yield self._update_resolvegroup_user_order(spec, setting)
        if not res:
            logging.info('ResolveGroup.OrderHandler.get() resolvegroup params error')
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

        logging.info('ResolveGroup.OrderHandler.put() {0} , action success'.format(session.email))
        mess = 'successfully' if res else 'failed'
        raise gen.Return({'message': mess})


class SearchHandler(ResolveGroupUtilsHandler):
    '''
        搜索支持组功能
        可以根据用户的用户名，支持组id，支持组的模糊匹配搜索出对应的支持组
        @param keywork: string
    '''
    @web.asynchronous
    @validate(
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    def get(self):
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="resolvegroup/search").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("ResolveGroup.SearchHandler.get(): {0}, begin action".format(session.email))

        limit = self.get_argument("limit", None)
        page = self.get_argument("page", None)
        if limit and int(limit) > 1000:
            raise APIError(400, message="Page limit over max 1000")
        spec = {'status': {'$ne': 'removed'}}
        keyword = self.get_argument('keyword', None)
        if keyword:
            spec = {"$and": [
                {"$or": [{"id": {'$regex': keyword, '$options': 'i'}},
                         {"name": {'$regex': keyword, '$options': 'i'}},
                         {"description": {'$regex': keyword, '$options': 'i'}}
                         ]
                 },
                {'status': {'$ne': 'removed'}}
                ]
            }
        if limit and page:
            try:
                returnData = yield self._query_resolvegroup_info(
                    spec, limit=int(limit), page=int(page), session=session)
            except ValueError as err:
                logging.error("ResolveGroup.ResolveGroupHandler.get Exception {}".format(err))
                raise APIError(400, message="ValueError Invalid literal for int")
        else:
            returnData = yield self._query_resolvegroup_info(spec, session=session)

        allUserInfo = yield self._query_account(session=session)
        if keyword:
            total_spec = spec
        else:
            total_spec = {'status': {'$ne': 'removed'}}
        total_count = yield self._query_resolvegroup_totalcount(total_spec)
        for tmpData in returnData:
            tmpMasters = []
            tmpUsers = []
            for user in allUserInfo:
                for mId in tmpData.get("masters"):
                    if user.get(u"status") == u"enabled" and user.get(u"id") == mId.get("uid"):
                        tmpMasters.append(user)
                        break
                for uId in tmpData.get("users"):
                    if user.get(u"status") == u"enabled" and user.get(u"id") == uId.get("uid"):
                        tmpUsers.append(user)
                        break
            tmpData['masters'] = tmpMasters
            tmpData['users'] = tmpUsers

        logging.info("ResolveGroup.ResolveGroupHandler.get(): {0}, action success".format(session.email))
        raise gen.Return({"data": returnData, "count": len(returnData), "total_count": total_count})
