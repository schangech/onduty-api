#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2016-08-25 14:40:13
# @Updated Date: 2016-09-06 16:19:17

"""
Activity handler interfaces.
"""

import logging
import json
import copy
import jinja2
import pymongo
from tornado import gen
from datetime import datetime, timedelta
from bson import json_util

from Runtime.Constants import (
    MIN_LENGTH,
    DATE_FORMAT,
    CHINA_TIME_ZONE,
    SUPPORTED_ACTIVITY_ACTIONS,
    SUPPORTED_NOTIFY_TYPES)
from Forge.Common.State import (
    TICKET_STATE_OPENED)
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil


class ActivityException(Exception):
    """A simple exception class used for Activity exceptions"""
    pass


class Activity(object):

    @staticmethod
    def format(action, **kwargs):
        """
            Render activity description with input kwargs.
        """
        try:
            template = copy.deepcopy(Activity._data[action])

            env = jinja2.Environment()
            return env.from_string(template).render(**kwargs)
        except Exception as error:
            raise ActivityException(error)

    _data = {
        'trigger': """
Triggered through the API.
{{event_description}} (View Message({{event_url}})).
        """,

        'open': """
Opened ticket {{ticket_id}} by {{requester}}.
        """,

        'acknowledge': """
Acknowledged by {{responser}} during {{duration}} through the website.
        """,

        'unacknowledge': """
Unacknowledged due to timeout.
        """,

        'assign': """
Assigned from {{requester}} to {{responser}}.
        """,

        'notify': """
Notified {{responser}} via {{notify_type}} at {{notify_target}}.
        """,

        'note': """
Note added by {{responser}}.
Note: {{note}}
        """,

        'resolve': """
Resolved ticket {{ticket_id}} by {{responser}}.
        """,

        'close': """
Closed ticket {{ticket_id}} by {{requester}}.
        """
    }


class ActivityHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /activity/
    """

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "ticket_id": {"type": "string", "minLength": MIN_LENGTH},
                "action": {"type": "string", "enum": SUPPORTED_ACTIVITY_ACTIONS},
                "detail": {
                    "type": "object",
                    "properties": {
                        "event_description": {"type": "string", "minLength": MIN_LENGTH},
                        "event_url": {"type": "string", "minLength": MIN_LENGTH},
                        "requester": {"format": "email"},
                        "responser": {"format": "email"},
                        "notify_type": {"type": "string", "minLength": MIN_LENGTH},
                        "notify_target": {"format": "email"},
                        "note": {"type": "string", "minLength": MIN_LENGTH},
                        "files": {"type": "array"}
                    }
                }
            },
            "required": ["ticket_id", "action", "detail"]
        },
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def post(self):
        """
            Add a new activity.
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Activity.ActivityHandler.post(): {0}, begin action".format(session.email))

        # check ticket
        ticket_id = self.body.get('ticket_id')
        ticket = yield AppServiceGlobal.db.ticket.find_one(
            {
                'id': ticket_id
            }
        )
        if not ticket:
            logging.error("Activity.ActivityHandler.post(): {0}, invalid input ticket id {1}".format(session.email, ticket_id))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

        detail = self.body.get('detail')
        detail['action'] = self.body.get('action')
        details = [detail]
        activity = yield self._create_rc_activity(session.email, ticket_id, details)
        if not activity:
            logging.error("Activity.ActivityHandler.post(): {0}, add new activity for ticket {1} failed".format(session.email, ticket_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        logging.info("Activity.ActivityHandler.post(): {0}, action success".format(session.email))
        raise gen.Return({"activity": activity})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def get(self):
        """
            uri:
                /activity/?ticketId=<ticket_id>&responser=<responser>&from=<started_time_string>&to=<ended_time_string>
            List all activities of special ticket.
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Activity.ActivityHandler.get(): {0}, begin action".format(session.email))

        spec = {}
        ticket_ids = self.get_arguments('ticketId')
        if ticket_ids:
            spec['ticket_id'] = {
                '$in': ticket_ids
            }
        responsers = self.get_arguments('responsers')
        if responsers:
            spec['responser'] = {
                '$in': responsers
            }
        started_time_string = self.get_argument('from', None)
        if started_time_string:
            spec['created_time'] = {
                '$gte': datetime.strptime(started_time_string, DATE_FORMAT) - timedelta(hours=CHINA_TIME_ZONE)
            }
        ended_time_string = self.get_argument('to', None)
        if ended_time_string:
            if 'created_time' not in spec:
                spec['created_time'] = {}
            spec['created_time'].update(
                {'$le': datetime.strptime(ended_time_string, DATE_FORMAT) - timedelta(hours=CHINA_TIME_ZONE)}
            )

        activities = []
        if spec:
            activities = yield AppServiceGlobal.db.activity.find(
                spec,
                {
                    '_id': False
                },
                sort=[('created_time', pymongo.DESCENDING)]
            ).to_list(None)

            activities = json.loads(json_util.dumps(activities, default=json_util.default))

        logging.info("Activity.ActivityHandler.get(): {0}, action success".format(session.email))
        raise gen.Return({"data": activities})
