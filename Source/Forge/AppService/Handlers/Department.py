#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
@File: /home/sendal/workspace/Onduty-API/src/Source/Forge/AppService/IPA/Department.py
@Time: Mar 8, 2017
@Desc:
@Author: sendal
@Email: <schangech@gmail.com>
@Version: 0.0.1
'''

import logging
import json
from tornado import gen

from Runtime.Constants import (MIN_LENGTH, LOGIN_SHELL)
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Result import Result
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil
from Forge.Common.Utils import ignored


class DepartmentHandler(APIHandler, AppServiceUtil):
    """docstring for GroupHandler"""

    @validate(
        output_schema={
            "type": "object",
            "properties": {
                "message": {"type": "string"}
            }
        }
    )
    @coroutine
    def get(self):
        limit = self.get_argument('limit', '')
        page = self.get_argument('page', '')
        depid = self.get_argument('id', '')
        q = self.get_argument("q", "")

        departments = []

        with ignored(Exception):

            if depid:
                url = '/department/{}'.format(depid)
            elif q:
                url = '/department?q={}'.format(q)
            else:
                url = '/department'

            print("-------------------", url)
            resp = yield self._request_metadata_api(method='GET', key=url)
            departments = json.loads(resp.body)

        logging.info("Department.DepartmentHandler.get(), action success")
        raise gen.Return(departments)


class UsersHandler(APIHandler, AppServiceUtil):

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def get(self):
        session = self.check_authorization(
            AppServiceGlobal.session_manager, self.request)
        logging.info(
            "Department.UsersHandler.get(): {0}, begin action".format(session.email))

        departments = []
        with ignored(Exception):
            filters = '?limit={0}&page={1}'.format(0, 0)
            resp = yield self._request_metadata_api(
                method='GET', key='/department', filters=filters)
            departments = eval(resp.json().get('data'))

        for depId in departments:
            users = []
            with ignored(Exception):
                filters = '?limit={0}&page={1}&q={2}'.format(
                    0, 0, depId.get('id'))
                resp = yield self._request_metadata_api(
                    method='GET', key='/user', filters=filters)
                users = eval(resp.json().get('data'))
            depId['users'] = users

        logging.info(
            "Department.DepartmentHandler.get(): {0}, action success".format(
                session.email))
        raise gen.Return({"data": departments})
