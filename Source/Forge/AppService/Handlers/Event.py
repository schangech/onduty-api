#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2016-08-17 10:01:53
# @Updated Date: 2016-09-21 10:36:56

"""
OnDuty agent event handler.
"""

import re
import time
import logging
import json
import pymongo
from tornado import gen
from datetime import datetime, timedelta
from bson import json_util

from Runtime.Constants import (
    MIN_LENGTH,
    MINUTES_PER_HOUR,
    DATE_FORMAT,
    TIME_FORMAT,
    DEFAULT_LIMIT_NUMBER,
    SUPPORTED_AGENTS,
    SUPPORTED_EVENTS,
    SUPPORTED_SEVERITIES,
    CHINA_TIME_ZONE,
    AUTO_TICKET_TRIGGER,
    DEFAULT_SUPPORT_DEPARTMENT,
    DEFAULT_SUPPORT_USER)
from Forge.Common.State import (
    AGENT_STATE_ACTIVE,
    TICKET_STATE_OPENED,
    TICKET_STATE_DONE)
from Forge.Common.Ops import OPS_ACTIVITY_RESOLVE
from Forge.Common.Utils import parse_department, ignored
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil


class EventHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /event/
    """

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def post(self):
        """
            Handler agent event data.
        """
        body = json.loads(self.request.body)
        body = self.lower_keys(body)
        # action_id = body.get('action_id').strip().encode('utf-8')
        agent_id = body.get('agent').get('agent_id').strip().encode('utf-8')
        service_key = body.get('service_key').strip().encode('utf-8')
        description = body.get('description', '').strip().encode('utf-8')
        event_type = body.get('event_type').strip().encode('utf-8')
        event_id = body.get('details').get('event_id').strip().encode('utf-8')
        status = body.get('details').get('status').strip().lower().encode('utf-8')
        endpoint = body.get('details').get('endpoint', '').strip().lower().encode('utf-8')
        severity = body.get('details').get('severity', '').strip().lower().encode('utf-8')
        priority = body.get('details').get('priority', 5)
        logging.info("Event.EventHandler.post(): {0}/{1}, begin to handler event {2} | {3}".format(agent_id, service_key, event_id, status))

        # agent heartbeat threshold is one hour
        # agent_heartbeat_threshold = datetime.utcnow() - timedelta(minutes=MINUTES_PER_HOUR)
        # check agent status
        agent = yield AppServiceGlobal.db.agent.find_one(
            {
                'id': agent_id,
                'status': AGENT_STATE_ACTIVE
            },
            {
                '_id': False
            }
        )
        if not agent:
            logging.error("Event.EventHandler.post(): Invalid agent {0}".format(agent_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        logging.info("Event.EventHandler.post(): {0} agent is active".format(agent_id))

        # check service key
        spec = {
            'key': service_key,
            'agents': agent_id
        }
        service_type = body.get('agent').get('queued_by').strip().encode('utf-8')
        if service_type:
            service_type = service_type[service_type.find('-')+1:]
            spec['type'] = service_type
        service = yield AppServiceGlobal.db.service.find_one(
            spec
        )
        if not service:
            logging.error("Event.EventHandler.post(): {0}, invalid service key {1}".format(agent_id, service_key))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

        # save and check data format
        event = {
            'agent_id': agent_id,
            'service_key': service_key,
            'description': description,
            'event_type': event_type,
            'endpoint': endpoint or body.get('details').get('hostname', ''),
            'report_by': service_type if service_type else '',
            'report_at': datetime.strptime(body.get('agent').get('queued_at').strip().encode('utf-8'), DATE_FORMAT),
            'details': body.get('details'),
        }
        # mapping severity
        if 'zabbix' in service_type.lower():
            for k, v in SUPPORTED_SEVERITIES.iteritems():
                if severity.lower() in v.get('severity', []):
                    event['details']['level'] = k
        else:
            # logging.debug('Event.EventHandler.post() {0} {1}'.format(service_type, event))
            event['details']['level'] = priority

        res = yield AppServiceGlobal.db.event.update(
            {
                'agent_id': agent_id,
                'service_key': service_key,
                'description': description,
                'event_type': event_type,
                'endpoint': endpoint,
                'report_by': event['report_by'],
                'details.event_id': event_id,
                'details.status': body['details']['status'],
            },
            {
                '$set': event,
                '$inc': {
                    'count': 1
                }
            },
            upsert=True,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("Event.EventHandler.post(): {0}, save event failed".format(event_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        # check rule engine
#         hostname = event['endpoint'] or event['detail'].get('hostname')
#         description = description
#         # 根据hostname, decscription 查询权重分布，列出前面十个
#         # yield self.get_cti_resolvegroup_oncall_user(hostname, description)
#         # yield self.endpoint_to_cti_ratio(hostname)

        # create ticket if new trigger event
        # 检查ticket中的状态，看是否被手动标记为已经解决，是，则新建工单，否则进行下面操作
        tickets = yield AppServiceGlobal.db.ticket.find(
            {
                'details.event_id': event_id,
                'description': description,
                'type': 'bug'
            },
            {
                '_id': False
            }
        ).to_list(None)
        if ((tickets and TICKET_STATE_OPENED not in [t.get('status') for t in tickets]) or not res['updatedExisting']) and event_type == 'trigger':
#            # 通过模糊匹配查找cti和resolvegroup和onduty人员
#            hostname = event['endpoint'] or event['detail'].get('hostname')
#            description = description
#            # 根据hostname, decscription 查询权重分布，列出前面十个
#            (ctiId, resolvegroup_id) = yield self.get_cti_resolvegroup_oncall_user(hostname, description)
            # 通过falcon传过来的action_id，查询表中对应的cti_id和resolvegroup_id
#             params = "id = {0}".format(action_id)
#             sql = "SELECT cti_id, resolvegroup_id FROM action where %s" % params
#             res = yield AppServiceGlobal.mysql_db.execute(sql)
#             ctiId = res.get('cti_id')
#             resolvegroup_id = res.get('resolvegroup_id')
            # ----------------2017-2-10----------------------
            hostname = event['endpoint'] or event['detail'].get('hostname')
            depart = parse_department(hostname.strip()) if hostname else None
            department = depart[1] if depart else None
            department = department or DEFAULT_SUPPORT_DEPARTMENT
            logging.debug("Event.EventHandler.post() department {0}".format(department))
            ctiDoc = {'alias': department}
            ctiId = yield self._query_cti_by_doc(ctiDoc)
            current_time = datetime.utcnow()

            doc = {
                'name': re.compile(department, re.IGNORECASE)
            }
            onduty_user = None
            with ignored(Exception):
                resolveGroupInfo = yield self._query_resolvegroup_info(doc)
                onduty_user = yield self._get_the_schedule(current_time, resolveGroupInfo[0].get('id'))
            onduty_user = onduty_user or DEFAULT_SUPPORT_USER

            ticket = {
                'type': 'bug',
                'severity': event['details'].get('level', 3),
                'description': event['description'],
                'requester': AUTO_TICKET_TRIGGER,
                'resolvegroupId': resolveGroupInfo[0].get('id') if len(resolveGroupInfo) > 0 else None,
                'responser': onduty_user,
                'details': event.get('details', {}),
                'ctiId': ctiId[0].get('id'),
                'status': TICKET_STATE_OPENED,
                'cc_list': [onduty_user],
                'updated_time': current_time,
                'created_time': current_time
            }
            ticket_id = yield self._create_rc_ticket(onduty_user, ticket)
            if not ticket_id:
                logging.error("Event.EventHandler.post(): auto create ticket failed")
                raise gen.Return({"message": "Handlered event {0} but failed to create ticket".format(event_id)})

            # create duty task
            data = {
                'job_id': ticket_id
            }
            response = yield self._scheduler(method='POST', params=data)
            if not response:
                logging.error("Event.EventHandler.post(): create duty task for ticket {0} failed".format(ticket_id))

        elif event_type == 'resolve':   # update ticket status when resolve event
            res = yield AppServiceGlobal.db.ticket.update(
                {
                    'details.event_id': event_id
                },
                {
                    '$set': {
                        'status': TICKET_STATE_DONE
                    }
                },
                w=AppServiceGlobal.config.mongo_db_copy
            )
            if not res:
                logging.error("Event.EventHandler.post(): {0}, update ticket status failed and please do it again manually".format(event_id))
            else:
                logging.info("Event.EventHandler.post(): {0}, update ticket status successlly".format(event_id))

                # add activity
                ticket = yield AppServiceGlobal.db.ticket.find_one(
                    {
                        'details.event_id': event_id
                    },
                    {
                        '_id': False,
                        'id': True
                    }
                )
                if not ticket:
                    logging.error("Event.EventHandler.post(): {0}, can't find event related ticket".format(event_id))
                else:
                    details = [
                        {
                            'action': OPS_ACTIVITY_RESOLVE,
                            'event_id': event_id
                        }
                    ]
                    activity = yield self._create_rc_activity('event-handler', ticket['id'], details)
                    if not activity:
                        logging.error("Event.EventHandler.post(): {0}, add resolved activity for ticket {1} failed".format(event_id, ticket['id']))

        logging.info("Event.EventHandler.post(): {0}, action success".format(event_id))
        raise gen.Return({"message": "Handlered event {0}".format(event_id)})

    @validate(
        output_schema={
            "type": "object",
            "properties": {
                "data": {"type": "array"}
            }
        }
    )
    @coroutine
    def get(self):
        """
            uri:
                /event/?agentId=<agent_id>&agentType=<agent_type>&eventType=<event_type>&eventId=<event_id>&severity=<severity>&page=page&limit=limit&from=<started_time_string>&to=<ended_time_string>
            get special event list.
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Event.EventHandler.get(): {0}, begin action".format(session.email))

        spec = {}

        agentId = self.get_argument('agentId', None)
        if agentId and isinstance(agentId, basestring):
            spec['agent_id'] = agentId
        agentType = self.get_argument('agentType', None)
        if agentType and isinstance(agentType, basestring):
            if agentType in SUPPORTED_AGENTS:
                spec['report_by'] = agentType
            else:
                logging.warning("Event.EventHandler.get(): {0}, invalid input agent type {1}".format(session.email, agentType))
        eventType = self.get_argument('eventType', None)
        if eventType and isinstance(eventType, basestring):
            if eventType in SUPPORTED_EVENTS:
                spec['event_type'] = eventType
            else:
                logging.warning("Event.EventHandler.get(): {0}, invalid input event type {1}".format(session.email, eventType))
        eventId = self.get_arguments('eventId')
        if eventId and isinstance(eventId, list):
            spec['details.event_id'] = {
                '$in': eventId
            }
        severity = self.get_arguments('severity')
        if severity and isinstance(severity, list):
            severity = [int(i) for i in severity]
            spec['details.level'] = {'$in': severity}  # {'$regex': '^{0}$'.format(severity), '$options': 'i'}
        started_time_string = self.get_argument('from', None)
        if started_time_string:
            spec['report_at'] = {
                '$gte': datetime.strptime(started_time_string, DATE_FORMAT) - timedelta(hours=CHINA_TIME_ZONE)
            }
        ended_time_string = self.get_argument('to', None)
        if ended_time_string:
            if 'report_at' not in spec:
                spec['report_at'] = {}
            spec['report_at'].update(
                {'$le': datetime.strptime(ended_time_string, DATE_FORMAT) - timedelta(hours=CHINA_TIME_ZONE)}
            )
        page = self.get_argument('page', 1)
        page = int(page) if page else 1
        if page < 0:
            logging.warning("Event.EventHandler.get(): {0}, invalid input page number {1}".format(session.email, page))
            page = 1
        limit = self.get_argument('limit', DEFAULT_LIMIT_NUMBER)
        limit = int(limit) if limit else DEFAULT_LIMIT_NUMBER
        if limit < 0:
            logging.warning("Event.EventHandler.get(): {0}, not or invalid input limit number {1}".format(session.email, limit))
            limit = DEFAULT_LIMIT_NUMBER

        count = yield AppServiceGlobal.db.event.find(
            spec
        ).count()

        events = yield AppServiceGlobal.db.event.find(
            spec,
            {
                '_id': False
            },
            skip=(page-1)*limit,
            limit=limit,
            sort=[('report_at', pymongo.DESCENDING)]
        ).to_list(None)
        events = json.loads(json_util.dumps(events, default=json_util.default))

        logging.info("Event.EventHandler.get(): {0}, action success".format(session.email))
        raise gen.Return({"data": events, "total": count})

    def lower_keys(self, x):
        if isinstance(x, list):
            return [self.lower_keys(v) for v in x]
        elif isinstance(x, dict):
            return dict((k.lower(), self.lower_keys(v)) for k, v in x.iteritems())
        else:
            return x


class StatisticHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /event/statistic/
    """

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def get(self):
        """
            uri:
                /event/statistic/?isResolved=<true|false>&agentId=<agent_id>&eventType=<event_type>&reportBy=<report_by>&from=<started_time_string>&to=<ended_time_string>
            get event statistic result;
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Event.StatisticHandler.get(): {0}, begin action".format(session.email))

        is_resolved = self.get_argument('isResolved', False)
        if is_resolved and isinstance(is_resolved, basestring):
            is_resolved = is_resolved.lower()
            if is_resolved in ['true', 'false']:
                is_resolved = True if is_resolved == 'true' else False
            else:
                logging.warning("Event.StatisticHandler.get(): {0}, invalid input is_resolved param {1}".format(session.email, is_resolved))
                is_resolved = False

        params = {}
        agent_id = self.get_arguments('agentId')
        if agent_id:
            params['agent_id'] = agent_id
        event_type = self.get_arguments('eventType')
        if event_type:
            params['event_type'] = event_type
        report_by = self.get_arguments('reportBy')
        if report_by:
            params['report_by'] = report_by
        from_time_string = self.get_argument('from', None)
        if from_time_string:
            params['from'] = datetime.strptime(from_time_string, DATE_FORMAT) - timedelta(hours=CHINA_TIME_ZONE)
        to_time_string = self.get_argument('to', None)
        if to_time_string:
            params['to'] = datetime.strptime(to_time_string, DATE_FORMAT) - timedelta(hours=CHINA_TIME_ZONE)

        # package pipeline
        pipeline = []
        match = self.__condition(**params)
        if match:
            pipeline.append({"$match": match})

        group = {
            '$group': {
                '_id': {
                    'event_id': '$details.event_id'
                },
                'event_type': {
                    '$addToSet': '$event_type'
                }
            }
        }
        pipeline.append(group)

        logging.debug("Event.StatisticHandler.get(): {0}, pipeline \n{1}".format(session.email, json.dumps(pipeline)))

        try:
            result = {
                'data': []
            }
            cursor = AppServiceGlobal.db.event.aggregate(pipeline)
            while (yield cursor.fetch_next):
                doc = cursor.next_object()
                if is_resolved and 'resolve' in doc['event_type']:
                    result['data'].append(doc['_id']['event_id'])
                if not is_resolved and 'resolve' not in doc['event_type']:
                    result['data'].append(doc['_id']['event_id'])
        except Exception as error:
            logging.error("Event.EventHandler.get(): {0}, execute event statistic exception, \n{1}".format(session.email, str(error)))
            raise APIError(400, flag='APS.HANDLER.EVENT.BAD_REQUEST')

        logging.debug("Event.EventHandler.get(): {0}, result number: {1}".format(session.email, len(result['data'])))
        logging.info("Event.EventHandler.get(): {0}, action success".format(session.email))
        raise gen.Return(result)

    def __condition(self, **kwargs):
        spec = {}

        if 'agent_id' in kwargs and kwargs['agent_id']:
            spec['agent_id'] = {
                '$in': kwargs['agent_id']
            }
        if 'event_type' in kwargs and kwargs['event_type']:
            spec['event_type'] = {
                '$in': kwargs['event_type']
            }
        if 'report_by' in kwargs and kwargs['report_by']:
            spec['report_by'] = {
                '$in': kwargs['report_by']
            }
        if 'from' in kwargs and kwargs['from']:
            spec['report_at'] = {
                '$gte': kwargs['from']
            }
        if 'to' in kwargs and kwargs['to']:
            if 'report_at' not in spec:
                spec['report_at'] = {
                    '$lte': kwargs['to']
                }
            else:
                spec['report_at']['$lte'] = kwargs['to']

        return spec
