#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
@Class:
@Date: 2017-09-25
@Module: Source.Forge.AppService.Handlers.app.Token
@Author: sendal <schangech@gmail.com>
@Version:
'''

import uuid
import logging
import json
from tornado import gen
from datetime import datetime
from Runtime.Constants import MIN_LENGTH
from Forge.Common.Utils import is_email, validate_base64, ignored
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


class TokenHandler(APIHandler, AppServiceUtil):
    # 针对第三方应用调用onduty使用的校验token

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "client_id": {"type": "string", "minLength": MIN_LENGTH},
                "grant_type": {"type": "string", "minLength": MIN_LENGTH},
                "client_secret": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["client_id", "grant_type", "client_secret"]
        },
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def post(self):
        logging.info("Token.TokenHandler.post() action start.")
        body = json.loads(self.request.body)
        # tid = str(uuid.uuid4())
        client_id = self._ipa_input_except(body, input_str="client_id")
        grant_type = self._ipa_input_except(body, input_str="grant_type")
        client_secret = self._ipa_input_except(body, input_str="client_secret")

        #
        appId = yield self._check_app(
            client_id=client_id, grant_type=grant_type, client_secret=client_secret)
        if not appId:
            logging.error("Token.TokenHandler.post() client app not found")
            raise APIError(404, message="404 not found client")

        # 检查code是否存在，如果存在，则更新，不存在，则新建
        isExist = yield self._check_app_access(app_id=appId)
        if isExist:
            code = yield self._update_app_access(
                app_id=appId, code=self.general_string())
        else:
            code = yield self._create_app_access(
                app_id=appId, grant_type=grant_type, code=self.general_string())

        logging.info("Token.TokenHandler.post() action succeeded.")
        raise gen.Return(code)

    def general_string(self):
        # 生成一串字符串
        import random
        import string
        salt = ''.join(random.sample(string.ascii_letters + string.digits, 16))
        return salt
