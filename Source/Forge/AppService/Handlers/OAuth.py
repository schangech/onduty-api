#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
@Class:
@Date: 2017-08-23
@Module: Handlers.OAuth
@Author: sendal <schangech@gmail.com>
@Version:
'''

import time
import requests
import logging
import json
import jsonschema
from tornado import gen

from Runtime.Constants import (
    MIN_LENGTH, LOGIN_SHELL)
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Result import Result
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil
from Forge.Common.Utils import ignored
from Forge.Common.Session import Session


class OAuthHandler(APIHandler, AppServiceUtil):

    @validate(
        output_schema={
            "type": "object"
        },
    )
    @coroutine
    def get(self):
        logging.info("OAuth.OAuthHandler.get() action start")

        auth_body = {
            "client_id": AppServiceGlobal.config.md_client_id,
            "oauth_url": AppServiceGlobal.config.md_oauth_auth,
            "response_type": "code",
            "state": "login"
        }

        logging.info("OAuth.OAuthHandler.get() action succeeded")
        raise gen.Return({"data": auth_body})


class CodeHandler(APIHandler, AppServiceUtil):

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def get(self):

        logging.info("OAuth.OAuthHandler.code() action start")

        code = self.get_argument("code", "")
        url = "{}?client_id={}&grant_type=authorization_code&code={}&&client_secret={}".format(
            AppServiceGlobal.config.md_oauth_token,
            AppServiceGlobal.config.md_client_id,
            code,
            AppServiceGlobal.config.md_secret)

        try:
            # 获取到token, refresh_token
            token_resp = yield self._get(url=url)
            if token_resp.code != 200:
                raise APIError(400, message="ms code expired")

            token = json.loads(token_resp.body)
            # 记录用户登录信息到session中
            headers = {
                "Authorization": "{} {}".format(token.get("token_type"), token.get("access_token"))
            }

            userinfo = yield self._request_metadata_api(
                method='GET',
                key="/userinfo",
                headers=headers)

            user = json.loads(userinfo.body).get("data")
        except Exception as err:
            logging.error("OAuth.OAuthHandler.code() oauth exception {}".format(err))
            raise APIError(400, message="OAuth server exception")

        try:
            resp = yield self._request_metadata_api(method='GET', key="/department", headers=headers)
            departments = json.loads(resp.body)
            (dp_leader, dep_name) = self.find_department_leader(user.get("department"),
                                                                departments.get("data"))
        except Exception as err:
            logging.error("OAuth.OAuthHandler.code() Find department Error {}".format(err))
            raise APIError(400, message="Find department error")

        session = Session(
            username=user.get('username'),
            email=user.get('email'),
            superior=dp_leader,
            telephone=user.get('telephone'),
            # 现在用的是部门名字，因为前段是根据名字来做权限划分
            department=dep_name,
            uid=user.get('id'),
            access_token=token.get("access_token"),
            refresh_token=token.get("refresh_token"),
            expires_at=token.get("expires_in") + int(time.time()),
            token_type=token.get("token_type")
        )

        session_id = self._create_session(session)
        logging.debug('OAuth.OAuthHandler.code() create session {0}'.format(session_id))

        result = {
            'username': session.username,
            'session': session.id,
            'email': session.email,
            'superior': session.superior,
            'telephone': session.telephone or '',
            'department': session.department,
            'uid': session.uid or ''
        }

        logging.info("OAuth.OAuthHandler.code() action succeeded")
        raise gen.Return({"data": result})

    def find_department_leader(self, departmentName, departments, leader=None, name=None):
        '''
        @param departmentName: department name/id
        @param departments: all department
        '''
        if not isinstance(departments, list) or not departments:
            return (False, False)
        for dep in departments:
            if dep.get("name") == departmentName or dep.get("id") == departmentName:
                leader = dep.get("leader")
                name = dep.get("name")
                break
        return (leader, name)
