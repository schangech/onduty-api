#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2016-08-28 11:05:02
# @Updated Date: 2017-02-17 14:57:33

"""
Schedule management interfaces.
"""

import logging
import json
import jsonschema
import uuid
import copy
from tornado import gen
from datetime import datetime, timedelta, time
from dateutil.relativedelta import relativedelta
from bson import json_util
from tornado_swagger import swagger

from Runtime.Constants import (
    MIN_LENGTH,
    SUPPORTED_SCHEDULERS,
    DATE_FORMAT)
from Forge.Common.State import (
    SCHEDULE_STATE_ACTIVE,
    SCHEDULE_STATE_REMOVED)
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil


class ScheduleHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /schedule/
    """

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "since": {"format": "date-time"},
                "resolvegroup": {"type": "string", "minLength": MIN_LENGTH},
                "users": {
                    "type": "array",
                    "items": {
                        "format": "email"
                    }
                },
                "rotation_type": {
                    "type": "string",
                    "enum": SUPPORTED_SCHEDULERS
                },
                "handoff_day": {
                    "type": "number",
                    "enum": [0, 1, 2, 3, 4, 5, 6]
                },
                "handoff_time": {"type": "array"},
                "backups": {
                    "type": "array",
                    "items": {
                        "format": "email"
                    }
                },
                "is_preview": {"type": "boolean"}
            },
            "required": ["since", "resolvegroup", "users", "rotation_type", "handoff_time", "is_preview"]
        },
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @coroutine
    @swagger.operation(nickname='create_schedule')
    def post(self):
        """
            @description: Add a new schedule.
            @notes:

            @param since: since
            @type since: string

            @param resolvegroup: resolvegroupId
            @type resolvegroup: string

            @param users: user email
            @type users: list

            @param rotation_type: ['weekly', 'daily']
            @type rotation_type: enumerate

            @param handoff_time: [8, 30, 0]
            @type handoff_time: list

            @param is_preview: is_preview
            @type is_preview: boolean

            @param handoff_day: [0, 1, 2, 3, 4, 5, 6]
            @type handoff_day: num

            @return 200: {}
            @raise 400: invalid input
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Schedule.ScheduleHandler.post(): {0}, begin action".format(session.email))

        handoff_day = self.body.get('handoff_day')
        users = self.body.get('users') or None
        rotation_type = self.body.get('rotation_type').strip().lower().encode('utf-8')
        ht_list = self.body.get('handoff_time', None)
        schedule = {
            'id': 'sched-{0}'.format(str(uuid.uuid4())[:8]),
            'start': datetime.strptime(self.body.get('since'), DATE_FORMAT),
            'users': users,
            'rotation_type': rotation_type,
            'handoff_day': handoff_day,
            'handoff_time': ht_list,
            'entries': [],
            'status': SCHEDULE_STATE_ACTIVE,
            'backups': self.body.get('backups', [])
        }
        # check resolve group and users
        rg_id = self.body.get('resolvegroup')
        rg = yield AppServiceGlobal.db.resolvegroup.find_one(
            {
                'id': rg_id
            }
        )
        if not rg or not rg.get('users', []):
            logging.warning("Schedule.ScheduleHandler.post(): {0}, invalid resolvegroup {1}".format(session.email, rg_id))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

        is_preview = self.body.get('is_preview', True)
        # 检查当前resolvegroup是否已经创建过排班了，如果已经创建过了，则禁止再创建排班，但是可以预览
        # 应该查找状态是active的排班，因为删除排班时没有真正的从数据库中删除，而是把状态设为不可用
        has_schedule = yield AppServiceGlobal.db.schedule.find_one(
            {
                'resolvegroup': rg_id,
                'status': SCHEDULE_STATE_ACTIVE
            }
        )
        if not is_preview and has_schedule:
            logging.info('Resolvegroup: {0} has already got a schedule!'.format(rg_id))
            raise APIError(400, rg.get('name'), flag="APS.HANDLER.RESOLVEGROUP.RGSCHEDU")

        schedule['resolvegroup'] = rg_id

        # check start/end timepoint
        schedule['start'] = max(schedule['start'], datetime.utcnow())
        logging.info('starting time: {0}'.format(schedule['start']))

        # 获取预览
        from_dt = self.body.get("from")
        to_dt = self.body.get("to")
        start_time = datetime.strptime(from_dt, DATE_FORMAT) if from_dt else datetime.utcnow()
        to_time = datetime.strptime(to_dt, DATE_FORMAT) if to_dt else datetime.utcnow() + relativedelta(months=3)
        # 结束预览

        # expand the schedule entries
        schedule['entries'] = self._expand_schedule(schedule['users'],
                                                    schedule['rotation_type'],
                                                    max(start_time, schedule['start'], datetime.utcnow()),
                                                    to_time,
                                                    handoff_day=schedule['handoff_day'],
                                                    handoff_time=schedule['handoff_time']
                                                    )
        if not is_preview:
            logging.info("Schedule.ScheduleHandler.post(): {0}, save schedule {1}".format(session.email, schedule['id']))

            res = yield AppServiceGlobal.db.schedule.insert(schedule)
            if not res:
                logging.error("Schedule.ScheduleHandler.post(): {0}, add new schedule {0} failed".format(session.email, schedule['id']))
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        else:
            schedule.pop('id')

        userInfo = yield self._query_account(email=schedule['users'], session=session)
        allUserInfo = [user for user in userInfo if user.get("status") == "enabled"]
        schedule['all_users'] = allUserInfo

        schedule = json.loads(json_util.dumps(schedule, default=json_util.default))
        logging.info("Schedule.ScheduleHandler.post(): {0}, action success, {1}".format(session.email, schedule['start']))
        raise gen.Return(schedule)

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "schedule_id": {"type": "string", "minLength": MIN_LENGTH},
                "since": {"format": "date-time"},
                "from": {"format": "date-time"},
                "to": {"format": "date-time"},
                "handoff_day": {
                    "type": "number",
                    "enum": [0, 1, 2, 3, 4, 5, 6]
                },
                "handoff_time": {"type": "array"},
                "users": {
                    "type": "array",
                    "items": {
                        "format": "email"
                    }
                },
                "backups": {
                    "type": "array",
                    "items": {
                        "format": "email"
                    }
                },
                "rotation_type": {"type": "string", "enum": SUPPORTED_SCHEDULERS},
                "is_preview": {"type": "boolean"}
            },
            "required": ["schedule_id", "users", "handoff_day", "handoff_time", "rotation_type"]
        },
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @coroutine
    @swagger.operation(nickname='put_schedule')
    def put(self):
        """
            @description: Update the special schedule.
            @notes:

            @param schedule_id: schedule_id
            @type schedule_id: string

            @param since: since
            @type since: date-time

            @param resolvegroup: resolvegroupId
            @type resolvegroup: string

            @param users: user email
            @type users: list

            @param rotation_type: ['weekly', 'daily']
            @type rotation_type: string

            @param handoff_time: [8, 30, 0]
            @type handoff_time: list

            @param is_preview: is_preview
            @type is_preview: boolean

            @param from: from
            @type from: date-time

            @param to: to
            @type to: date-time

            @param handoff_day: [0, 1, 2, 3, 4, 5, 6]
            @type handoff_day: num

            @return 200: {}
            @raise 400: invalid input
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Schedule.ScheduleHandler.put(): {0}, begin action".format(session.email))

        sched_id = self.body.get("schedule_id")
        schedule = yield AppServiceGlobal.db.schedule.find_one(
            {
                'id': sched_id,
                'status': SCHEDULE_STATE_ACTIVE
            },
            {
                '_id': False
            }
        )
        if not schedule:
            logging.error("Schedule.ScheduleHandler.put(): {0}, invalid input schedule id {1}".format(session.email, sched_id))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

        # 保存排班history到数据库
        full_schedule = yield self._get_full_schedule(
            spec={'id': sched_id}, start_time=schedule['start'], end_time=datetime.utcnow())
        history_entry = full_schedule[0]['entries']

        # update schedule
        setting = {}

        handoff_day = self.body.get('handoff_day', None)
        setting['handoff_day'] = handoff_day if handoff_day is not None else schedule.get('handoff_day')
        ht_list = self.body.get('handoff_time', None)

        setting['handoff_time'] = ht_list

        # 计算开始时间
        start_tp_string = self.body.get('since', None)
        if start_tp_string is not None:
            setting['start'] = datetime.strptime(start_tp_string, DATE_FORMAT)
            logging.info('starting time: {0}'.format(setting['start']))

        # set users
        users = self.body.get('users', None)

        if users:
            setting['users'] = users
        else:
            setting['users'] = schedule['users']

        # set rotation type
        rotation_type = self.body.get('rotation_type')
        if rotation_type:
            setting['rotation_type'] = rotation_type

        # set backups
        backups = self.body.get('backups', None)
        if backups:
            setting['backups'] = backups

        # 获取预览
        from_dt = self.body.get("from")
        to_dt = self.body.get("to")
        start_time = datetime.strptime(from_dt, DATE_FORMAT) if from_dt else datetime.utcnow()
        to_time = datetime.strptime(to_dt, DATE_FORMAT) if to_dt else datetime.utcnow() + relativedelta(months=3)

        new_entries = self._expand_schedule(users, rotation_type, start_time, to_time, handoff_day, ht_list)
        schedule['entries'] = new_entries

        is_preview = self.body.get('is_preview', True)
        if not is_preview:
            logging.info("Schedule.ScheduleHandler.put(): {0}, update schedule {1}".format(session.email, sched_id))

            # save history
            if history_entry and isinstance(history_entry, list):
                res = yield AppServiceGlobal.db.history.update(
                    {'id': sched_id},
                    {'$set': {'entries': history_entry}},
                    upsert=True,
                    w=AppServiceGlobal.config.mongo_db_copy
                )
                if not res:
                    logging.error(
                        "Schedule.ScheduleHandler.put(): {0}, save history error {1}".format(session.email, sched_id))
                    raise APIError(400, flag="COMMON.GLOBAL.ERROR_DB")

            res = yield AppServiceGlobal.db.schedule.update(
                {
                    'id': sched_id
                },
                {
                    '$set': setting
                },
                w=AppServiceGlobal.config.mongo_db_copy
            )
            if not res:
                logging.error("Schedule.ScheduleHandler.put(): {0}, update scheudle {1} failed".format(session.email, sched_id))
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        schedule.update(setting)

        userInfo = []
        for email in setting['users']:
            tmpUserInfo = yield self._query_account(email=email)
            userInfo = userInfo + tmpUserInfo
        allUserInfo = [user for user in userInfo if user.get("status") == "enabled"]
        schedule['all_users'] = allUserInfo

        schedule = json.loads(json_util.dumps(schedule, default=json_util.default))

        logging.info("Schedule.ScheduleHandler.put(): {0}, action success".format(session.email))
        raise gen.Return(schedule)

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "schedule_id": {"type": "string", "minLength": MIN_LENGTH},
                "since": {"format": "date-time"},
                "until": {"format": "date-time"},
                "user": {"format": "email"}
            },
            "required": ["schedule_id", "since", "until", "user"]
        },
        output_schema={
            "type": "object"
        },
        format_checker=jsonschema.FormatChecker()
    )
    @coroutine
    @swagger.operation(nickname='path_schedule')
    def patch(self):
        """
            @description: Update the special section of schedule entries.
            @notes:

            @param schedule_id: schedule_id
            @type schedule_id: string

            @param since: since
            @type since: date-time

            @param until: until
            @type until: date-time

            @param users: user email
            @type users: string

            @return 200: {}
            @raise 400: invalid input
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Schedule.ScheduleHandler.patch(): {0}, begin action".format(session.email))

        sched_id = self.body.get("schedule_id")
        schedule = yield AppServiceGlobal.db.schedule.find_one(
            {
                'id': sched_id,
                'status': SCHEDULE_STATE_ACTIVE
            },
            {
                '_id': False
            }
        )
        if not schedule:
            logging.error("Schedule.ScheduleHandler.patch(): {0}, invalid input schedule id {1}".format(session.email, sched_id))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

        entry_start_tp = datetime.strptime(self.body['since'], DATE_FORMAT)
        entry_end_tp = datetime.strptime(self.body['until'], DATE_FORMAT)
        new_user = self.body['user']

        # timestamp = timedelta(days=1) if schedule['rotation_type'] == 'daily' else timedelta(days=7)
        # check entry start/end timepoint
        if entry_start_tp < datetime.utcnow() or entry_end_tp < datetime.utcnow() or entry_end_tp < entry_start_tp or entry_start_tp == entry_end_tp:
            logging.error("Schedule.ScheduleHandler.patch(): {0}, invalid input entry start/end time".format(session.email))
            raise APIError(400, message="Invalid input entry start/end time!")

        new_item = {
            'start': entry_start_tp,
            'end': entry_end_tp,
            'user': new_user
        }
        new_except = self._merge_new_exception_into_old(new_item, schedule.get("exception"))
        res = yield AppServiceGlobal.db.schedule.update(
            {
                'id': sched_id
            },
            {
                '$set': {
                    'exception': new_except
                }
            },
            # {
            #     '$addToSet': {
            #         'exception': new_item
            #     }
            # },
            w=AppServiceGlobal.config.mongo_db_copy
        )

        if not res:
            logging.error("Schedule.ScheduleHandler.patch(): {0}, update scheudle {1} failed".format(session.email, sched_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        schedule = json.loads(json_util.dumps(schedule, default=json_util.default))

        logging.info("Schedule.ScheduleHandler.patch(): {0}, action success".format(session.email))
        raise gen.Return(schedule)

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    @swagger.operation(nickname='get_schedule')
    def get(self):
        """
            @description: Get all or special schedules information.
            @notes: /schedule/?scheduleId=<schedule_id>&from=<started_time_string>&to=<ended_time_string>&resolvegroupId=<resolvegroupId>&mine=true

            @param scheduleId: schedule_id
            @type scheduleId: string

            @param from: started_time_string
            @type from: date-time

            @param to: ended_time_string
            @type to: date-time

            @param resolvegroup: resolvegroupId
            @type resolvegroup: string

            @param mine: 仅获取我所在的支持组的排班 [true, flase]
            @type mine: boolean

            @return 200: {}
            @raise 400: invalid input
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Schedule.ScheduleHandler.get(): {0}, begin action".format(session.email))

        spec = {
            'status': SCHEDULE_STATE_ACTIVE,
        }
        mine = self.get_argument('mine', 'false')
        if mine and mine.lower() == u'true':
            # 获取我所在的支持组的排班
            user_id = session.uid
            doc = {'users.uid': {'$in': [user_id]}, 'status': {'$ne': 'removed'}}
            rgs = yield self._query_resolvegroup_info(doc)
            rg_ids = []
            if rgs and isinstance(rgs, list):
                rg_ids = [rg['id'] for rg in rgs]
            spec['resolvegroup'] = {
                '$in': rg_ids
            }
        schedule_ids = self.get_arguments('scheduleId')
        if schedule_ids and isinstance(schedule_ids, list):
            spec['id'] = {
                '$in': schedule_ids
            }
        resolvegroupIds = self.get_arguments('resolvegroupId')
        if resolvegroupIds and isinstance(resolvegroupIds, list):
            spec['resolvegroup'] = {
                '$in': resolvegroupIds
            }

        started_time_string = self.get_argument('from', None)
        start_time = datetime.strptime(started_time_string, DATE_FORMAT) if started_time_string else datetime.utcnow()
        ended_time_string = self.get_argument('to', None)
        end_time = datetime.strptime(ended_time_string, DATE_FORMAT) if ended_time_string else datetime.utcnow() + relativedelta(months=3)

        result = yield self._get_full_schedule(spec, start_time, end_time, method='GET')
        schedules = json.loads(json_util.dumps(result, default=json_util.default))
        # 获取每一个sched里面entries中的所有用户的完整信息
        for sched in schedules:
            userInfo = []
            users = list(set(sched.get('users') + [entry['user'] for entry in sched['entries']]))
            for user in users:
                tmpUser = yield self._query_account(email=user, session=session)
                if tmpUser:
                    userInfo = userInfo + tmpUser
            allUserInfo = [user for user in userInfo if user.get("status") == "enabled"]
            sched['all_users'] = allUserInfo

        logging.info("Schedule.ScheduleHandler.get(): {0}, action success".format(session.email))
        raise gen.Return({"data": schedules})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    @swagger.operation(nickname='delete_schedule')
    def delete(self):
        """
            @description: Delete special schedules.
            @path: /schedule/?scheduleId=<schedule_id>

            @param schedule_id: schedule_id
            @type schedule_id: string

            @return 200: {}
            @raise 400: invalid input
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Schedule.ScheduleHandler.delete(): {0}, begin action".format(session.email))

        spec = {}
        schedule_ids = self.get_arguments('scheduleId')
        if schedule_ids and isinstance(schedule_ids, list):
            spec['id'] = {
                '$in': schedule_ids
            }

        # 添加检查schedu是否是默认的
        for sid in schedule_ids:
            checkSchedules = yield AppServiceGlobal.db.schedule.find(
                {'id': sid},
                {
                    '_id': False
                }
            ).to_list(None)
            if checkSchedules and isinstance(checkSchedules, list) and checkSchedules[0].get('tag'):
                logging.error("Schedule.ScheduleHandler.delete(): {0} forbidden delete".format(sid))
                raise APIError(400, flag="DUTY.HANDLER.SCHEDULER.DELETE_FAILED".format(sid))

        if not spec:
            logging.error("Schedule.ScheduleHandler.delete(): {0}, not input schedule id".format(session.email))
            raise APIError(400, flag="DUTY.HANDLER.SCHEDULER.DELETE_FAILED".format(sid))

        res = yield AppServiceGlobal.db.schedule.update(
            spec,
            {
                '$set': {
                    'status': SCHEDULE_STATE_REMOVED
                }
            },
            multi=True,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("Schedule.ScheduleHandler.delete(): {0}, remove schedule {0} failed".format(session.email, schedule_ids))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        logging.info("Schedule.ScheduleHandler.delete(): {0}, action success".format(session.email))
        raise gen.Return({"message": "remove schedule {0} successfully".format(schedule_ids)})

    @coroutine
    def _get_full_schedule(self, spec, start_time, end_time, method='PUT'):
        """获取完整的排班，包括历史记录和exception"""
        schedules = yield AppServiceGlobal.db.schedule.find(
            spec,
            {
                '_id': False
            }
        ).to_list(None)

        result = []
        for _, schedule in enumerate(schedules):

            handoff_day = schedule.get('handoff_day')
            handoff_time = schedule['handoff_time']

            schedule_id = schedule['id']
            history = yield AppServiceGlobal.db.history.find_one(
                {'id': schedule_id},
                {'_id': False}
            )
            # 没有history的情况，如果没有history,获取排班里的start时间
            if history and isinstance(history.get('entries'), list):
                new_schedule_time = history['entries'][-1]['end']
            else:
                new_schedule_time = schedule['start']

            # 开始时间存在疑问
            # 以历史结束时间作为起点,如果一个排版执行到一个周期的一半,此时用户更新了排版,则又开始,用户可能会多排班一半
            # 如果没有历史,则成立
            # 暂时这个问题先忽略
            expanded_entries = self._expand_schedule(
                schedule['users'], schedule['rotation_type'], new_schedule_time, end_time, handoff_day, handoff_time)
            # 保存exception合并之后的entries，这里的entries只用与获取当天排班的人员
            if method != 'GET' or isinstance(schedule.get('entries'), list) and schedule['entries'][-1]['end'] < datetime.utcnow():
                if expanded_entries:
                    res = yield AppServiceGlobal.db.schedule.update(
                        {'id': schedule_id},
                        {'$set': {'entries': expanded_entries}},
                        w=AppServiceGlobal.config.mongo_db_copy
                    )
                    logging.info('Saving entries to schedule result: {}'.format(res.get('updatedExisting')))

            # 获取exception,并合并到entries
            if schedule.get('exception') and isinstance(schedule.get('exception'), list):
                expanded_entries = self._merge_exceptions(schedule.get('exception'), expanded_entries)

            if history and isinstance(history['entries'], list):
                schedule['entries'] = history['entries']
            if expanded_entries:
                schedule['entries'] = expanded_entries

            # filter entries between start_time and end_time range
            schedule['entries'] = [entry for entry in schedule['entries'] if
                                   entry['end'] > start_time and entry['start'] < end_time]

            for e in schedule['entries']:
                pass

            # append resolvegroup info
            resolvegroupid = schedule['resolvegroup']
            resolvegroup = yield AppServiceGlobal.db.resolvegroup.find_one({'id': resolvegroupid})
            if resolvegroup:
                schedule['masters'] = resolvegroup.get('masters')
                schedule['name'] = resolvegroup.get('name')
            result.append(schedule)

        raise gen.Return(result)

    def _expand_schedule(self, users, rotation_type, from_tp, to_tp=None, handoff_day=None, handoff_time=None):
        """
        需要实时计算出entry，并与exception合并，最终返回
        :param users: 排班用户列表，根据这个顺序去生成排班
        :param rotation_type: 轮班类型
        :param from_tp: 开始时间
        :param to_tp: 结束时间
        :param handoff_day: 换班日
        :param handoff_time: 换班时间
        :return:
        """
        # logging.debug('handoff_day: %s, handoff_time: %s' % (handoff_day, handoff_time))
        if to_tp is None:
            to_tp = from_tp + relativedelta(months=3)

        handoff_hours, handoff_minutes, handoff_seconds = handoff_time[0], handoff_time[1], handoff_time[2]
        sched_entries = []
        index = 0
        cur_from = from_tp

        if rotation_type.lower() == 'daily':
            # 每天轮班
            delta = timedelta(days=1)
            rela = relativedelta(hour=handoff_hours, minute=handoff_minutes, second=handoff_seconds, microsecond=0)
        else:
            # 每周轮班
            delta = timedelta(weeks=1)
            rela = relativedelta(weekday=handoff_day, hour=handoff_hours, minute=handoff_minutes, second=handoff_seconds, microsecond=0)
            # rela = relativedelta(hour=handoff_hours, minute=handoff_minutes, second=handoff_seconds, microsecond=0)

        # while cur_from < to_tp:
        while cur_from < to_tp or index % len(users) != 0:
            cur_end = from_tp + rela + index * delta
            # 第一个entry的计算时，会出现cur_end小于等于cur_from的情况
            if cur_end <= cur_from:
                cur_end += delta
            user = users[index % len(users)]
            item = {
                'start': cur_from,
                'end': cur_end,
                'user': user
            }
            sched_entries.append(item)
            cur_from = cur_end
            index += 1
        return sched_entries

    def _merge_exceptions(self, exceptions, entries):
        """合并exception到排班中"""
        # logging.info('----- ---  exceptions: {}'.format(exceptions))
        # logging.info('----- ---  entries: {}'.format(entries))

        # result_entries = []
        # for exception in exceptions:
        #     result_entries = []
        #     start_idx = None
        #     end_idx = None
        #     for idx, entry in enumerate(entries):
        #
        #         if start_idx is not None and end_idx is not None:
        #             break
        #         if entry['end'] > exception['start'] >= entry['start']:
        #             start_idx = idx
        #         if entry['start'] < exception['end'] <= entry['end']:
        #             end_idx = idx
        #     else:  # exception不在entries范围内，不进行合并
        #         return entries
        #     if start_idx > 0:
        #         result_entries.extend(entries[:start_idx])
        #
        #     if exception['start'] > entries[start_idx]['start']:
        #         result_entries.append(
        #             {
        #                 'start': entries[start_idx]['start'],
        #                 'end': exception['start'],
        #                 'user': entries[start_idx]['user']
        #             }
        #         )
        #
        #     result_entries.append(exception)
        #
        #     if exception['end'] < entries[end_idx]['end']:
        #         result_entries.append(
        #             {
        #                 'start': exception['end'],
        #                 'end': entries[end_idx]['end'],
        #                 'user': entries[end_idx]['user']
        #             }
        #         )
        #
        #     if end_idx < len(entries):
        #         result_entries.extend(entries[end_idx + 1:])
        #
        #     if len(result_entries) > 1 and result_entries[0]['user'] == result_entries[1]['user']:
        #         result_entries[1]['start'] = result_entries[0]['start']
        #         result_entries = result_entries[1:]
        #     if len(result_entries) > 2 and result_entries[-1]['user'] == result_entries[-2]['user']:
        #         result_entries[-2]['end'] = result_entries[-1]['end']
        #         result_entries = result_entries[:-1]
        #     entries = result_entries
        # return result_entries
        # By QiRui.Su
        # 以时间为切入点
        # 步骤1 判断临时节点 当前循环的节点 的起始时间
        # 如果大于
        #   则判断结束时间
        #     如果大于,则继续下一个循环节点的步骤1
        #     如果小于等于,则判断用户
        #       如果相同，则继续下一个临时节点
        #       如果不同，则将循环节点的　结束时间　设置为临时节点的　起始时间，并将循环节点加入到列表中
        # 如果小于
        #   则判断临时节点的结束时间

        if isinstance(exceptions, list):
            for tmp_exce in exceptions:
                entries = self._one_exception_merge_to_entires(tmp_exce, entries)
        elif isinstance(exceptions, dict):
            entries = self._one_exception_merge_to_entires(exceptions, entries)
        if not entries and isinstance(exceptions, dict):
            entries.append(exceptions)
        if not entries and isinstance(exceptions, list):
            entries = entries + exceptions
        return entries

    def _merge_new_exception_into_old(self, in_entry, old_entries):
        """每次新添加excption，先跟数据库中的excption进行合并"""
        result = []
        merge_flag = False
        if not old_entries:
            return [in_entry]

        for old_entry in old_entries:
            if old_entry['start'] < in_entry['start'] < old_entry['end']:
                if in_entry['user'] != old_entry['user']:
                    result.append({
                        'start': old_entry['start'],
                        'end': in_entry['start'],
                        'user': old_entry['user']
                    })
                    result.append(in_entry)
                    if in_entry['end'] < old_entry['end']:
                        result.append({
                            'start': in_entry['end'],
                            'end': old_entry['end'],
                            'user': old_entry['user']
                        })
                else:
                    result.append({
                        'start': old_entry['start'],
                        'end': max(in_entry['end'], old_entry['end']),
                        'user': old_entry['user']
                    })
                merge_flag = True
            elif in_entry['start'] < old_entry['start'] < in_entry['end']:
                if in_entry['user'] != old_entry['user']:
                    result.append(in_entry)
                    if in_entry['end'] < old_entry['end']:
                        result.append({
                            'start': in_entry['end'],
                            'end': old_entry['end'],
                            'user': old_entry['user']
                        })
                else:
                    result.append({
                        'start': in_entry['start'],
                        'end': max(in_entry['end'], old_entry['end']),
                        'user': in_entry['user']
                    })
                merge_flag = True
            else:
                result.append(old_entry)
        if not merge_flag:
            result.append(in_entry)
        return result

    def _one_exception_merge_to_entires(self, one_exception, entries):
        '''
        @param one_exception: 一个临时exception节点对象,dict object
        @param entries: 生成的entries列表,list object
        '''
        merge_entries = []
        # exception中的起始时间可能比entries大，小，或者相等
        for xh_entry in entries:
            if one_exception.get("start") > xh_entry.get("start"):
                if one_exception.get("start") > xh_entry.get("end"):
                    merge_entries.append(xh_entry)
                    continue
                else:
                    if one_exception.get("user") != xh_entry.get("user"):
                        tmp_copy = copy.deepcopy(xh_entry)
                        # tmp_copy = xh_entry
                        tmp_copy["end"] = one_exception["start"]
                        merge_entries.append(tmp_copy)
                        # Todo, 判断临时节点的结束时间和循环列表节点的结束时间
                        if one_exception.get("end") < xh_entry.get("end"):
                            merge_entries.append(one_exception)
                            xh_entry["start"] = one_exception.get("end")
                            merge_entries.append(xh_entry)
                        # elif one_exception.get("end") >= xh_entry.get("end"):
                        else:
                            # 需要处理，如果是同一个人，则需要合并
                            merge_entries.append(one_exception)
                    # elif one_exception.get("user") == xh_entry.get("user"):
                    else:
                        # Todo, 判断临时节点的结束时间和循环列表节点的结束时间
                        xh_entry["end"] = one_exception.get("end")
                        merge_entries.append(xh_entry)
            # 当节点处于空白节点的情况
            # elif one_exception.get("start") <= xh_entry.get("start") and one_exception.get("end") < xh_entry.get("start"):
            #     merge_entries.append(one_exception)
            else:
                # one_exception.get("start") <= xh_entry.get("start")
                # merge_entries.append(one_exception)
                if merge_entries:
                    last_tmp_node = merge_entries[-1]
                    # 临时节点
                    if one_exception.get("start") > last_tmp_node.get("end"):
                        merge_entries.append(one_exception)
                        last_tmp_node = merge_entries[-1]
                    # end...
                    if xh_entry.get("start") > last_tmp_node.get("end"):
                        merge_entries.append(xh_entry)
                        continue
                    # elif xh_entry.get("start") <= last_tmp_node.get("end"):
                    else:
                        if xh_entry.get("end") <= last_tmp_node.get("end"):
                            continue
                        # elif xh_entry.get("end") > last_tmp_node.get("end"):
                        else:
                            if xh_entry.get("user") == last_tmp_node.get("user"):
                                last_tmp_node["end"] = xh_entry.get("end")
                                continue
                            else:
                                xh_entry["start"] = last_tmp_node.get("end")
                                merge_entries.append(xh_entry)
                else:
                    merge_entries.append(xh_entry)
        return merge_entries


class DutyHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /schedule/duty/
    """

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    @swagger.operation(nickname='get_schedule_duty')
    def get(self):
        """
            @description: Get the onduty user in the special schedule.
            @notes: uri /schedule/?scheduleId=<schedule_id>&specialTP=<special_tp>

            @param scheduleId: scheduleId
            @type scheduleId: string

            @param specialTP: special time point
            @type specialTP: datetime

            @param scheduleId: scheduleId
            @type scheduleId: string

            @return 200: {}
            @raise 400: invalid input
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Schedule.DutyHandler.get(): {0}, begin action".format(session.email))

        spec = {
            'status': SCHEDULE_STATE_ACTIVE
        }
        special_tp_str = self.get_argument('specialTP', None)
        special_tp = datetime.strptime(special_tp_str, DATE_FORMAT) if special_tp_str else datetime.utcnow()
        spec['start'] = {
            '$lt': special_tp
        }

        schedule_ids = self.get_arguments('scheduleId')
        if schedule_ids:
            spec['id'] = {
                '$in': schedule_ids
            }
        cti_ids = self.get_arguments('ctId')
        if cti_ids:
            spec['cti'] = {
                '$in': cti_ids
            }

        schedules = yield AppServiceGlobal.db.schedule.find(
            spec
        ).to_list(None)

        result = {
            'specialTP': special_tp_str if special_tp_str else special_tp.strftime(DATE_FORMAT),
            'duty': []
        }
        for idx, schedule in enumerate(schedules):
            if not schedule['end'] or schedule['end'] > special_tp:
                the_entry = [entry for entry in schedule['entries'] if entry['start'] < special_tp < entry['end']]
                if the_entry:
                    the_entry[0].update({'id': schedule['id'], 'cti': schedule.get('cti')})
                    result['duty'].append(the_entry[0])

        result = json.loads(json_util.dumps(result, default=json_util.default))

        logging.info("Schedule.DutyHandler.get(): {0}, action success".format(session.email))
        raise gen.Return(result)
