#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
# File: SLA.py
# Time: 6:06:09 PM
# Desc:
# Author: sendal
# Email: <schangech@gmail.com>
# Version: 0.0.1
'''

import uuid
import time
import logging
import json
import jsonschema
from datetime import datetime, timedelta
from bson import json_util
from tornado import gen
from Runtime.Constants import (
    MIN_LENGTH,
    LOGIN_SHELL,
    DEFAULT_LOCALE,
    SUPPORTED_SEVERITIES,
    MINUTES_PER_HOUR)
from Forge.Common.State import (
    TICKET_STATE_OPENED)
from Forge.Common.Result import Result
from Forge.Common.Ops import (
    OPS_ACCOUNT_RESET_PASSWORD,
    OPS_ACCOUNT_ACTIVE,
    OPS_ACCOUNT_UPDATE_PASSWORD)
from Forge.Common.Utils import is_email, validate_base64
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Email import Email
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil
from Forge.Common.Utils import ignored, parse_department, ipa_user_info_format_joint, is_phonenumbers

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


class SLAUtilsHandler(APIHandler, AppServiceUtil):
    @gen.coroutine
    def _query_sla(self, doc=None):
        doc = doc if doc else {}
        res = yield AppServiceGlobal.db.sla.find(
            doc,
            {
                '_id': False
            }
        ).to_list(None)
        if not res:
            raise gen.Return([])

        result = json.loads(json_util.dumps(res, default=json_util.default))
        raise gen.Return(result)

    @coroutine
    def _insert_default_sla(self, resolvegroup_id):

        """
        Insert default sla for resolvegroup
        """
        logging.info('Insert default root cause for resolvegroup {0}'.format(resolvegroup_id))

        default_sla = []
        for index in range(1, 6):
            sla = {
                'severity': SUPPORTED_SEVERITIES[index]['level'],
                'response_time': SUPPORTED_SEVERITIES[index]['time-threshold']*MINUTES_PER_HOUR,
                'processing_time': SUPPORTED_SEVERITIES[index]['processing_time']*MINUTES_PER_HOUR,
                'description': SUPPORTED_SEVERITIES[index]['description']
            }
            default_sla.append(sla)

        doc = {
            'id': resolvegroup_id,
            'sla': default_sla,
            'customize': False,
            'escalation': []
        }

        res = yield AppServiceGlobal.db.sla.insert(doc)
        if not res:
            logging.error("add default sla {0} failed".format(resolvegroup_id))

        result = json.loads(json_util.dumps(doc, default=json_util.default))

        raise gen.Return(result)

    @gen.coroutine
    def _update_sla(self, rg_id=None, setting=None):
        doc = {
            '$set': setting
        }
        res = yield AppServiceGlobal.db.sla.update(
            {
                'id': rg_id
            },
            doc,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        info = json.loads(json_util.dumps(setting, default=json_util.default))
        raise gen.Return(info)


class SLAHandler(SLAUtilsHandler):
    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "id": {"type": "string", "minLength": MIN_LENGTH},
                "sla": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "severity": {"type": "number", "enum": [1, 2, 3, 4, 5]},
                            "response_time": {"type": "integer"},       # 响应时间
                            "processing_time": {"type": "integer"},     # 处理时间
                            "description": {"type": "string", "minLength": MIN_LENGTH}
                        }
                    }
                },
                "escalation": {
                    "type": "array",
                    "items": {
                        "type": "array",
                        "items": {
                            "type": "string", "minLength": MIN_LENGTH
                        }
                    }
                },
                "customize": {"type": "boolean"}
            },
            "required": ["id", "sla", "customize"]
        },
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def post(self):
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("SLA.SLAHandler.post(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        rg_id = self._ipa_input_except(body, input_str="id")
        sla = self._ipa_input_except(body, input_str="sla")
        escalation = self._ipa_input_except(body, input_str="escalation")
        customize = self._ipa_input_except(body, input_str="customize")

        # 先判断resolvegroup是不是存在
        rg = yield AppServiceGlobal.db.resolvegroup.find_one(
            {
                'id': rg_id
            }
        )
        if not rg:
            logging.warning("{0}, invalid resolvegroup {1}".format(session.email, rg_id))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

        setting = {
            'id': rg_id,
            'sla': sla,
            'customize': customize
        }
        if escalation is not None:
            setting['escalation'] = escalation

        res = yield AppServiceGlobal.db.sla.insert(setting)

        if not res:
            logging.error("{0}, add new sla {0} failed".format(session.email, setting['id']))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        else:
            setting.pop('_id')

        result = json.loads(json_util.dumps(setting, default=json_util.default))
        logging.info("{0}, action success".format(session.email))
        raise gen.Return({"data": result})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def get(self):
        # /sla?id=<resolvegroup_id>
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("SLA.SLAHandler.get(): {0}, begin action".format(session.email))

        rg_id = self.get_argument('id', None)
        doc = {'id': rg_id} if rg_id else {}
        sla_result = yield self._query_sla(doc)
        if not sla_result:
            # 没有查询到sla,需要添加默认的sla到数据库中
            sla_result = yield self._insert_default_sla(rg_id)

        raise gen.Return({"data": sla_result})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "id": {"type": "string", "minLength": MIN_LENGTH},
                "sla": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "severity": {"type": "number", "enum": [1, 2, 3, 4, 5]},
                            "response_time": {"type": "integer"},       # 响应时间
                            "processing_time": {"type": "integer"},     # 处理时间
                            "description": {"type": "string", "minLength": MIN_LENGTH}
                        }
                    }
                },
                "escalation": {
                    "type": "array",
                    "items": {
                        "type": "array",
                        "items": {
                            "type": "string", "minLength": MIN_LENGTH
                        }
                    }
                },
                "customize": {"type": "boolean"}
            },
            "required": ["id"]
        },
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def put(self):
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("SLA.SLAHandler.put(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        rg_id = self._ipa_input_except(body, input_str="id")
        sla = self._ipa_input_except(body, input_str="sla")
        escalation = self._ipa_input_except(body, input_str="escalation")
        customize = self._ipa_input_except(body, input_str="customize")

        setting = {'id': rg_id}
        if sla:
            setting['sla'] = sla
        if escalation is not None:
            setting['escalation'] = escalation
        if customize is not None:
            setting['customize'] = customize

        check_sla_is_exist = yield self._query_sla({'id': rg_id})
        if not check_sla_is_exist:
            logging.warning('SLA.SLAHandler.put() {0} not found'.format(rg_id))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")
        returnRes = yield self._update_sla(rg_id, setting)
        logging.info("SLA.SLAHandler.put(): {0} action success".format(session.email))
        raise gen.Return({"message": returnRes})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def delete(self):
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("SLA.SLAHandler.delete(): {0}, begin action".format(session.email))

        rg_ids = self.get_arguments('id')
        if not rg_ids:
            logging.error("SLA.SLAHandler.delete() is forbidden")
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

        doc = {'id': {'$in': rg_ids}}
        returnRes = yield AppServiceGlobal.db.sla.remove(doc)

        logging.info("SLA.SLAHandler.delete() {0} action successfully".format(session.email))
        result = 'successfully' if returnRes else 'failed'
        raise gen.Return({'message': result})


class DefaultHandler(APIHandler, AppServiceUtil):

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def get(self):
        """
            uri:
                /sla/default
            Get default sla information
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("{0}, begin action".format(session.email))

        default_sla = []
        for index in range(1, 6):
            sla = {
                'severity': SUPPORTED_SEVERITIES[index]['level'],
                'response_time': SUPPORTED_SEVERITIES[index]['time-threshold'] * MINUTES_PER_HOUR,
                'processing_time': SUPPORTED_SEVERITIES[index]['processing_time'] * MINUTES_PER_HOUR,
                'description': SUPPORTED_SEVERITIES[index]['description']
            }
            default_sla.append(sla)

        raise gen.Return({"data": default_sla})
