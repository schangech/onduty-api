#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: liuzhenwu

"""
Ruleengine event handler.
"""

import logging
import json
import pymongo
from tornado import gen
from datetime import datetime, timedelta
from bson import json_util

from Runtime.Constants import (
    MIN_LENGTH,
    MINUTES_PER_HOUR,
    DATE_FORMAT,
    TIME_FORMAT,
    DEFAULT_LIMIT_NUMBER,
    SUPPORTED_AGENTS,
    SUPPORTED_EVENTS,
    SUPPORTED_SEVERITIES,
    CHINA_TIME_ZONE,
    AUTO_TICKET_TRIGGER,
    DEFAULT_SUPPORT_DEPARTMENT,
    DEFAULT_SUPPORT_USER)
from Forge.Common.State import (
    AGENT_STATE_ACTIVE,
    TICKET_STATE_OPENED,
    TICKET_STATE_PROCESSING,
    TICKET_STATE_PENDING,
    TICKET_STATE_DONE)
from Forge.Common.Ops import OPS_ACTIVITY_RESOLVE, OPS_TICKET_UPDATE
from Forge.Common.Utils import parse_department, ignored
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Email import Email
from Forge.Common.Wechat import Wechat
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil


class Ruleengine(APIHandler, AppServiceUtil):
    """
        endpoint: /ruleengine/
    """

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def post(self):
        """
            Handler agent event data.
        """
        body = json.loads(self.request.body)
        body = self.lower_keys(body)
        _in_event = body.get('event')
        rule_list = body.get('rule_list', [])

        logging.debug('in event: %s' % _in_event)
        logging.debug('rule_list: %s' % rule_list)

        # action_id = _in_event.get('action_id').strip().encode('utf-8')
        agent_id = _in_event.get('agent').get('agent_id').strip().encode('utf-8')
        service_key = _in_event.get('service_key').strip().encode('utf-8')
        description = _in_event.get('description', '').strip().encode('utf-8')
        event_type = _in_event.get('event_type').strip().encode('utf-8')
        event_id = _in_event.get('details').get('event_id').strip().encode('utf-8')
        status = _in_event.get('details').get('status').strip().lower().encode('utf-8')
        endpoint = _in_event.get('details').get('endpoint', '').strip().lower().encode('utf-8')
        priority = _in_event.get('details').get('priority', 5)
        report_at = datetime.strptime(_in_event.get('agent').get('queued_at').strip().encode('utf-8'), DATE_FORMAT)
        logging.info(
            "Event.EventHandler.post(): {0}/{1}, begin to handler event {2} | {3}".format(
                agent_id, service_key, event_id, status))

        # agent heartbeat threshold is one hour
        # agent_heartbeat_threshold = datetime.utcnow() - timedelta(minutes=MINUTES_PER_HOUR)
        # check agent status
        agent = yield AppServiceGlobal.db.agent.find_one(
            {
                'id': agent_id,
                'status': AGENT_STATE_ACTIVE
            },
            {
                '_id': False
            }
        )
        if not agent:
            logging.error("Event.EventHandler.post(): Invalid agent {0}".format(agent_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        logging.info("Event.EventHandler.post(): {0} agent is active".format(agent_id))

        # check service key
        spec = {
            'key': service_key,
            'agents': agent_id
        }
        service_type = _in_event.get('agent').get('queued_by').strip().encode('utf-8')
        if service_type:
            service_type = service_type[service_type.find('-') + 1:]
            spec['type'] = service_type
        service = yield AppServiceGlobal.db.service.find_one(
            spec
        )
        if not service:
            logging.error("Event.EventHandler.post(): {0}, invalid service key {1}".format(agent_id, service_key))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

        # save and check data format
        event = {
            'agent_id': agent_id,
            'service_key': service_key,
            'description': description,
            'event_type': event_type,
            'endpoint': endpoint,
            'report_by': service_type if service_type else '',
            'report_at': report_at,
            'details': _in_event.get('details')
        }
        event['details']['level'] = priority

        res = yield AppServiceGlobal.db.event.update(
            {
                'agent_id': event['agent_id'],
                'service_key': event['service_key'],
                'description': event['description'],
                'event_type': event['event_type'],
                'endpoint': event['endpoint'],
                'report_by': event['report_by'],
                'details.event_id': event['details']['event_id'],
                'details.status': event['details']['status']
            },
            {
                '$set': event,
                '$inc': {
                    'count': 1
                }
            },
            upsert=True,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("Event.EventHandler.post(): {0}, save event failed".format(event_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        # create ticket if new trigger event
        # 检查ticket中的状态，看是否被手动标记为已经解决，是，则新建工单，否则进行下面操作
        tickets = yield AppServiceGlobal.db.ticket.find(
            {
                'details.event_id': event_id,
                'description': description,
                'type': 'bug'
            },
            {
                '_id': False
            }
        ).to_list(None)
        ticket_status_list = [t.get('status') for t in tickets]
        if ((tickets and not (set([TICKET_STATE_PROCESSING, TICKET_STATE_OPENED, TICKET_STATE_PENDING]) & set(ticket_status_list))) or not res['updatedExisting']) and event_type == 'trigger':
            current_time = datetime.utcnow()

            for rule in rule_list:
                if rule.get('alert') or rule.get("alert") is None:
                    rule = rule['rule']
                    rg_id = rule['resolvegroup']
                    cti_id = rule['cti']
                    user_id = rule['user']

                    rule_user = yield self._query_account(uid=user_id)
                    with ignored(Exception):
                        onduty_user = yield self._get_the_schedule(current_time, rg_id)

                    # 如果规则里指定了用户，那么就用规则里的用户，负责用当前值班人员
                    responser = rule_user[0]['email'] if user_id else onduty_user

                    ticket = {
                        'type': 'bug',
                        'severity': rule.get('severity') or priority,
                        'description': event['description'],
                        'requester': AUTO_TICKET_TRIGGER,
                        'resolvegroupId': rg_id,
                        'responser': responser,
                        'details': event.get('details', {}),
                        'ctiId': cti_id,
                        'status': TICKET_STATE_OPENED,
                        'cc_list': [responser] if responser else [onduty_user],
                        'updated_time': current_time,
                        'created_time': current_time
                    }

                    ticket_id = yield self._create_rc_ticket(onduty_user, ticket)
                    if not ticket_id:
                        logging.error("Event.EventHandler.post(): auto create ticket failed")
                        raise gen.Return({"message": "Handlered event {0} but failed to create ticket".format(event_id)})

                    # create duty task
                    data = {
                        'job_id': ticket_id
                    }
                    response = yield self._scheduler(method='POST', params=data)
                    if not response:
                        logging.error("Event.EventHandler.post(): create duty task for ticket {0} failed".format(ticket_id))

        elif event_type == 'resolve':  # update ticket status when resolve event
            res = yield AppServiceGlobal.db.ticket.update(
                {
                    'details.event_id': event_id
                },
                {
                    '$set': {
                        'status': TICKET_STATE_DONE
                    }
                },
                multi=True,
                w=AppServiceGlobal.config.mongo_db_copy
            )
            if not res:
                logging.error(
                    "Event.EventHandler.post(): {0}, update ticket status failed and please do it again manually".format(
                        event_id))
            else:
                tickets = yield AppServiceGlobal.db.ticket.find(
                    {
                        'details.event_id': event_id
                    },
                    {
                        '_id': False
                    }
                ).to_list(None)
                for ticket in tickets:
                    email_args = {
                        'ticket_id': ticket['id'],
                        'update_user': 'API',
                        'ticket_severity': ticket['severity'],
                        'ticket_title': ticket['description'],
                        'ticket_status': ticket['status'],
                        'ticket_creater': ticket['requester'],
                        'ticket_url': 'http://{0}/tickets/{1}'.format(AppServiceGlobal.config.domain, ticket['id']),
                        'create_time': datetime.strftime(ticket['created_time'], '%Y-%m-%d %H:%M:%S'),
                        'ticket_details': ticket['details'].get('content'),
                        'domain': AppServiceGlobal.config.domain
                    }

                    email_format = Email.format(OPS_TICKET_UPDATE, **email_args)
                    self._send_email(receiver=[ticket['responser']],
                                     subject=email_format.get('Subject'),
                                     content=email_format.get('Body'),
                                     format='html')

                    # send wechat
                    created_time = ticket.get('created_time') + timedelta(hours=CHINA_TIME_ZONE)
                    created_time = created_time.strftime(TIME_FORMAT)
                    wechat_format = Wechat.format('resolve', id=ticket['id'], description=ticket.get('description'),
                                                  created_time=created_time, status=ticket.get('status'),
                                                  severity=ticket.get('severity'),
                                                  hostname=ticket.get('details').get('hostname'),
                                                  ip=ticket.get('details').get('ip'), url=email_args['ticket_url'])

                    # send to the wechat user
                    self._send_wechat(receiver=[ticket['responser'].split('@')[0]], type="single",
                                      content=wechat_format)

                logging.info("Event.EventHandler.post(): {0}, update ticket status successlly".format(event_id))

                # add activity
                ticket = yield AppServiceGlobal.db.ticket.find_one(
                    {
                        'details.event_id': event_id
                    },
                    {
                        '_id': False,
                        'id': True
                    }
                )
                if not ticket:
                    logging.error("Event.EventHandler.post(): {0}, can't find event related ticket".format(event_id))
                else:
                    details = [
                        {
                            'action': OPS_ACTIVITY_RESOLVE,
                            'event_id': event_id
                        }
                    ]
                    activity = yield self._create_rc_activity('event-handler', ticket['id'], details)
                    if not activity:
                        logging.error(
                            "Event.EventHandler.post(): {0}, add resolved activity for ticket {1} failed".format(
                                event_id, ticket['id']))

        logging.info("Event.EventHandler.post(): {0}, action success".format(event_id))
        raise gen.Return({"message": "Handlered event {0}".format(event_id)})

    def lower_keys(self, x):
        if isinstance(x, list):
            return [self.lower_keys(v) for v in x]
        elif isinstance(x, dict):
            return dict((k.lower(), self.lower_keys(v)) for k, v in x.iteritems())
        else:
            return x
