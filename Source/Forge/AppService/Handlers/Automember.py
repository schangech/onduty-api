#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: QiRui.Su schangech@gmail.com
# @Date:   2016-09-01 10:48:56
# @Last Modified time: 2016-09-19 14:39:23

'''
    FreeIPA automember interface
'''

import logging
import json
import jsonschema
from tornado import gen

from Runtime.Constants import (MIN_LENGTH, LOGIN_SHELL)
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Result import Result
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil


class HostHandler(APIHandler, AppServiceUtil):
    """
    docstring for AutomemberHandler
    endpoint:
        /automember/host/
    """

    @validate(
        output_schema={
            "type": "object",
            "properties": {
                "message": {"type": "string"}
            }
        },
        format_checker=jsonschema.FormatChecker()
    )
    @coroutine
    def get(self):
        """
            /automember/host/?type=<group,hostgroup>&cn=<hostgroup rule name>
        """

        session = self.check_authorization(AppServiceGlobal.session_manager,
                                           self.request)
        logging.info("Automember.HostHandler.get(): {0}, begin action".format(session.email))

        cn = self.get_arguments('cn')
        type = self.get_argument('type', 'hostgroup').lower().strip()

        logging.info("Get the params is {0}, {1}".format(type, cn))

        data = {
            "method": "automember_find",
            "params": [[], {"type": type}],
            "id": 0
        }

        automember_host = self._ipa_request(session.email,
                                            session.id,
                                            'POST',
                                            json.dumps(data))

        logging.info("Automember.HostHandler.get(): {0}, action success".format(session.email))
        raise gen.Return({"data": automember_host})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "cn": {"type": "string", "minLength": MIN_LENGTH},
                "type": {"type": "string", "minLength": MIN_LENGTH},
                "description": {"type": "string", "minLength": MIN_LENGTH},
                "automemberinclusiveregex": {"type": "string", "minLength": MIN_LENGTH},
                "automemberexclusiveregex": {"type": "string", "minLength": MIN_LENGTH}
            }
        },
        output_schema={
            "type": "object",
            "properties": {
                "message": {"type": "string"}
            }
        },
    )
    @coroutine
    def post(self):
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Automember.HostHandler.post(): {0}, begin action".format(session.email))

        body = json.loads(self.request.body)
        cn = body.get("cn").lower().strip().encode('utf-8')
        type = self._ipa_input_except(body, "type")
        automemberinclusiveregex = self._ipa_input_except(body, "automemberinclusiveregex")
        automemberexclusiveregex = self._ipa_input_except(body, "automemberexclusiveregex")
        description = self._ipa_input_except(body, "description")
        key = self._ipa_input_except(body, "key")

        # only devops can do it
        (flag, adsession) = self._check_user_is_devops_or_admin_group(session.department)
        if not flag:
            logging.info("Account.AccountHandler.post(): {0} is not devops or admin group".format(session.email))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

        data = {
            "method": "automember_add",
            "params": [[], {"cn": cn, "type": type, "description": description}],
            "id": 0
        }

        automember_rule = self._ipa_request(session.email,
                                            adsession,
                                            'POST',
                                            json.dumps(data))
        logging.info("Automember.HostHandler.automember_add.post(): {0}, action success {1}".format(session.email, automember_rule))

        data = {
            "method": "automember_add_condition",
            "params": [[], {"cn": cn, "type": type, "key": key, "automemberinclusiveregex": automemberinclusiveregex, "automemberexclusiveregex": automemberexclusiveregex}],
            "id": 0
        }

        automember_add_condition = self._ipa_request(session.email, session.id, 'POST', json.dumps(data))
        logging.info("Automember.HostHandler.automember_add_condition.post(): {0}, action success".format(session.email))

        raise gen.Return({"data": automember_add_condition})
