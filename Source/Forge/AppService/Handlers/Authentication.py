#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
Created on 2016年8月25日

Administrator 上午11:37:53

@author: QiRui.Su <schangech@gmail.com>
'''

import logging
import json
import jsonschema
from tornado import gen

from Runtime.Constants import (
    MIN_LENGTH, LOGIN_SHELL)
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Result import Result
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil


class AuthenticationHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /Authentication/
    """

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "email": {"format": "email"},
                "password": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["email", "password"]
        },
        output_schema={
            "type": "object",
            "properties": {
                "message": {"type": "string"}
            }
        },
        format_checker=jsonschema.FormatChecker()
    )
    @coroutine
    def get(self):
        """
            Authentication get.
        """
        email = self.get_argument('email').lower().strip().encode('utf-8')
        password = self.get_argument('password').encode('utf-8')
        username = email.split('@')[0]

        logging.debug("Account.AccountHandler.get(): {0}|{1}|{2}".format(email, password))
        # verify from IPA
        # session = self._ipa_login_password(username, password)
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Account.AccountHandler.get(): {0}, begin action".format(session.email))

        # Get IPA group infomation
        data = {
            "method": "pwpolicy_find",
            "params": [[username], {}],
            "id": 0
        }

        resp = self._ipa_request(username, session, 'POST', json.dumps(data))

        logging.info("Account.AccountHandler.get(): {0}, action success".format(username))

        raise gen.Return(resp)
