#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2016-09-14 10:49:26
# @Updated Date: 2016-09-20 14:33:19

"""
Ticket category handler interfaces.
"""

import logging
import uuid
from tornado import gen

from Runtime.Constants import (
    MIN_LENGTH,
    GENERAL_CATEGORY)
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil


class CategoryHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /category/
    """

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "region": {"type": "string", "minLength": MIN_LENGTH},
                "department": {"type": "string", "minLength": MIN_LENGTH},
                "name": {"type": "string", "minLength": MIN_LENGTH},
                "description": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["region", "department", "name", "description"]
        },
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def post(self):
        """
            add new category.
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Category.CategoryHandler.post(): {0}, begin action".format(session.email))

        region = self.body.get('region')
        department = self.body.get('department')
        name = self.body.get('name', None)
        description = self.body.get('description', None)
        supported_departments = yield AppServiceGlobal.db.rc.find(
            {},
            {
                '_id': False,
                'department': True
            }
        ).to_list(None)
        supported_departments = [item['department'] for item in supported_departments if 'department' in item and item['department']]
        if not department or not name or department not in supported_departments:
            logging.error("Category.CategoryHandler.post(): {0}, invalid input department {1}/{2}".format(session.email, region, department))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

        category = {
            'id': 'cat-{0}'.format(str(uuid.uuid4())[:8]),
            'name': name,
            'description': description
        }

        res = yield AppServiceGlobal.db.category.update(
            {
                'region': region,
                'department': department
            },
            {
                '$set': {
                    'region': region,
                    'department': department
                },
                '$push': {
                    'category': category
                }
            },
            upsert=True,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("Category.CategoryHandler.post(): {0}, add new category {1}/{2} failed".format(session.email, region, department))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        logging.info("Category.CategoryHandler.post(): {0}, action success".format(session.email))
        raise gen.Return({"data": category})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "region": {"type": "string", "minLength": MIN_LENGTH},
                "department": {"type": "string", "minLength": MIN_LENGTH},
                "id": {"type": "string", "minLength": MIN_LENGTH},
                "name": {"type": "string", "minLength": MIN_LENGTH},
                "description": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["id", "name"]
        },
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def put(self):
        """
            Update specified category.
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Category.CategoryHandler.put(): {0}, begin action".format(session.email))

        region = self.body.get('region', None)
        department = self.body.get('department', None)
        cat_id = self.body.get('id', None)
        name = self.body.get('name', None)
        description = self.body.get('description', None)
        if not cat_id:
            logging.error("Category.CategoryHandler.put(): {0}, invalid input param category".format(session.email))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        spec = {
            'category.id': cat_id
        }
        if region:
            spec['region'] = region
        if department:
            spec['department'] = department

        setting = {}
        if name:
            setting['category.$.name'] = name
        if description:
            setting['category.$.description'] = description

        res = yield AppServiceGlobal.db.category.update(
            spec,
            {
                '$set': setting
            },
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("Category.CategoryHandler.put(): {0}, update category {1}/{2} failed".format(session.email, department, cat_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        logging.info("Category.CategoryHandler.put(): {0}, action success".format(session.email))
        raise gen.Return({"message": "Update category {0}/{1} successfully".format(department, cat_id)})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def get(self):
        """
            uri:
                /category/?region=<region>&department=<department>&id=<cat_id>
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Category.CategoryHandler.get(): {0}, begin action".format(session.email))

        regions = self.get_arguments('region')
        regions.append(GENERAL_CATEGORY)
        departments = self.get_arguments('department')
        departments.append(GENERAL_CATEGORY)
        cat_ids = self.get_arguments('id')
        if not regions.remove(GENERAL_CATEGORY) and not departments.remove(GENERAL_CATEGORY):
            spec = {}
        else:
            spec = {
                'region': {
                    '$in': regions
                },
                'department': {
                    '$in': departments
                }
            }

        categories = yield AppServiceGlobal.db.category.find(
            spec,
            {
                '_id': False
            }
        ).to_list(None)

        cateTotal = []
        for categorie in categories:
            tList = []
            tDict = {}
            tDict['region'] = categorie['region']
            tDict['department'] = categorie['department']
            if cat_ids and isinstance(cat_ids, list):
                for catId in cat_ids:
                    # 匹配出需要查询的多个catId结果，合并为一个列表
                    tList = tList + [cate for cate in categorie['category'] if catId == cate['id'] ]
                if not tList:
                    continue
                tDict['category'] = tList
                cateTotal.append(tDict)
            else:
                cateTotal.append(categorie)
        categories = cateTotal
        if cat_ids:
            for idx, category in enumerate(categories):
                if category['region'] == GENERAL_CATEGORY and category['department'] == GENERAL_CATEGORY:
                    continue

                categories[idx]['category'] = [cat for cat in category['category'] if cat['id'] in cat_ids]

        logging.info("Category.CategoryHandler.get(): {0}, action success".format(session.email))
        raise gen.Return({"data": categories})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def delete(self):
        """
            uri:
                /category/?region=<region>&department=<department>&id=<category_id>
        """

        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Category.CategoryHandler.delete(): {0}, begin action".format(session.email))

        regions = self.get_arguments('region')
        departments = self.get_arguments('department')
        cat_ids = self.get_arguments('id')
        if not cat_ids:
            logging.error("Category.CategoryHandler.delete(): {0}, invalid input param category id".format(session.email))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        spec = {}
        if regions:
            spec['region'] = {
                '$in': regions
            }
        if departments:
            spec['department'] = {
                '$in': departments
            }

        res = yield AppServiceGlobal.db.category.update(
            spec,
            {
                '$pull': {
                    'category': {
                        'id': {
                            '$in': cat_ids
                        }
                    }
                }
            },
            multi=True,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("Category.CategoryHandler.delete(): {0}, delete {1}/{2} categories {3} failed".format(session.email, regions, departments, cat_ids))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        logging.info("Category.CategoryHandler.delete(): {0}, action success".format(session.email))
        raise gen.Return({"message": "Remove categories {0}/{1} {2} successfully".format(regions, departments, cat_ids)})
