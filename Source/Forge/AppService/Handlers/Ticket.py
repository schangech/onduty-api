#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2016-07-25 14:40:00
# @Updated Date: 2016-09-23 14:00:15

"""
Tickets handler interfaces.
"""

import logging
import json
import pymongo
import uuid
from tornado import gen
from datetime import datetime, timedelta
from bson import json_util
from tornado_swagger import swagger

from Runtime.Constants import (
    MIN_LENGTH,
    DATE_FORMAT,
    CHINA_TIME_ZONE,
    DEFAULT_LIMIT_NUMBER,
    DEFAULT_EMAIL_SUFFIX,
    SUPPORTED_SEVERITIES_COUNT)
from Forge.Common.State import (
    TICKET_STATE_OPENED,
    TICKET_STATE_PROCESSING)
from Forge.Common.Utils import parse_department, is_email, ignored
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil
# prometheus
from prometheus_client import multiprocess
from prometheus_client import generate_latest, REGISTRY, Gauge, Counter


class CountHandler(APIHandler, AppServiceUtil):
    """
        Search Ticket by Ticket_ID/Ticket_Description
    """
    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='get_ticket_count')
    def get(self):
        """
            @description: 得到未解决完／本组报障／分配给我[resp]／我创建的[req]／和我相关的[cc]　报障个数
            @notes:

            @param resolvegroupId: resolvegroupId
            @type resolvegroupId: string

            @param status: ticket status
            @type status: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="ticket/count").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.CountHandler.post(): {0}, begin action".format(session.email))
        params = {}
        uid = session.uid
        user = [session.email + DEFAULT_EMAIL_SUFFIX if not is_email(session.email) else session.email]
        # 全部报障，分配给我的，我创建的	&&	支持组/报障状态
        rg_param = {}
        status_param = {}
        type_param = {}
        rg_ids = self.get_arguments('resolvegroupId')
        if rg_ids:
            rg_param['resolvegroupId'] = {'$in': rg_ids}
        status = self.get_arguments('status')
        if status:
            status_param['status'] = {'$in': status}
        t_type = self.get_arguments('type')
        if t_type:
            type_param['type'] = {'$in': t_type}

        params['requester'] = user
        params['responser'] = user
        params['cc_list'] = user
        if uid:
            rgInfo = yield self._query_resolvegroup_info({'users.uid': {'$in': [uid]}})
            rgIds = [rg.get('id') for rg in rgInfo if rg.get('id')]
        key_filters = []
        key_filters.append({'resolvegroupId': {'$in': rgIds}})
        key_filters.append({'requester': {'$in': user}})
        key_filters.append({'responser': {'$in': user}})
        key_filters.append({'cc_list': {'$in': user}})

        res_count = []
        for key, value in params.iteritems():
            count = yield AppServiceGlobal.db.ticket.find(
                {'$and': [
                    {key: {"$in": value}},
                    rg_param,
                    status_param,
                    type_param
                ]}
            ).count()
            res_count.append({key: count})
        count = yield AppServiceGlobal.db.ticket.find(
            {'$and': [
                {"$or": key_filters},
                rg_param,
                status_param,
                type_param
            ]}
        ).count()
        res_count.append({"group": count})

        logging.info("Ticket.CountHandler.post(): {0}, action success".format(session.email))
        raise gen.Return({"data": res_count})


class SearchHandler(APIHandler, AppServiceUtil):
    """
        Search Ticket by Ticket_ID/Ticket_Description
    """
    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='get_ticket_search')
    def get(self):
        """
            @description: Search Ticket by Ticket_ID/Ticket_Description(support Fuzzy query)
            @notes: /Handlers/ticket/search

            @param key: key
            @type key: string

            @param type: type
            @type type: string

            @return 200: {"data": "..."}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="ticket/search").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.SearchHandler.post(): {0}, begin action".format(session.email))
        spec = {}
        if not self.get_argument("key", None):
            raise gen.Return({"data": []})
        if self.get_argument('key', None):
            spec = {"$or": [
                {"description": {
                    '$regex': self.get_argument('key', None), '$options': 'i'}
                 },
                {"id": self.get_argument('key', None)}]}

        # 查询类型查询,支持查询bug,account...
        rule_list = []
        type_ = self.get_argument('type', None)
        if type_:
            rule_list.append({"type": type_})
        status = self.get_arguments('status')
        if status:
            rule_list.append({"status": {"$in": status}})
        resolvegroupIds = self.get_arguments("resolvegroupId")
        if resolvegroupIds:
            rule_list.append({"resolvegroupId": {"$in": resolvegroupIds}})

        sort_by = self.get_argument('sort_by', None)
        order = self.get_argument('order', pymongo.DESCENDING)
        order = int(order) if isinstance(order, basestring) else order

        page = self.get_argument('page', 1)
        page = int(page) if page else 1
        if page < 0:
            logging.warning(
                "Ticket.TicketHandler.get(): {0}, invalid input page number {1}".format(session.email, page))
            page = 1
        limit = self.get_argument('limit', DEFAULT_LIMIT_NUMBER)
        try:
            limit = int(limit) if limit else DEFAULT_LIMIT_NUMBER
        except Exception:
            raise APIError(400, message='limit is invalid parameters')
        # tickets = yield AppServiceGlobal.db.ticket.find(
        result = []
        rule_list.append(spec)
        spec = {"$and": rule_list}
        cursor = AppServiceGlobal.db.ticket.aggregate([
            {"$match": spec},
            {"$project": {
                "_id": 0,
                "id": 1,
                "status": 1,
                "responser": 1,
                "description": 1,
                "flag": 1,
                "requester": 1,
                "created_time": 1,
                "resolvegroupId": 1,
                "severity": 1,
                "cc_list": 1,
                "updated_time": 1,
                "note": 1,
                "details": 1,
                "ctiId": 1,
                "type": 1,
                "rootcause_id": 1,
                "related_tickets": 1,
                "duplicated_tickets": 1,

                "age": {
                    "$cond": {
                        "if": {
                            "$or": [{
                                "$eq": ["$status", 'closed']
                            }, {
                                "$eq": ["$status", 'resolved']
                            }]
                        },
                        "then": {
                            "$divide": [{"$subtract": ["$updated_time", '$created_time']}, 1]
                        },
                        "else": {
                            # "$divide": [{"$subtract": [datetime.utcnow(), '$created_time']}, 1]
                            "$divide": [{"$subtract": [datetime.utcnow(), '$created_time']}, 1]
                        }
                    }
                }
            }
            },
            {
                "$sort": {sort_by or "age": order or -1}
            },
            {"$skip": (page - 1) * limit},
            {"$limit": limit}
            # {"$skip": (page - 1) * limit},
            # {"$limit": limit}
        ])
        while (yield cursor.fetch_next):
            doc = cursor.next_object()
            result.append(doc)
        # logging.debug('-------tickets------- {}'.format(type(tickets)))
        # 需要新增一个age显示字段，用户表示当前ticket从创建到现在已经存在多久了
        # [状态为open,process,pending则age为创建到现在]
        # [状态为resolve, closed则age为创建到完成]
        # tickets = json.loads(json_util.dumps(tickets, default=json_util.default))
        tickets = json.loads(json_util.dumps(result, default=json_util.default))
        # ).to_list(None)
        # new_tickets = []
        # for ticket in tickets:
        #     if ticket.get('status') in ['closed', 'resolved']:
        #         ticket['age'] = int((ticket.get('updated_time') - ticket.get('created_time')).total_seconds())
        #     else:
        #         ticket['age'] = int((datetime.utcnow() - ticket.get('created_time')).total_seconds())
        #     new_tickets.append(ticket)

        # tickets = json.loads(json_util.dumps(tickets, default=json_util.default))
        # tickets = json.loads(json_util.dumps(new_tickets, default=json_util.default))
        logging.info("Ticket.SearchHandler.post(): {0}, action success".format(session.email))
        raise gen.Return({"data": tickets})


class TicketHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /ticker/ticketer/
    """

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "type": {"type": "string", "minLength": MIN_LENGTH},
                "severity": {"type": "integer", "minimum": 1, "maximum": SUPPORTED_SEVERITIES_COUNT},
                "description": {"type": "string", "minLength": MIN_LENGTH},
                "requester": {"type": "string", "minLength": MIN_LENGTH},
                "responser": {"type": "string", "minLength": MIN_LENGTH},
                "details": {"type": "object"},
                "ctiId": {"type": "string", "minLength": MIN_LENGTH},
                "category": {"type": "string", "minLength": MIN_LENGTH},
                "attachment_ids": {"type": "array"}
            },
            "required": ["type", "severity", "description", "requester"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='post_ticket')
    def post(self):
        """
            @description: Create a new ticket.
            @notes:

            @param type: [bug, authorization, workflow]
            @type type: string

            @param severity: incident level [1-5]
            @type severity: int

            @param description: description about the incident
            @type description: string

            @param requester: user who report the ticket/on duty(user id)
            @type requester: string

            @param responser: user who handler the incident(user id)
            @type responser: string

            @param resolvegroupId: resolvegroup Id
            @type resolvegroupId: string

            @param details: detail information abount the ticket
            @type details: dict

            @param attachment_ids: attachment id list
            @type attachment_ids: list

            @return 200: {"ticket_id": ticket_id}
            @raise 400: invalid input
        """
        print("-----------------start -----time -------", datetime.utcnow())
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="ticket").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.TicketHandler.post(): {0}, begin action".format(session.email))
        print("-----------------start check auth end -----time -------", datetime.utcnow())
        current_time = datetime.utcnow()
        ticket = {
            'type': self.body.get('type').lower(),
            'severity': self.body.get('severity'),
            'description': self.body.get('description'),
            'requester': self.body.get('requester'),
            'responser': self.body.get('responser', None),
            'resolvegroupId': self.body.get('resolvegroupId', None),
            'details': self.body.get('details', {}),
            'ctiId': self.body.get('ctiId', None),
            'category': self.body.get('category', None),
            'status': TICKET_STATE_OPENED,
            'note': self.body.get('note', None),
            # 'cc_list': [self.body.get('responser')] if self.body.get('responser', None) else [self.body.get('requester')],
            'created_time': current_time,
            'updated_time': current_time,
            'attachment': self.body.get('attachment_ids', [])
        }
        # update department if not specified
        if not ticket['ctiId']:
            depart = parse_department(ticket['details']['hostname']) if 'hostname' in ticket['details'] else None
            depName = depart[1] if depart else None
            doc = {
                'alias': depName
            }
            res = yield self._query_cti_by_doc(doc)
            ticket['ctiId'] = res[0]['id'] if res and isinstance(res, list) else None

        # set responser if not sepcified
        if not ticket['responser']:
            resolveGroupInfo = []
            if ticket['resolvegroupId']:
                logging.debug('Ticket.TicketHandler.Post() resolvegroup is Null')
                doc = {
                    'id': {
                        '$in': [ticket['resolvegroupId']]
                    }
                }
                resolveGroupInfo = yield self._query_resolvegroup_info(doc)
            if resolveGroupInfo:
                ticket['responser'] = yield self._get_the_schedule(current_time, resolveGroupInfo[0].get('id'))
            else:
                doc = {
                    'ctis': {
                        '$in': [ticket['ctiId']]
                    }
                }
                resolveGroupInfo = yield self._query_resolvegroup_info(doc)
                ticket['responser'] = yield self._get_the_schedule(current_time, resolveGroupInfo[0].get('id'))
        print("-----------------start rc create ticket -----time -------", datetime.utcnow())
        ticket_id = yield self._create_rc_ticket(session.email, ticket)
        print("-----------------start rc end create ticket-----time -------", datetime.utcnow())
        if not ticket_id:
            logging.error("Ticket.TicketHandler.post(): {0}, create new ticket failed, {1} | {2}".format(session.email,
                                                                                                         ticket_id,
                                                                                                         ticket['description']))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        # only bug ticket to create duty task of the ticket
        if ticket['type'] == 'bug':
            data = {
                'job_id': ticket_id
            }
            response = yield self._scheduler(method='POST', params=data)
            if not response:
                logging.error("Ticket.TicketHandler.post(): {0}, create duty task for ticket {1} failed".format(session.email, ticket_id))

        logging.info("Ticket.TicketHandler.post(): {0}, action success".format(session.email))
        raise gen.Return({"ticket_id": ticket_id})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "ticket_id": {"type": "string", "minLength": MIN_LENGTH},
                "severity": {"type": "integer", "minimum": 1, "maximum": SUPPORTED_SEVERITIES_COUNT},
                "description": {"type": "string", "minLength": MIN_LENGTH},
                "responser": {"type": "string", "minLength": MIN_LENGTH},
                "status": {"type": "string", "minLength": MIN_LENGTH},
                "cc_list": {"type": "array"},
                "details": {"type": "object"},
                "department": {"type": "string", "minLength": MIN_LENGTH},
                "category": {"type": "string", "minLength": MIN_LENGTH},
                "rootcause_id": {"type": "string", "minLength": MIN_LENGTH},
            },
            "required": ["ticket_id"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='put_ticket')
    def put(self):
        """
            @description: Update the ticket.
            @notes:

            @param ticket_id: ticket id
            @type ticket_id: string

            @param status: ticket status
            @type status: string

            @param severity: incident level [1-5]
            @type severity: int

            @param description: description about the incident
            @type description: string

            @param requester: user who report the ticket/on duty(user id)
            @type requester: string

            @param rootcause_id: rootcause
            @type rootcause_id: string

            @param resolvegroupId: resolvegroup Id
            @type resolvegroupId: string

            @param details: detail information abount the ticket
            @type details: dict

            @return 200: {"message": "update ticket {0} successfully"}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='PUT', endpoint="ticket").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.TicketHandler.put(): {0}, begin action".format(session.email))

        ticket_id = self.body.get('ticket_id')

        setting = {
            'updated_time': datetime.utcnow()
        }
        pushing = []
        severity = self.body.get('severity', None)
        if severity:
            setting['severity'] = severity
        description = self.body.get('description', None)
        if description:
            setting['description'] = description
        responser = self.body.get('responser', None)
        if responser:
            setting['responser'] = responser
            pushing.append(responser)
        ctiId = self.body.get('ctiId', None)
        if ctiId:
            setting['ctiId'] = ctiId
        resolvegroupId = self.body.get('resolvegroupId', None)
        if resolvegroupId:
            setting['resolvegroupId'] = resolvegroupId
        details = self.body.get('details', None)
        if details:
            setting['details'] = details
        status = self.body.get('status', None)
        if status:
            setting['status'] = status
        cc_list = self.body.get('cc_list', None)
        if cc_list:
            # pushing = list(set(pushing + cc_list))
            setting['cc_list'] = list(set(pushing + cc_list))
        category = self.body.get('category', None)
        if category:
            setting['category'] = category

        rootcause_id = self.body.get('rootcause_id', None)
        if rootcause_id:
            setting['rootcause_id'] = rootcause_id

        note = self.body.get('note', None)

        if setting:
            ticket = yield AppServiceGlobal.db.ticket.find_one(
                {
                    'id': ticket_id
                }
            )
            if not ticket:
                logging.error("Ticket.TicketHandler.put(): {0}, can not find the ticket {1}".format(session.email, ticket_id))
                raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")
            if isinstance(note, dict):
                setting["note"] = note.get("reason") or None
            res = yield self._update_rc_ticket(session.email,
                                               ticket,
                                               setting,
                                               note=note)
            if not res:
                logging.error("Ticket.TicketHandler.put(): {0}, update ticket {1} failed".format(session.email, ticket_id))
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

            # update ticket status
            if ticket['type'] == 'bug' and ticket['status'] != status:
                data = {
                    'job_id': ticket_id
                }
                response = yield self._scheduler(method='POST', params=data)
                if not response:
                    logging.error("Ticket.TicketHandler.put(): {0}, update duty task for ticket {1} failed".format(
                        session.email, ticket_id))

        logging.info("Ticket.TicketHandler.put(): {0}, action success".format(session.email))
        raise gen.Return({"message": "update ticket {0} successfully".format(ticket_id)})

    @validate(
        output_schema={
            "type": "object"
            # "properties": {
            #     "data": {"type": "array"}
            # }
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='get_ticket')
    def get(self):
        """
            @description: Get special ticket or tickets.
            @notes: uri: /ticket/?ticketId=<ticket_id>&requester=<requester>&responser=<responser>
            &severity=<severity>&status=<status>&from=<started_time_string>&to=<ended_time_string>
            &page=page&limit=limit&ctiId=<ctiId>&type=<type>&flag=<flag>

            @param ticketId: ticketId
            @type ticketId: string

            @param requester: requester
            @type requester: string

            @param responser: responser
            @type responser: string

            @param severity: severity
            @type severity: int

            @param status: status
            @type status: string

            @param from: started_time_string
            @type from: string

            @param to: ended_time_string
            @type to: string

            @param status: status
            @type status: string

            @param flag: flag
            @type flag: string

            @param page: page
            @type page: int

            @param limit: limit
            @type limit: int

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="ticket").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.TicketHandler.get(): {0}, begin action".format(session.email))

        spec = {}
        account = self.get_argument('type', None)
        ticket_ids = self.get_arguments('ticketId')
        flag = self.get_argument('flag', None)
        sort_by = self.get_argument('sort_by', None)
        order = self.get_argument('order', pymongo.DESCENDING)
        order = int(order) if isinstance(order, basestring) else order
        if account and isinstance(account, basestring) and account.lower() == 'account':
            # user register info
            spec = {"$and": [{}, {"type": "account"}]}

        elif ticket_ids and isinstance(ticket_ids, list):
            spec = {'id': {
                '$in': [str(tid) for tid in ticket_ids if tid]
                }
            }

        elif flag and flag.lower() == 'index':
            # Find out All tickets about me
            queryKeys = []
            ctiId = yield self._or_query('ctiId', session)
            requester = yield self._or_query('requester', session)
            responser = yield self._or_query('responser', session)
            cc_list = yield self._or_query('cc_list', session)
            queryKeys.append(ctiId)
            queryKeys.append(requester)
            queryKeys.append(responser)
            queryKeys.append(cc_list)
            spec = {"$and": [{"$or": queryKeys}, {"type": "bug"}]}

        elif flag and flag.lower() == 'admin':
            filters = [{"type": "bug"}]
            severity = self.get_arguments('severity')
            if severity and isinstance(severity, list):
                filters.append({'severity': {'$in': [int(i) for i in severity]}})

            status = self.get_arguments('status')
            if status and isinstance(status, list):
                filters.append({'status': {'$in': status}})
            spec = {"$and": filters}
        else:
            queryKeys = list()
            joint = list()

            resolvegroup = self.get_arguments('resolvegroupId')
            if resolvegroup:
                joint.append({'resolvegroupId': {'$in': resolvegroup}})
            doc = {'users.uid': {'$in': [session.uid]}}
            rgInfo = yield self._query_resolvegroup_info(doc, session=session)
            rgIds = [rg.get('id') for rg in rgInfo if rg.get('id')]
            queryKeys.append({'resolvegroupId': {'$in': rgIds}})

            requester = self.get_arguments('requester')
            if requester and isinstance(requester, list):
                joint.append({'requester': {'$in': requester}})
            # else:
            spec = yield self._or_query('requester', session)
            queryKeys.append(spec)

            responser = self.get_arguments('responser')
            if responser and isinstance(responser, list):
                joint.append({'responser': {'$in': responser}})
            # else:
            spec = yield self._or_query('responser', session)
            queryKeys.append(spec)

            cc_list = self.get_arguments('cc_list')
            if cc_list and isinstance(cc_list, list):
                joint.append({'cc_list': {'$in': cc_list}})
            # else:
            spec = yield self._or_query('cc_list', session)
            queryKeys.append(spec)

            filters = []
            severity = self.get_arguments('severity')
            if severity and isinstance(severity, list):
                spec = {'severity': {'$in': [int(i) for i in severity]}}
                filters.append(spec)

            status = self.get_arguments('status')
            if status and isinstance(status, list):
                spec = {'status': {'$in': status}}
                filters.append(spec)

            started_time_string = self.get_argument('from', None)
            if started_time_string:
                spec = {'updated_time': {'$gte': datetime.strptime(started_time_string, DATE_FORMAT) - timedelta(hours=CHINA_TIME_ZONE)}}
                filters.append(spec)

            ended_time_string = self.get_argument('to', None)
            if ended_time_string:
                spec = {}
                if 'updated_time' not in spec:
                    spec['updated_time'] = {}
                spec['updated_time'].update(
                    {'$le': datetime.strptime(ended_time_string, DATE_FORMAT) - timedelta(hours=CHINA_TIME_ZONE)}
                )
                filters.append(spec)

            ticket_types = self.get_arguments('type')
            if ticket_types:
                spec = {'type': {'$in': ticket_types}}
                filters.append(spec)

            spec = {'$and': [{'$or': queryKeys}] + filters + joint}
            # spec = json.loads(json.dumps(spec))

        page = self.get_argument('page', 1)
        page = int(page) if page else 1
        if page < 0:
            logging.warning("Ticket.TicketHandler.get(): {0}, invalid input page number {1}".format(session.email, page))
            page = 1
        limit = self.get_argument('limit', DEFAULT_LIMIT_NUMBER)
        try:
            limit = int(limit) if limit else DEFAULT_LIMIT_NUMBER
        except Exception:
            raise APIError(400, message='limit is invalid parameters')

        logging.debug('Ticket.get_ticket.get() ----- {spec} {limit}'.format(spec=spec, limit=limit))
        count = yield AppServiceGlobal.db.ticket.find(
            spec
        ).count()

        sort_doc = []
        if sort_by is not None:
            sort_doc.append((sort_by, order))
        if sort_by is None or sort_by != 'updated_time':
            # 如果指定排序，或者按照其他字段排序，默认再添加一个按更新时间排序
            sort_doc.append(('updated_time', pymongo.DESCENDING))

        result = []
        cursor = AppServiceGlobal.db.ticket.aggregate([
            {"$match": spec},
            {"$project": {
                "_id": 0,
                "id": 1,
                "status": 1,
                "responser": 1,
                "description": 1,
                "flag": 1,
                "requester": 1,
                "created_time": 1,
                "resolvegroupId": 1,
                "severity": 1,
                "cc_list": 1,
                "updated_time": 1,
                "note": 1,
                "details": 1,
                "ctiId": 1,
                "type": 1,
                "rootcause_id": 1,
                "related_tickets": 1,
                "duplicated_tickets": 1,
                "attachment": 1,

                "age": {
                    "$cond": {
                        "if": {
                            "$or": [{
                                "$eq": ["$status", 'closed']
                            }, {
                                "$eq": ["$status", 'resolved']
                            }]
                        },
                        "then": {
                            "$divide": [{"$subtract": ["$updated_time", '$created_time']}, 1]
                        },
                        "else": {
                            "$divide": [{"$subtract": [datetime.utcnow(), '$created_time']}, 1]
                        }
                    }
                }
            }
            },
            {
                "$sort": {sort_by or "age": order or -1}
            },
            {"$skip": (page - 1) * limit},
            {"$limit": limit}
        ]
        )
        while (yield cursor.fetch_next):
            doc = cursor.next_object()
            attachment_ids = doc.get('attachment', [])
            if attachment_ids:
                attachments = yield AppServiceGlobal.db.attachment.find({'id': {'$in': attachment_ids}},
                                                                        {'_id': None}).to_list(None)
                if attachments:
                    doc['attachment_content'] = attachments
            result.append(doc)
        tickets = json.loads(json_util.dumps(result, default=json_util.default))
        # 返回列表中需要包含CTI/RG的完整信息
        try:
            # logging.debug('-------start time--------- {}'.format(datetime.now()))
            res = yield AppServiceGlobal.db.cti.find(
                {},
                {'_id': False}
            ).to_list(None)
            if not res:
                logging.warn("AppServiceUtil._query_all_CTI_info(): query DB CTI failed")
                # raise gen.Return(False)
            allCTI = json.loads(json_util.dumps(res, default=json_util.default))
            for ticket in tickets:
                ticket['ctiInfo'] = yield self._query_all_CTI_info(
                    ctiList=[ticket.get("ctiId")], allCTI=allCTI)
        except Exception:
            pass
        # logging.debug('-------end time--------- {}'.format(datetime.now()))
        logging.info("Ticket.TicketHandler.get(): {0}, action success".format(session.email))
        raise gen.Return({"data": tickets, "total": count})

    @gen.coroutine
    def _or_query(self, keyword, session):
        querySpec = {}
        if keyword == 'ctiId':
            # 根据登录用户查找用户id,根据用户id查找rg，根据rg找出所有的ctiId
            loginUserInfo = yield self._query_account(email=session.email, session=session)
            values = []
            with ignored(Exception):
                uid = loginUserInfo[0].get('id')
                doc = {'users.uid': {'$in': [uid]}}
                rgInfo = yield self._query_resolvegroup_info(doc)
                rgCtiId = rgInfo[0].get('ctis')
                values = list(set(rgCtiId))
        else:
            values = [session.email + DEFAULT_EMAIL_SUFFIX if not is_email(session.email) else session.email]
        if isinstance(values, list):
            querySpec[keyword] = {'$in': values}
        elif isinstance(values, basestring):
            querySpec[keyword] = {'$in': [values]}
        else:
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        raise gen.Return(querySpec)


class AttachmentHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /ticket/attachment
    """

    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @gen.coroutine
    def delete(self):
        AppServiceGlobal.REQUESTS.labels(method='DELETE', endpoint="ticket/attachment").inc()
        session = self.check_authorization(
            AppServiceGlobal.session_manager, self.request)
        logging.info("%s, delete object begin action." % session.email)

        ticket_id = self.get_argument('ticket_id', None)
        attachment_id = self.get_argument('attachment_id')

        spec = {'id': attachment_id}
        att = yield AppServiceGlobal.db.attachment.find_one(spec)
        if not att:
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_DB")
        res = yield AppServiceGlobal.db.attachment.remove(spec)
        if not res:
            logging.info("Ticket.AttachmentHandler.delete(): {0}, delete ticket attachment failed".format(session.email))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_DB")

        if ticket_id:
            spec = {'id': ticket_id}
            setting = {'$pull': {'attachment': attachment_id}}
            res = yield AppServiceGlobal.db.ticket.update(spec, setting)
            if not res:
                logging.info(
                    "Ticket.AttachmentHandler.delete(): {0}, delete ticket attachment failed".format(
                        session.email))
                raise APIError(400, flag="COMMON.GLOBAL.ERROR_DB")

        raise gen.Return({'message': 'success'})


class RelatedHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /ticket/related
    """

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "ticket_id": {"type": "string", "minLength": MIN_LENGTH},
                "related_id": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["ticket_id", "related_id"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @gen.coroutine
    @swagger.operation(nickname='post_related')
    def post(self):
        """
            @description: Add a related ticket
            @notes:

            @param ticket_id: ticket_id
            @type ticket_id: string

            @param related_id: related_id
            @type related_id: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="ticket/relate").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.RelatedHandler.post(): {0}, begin action".format(session.email))

        ticket_id = self.body.get('ticket_id', None)
        related_ticket = self.body.get('related_id', None)

        setting = {}
        if related_ticket.startswith('case-'):
            related_id = related_ticket
        # input ticket url
        elif related_ticket.count('/') and related_ticket.split('/')[-1].startswith('case-'):
            related_id = related_ticket.split('/')[-1]
        else:
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

        ticket = yield AppServiceGlobal.db.ticket.find_one(
            {
                'id': ticket_id
            }
        )
        if not ticket:
            logging.error(
                "Ticket.RelatedHandler.post(): {0}, can not find the ticket {1}".format(session.email, ticket_id))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

        related_tickets = ticket.get('related_tickets', [])
        related_tickets.append(related_id)
        setting['related_tickets'] = list(set(related_tickets))

        # update ticket
        try:
            res = yield self._update_rc_ticket(session.email, ticket, setting)
        except Exception:
            res = True
        if not res:
            logging.error("Ticket.RelatedHandler.post(): {0}, update ticket {1} failed".format(session.email, ticket_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        raise gen.Return({'message': 'success'})

    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @gen.coroutine
    @swagger.operation(nickname='delete_related')
    def delete(self):
        """
            @description: Delete a related ticket
            @notes:

            @param ticket_id: ticket_id
            @type ticket_id: string

            @param related_id: related_id
            @type related_id: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='DELETE', endpoint="ticket/related").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.RelatedItemsHandler.delete(): {0}, begin action".format(session.email))

        ticket_id = self.get_argument('ticket_id', None)
        related_id = self.get_argument('related_id', None)

        ticket = yield AppServiceGlobal.db.ticket.find_one(
            {
                'id': ticket_id
            }
        )
        if not ticket:
            logging.error(
                "Ticket.TicketHandler.put(): {0}, can not find the ticket {1}".format(session.email, ticket_id))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

        setting = {}
        related_tickets = ticket.get('related_tickets', [])
        if related_id in related_tickets:
            related_tickets.remove(related_id)
        setting['related_tickets'] = related_tickets

        # update ticket
        res = yield self._update_rc_ticket(session.email, ticket, setting)
        if not res:
            logging.error("Ticket.TicketHandler.put(): {0}, update ticket {1} failed".format(session.email, ticket_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        raise gen.Return({'message': 'success'})


class DuplicatedHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /ticket/duplicated
    """

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "ticket_id": {"type": "string", "minLength": MIN_LENGTH},
                "duplicated_id": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["ticket_id", "duplicated_id"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @gen.coroutine
    @swagger.operation(nickname='post_duplicated')
    def post(self):
        """
            @description: Add a duplicated ticket
            @notes:

            @param ticket_id: ticket_id
            @type ticket_id: string

            @param duplicated_id: duplicated_id
            @type duplicated_id: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="ticket/duplicated").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.DuplicatedHandler.post(): {0}, begin action".format(session.email))

        ticket_id = self.body.get('ticket_id', None)
        duplicated_ticket = self.body.get('duplicated_id', None)

        setting = {}
        if duplicated_ticket.startswith('case-'):
            duplicated_id = duplicated_ticket
        # input ticket url
        elif duplicated_ticket.count('/') and duplicated_ticket.split('/')[-1].startswith('case-'):
            duplicated_id = duplicated_ticket.split('/')[-1]
        else:
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

        ticket = yield AppServiceGlobal.db.ticket.find_one(
            {
                'id': ticket_id
            }
        )
        if not ticket:
            logging.error(
                "Ticket.DuplicatedHandler.post(): {0}, can not find the ticket {1}".format(session.email, ticket_id))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

        duplicated_tickets = ticket.get('duplicated_tickets', [])
        duplicated_tickets.append(duplicated_id)
        setting['duplicated_tickets'] = list(set(duplicated_tickets))

        # update ticket
        res = yield self._update_rc_ticket(session.email, ticket, setting)
        if not res:
            logging.error(
                "Ticket.DuplicatedHandler.post(): {0}, update ticket {1} failed".format(session.email, ticket_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        raise gen.Return({'message': 'success'})

    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @gen.coroutine
    @swagger.operation(nickname='delete_duplicated')
    def delete(self):
        """
            @description: Delete a duplicated ticket
            @notes:

            @param ticket_id: ticket_id
            @type ticket_id: string

            @param duplicated_id: duplicated_id
            @type duplicated_id: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='DELETE', endpoint="ticket/duplicated").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.DuplicatedHandler.delete(): {0}, begin action".format(session.email))

        ticket_id = self.get_argument('ticket_id', None)
        duplicated_id = self.get_argument('duplicated_id', None)

        ticket = yield AppServiceGlobal.db.ticket.find_one(
            {
                'id': ticket_id
            }
        )

        if not ticket:
            logging.error(
                "Ticket.DuplicatedHandler.delete(): {0}, can not find the ticket {1}".format(session.email, ticket_id))
            raise APIError(400, flag="COMMON.GLOBAL.ERROR_PARAM")

        setting = {}
        duplicated_tickets = ticket.get('duplicated_tickets', [])
        if duplicated_id in duplicated_tickets:
            duplicated_tickets.remove(duplicated_id)
        setting['duplicated_tickets'] = duplicated_tickets

        # update ticket
        res = yield self._update_rc_ticket(session.email, ticket, setting)
        if not res:
            logging.error("Ticket.DuplicatedHandler.delete(): {0}, update ticket {1} failed".format(session.email, ticket_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        raise gen.Return({'message': 'success'})


class WatcherHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /ticket/watcher/
    """

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "ticket_id": {"type": "string", "minLength": MIN_LENGTH}
            },
            "required": ["ticket_id"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='post_watch')
    def post(self):
        """
            @description: Watch a ticket.
            @notes:

            @param ticket_id: ticket_id
            @type ticket_id: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="ticket/watcher").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.WatcherHandler.post(): {0}, begin action".format(session.email))

        ticket_id = self.body.get('ticket_id')
        # cc_list = self.body.get('')

        res = yield AppServiceGlobal.db.ticket.update(
            {
                'id': ticket_id
            },
            {
                '$set': {
                    'updated_time': datetime.utcnow()
                },
                '$addToSet': {
                    'cc_list': session.email
                }
            },
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("Ticket.WatcherHandler.post(): {0}, update ticket {1} failed".format(session.email, ticket_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        logging.info("Ticket.WatcherHandler.post(): {0}, action success".format(session.email))
        raise gen.Return({"message": "Watched ticket {0} successfully".format(ticket_id)})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='delete_watch')
    def delete(self):
        """
            @description: Unwatch the ticket.
            @notes:

            @param ticket_id: ticket_id
            @type ticket_id: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='DELETE', endpoint="ticket/watcher").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.WatcherHandler.delete(): {0}, begin action".format(session.email))

        ticket_ids = self.get_arguments('ticket_id')

        if ticket_ids and isinstance(ticket_ids, list):
            res = yield AppServiceGlobal.db.ticket.update(
                {
                    'id': {
                        '$in': ticket_ids
                    }
                },
                {
                    '$set': {
                        'updated_time': datetime.utcnow()
                    },
                    '$pull': {
                        'cc_list': session.email
                    }
                },
                multi=True,
                w=AppServiceGlobal.config.mongo_db_copy
            )
            if not res:
                logging.error("Ticket.WatcherHandler.delete(): {0}, remove user from ticket {1} watchlist failed".format(session.email, ticket_ids))
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        logging.info("Ticket.WatcherHandler.delete(): {0}, action success".format(session.email))
        raise gen.Return({"message": "Unwatched tickets {0} successfully".format(ticket_ids)})


class CommentHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /ticket/comment/
    """

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='get_comment')
    def get(self):
        """
            @description: Get a comment
            @notes: /ticket/comment/?ticketId=<ticket_id>&commentId=<comment_id>

            @param ticketId: ticketId
            @type ticketId: string

            @param commentId: commentId
            @type commentId: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="ticket/comment").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.StatisticHandler.get(): {0}, begin action".format(session.email))

        comments = []
        commentId = self.get_argument('commentId', None)
        if commentId is not None:
            comments = yield AppServiceGlobal.db.comment.find_one({'id': commentId}, {'_id': False})

        elif self.get_argument('ticketId') is not None:
            ticketId = self.get_argument('ticketId')
            comments = yield AppServiceGlobal.db.comment.find(
                {'ticket_id': ticketId, 'reply_tid': {'$exists': False}},
                {'_id': False}
            ).to_list(None)

            for comment in comments:
                subComments = yield AppServiceGlobal.db.comment.find(
                    {'ticket_id': ticketId, 'reply_tid': comment.get('id')},
                    {'_id': False}).sort('created_time', 1).to_list(None)
                comment['comments'] = subComments

        if not comments:
            raise gen.Return({'data': []})

        uids = self.get_user_ids(comments)
        users = []
        for uid in uids:
            userInfoTmp = yield self._query_account(uid=uid, session=session)
            users = userInfoTmp + users
        yield self.format_user(comments, users)

        comments = json.loads(json_util.dumps(comments or [], default=json_util.default))
        raise gen.Return({'data': comments})

    @coroutine
    def format_user(self, data, users):
        if isinstance(data, list):
            for d in data:
                self.format_user(d, users)
        elif isinstance(data, dict):
            for k, v in data.items():
                if isinstance(v, (dict, list)):
                    self.format_user(v, users)
                if k in ['user', 'reply_uid'] and isinstance(v, basestring):
                    for user in users:
                        if user.get('id') == v:
                            data[k] = user

    def get_user_ids(self, data):
        uids = []
        if isinstance(data, list):
            for d in data:
                uids.extend(self.get_user_ids(d))
        elif isinstance(data, dict):
            for k, v in data.items():
                if isinstance(v, (dict, list)):
                    uids.extend(self.get_user_ids(v))
                if k == 'user' and isinstance(v, basestring):
                    uids.append(v)

        return uids

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "ticket_id": {"type": "string", "minLength": MIN_LENGTH},
                "content": {"type": "string", "minimum": MIN_LENGTH},
                "at": {
                    "type": "array",
                    "items": {"format": "email"}
                },
                "reply_tid": {"type": "string", "minLength": MIN_LENGTH},
                "reply_cid": {"type": "string"}
            },
            "required": ["ticket_id", "content"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='post_comment')
    def post(self):
        """
            @description: Add a comment
            @notes:

            @param ticket_id: ticket_id
            @type ticket_id: string

            @param content: content
            @type content: string

            @param at: '@'user list
            @type at: string

            @param reply_tid: reply_tid
            @type reply_tid: string

            @param reply_cid: reply_cid
            @type reply_cid: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="ticket/comment").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.CommentHandler.post(): {0}, begin action".format(session.email))

        ticket_id = self.body.get('ticket_id')
        content = self.body.get('content')
        user = session.uid
        at_list = self.body.get('at')
        reply_tid = self.body.get('reply_tid', None)
        reply_cid = self.body.get('reply_cid', None)

        comment = {
            'id': 'cmt-{0}'.format(str(uuid.uuid4()).split('-')[-1]),
            'ticket_id': ticket_id,
            'content': content,
            'user': user,
            'created_time': datetime.utcnow()
        }

        if at_list is not None:
            comment['at'] = at_list
        if reply_tid is not None:
            comment['reply_tid'] = reply_tid
        if reply_cid is not None:
            comment['reply_cid'] = reply_cid

        if (reply_cid and reply_cid != reply_tid):
            reply_ticket = yield AppServiceGlobal.db.comment.find_one({'id': reply_cid})
            reply_uid = reply_ticket.get('user') if reply_ticket else None
            if reply_uid is not None:
                comment['reply_uid'] = reply_uid

        res = yield AppServiceGlobal.db.comment.insert(comment)
        if not res:
            logging.error("Ticket.CommentHandler.post(): {0}, add comment {1} failed".format(session.email, ticket_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        raise gen.Return({"message": "add comment for ticket {0} successfully".format(ticket_id)})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='delete_comment')
    def delete(self):
        """
            @description: Delete a comment
            @notes: /ticket/comment/?commentId=<commentId>

            @param commentId: commentId
            @type commentId: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='DELETE', endpoint="ticket/comment").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.StatisticHandler.get(): {0}, begin action".format(session.email))

        comment_id = self.get_argument('commentId')
        res = yield AppServiceGlobal.db.comment.remove(
            {'id': comment_id}
        )
        if not res:
            logging.error("Ticket.CommentHandler.delete(): {0}, remove comment {1} failed".format(session.email, comment_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        raise gen.Return({'message': "success"})


class StatisticHandler(APIHandler, AppServiceUtil):
    """
        endpoint: /ticket/statistic/
    """

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='get_statistic')
    def get(self):
        """
            @description: Get ticket statistic.
            @notes: /ticket/statistic/?severity=<severity>&status=<status>&ctiId=<ctiId>&from=<started_time_string>&to=<ended_time_string>&type=<type>

            @param severity: severity
            @type severity: string

            @param status: status
            @type status: string

            @param from: started_time_string
            @type from: datetime

            @param to: ended_time_string
            @type to: datetime

            @param type: ticket type
            @type type: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="ticket/statistic").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.StatisticHandler.get(): {0}, begin action".format(session.email))

        params = {}
        # loginUserInfo = yield self._query_account(email=session.email)
        # ctiId = []
        uid = session.uid
        # with ignored(Exception):
        #     uid = loginUserInfo[0].get('id')
#             doc = {'users': {'$in': [uid]}}
#             rgInfo = yield self._query_resolvegroup_info(doc)
#             # ctiId = rgInfo[0].get('ctis')
#             ctiId = list(set([ctiId.extend(ctis.get('ctis')) for ctis in rgInfo if ctis.get('ctis')]))

        user = [session.email + DEFAULT_EMAIL_SUFFIX if not is_email(session.email) else session.email]
        requester = user
        responser = user
        cc_list = user
        # params['ctiId'] = ctiId
        params['requester'] = requester
        params['responser'] = responser
        params['cc_list'] = cc_list

        resolvegroup = self.get_arguments('resolvegroupId')
        if resolvegroup:
            params['resolvegroup'] = resolvegroup
        else:
            doc = {'users.uid': {'$in': [uid]}}
            rgInfo = yield self._query_resolvegroup_info(doc)
            rgIds = [rg.get('id') for rg in rgInfo if rg.get('id')]
            params['resolvegroup'] = rgIds

        severity = self.get_arguments('severity')
        if severity:
            params['severity'] = severity
        status = self.get_arguments('status')
        if status:
            params['status'] = status

#         ticket_type = self.get_arguments('type')
#         if ticket_type:
#             params['type'] = ticket_type
        from_time_string = self.get_argument('from', None)
        if from_time_string:
            params['from'] = datetime.strptime(from_time_string, DATE_FORMAT)
        to_time_string = self.get_argument('to', None)
        if to_time_string:
            params['to'] = datetime.strptime(to_time_string, DATE_FORMAT)

        # package pipeline
        pipeline = []
        match = self.__match(**params)
        if match:
            pipeline.append(match)
        # logging.debug('----match-- {0}'.format(match))
        group = self.__group(**params)
        if group:
            pipeline.append(group)
        # logging.debug('----group-- {0}'.format(group))
        logging.debug("Ticket.StatisticHandler.get(): {0}, \n pipeline={1}".format(session.email, pipeline))

        try:
            result = {
                'result': []
            }
            cursor = AppServiceGlobal.db.ticket.aggregate(pipeline)
            while (yield cursor.fetch_next):
                doc = cursor.next_object()
                result['result'].append(doc)
        except Exception as error:
            logging.error("Ticket.StatisticHandler.get(): {0}, execute ticket statistic exception, \n{1}".format(session.email, str(error)))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        logging.info("Ticket.StatisticHandler.get(): {0}, action success".format(session.email))
        raise gen.Return(result)

    def __match(self, **kwargs):

        orTempSpec = []
        andTempSpec = []
        status_spec = {}
        if 'resolvegroup' in kwargs and kwargs['resolvegroup']:
            spec = {}
            spec['resolvegroupId'] = {
                '$in': kwargs['resolvegroup']
            }
            orTempSpec.append(spec)
        if 'status' in kwargs and kwargs['status']:
            # spec = {}
            status_spec['status'] = {
                '$in': kwargs['status']
            }
            # orTempSpec.append(spec)
        if 'type' in kwargs and kwargs['type']:
            spec = {}
            spec['type'] = {
                '$in': kwargs['type']
            }
            orTempSpec.append(spec)
#         if 'ctiId' in kwargs and kwargs['ctiId']:
#             spec = {}
#             spec['ctiId'] = {
#                 '$in': kwargs['ctiId']
#             }
#             orTempSpec.append(spec)
        if 'cc_list' in kwargs and kwargs['cc_list']:
            spec = {}
            spec['cc_list'] = {
                '$in': kwargs['cc_list']
            }
            orTempSpec.append(spec)
        if 'requester' in kwargs and kwargs['requester']:
            spec = {}
            spec['requester'] = {
                '$in': kwargs['requester']
            }
            orTempSpec.append(spec)
        if 'responser' in kwargs and kwargs['responser']:
            spec = {}
            spec['responser'] = {
                '$in': kwargs['responser']
            }
            orTempSpec.append(spec)
        if 'from' in kwargs and kwargs['from']:
            spec = {}
            spec['created_time'] = {
                '$gte': kwargs['from']
            }
            andTempSpec.append(spec)
        if 'to' in kwargs and kwargs['to']:
            spec = {}
            if 'created_time' not in spec:
                spec['created_time'] = {
                    '$lte': kwargs['to']
                }
            else:
                spec['created_time']['$lte'] = kwargs['to']
            andTempSpec.append(spec)

        andTempSpec.append({"type": "bug"})
        andTempSpec.append(status_spec)
        andTempSpec.append({'$or': orTempSpec})
        # merge_list = [{'$or': orTempSpec}, {"type": "bug"}, status_spec]
        # return {"$match": {"$and": merge_list}}
        return {"$match": {"$and": andTempSpec}}

    def __group(self, **kwargs):

        group_filter = {
            'count': {
                '$sum': 1
            },
            '_id': {}
        }

        # datetime group
        if 'from' in kwargs or 'to' in kwargs:
            now = datetime.utcnow()
            from_time = kwargs.get('from', now)
            to_time = kwargs.get('to', now)

            group_filter['_id'] = {
                'year': {'$year': '$created_time'},
                'month': {'$month': '$created_time'}
            }

            time_delta = to_time - from_time
            # default: time_delta in (3months, ...) then set interval into 1 month
            if time_delta > timedelta(days=14) and time_delta <= timedelta(days=90):  # time_delta in (2weeks, 3months] then set interval into 1 week
                group_filter['_id'].update(
                    {
                        'week': {'$week': '$created_time'}
                    }
                )
            if time_delta <= timedelta(days=14):  # time_delta in (0, 2weeks] then set interval into 1 day
                group_filter['_id'].update(
                    {
                        'day': {'$dayOfMonth': '$created_time'}
                    }
                )

        if 'status' in kwargs:
            group_filter['_id'].update(
                {
                    'status': '$status'
                }
            )

#         if 'ctiId' in kwargs:
#             group_filter['_id'].update(
#                 {
#                     'ctiId': '$ctiId'
#                 }
#             )

        if 'type' in kwargs:
            group_filter['_id'].update(
                {
                    'type': '$type'
                }
            )

#         # New Support request/responser/cc_list
#         if 'responser' in kwargs:
#             group_filter['_id'].update(
#                 {
#                     'responser': '$responser'
#                 }
#             )
#
#         if 'requester' in kwargs:
#             group_filter['_id'].update(
#                 {
#                     'requester': '$requester'
#                 }
#             )

        return {'$group': group_filter}


class NotifyHandler(APIHandler, AppServiceUtil):
    """
        Ticker Nofity List
    """
    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='get_notify')
    def get(self):
        """
            @description: Get ticket notify list.
            @notes: /Handlers/ticket/notify?ticketId=<ticketId>

            @param ticketId: ticketId
            @type ticketId: string

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='GET', endpoint="ticket/nofity").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.NotifyHandler.post(): {0}, begin action".format(session.email))

        ticket_id = self.get_argument('ticketId', None)
        ticket = yield AppServiceGlobal.db.notify.find(
            {"ticketId": ticket_id},
            {"_id": False, "created_time": False, "updated_time": False}
        ).to_list(None)

        logging.info("Ticket.NotifyHandler.post(): {0}, action success".format(session.email))
        raise gen.Return({"data": ticket})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "ticketId": {"type": "string", "minLength": MIN_LENGTH},
                "description": {"type": "string", "minLength": MIN_LENGTH},
                "user": {
                    "type": "array",
                    "items": {"type": "string", "minLength": MIN_LENGTH}
                },
                "email": {
                    "type": "array",
                    "items": {"type": "string", "minLength": MIN_LENGTH}
                },
                "wechat": {
                    "type": "array",
                    "items": {"type": "string", "minLength": MIN_LENGTH}
                },
                "sms": {
                    "type": "array",
                    "items": {"type": "string", "minLength": MIN_LENGTH}
                },
            },
            "required": ["ticketId"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='post_notify')
    def post(self):
        """
            @description: Add ticket notify.
            @notes: /Handlers/ticket/notify

            @param ticketId: ticketId
            @type ticketId: string

            @param description: description
            @type description: string

            @param user: user
            @type user: list

            @param email: email
            @type email: list

            @param wechat: wechat
            @type wechat: list

            @param sms: sms
            @type sms: list

            @return 200: {}
            @raise 400: invalid input
        """
        AppServiceGlobal.REQUESTS.labels(method='POST', endpoint="ticket/nofity").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.NotifyHandler.post(): {0}, begin action".format(session.email))
        current_time = datetime.utcnow()

        spec = {"ticketId": self.body.get("ticketId")}
        if (yield AppServiceGlobal.db.notify.find(spec).to_list(None)):
            # update
            setting = {}
            if self.body.get("user"):
                validEle = [user.strip() for user in self.body.get("user") if user.strip()]
                setting['user'] = {'$each': validEle or []}
            if self.body.get("email"):
                validEle = [email.strip() for email in self.body.get("email") if email.strip()]
                setting['email'] = {'$each': validEle or []}
            if self.body.get("wechat"):
                validEle = [wechat.strip() for wechat in self.body.get("wechat") if wechat.strip()]
                setting['wechat'] = {'$each': validEle or []}
            if self.body.get("sms"):
                validEle = [sms.strip() for sms in self.body.get("sms") if sms.strip()]
                setting['sms'] = {'$each': validEle or []}
            if self.body.get("other"):
                # validEle = [other for other in self.body.get("other") if other.strip()]
                setting['other'] = {'$each': self.body.get("other")}
            res = yield AppServiceGlobal.db.notify.update(
                spec,
                {
                    '$addToSet': setting
                },
                w=AppServiceGlobal.config.mongo_db_copy
            )
            if not res:
                logging.error("Ticket.NotifyHandler.post(): {0}, update nofity failed".format(session.email))
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        else:
            nofity = {
                'id': 'notify-{0}'.format(str(uuid.uuid4()).split('-')[-1]),
                'ticketId': self.body.get('ticketId').lower(),
                'description': self.body.get('description'),
                'user': self.body.get('user', []),
                'email': self.body.get('email', []),
                'wechat': self.body.get('wechat', []),
                'sms': self.body.get('sms', []),
                'other': self.body.get('other', None),
                'created_time': current_time,
                'updated_time': current_time
            }
            # 可以不用校验数据的有效性，取数据通知的时候需要做数据校验，以为存在用户删除，不会删除这边的情况
            res = yield AppServiceGlobal.db.notify.insert(nofity)
            if not res:
                logging.error("Ticket.NotifyHandler.post(): {0}, notify list failed".format(session.email))
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        raise gen.Return({"message": "{} is success".format(self.body.get("ticketId"))})

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "id": {"type": "string", "minLength": MIN_LENGTH},
                "ticketId": {"type": "string", "minLength": MIN_LENGTH},
                "description": {"type": "string", "minLength": MIN_LENGTH},
                "user": {
                    "type": "array",
                    "items": {"type": "string", "minLength": MIN_LENGTH}
                },
                "email": {
                    "type": "array",
                    "items": {"type": "string", "minLength": MIN_LENGTH}
                },
                "wechat": {
                    "type": "array",
                    "items": {"type": "string", "minLength": MIN_LENGTH}
                },
                "sms": {
                    "type": "array",
                    "items": {"type": "string", "minLength": MIN_LENGTH}
                },
            },
            "required": ["id"]
        },
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='put_notify')
    def put(self):
        '''
            @description: Update the ticket notify.
            @notes: /Handlers/ticket/notify

            @param id: notify id
            @type id: string

            @param ticketId: ticketId
            @type ticketId: string

            @param description: description
            @type description: string

            @param user: user
            @type user: list

            @param email: email
            @type email: list

            @param wechat: wechat
            @type wechat: list

            @param sms: sms
            @type sms: list

            @return 200: {}
            @raise 400: invalid input
        '''
        AppServiceGlobal.REQUESTS.labels(method='PUT', endpoint="ticket/nofity").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.NotifyHandler.put(): {0}, begin action".format(session.email))
        current_time = datetime.utcnow()
        ticketId = self.body.get("ticketId", None)
        spec = {"id": self.body.get("id")}
        setting = {'updated_time': current_time}
        setting = {}
        if self.body.get("email"):
            setting['email'] = self.body.get("email")
        if self.body.get("wechat"):
            setting['wechat'] = self.body.get("wechat")
        if self.body.get("sms"):
            setting['sms'] = self.body.get("sms")
        if self.body.get("other"):
            setting['other'] = self.body.get("other")
        if not setting:
            raise APIError(400, message='Invalid Parameters error, email/wechat/sms is empty')

        res = yield AppServiceGlobal.db.notify.update(
            spec,
            {
                '$set': setting
            },
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("Ticket.NotifyHandler.put(): {0}, update nofity failed".format(session.email))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        logging.info("Ticket.NotifyHandler.post(): {0}, action success".format(session.email))
        raise gen.Return({"message": "update nofity {} succeed".format(ticketId)})

    @validate(
        output_schema={
            "type": "object"
        }
    )
    @AppServiceGlobal.IN_PROGRESS.track_inprogress()
    @coroutine
    @swagger.operation(nickname='delete_notify')
    def delete(self):
        '''
            @description: Delete the ticket notify.
            @notes: /Handlers/ticket/notify?<ticketId=ticketId>

            @param ticketId: ticketId
            @type ticketId: string

            @return 200: {}
            @raise 400: invalid input
        '''
        AppServiceGlobal.REQUESTS.labels(method='DELETE', endpoint="ticket/nofity").inc()
        session = self.check_authorization(AppServiceGlobal.session_manager, self.request)
        logging.info("Ticket.NotifyHandler.delete(): {0}, begin action".format(session.email))
        current_time = datetime.utcnow()

        ticketId = self.get_argument('ticketId', None)
        if not ticketId:
            logging.error("Ticket.NotifyHandler.delete(): {0}, notify list failed".format(session.email))
            raise APIError(400, message='Invalid Parameters error')
        spec = {"ticketId": ticketId}
        if (yield AppServiceGlobal.db.notify.find(spec).to_list(None)):
            setting = {}
            if self.get_argument('user', None):
                setting['user'] = self.get_argument('user', None)
            if self.get_argument('email', None):
                setting['email'] = self.get_argument('email', None)
            if self.get_argument('wechat', None):
                setting['wechat'] = self.get_argument('wechat', None)
            if self.get_argument('sms', None):
                setting['sms'] = self.get_argument('sms', None)
            if self.get_argument('other', None):
                setting['other'] = self.get_argument('other', None)
            res = yield AppServiceGlobal.db.notify.update(
                spec,
                {
                    '$pull': setting
                },
                w=AppServiceGlobal.config.mongo_db_copy
            )
            if not res:
                logging.error("Ticket.NotifyHandler.delete(): {0}, update nofity failed".format(session.email))
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        else:
            logging.error("Ticket.NotifyHandler.delete(): {0}, notify list failed".format(session.email))
            raise APIError(400, message='404 Not Found Ticket')

        raise gen.Return({"message": "{} is success".format(ticketId)})
