#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:31:19
# @Updated Date: 2016-11-14 11:50:02
from matplotlib.dates import SEC_PER_HOUR

"""
AppService internal utils.
"""

import logging
import json
import uuid
import time
import requests
import hashlib
import base64
import io
from datetime import datetime, timedelta
from PIL import Image
import magic
import phonenumbers
from phonenumbers import NumberParseException


from bson import json_util
from tornado import gen
from tornado.httpclient import HTTPRequest, AsyncHTTPClient
from tornado.httpclient import HTTPError
from Runtime.Constants import (
    DEFAULT_SUPPORT_USER,
    AUTO_TICKET_TRIGGER,
    TIME_FORMAT,
    ONCALL_EMAIL,
    ONCALL_WECHAT,
    ONCALL_SMS,
    FILE_TYPE,
    CHINA_TIME_ZONE)
from Runtime.Debug import Debug
from Runtime.Constants import (BUCKETNAME, SERVICETOKEN, IMGSIZE)
from Forge.Common.Ops import (
    OPS_ACTIVITY_ASSIGN,
    OPS_ACTIVITY_TRIGGER,
    OPS_ACTIVITY_OPEN,
    OPS_ACTIVITY_ACKNOWLEDGE,
    OPS_ACTIVITY_RESOLVE,
    OPS_ACTIVITY_UPDATE,
    OPS_ACTIVITY_CLOSE,
    OPS_TICKET_UPDATE,
    OPS_TICKET_CREATE,
    OPS_ACCOUNT_CREATE)
from Forge.Common.State import (
    TICKET_STATE_OPENED,
    TICKET_STATE_DONE,
    TICKET_STATE_CLOSED,
    SCHEDULE_STATE_ACTIVE)
from Forge.Common.Utils import is_email, parse_department, ignored
from Forge.Common.Email import Email
from Forge.Common.Wechat import Wechat
from Forge.Common.RequestHandlers import APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal


import sys
reload(sys)
sys.setdefaultencoding('utf-8')


# ----------------------------------------------------------------------------------------------
#   Exception:
# ----------------------------------------------------------------------------------------------
class APIException(Exception):
    """A simple exception class used for APIException"""
    pass


class RCOSException(Exception):
    """A simple exception class used for RCOSException"""
    pass


class NetDiskException(Exception):
    """A simple exception class used for NetDiskException"""
    pass


class AppServiceUtil(Debug):

    USER_AGENT = 'OnDuty-API-client'

    def _debug(self):
        self._set(AppServiceGlobal.debug)

    # ----------------------------------------------------------------------------------------------
    # HTTP Fetch
    # ----------------------------------------------------------------------------------------------
    def _fetch(self, url, callback=None):
        logging.debug("AppServiceUtil._fetch(): {0}".format(url))
        AppServiceGlobal.http_client.fetch(url, callback if callback else self._response)

    @gen.coroutine
    def _async_fetch(self, url):
        logging.debug("AppServiceUtil._async_fetch(): {0}".format(url))
        try:
            resp = yield AppServiceGlobal.http_client.fetch(url)
        except Exception as error:
            logging.error("AppServiceUtil._async_fetch(): {0} - request exception {1}".format(url, str(error)))
            raise gen.Return(False)

        raise gen.Return(resp)

    # ----------------------------------------------------------------------------------------------
    # Session
    # ----------------------------------------------------------------------------------------------
    def _create_session(self, session):
        try:
            return AppServiceGlobal.session_manager.create(session)
        except Exception as error:
            logging.error("AppServiceUtil._create_session(): {0}, 'create session', \n{1}".format(
                session.email, str(error)))
            raise APIError(403, session.email, flag='COMMON.SESSION.CREATE_FAILED')

    def _destroy_session(self, session):
        try:
            AppServiceGlobal.session_manager.delete(session)
        except Exception as error:
            logging.error("AppServiceUtil._destroy_session() {0} destroy session \n{1}".format(
                session, str(error)))
            raise APIError(403, None, session, flag='COMMON.SESSION.DESTROY_FAILED')

    def _update_session(self, session):
        try:
            AppServiceGlobal.session_manager.update(session)
        except Exception as error:
            logging.error("AppServiceUtil._update_session() {0}/{1} update exception \n{2}".format(
                session.email, session.uid, str(error)))
            raise APIError(403, session.email, session.id, flag='COMMON.SESSION.UPDATE_FAILED')

    def _verify_session(self, session):
        try:
            return AppServiceGlobal.session_manager.verify(session)
        except Exception as error:
            logging.error("AppServiceUtil._verify_session() {0} verify exception \n{1}".format(
                session, str(error)))
            raise APIError(403, None, session, flag='COMMON.SESSION.VERIFY_FAILED')

    def _get_session(self, session):
        try:
            return AppServiceGlobal.session_manager.get(session)
        except Exception as error:
            logging.error("AppServiceUtil._get_session() {} get user session \n{}".format(
                session, str(error)))
            raise APIError(403, session, flag='COMMON.SESSION.GET_FAILED')

    # ----------------------------------------------------------------------------------------------
    # Email
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def _send_email(receiver, subject, content, sender=None, format='text'):
        data = {
            'receiver': receiver,
            'subject': subject,
            'content': content,
            'format': format
        }
        if sender is not None:
            data.update({'sender': sender})

        logging.info(u"AppServiceUtil._send_email(): Sender:{0}, Receiver:{1}, Subject:{2}".format(
            sender, receiver, subject))

        AppServiceGlobal.redis_db.lpush(ONCALL_EMAIL, str(data))

    # ----------------------------------------------------------------------------------------------
    # Wechat
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def _send_wechat(receiver, type, content, sender=None):
        """
        type为single表示单发，group为群发
        群发时receiver为群聊的id，群聊id与resolvegroupname对应
        """
        data = {
            'receiver': receiver,
            'type': type,
            'content': content
        }
        if sender is not None:
            data.update({'sender': sender})

        AppServiceGlobal.redis_db.lpush(ONCALL_WECHAT, str(data))

    # ----------------------------------------------------------------------------------------------
    # SMS
    # ----------------------------------------------------------------------------------------------
    @staticmethod
    def _send_sms(mobile, content):
        data = {
            'mobile': mobile,
            'content': content
        }

        logging.info(u"AppServiceUtil._send_sms(): mobile:{0}, content:{1}".format(mobile, content))

        AppServiceGlobal.redis_db.lpush(ONCALL_SMS, json.dumps(data))

    @staticmethod
    def get_short_url(long_url):

        short_url = ''
        url = 'http://api.t.sina.com.cn/short_url/shorten.json?source=1681459862&url_long=%s' % long_url
        try:
            req = requests.get(url, timeout=5.0)
            short_url = req.json()[0].get('url_short')
        except Exception as e:
            logging.info('AppServiceUtil.get_short_url() failed: %s' % e)

        return short_url if short_url else long_url

    @gen.coroutine
    def _is_nogroup(self, department, falg=False):
        try:
            filters = '?q={name}'.format(name=department)
            resp = yield self._request_metadata_api(
                method='GET', key='/department', filters=filters)
            mdReturn = self._request_return_format(resp).get('data')
            flag = True if mdReturn else False
        except Exception as e:
            logging.warning('AppServiceUtil._is_nogroup {0}'.format(e))
        raise gen.Return(flag)

    @gen.coroutine
    def _is_devops_group(self, department, flag=False):
        try:
            filters = '?q={name}'.format(name=department)
            resp = yield self._request_metadata_api(
                method='GET', key='/department', filters=filters)
            mdReturn = self._request_return_format(resp).get('data')
            flag = True if mdReturn else False
        except Exception as e:
            logging.warning('AppServiceUtil._is_devops_group {0}'.format(e))
        raise gen.Return(flag)

    @gen.coroutine
    def _check_user_is_exist(self, email):
        url = "/username/{}/exists".format(email)
        resp = yield self._request_metadata_api(method='GET', key=url)

        flag = True if resp.body else False
        raise gen.Return(flag)

    @gen.coroutine
    def _check_userId_is_exist(self, uid):
        '''
            根据用户的id查询用户是否是一个激活用户
        '''
        url = "/user/{}".format(uid)
        resp = yield self._request_metadata_api(method='GET', key=url)

        flag = True if resp.body else False
        raise gen.Return(flag)

    @gen.coroutine
    def _check_email_is_exist(self, email):
        # 从Mongodb中获取数据进行判断
        url = "/email/{}/exists".format(email)
        resp = yield self._request_metadata_api(method='GET', key=url)

        flag = True if resp.body else False
        raise gen.Return(flag)

    # ----------------------------------------------------------------------------------------------
    # schedule
    # ----------------------------------------------------------------------------------------------
    @gen.coroutine
    def _get_the_schedule(self, timepoint, resolveGroupId):
        user = None
        schedule = yield AppServiceGlobal.db.schedule.find_one(
            {
                'resolvegroup': resolveGroupId,
                'status': SCHEDULE_STATE_ACTIVE
            }
        )
        # first get the special department schedule
        if schedule:
            user = [entry['user'] for entry in schedule['entries'] if entry['start'] <= timepoint <= entry['end']]
            if user:
                user = user[0]
                if not is_email(user):
                    user = '{0}@rongcapital.cn'.format(user)

        # can't find any schedule and return the default supporter
        user = user or DEFAULT_SUPPORT_USER
        raise gen.Return(user)

    @gen.coroutine
    def _remove_user_from_schedule(self, spec, resolvegroupid=None, scheduleid=None,
                                   user_email=None, user_id=None, session=None):
        """将用户从排班中删除"""
        if resolvegroupid:
            spec = {
                'resolvegroup': resolvegroupid,
                'status': SCHEDULE_STATE_ACTIVE
            }
        elif scheduleid:
            spec = {
                'id': scheduleid,
                'status': SCHEDULE_STATE_ACTIVE
            }
        else:
            spec = spec

        schedule = yield AppServiceGlobal.db.schedule.find_one(spec)
        if not schedule:
            raise gen.Return(False)
        users = schedule['users']

        if user_id:
            user = yield self._query_account(uid=user_id, session=session)
            if user:
                user_email = user[0]['email']
            else:
                raise gen.Return(False)

        if user_email in users:
            users.remove(user_email)
        setting = {'$set': {'users': users}}
        res = yield AppServiceGlobal.db.schedule.update(
            spec,
            setting,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        result = res.get('updatedExisting')
        raise gen.Return(result)

    # ----------------------------------------------------------------------------------------------
    # ticket
    # ----------------------------------------------------------------------------------------------
    @gen.coroutine
    def _create_rc_ticket(self, user, ticket, session=None):
        ticket['id'] = 'case-{0}'.format(str(uuid.uuid4()).split('-')[-1])
        res = yield AppServiceGlobal.db.ticket.insert(ticket)
        if not res:
            logging.error("AppServiceUtil._create_rc_ticket(): {0} create failed {1} | {2}".format(
                user, ticket['id'], ticket['description']))
            raise gen.Return(False)

        # 添加通知列表
        responser_email = ticket['responser']
        print("-----------------start query account -----time -------", datetime.utcnow())
        responser = yield self._query_account(email=responser_email, session=session)
        print("-----------------start query account end -----time -------", datetime.utcnow())
        if not responser:
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        responser = responser[0]

        current_time = datetime.utcnow()
        nofity = {
            'id': 'notify-{0}'.format(str(uuid.uuid4()).split('-')[-1]),
            'ticketId': ticket['id'],
            'user': [responser['id']],
            'email': [responser['email']],
            'wechat': [responser.get('wechat')] if responser.get('wechat') else [],
            'created_time': current_time,
            'updated_time': current_time
        }

        res = yield AppServiceGlobal.db.notify.insert(nofity)
        if not res:
            logging.error("AppServiceUtil._create_rc_ticket.post(): create notify list failed")
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        # Email notify the responser and cc_list
        email_args = {
            'ticket_id': ticket['id'],
            'ticket_severity': ticket['severity'],
            'ticket_title': ticket['description'],
            'ticket_status': ticket['status'],
            'ticket_creater': ticket['requester']
        }

        # 获取部门
        dp_name = ''
        dp_id = ticket['details'].get('department')
        if dp_id:
            filters = '?id={0}'.format(dp_id)
            resp = yield self._request_metadata_api(
                method='GET', key='/department', filters=filters)
            department = self._request_return_format(resp).get('data')
            if isinstance(department, list):
                department = department[0]
                dp_name = department.get('name')

        if ticket['type'] == 'account':
            email_args['ticket_url'] = 'http://{0}/audits/{1}'.format(AppServiceGlobal.config.domain,
                                                                      ticket['id'])
            email_args['create_time'] = datetime.strftime(ticket['created_time'], '%Y-%m-%d')
            email_args['fullname'] = '{0} {1}'.format(ticket['details'].get('firstname'),
                                                      ticket['details'].get('lastname'))
            email_args['username'] = ticket['details'].get('username')
            email_args['email_address'] = ticket['details'].get('email')
            email_args['phone_number'] = ticket['details'].get('telephone')
            email_args['department'] = dp_name
            email_args['domain'] = AppServiceGlobal.config.domain
            template_name = OPS_ACCOUNT_CREATE
        else:
            email_args['ticket_url'] = 'http://{0}/tickets/{1}'.format(AppServiceGlobal.config.domain,
                                                                       ticket['id'])
            email_args['create_time'] = datetime.strftime(ticket['created_time'], '%Y-%m-%d %H:%M:%S')
            email_args['ticket_details'] = ticket['details'].get('content')
            email_args['domain'] = AppServiceGlobal.config.domain
            template_name = OPS_TICKET_CREATE

            responser_mobile = responser.get('telephone')
            if responser_mobile:
                try:
                    parser = phonenumbers.parse(responser_mobile)
                    responser_mobile = phonenumbers.format_number(parser, phonenumbers.PhoneNumberFormat.NATIONAL).replace(' ', '')
                except NumberParseException:
                    logging.error('AppServiceUtil._create_rc_ticket() error phone number: {}/{}'.format(responser_email,
                                                                                                        responser_mobile))
                print("-----------------start get short url -----time -------", datetime.utcnow())
                ticket_short_url = self.get_short_url(email_args['ticket_url'])
                print("-----------------start get short url end -----time -------", datetime.utcnow())
                sms_content = '新的报障概述：{}，详情请访问Onduty平台链接：{}，请及时处理。'.format(ticket['description'], ticket_short_url)
                print("-----------------start send sms -----time -------", datetime.utcnow())
                self._send_sms(mobile=[responser_mobile], content=sms_content)
                print("-----------------start send sms -----time -------", datetime.utcnow())

        email_format = Email.format(template_name, **email_args)

        print("-----------------start send email -----time -------", datetime.utcnow())
        receiverList = []
        if ticket['responser']:
            receiverList.append(ticket.get('responser'))
        else:
            receiverList.append(DEFAULT_SUPPORT_USER)
        if isinstance(ticket.get('cc_list'), list):
            receiverList = receiverList + ticket.get('cc_list')
        receiverList = list(set(receiverList))
        if receiverList:
            self._send_email(receiver=receiverList,
                             subject=email_format.get('Subject'),
                             content=email_format.get('Body'),
                             format='html')
        print("-----------------start send email -----time -------", datetime.utcnow())
        # send wechat
        created_time = ticket.get('created_time') + timedelta(hours=CHINA_TIME_ZONE)
        created_time = created_time.strftime(TIME_FORMAT)
        wechat_format = Wechat.format('bug', id=ticket['id'], description=ticket.get('description'),
                                      created_time=created_time, status=ticket.get('status'),
                                      severity=ticket.get('severity'), hostname=ticket.get('details').get('hostname'),
                                      ip=ticket.get('details').get('ip'), url=email_args['ticket_url'])

        # send to the wechat user
        self._send_wechat(receiver=[responser['username']], type="single", content=wechat_format)

        details = []
        # create trigger activity
        if 'requester' in ticket and ticket['requester']:
            if ticket['requester'] == AUTO_TICKET_TRIGGER:
                action = OPS_ACTIVITY_TRIGGER
            else:
                action = OPS_ACTIVITY_OPEN
            logging.debug("AppServiceUtil._create_rc_ticket(): {0}, {1} ticket {2} through".format(
                user, action, ticket['id']))
            detail = {
                'action': action,
                'requester': ticket['requester'],
                'description': ticket['description']
            }
            # add event id if AUTO_TICKET_TRIGGER
            if action == AUTO_TICKET_TRIGGER and 'details' in ticket and ticket['details'].get('event_id', None):
                detail['event_id'] = ticket['details']['event_id']
            details.append(detail)
        # create assign activity
        if ticket['requester'] != AUTO_TICKET_TRIGGER and 'responser' in ticket and ticket['responser']:
            logging.debug("_create_rc_ticket(): {0}, {1} the ticket {2} to {3}".format(
                user, OPS_ACTIVITY_ASSIGN, ticket['id'], ticket['responser']))
            detail = {
                'action': OPS_ACTIVITY_ASSIGN,
                'requester': user,
                'origin_responser': '',
                'new_responser': ticket['responser']
            }
            details.append(detail)
        if details:
            activity = yield self._create_rc_activity(user, ticket['id'], details)
            if not activity:
                logging.error("AppServiceUtil._create_rc_ticket(): {0}, create activity {1} failed".format(
                    user, ticket['id']))

        logging.info("AppServiceUtil._create_rc_ticket(): {0} ticket {1} successfully".format(
            user, ticket['id']))
        raise gen.Return(ticket['id'])

    @gen.coroutine
    def _update_rc_ticket(self, user, ticket, setting, pushing=None, note=None, approval=None,
                          operator=None, session=None):
        doc = {
            '$set': setting
        }
        res = yield AppServiceGlobal.db.ticket.update(
            {
                'id': ticket['id']
            },
            doc,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("AppServiceUtil._update_rc_ticket() {0} update ticket {1} failed".format(
                user, ticket['id']))
            raise gen.Return(False)

        details = []
        # acknowledge activity
        # 创建ticket之后进行编辑不应该认为是acknowledge
        if ticket['created_time'] == ticket['updated_time']:
            logging.debug("AppServiceUtil._update_rc_ticket(): {0}, acknowledge ticket {1}".format(
                user, ticket['id']))

            try:
                durTime = setting['updated_time'] - ticket['created_time']
                dur_seconds = int(durTime.total_seconds())
            except Exception:
                dur_seconds = 86400

            detail = {
                'action': OPS_ACTIVITY_ACKNOWLEDGE,
                'responser': operator or user,
                'duration': dur_seconds
            }
            if note:
                detail['note'] = note
            if approval is not None:
                detail['approval'] = approval
            details.append(detail)
        # assign
        if 'responser' in setting and setting['responser'] != ticket['responser']:
            logging.debug("AppServiceUtil._update_ticket() {0} ticket {1} from {2} to {3}".format(
                user, ticket['id'], ticket['requester'], setting['responser']))

            detail = {
                'action': OPS_ACTIVITY_ASSIGN,
                'requester': user,
                'origin_responser': ticket['responser'],
                'new_responser': setting['responser']
            }
            details.append(detail)
        # update status
        if 'status' in setting and setting['status'] != ticket['status']:
            logging.debug("AppServiceUtil._update_rc_ticket(): {0}, update ticket {1} status from {2} to {3}".format(
                user, ticket['id'], ticket['status'], setting['status']))

            detail = {
                'origin_status': ticket['status'],
                'new_status': setting['status'],
                'requester': user
            }
            if approval is not None:
                detail['approval'] = approval
            if setting['status'] == TICKET_STATE_OPENED:
                detail['action'] = OPS_ACTIVITY_OPEN
            elif setting['status'] == TICKET_STATE_DONE:
                detail['action'] = OPS_ACTIVITY_RESOLVE
            elif setting['status'] == TICKET_STATE_CLOSED:
                detail['action'] = OPS_ACTIVITY_CLOSE
            else:
                detail['action'] = OPS_ACTIVITY_UPDATE
            details.append(detail)
        # update severity
        if 'severity' in setting and setting['severity'] != ticket['severity']:
            logging.debug("AppServiceUtil._update_rc_ticket(): {0}, update ticket {1} severity from {2} to {3}".format(
                user, ticket['id'], ticket['severity'], setting['severity']))

            detail = {
                'action': OPS_ACTIVITY_UPDATE,
                'origin_severity': ticket['severity'],
                'new_severity': setting['severity'],
                'requester': user
            }
            details.append(detail)
        # add activity
        if details:
            activity = yield self._create_rc_activity(user, ticket['id'], details)
            if not activity:
                logging.error("AppServiceUtil._update_rc_ticket(): {0}, add {1} failed".format(
                    user, ticket['id']))
            else:  # send notification email to cc_list
                logging.debug("AppServiceUtil._update_rc_ticket(): {0} send email of ticket {1}".format(
                    user, ticket['id']))

                if ticket['type'] != 'account':
                    email_args = {
                        'ticket_id': ticket['id'],
                        'update_user': user or operator,
                        'ticket_severity': ticket['severity'],
                        'ticket_title': ticket['description'],
                        'ticket_status': ticket['status'],
                        'ticket_creater': ticket['requester'],
                        'ticket_url': 'http://{0}/tickets/{1}'.format(AppServiceGlobal.config.domain, ticket['id']),
                        'create_time': datetime.strftime(ticket['created_time'], '%Y-%m-%d %H:%M:%S'),
                        'ticket_details': ticket['details'].get('content'),
                        'domain': AppServiceGlobal.config.domain
                    }

                    email_format = Email.format(OPS_TICKET_UPDATE, **email_args)

                    notify = yield AppServiceGlobal.db.notify.find_one(
                        {"ticketId": ticket['id']},
                        {"_id": False, "created_time": False, "updated_time": False}
                    )
                    all_email = [ticket.get('responser')]
                    notify_users = []
                    if notify:
                        try:
                            notify_users = notify.get('user', [])
                            notify_emails = notify.get('email', [])
                            notify_wechat = notify.get('wechat', [])
                            allUserInfo = yield self._query_account(session=session)
                            notify_users = [user for user in allUserInfo if user['id'] in notify_users and user.get(u"status") == u"enabled"]
                            notify_user_email = [user['email'] for user in notify_users]
                            all_email = list(set(notify_user_email + notify_emails))
                            all_email = list(set(all_email.append(ticket.get('responser'))))
                        except Exception as e:
                            logging.error('AppServiceUtil._update_rc_ticket(): get notify email failed {}'.format(e))
                    if all_email:
                        self._send_email(receiver=all_email,
                                         subject=email_format.get('Subject'),
                                         content=email_format.get('Body'),
                                         format='html')
                        # send wechat
                        created_time = ticket.get('created_time') + timedelta(hours=CHINA_TIME_ZONE)
                        created_time = created_time.strftime(TIME_FORMAT)
                        wechat_format = Wechat.format('bug', id=ticket['id'], description=ticket.get('description'),
                                                      created_time=created_time, status=ticket.get('status'),
                                                      severity=ticket.get('severity'),
                                                      hostname=ticket.get('details').get('hostname'),
                                                      ip=ticket.get('details').get('ip'), url=email_args['ticket_url'])

                        # send to the wechat user
                        self._send_wechat(receiver=[u['username'] for u in notify_users], type="single", content=wechat_format)

        logging.info("AppServiceUtil._update_rc_ticket(): {0}, update ticket {1} successfully".format(user, ticket['id']))
        raise gen.Return(True)

    # ----------------------------------------------------------------------------------------------
    # activity
    # ----------------------------------------------------------------------------------------------
    @gen.coroutine
    def _create_rc_activity(self, username, ticket_id, details):
        activity = {
            'ticket_id': ticket_id,
            'details': details,
            'created_time': datetime.utcnow(),
        }
        res = yield AppServiceGlobal.db.activity.insert(activity)
        if not res:
            logging.error("AppServiceUtil._create_activity(): {0}, add activity {1} failed".format(
                username, ticket_id))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        activity = json.loads(json_util.dumps(activity, default=json_util.default))
        raise gen.Return(activity)

    def _filterUserInfo(self, note):
        '''
        换行、TAB、空格、@不是第一个
        '''
        userInfoList = []
        for line in note.split('\n'):
            for word in [x for x in line.split(' ') if len(x) >= 0]:
                word = word.replace('\t', '')
                if word.find('@') == 0:
                    userInfoList.append(word)
        return userInfoList

    # ----------------------------------------------------------------------------------------------
    # duty task
    # ----------------------------------------------------------------------------------------------
    @gen.coroutine
    def _scheduler(self, method, params):

        url = 'http://{0}:{1}/Handlers/scheduler/'.format(AppServiceGlobal.config.scheduler_host,
                                                          AppServiceGlobal.config.scheduler_port)
        headers = {
            'Content-Type': 'application/json',
            'host': '{0}:{1}'.format(AppServiceGlobal.config.scheduler_host,
                                     AppServiceGlobal.config.scheduler_port),
            'User-Agent': self.USER_AGENT,
        }

        data = None
        if method.upper() in ['DELETE', 'GET']:
            url = '{0}jobId={1}'.format(url, '&jobId='.join(params['job_ids']))
        elif params:
            data = json.dumps(params)

        logging.info("AppServiceUtil.__scheduler(): {0} - {1} - {2}".format(url, method, data))
        req = HTTPRequest(url=url,
                          method=method,
                          headers=headers,
                          body=data,
                          connect_timeout=5.0,
                          request_timeout=10.0,
                          follow_redirects=True,
                          max_redirects=5,
                          use_gzip=True)
        response = yield self._async_fetch(req)
        if not response:
            logging.error("AppServiceUtil.__scheduler(): {0} - {1} - external error".format(url, method))
            raise gen.Return(False)

        try:
            result = json.loads(response.body)
            logging.info("AppServiceUtil.__scheduler(): {0} - {1} - {2}".format(url, method, response.body))
        except Exception as error:
            logging.warning("AppServiceUtil.__scheduler(): {0}, {1}, \n{2}".format(url, str(error)), response.body)
            raise gen.Return(False)

        raise gen.Return(result)

    # ----------------------------------------------------------------------------------------------
    # cti node
    # ----------------------------------------------------------------------------------------------
    @gen.coroutine
    def _create_cti_node(self, ctiId=None, name=None, desc=None, parent=None, ctiType=None, ctiAlias=None):
        doc = {
            'id': ctiId,
            'name': name,
            'description': desc,
            'parent': parent,
            'type': ctiType,
            'created_time': datetime.utcnow(),
            'alias': ctiAlias
        }
        res = yield AppServiceGlobal.db.cti.insert(doc)
        if not res:
            # logging.error("AppServiceUtil._create_cti_node(): {0}, add new cti failed".format(name))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        cti = json.loads(json_util.dumps(doc, default=json_util.default))
        raise gen.Return(cti)

    @gen.coroutine
    def _update_cti_node(self, ctiId=None, setting=None):
        doc = {
            '$set': setting
        }
        res = yield AppServiceGlobal.db.cti.update(
            {
                'id': ctiId
            },
            doc,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            logging.error("AppServiceUtil._update_cti_node(): {0}, update cti failed".format(ctiId))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')
        cti = json.loads(json_util.dumps(setting, default=json_util.default))
        raise gen.Return(cti)

    @gen.coroutine
    def _query_cti_node(self, ctiId=None):
        if ctiId:
            doc = {'id': ctiId}
        else:
            doc = {'status': {'$ne': 'removed'}}
        res = yield AppServiceGlobal.db.cti.find(
            doc,
            {'_id': False}
        ).to_list(None)
        if not res:
            logging.error("AppServiceUtil._query_cti_node(): {0}, query cti failed".format(ctiId))
            raise gen.Return(False)

        logging.debug('AppServiceUtil._query_cti_node() query {0}'.format(ctiId))
        cti = json.loads(json_util.dumps(res, default=json_util.default))
        raise gen.Return(cti)

    @gen.coroutine
    def _query_all_CTI_info(self, ctiList=[], allCTI=None):
        '''
        @param ctiList: NEED TO QUERY CTI ID LIST
        '''
        if not allCTI:
            doc = {}
            res = yield AppServiceGlobal.db.cti.find(
                doc,
                {'_id': False}
            ).to_list(None)
            if not res:
                logging.error("AppServiceUtil._query_all_CTI_info(): query DB CTI failed")
                raise gen.Return(False)
            allCTI = json.loads(json_util.dumps(res, default=json_util.default))

        findALL = []
        for ctiOne in ctiList:
            for CTI in allCTI:
                if CTI.get("id") == ctiOne:
                    findALL.append(CTI)
                    break

        raise gen.Return(findALL)

    @gen.coroutine
    def _delete_cti_node(self, ctiId):
        if ctiId:
            doc = {'id': ctiId}
        res = yield AppServiceGlobal.db.cti.update(
            doc,
            {'$set': {'status': 'removed'}}
        )
        if not res:
            logging.error("AppServiceUtil._delete_cti_node(): {0}, delete cti failed".format(ctiId))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        cti = json.loads(json_util.dumps(res, default=json_util.default))
        raise gen.Return(cti)

    @gen.coroutine
    def _query_cti_by_parent_node_id(self, parentId=None, allCTI=None):
        '''
        @param parentId string: 查询当前CTI的父节点
        @param allCTI string: 所有的CTI节点数据
        '''
        findCTI = []
        if allCTI:
            for cti in allCTI:
                if cti and isinstance(cti, dict) and cti.get("parent") == parentId:
                    findCTI.append(cti)
        if findCTI:
            findCTI = json.loads(json_util.dumps(findCTI, default=json_util.default))
            raise gen.Return(findCTI)
        else:
            raise gen.Return(False)

    @gen.coroutine
    def _query_cti_by_child_node_id(self, childId=None):
        doc = {'id': childId, 'status': {'$ne': 'removed'}}
        res = yield AppServiceGlobal.db.cti.find(
            doc,
            {'_id': False}
        ).to_list(None)
        if not res:
            raise gen.Return(False)

        cti = json.loads(json_util.dumps(res, default=json_util.default))
        raise gen.Return(cti)

    @gen.coroutine
    def _query_cti_by_doc(self, doc, deepth=None):
        res = yield AppServiceGlobal.db.cti.find(
            doc,
            {'_id': False}
        ).to_list(None)
        if not res:
            # logging.error("AppServiceUtil._query_cti_by_doc(): {0} not find".format(doc))
            raise gen.Return(False)

        if deepth is not None:
            result = []
            for cti in res:
                cur_deepth = yield self.check_cti_type(cti_id=cti['id'])
                if cur_deepth == deepth:
                    result.append(cti)
            res = result

        cti = json.loads(json_util.dumps(res, default=json_util.default))
        raise gen.Return(cti)

    @gen.coroutine
    def _query_cti_category(self, category):
        # 查询cti中的category
        res = yield AppServiceGlobal.db.cti.find_one(
            {
                "$or": [
                    {"name": category, "parent": None},
                    {"id": category, "parent": None}
                ]
            },
            {'_id': False}
        )
        if not res:
            raise gen.Return(False)

        category = json.loads(json_util.dumps(res, default=json_util.default))
        raise gen.Return(category)

    @gen.coroutine
    def _query_cti_type(self, category, t_name):
        # 根据名字查询cti中的type
        res = yield self._query_cti_category(category)
        if not res:
            raise gen.Return(False)
        category_id = res.get("id")
        res = yield AppServiceGlobal.db.cti.find_one(
            {
                "$or": [
                    {"parent": category_id, "name": t_name},
                    {"parent": category_id, "id": t_name},
                ]
            },
            {'_id': False}
        )
        cti_type = json.loads(json_util.dumps(res, default=json_util.default))
        raise gen.Return(cti_type)

    @gen.coroutine
    def _query_cti_item(self, category, t_name, item_name):
        # 根据item名字查询cti中的item
        res = yield self._query_cti_type(category, t_name)
        if not res:
            raise gen.Return(False)
        cti_type_id = res.get("id")
        res = yield AppServiceGlobal.db.cti.find_one(
            {
                "$or": [
                    {"parent": cti_type_id, "name": item_name},
                    {"parent": cti_type_id, "id": item_name},
                ]
            },
            {'_id': False}
        )
        cti_item = json.loads(json_util.dumps(res, default=json_util.default))
        raise gen.Return(cti_item)

    @gen.coroutine
    def _one_by_one_parent_id(self, parentId=None, filter=False):
        # 查询所有的没有删除的cti,以此数据做总数据,避免下面没有远程查询,优化io等待
        doc = {}
        res = yield AppServiceGlobal.db.cti.find(
            doc,
            {'_id': False}
        ).to_list(None)
        allEnabledCTI = json.loads(json_util.dumps(res, default=json_util.default))

        # 支持传输多个父节点功能
        parent_list = []
        if parentId and isinstance(parentId, list):
            for pid in parentId:
                curParNode = yield self._query_cti_node(ctiId=pid)
                curParNode[0]['children'] = yield self._create_tree_by_parent_node(
                    parentId=pid, allCTI=allEnabledCTI)
                parent_list = parent_list + curParNode
        else:
            tTree = yield self._create_tree_by_parent_node(allCTI=allEnabledCTI)
            parent_list = parent_list + tTree
        if filter and filter.lower() == u'true':
            parent_list = yield self._findout_cti_in_resolvegroup(parent_list)
        raise gen.Return(parent_list)

    @gen.coroutine
    def _create_tree_by_parent_node(self, parentId=None, allCTI=None):
        if not allCTI:
            doc = {}
            res = yield AppServiceGlobal.db.cti.find(
                doc,
                {'_id': False}
            ).to_list(None)
            allCTI = json.loads(json_util.dumps(res, default=json_util.default))
        #
        # 从上往下找寻子节点,得到一个从根节点开始的完整的树状结构
        #
        if not parentId:
            # 得到所有父节点为null的cti,包括部门cti
            res = yield self._query_cti_by_parent_node_id(allCTI=allCTI)
            if res and isinstance(res, list):
                for node in res:
                    node['children'] = yield self._create_tree_by_parent_node(parentId=node['id'], allCTI=allCTI)
        else:
            res = yield self._query_cti_by_parent_node_id(parentId=parentId, allCTI=allCTI)
            if res and isinstance(res, list):
                for node in res:
                    node['children'] = yield self._create_tree_by_parent_node(parentId=node['id'], allCTI=allCTI)
        raise gen.Return(res)

    @gen.coroutine
    def _one_by_one_child_id(self, ctiIds=None, filter=False):
        # 支持传输多个子节点查询功能
        # 查询当前子节点的所有上级节点到根节点
        childList = []
        if ctiIds and isinstance(ctiIds, list):
            for cid in ctiIds:
                curChildNode = yield self._query_cti_node(ctiId=cid)
                if not curChildNode:
                    raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
                curChildNode[0]['parentnode'] = yield self._create_tree_by_child_id_node(ctiId=cid)
                childList = childList + curChildNode
        elif ctiIds and isinstance(ctiIds, basestring):
            curChildNode = yield self._query_cti_node(ctiId=ctiIds)
            if not curChildNode:
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
            curChildNode[0]['parentnode'] = yield self._create_tree_by_child_id_node(ctiId=ctiIds)
            childList = childList + curChildNode
        else:
            tTree = yield self._create_tree_by_child_id_node(ctiId=ctiIds)
            childList = childList + tTree
        if filter and filter.lower() == u'true':
            childList = yield self._findout_cti_in_resolvegroup(childList)
        raise gen.Return(childList)

    @gen.coroutine
    def _create_tree_by_child_id_node(self, ctiId=None):
        #
        # 从当前节点追述到根节点
        #
        if not ctiId:
            # 1. 如果用户没有传输任何id,表示返回空
            # 2. 反向查找到了根节点了，父节点为空
            pass
        else:
            curNode = yield self._query_cti_node(ctiId=ctiId)
            curParentId = curNode[0]['parent']
            res = yield self._query_cti_by_child_node_id(childId=curParentId)
            if res:
                res[0]['parentnode'] = yield self._create_tree_by_child_id_node(ctiId=res[0]['id'])
        try:
            res = res if res else []
        except Exception:
            res = []
        raise gen.Return(res)

    @gen.coroutine
    def get_parent_list(self, childId, result_list):
        """根据子节点获取所有的父节点，返回[category, type, item]"""
        if not childId:
            # 没有父节点，认为是找到了根节点
            result_list.reverse()
            raise gen.Return
        curNode = yield self._query_cti_node(ctiId=childId)
        if curNode:
            result_list.append(curNode[0])
            curChildId = curNode[0]['parent']
            yield self.get_parent_list(curChildId, result_list)

    @gen.coroutine
    def _insert_fileds_to_cti(self, ctiId, session=None):
        cid = ctiId['id']
        ctiId['staffs'] = yield self._query_account_by_cti(cid, session=session)
        if ctiId.get('children') and isinstance(ctiId.get('children'), list):
            for cti_id in ctiId.get('children'):
                cti_id = yield self._insert_fileds_to_cti(cti_id)
        else:
            logging.debug('AppServiceUtil._insert_fileds_to_cti() not found children')
        raise gen.Return(ctiId)

    @gen.coroutine
    def _findout_cti_in_resolvegroup(self, ctiTree):
        # 通过找出所有支持组中包含有cti的进行比对
        res = yield AppServiceGlobal.db.resolvegroup.find(
            {"ctis": {"$ne": []}, "status": {"$ne": "removed"}},
            {
                'ctis': True,
                '_id': False
            }
        ).to_list(None)
        ctis = [cti.get('ctis') for cti in res if cti]
        try:
            ctis = list(set([cti for t in ctis for cti in t]))
        except Exception:
            ctis = []
        # logging.debug('AppServiceUtil._findout_cti_in_resolvegroup() ctis {0} '.format(ctis))
        validCtis = []
        for tree in ctiTree:
            (resTemp, flag) = self._isInTree(tree, keyWords=ctis)
            if flag:
                validCtis.append(resTemp)
        # logging.debug('AppServiceUtil._findout_cti_in_resolvegroup()')
        raise gen.Return(validCtis)

    def _checkLeafNodeIsNone(self, tree=[]):
        if tree.get('children'):
            pass

    def _isInTree(self, tree=None, keyWords=None, upNewTree={'children': []}, flag=False):
        # 表示第一次从根节点出发
        newTree = {}
        newTree['id'] = tree.get('id')
        newTree['name'] = tree.get('name')
        newTree['description'] = tree.get('description')
        newTree['parent'] = tree.get('parent')
        newTree['created_time'] = tree.get('created_time')
        newTree['type'] = tree.get('type')
        newTree['alias'] = tree.get('alias')
        if tree.get('children'):
            newTree['children'] = []
            for tree in tree.get('children'):
                (tree, flag) = self._isInTree(tree, keyWords=keyWords, upNewTree=newTree, flag=flag)
        if isinstance(tree, dict) and newTree.get('id') in keyWords:
            flag = True
            upNewTree['children'].append(newTree)
            return (newTree, flag)
        if not newTree.get('parent'):
            upNewTree['children'].append(newTree)
            return (newTree, flag)
        if isinstance(tree, dict) and newTree.get('parent') and newTree.get('children'):
            upNewTree['children'].append(newTree)
            return (newTree, flag)
        else:
            return (newTree, flag)

    @gen.coroutine
    def format_cti(self):
        """将所有cti转换成cti对象"""
        result = []
        cti_tree = yield self._create_tree_by_parent_node()
        cti_tree = cti_tree or []
        for c in cti_tree:
            if c.get('children'):
                t_list = c.pop('children')
                for t in t_list:
                    if t.get('children'):
                        i_list = t.pop('children')
                        for i in i_list:
                            cti_item = {
                                'category': c,
                                'type': t,
                                'item': i
                            }
                            result.append(cti_item)

        raise gen.Return(result)

    @gen.coroutine
    def check_cti_type(self, cti_id):
        """
        0：category
        1: type
        2: item
        -1: 不存在
        :param cti_id:
        :return:
        """
        deepth = -1
        while cti_id:
            cti = yield self._query_cti_node(cti_id)
            if not cti: break
            cti_id = cti[0].get('parent')
            deepth += 1

        raise gen.Return(deepth)

    # ----------------------------------------------------------------------------------------------
    # cti account
    # ----------------------------------------------------------------------------------------------

    @gen.coroutine
    def _query_account(self, uid=None, email=None, name=None, filter_status=False, session=None):
        """
        查询用户信息, uid可以为list, filter_status为True表示只显示enabled用户

        """
        logging.warning("\n--- email {} \n --- name {} \n ---- session {} \n".format(email, name, session))
        get_data = []
        if uid:
            if isinstance(uid, basestring):
                url = "/user/{}".format(uid)
                res = yield self._sub_query_account(uid, url, email, name, filter_status, session)
                get_data = get_data + res
            elif isinstance(uid, list):
                for ud in uid:
                    url = "/user/{}".format(ud)
                    res = yield self._sub_query_account(uid, url, email, name, filter_status, session)
                    get_data = get_data + res

        elif email:
            if isinstance(email, basestring):
                url = "/user?q={}".format(email)
                res = yield self._sub_query_account(uid, url, email, name, filter_status, session)
                get_data = get_data + res
            elif isinstance(email, list):
                for em in email:
                    url = "/user?q={}".format(em)
                    res = yield self._sub_query_account(uid, url, email, name, filter_status, session)
                    get_data = get_data + res

        elif name:
            if isinstance(name, basestring):
                url = "/user?q={}".format(name)
                res = yield self._sub_query_account(uid, url, email, name, filter_status, session)
                get_data = get_data + res
            elif isinstance(name, list):
                for n in name:
                    url = "/user?q={}".format(n)
                    res = yield self._sub_query_account(uid, url, email, name, filter_status, session)
                    get_data = get_data + res

        else:
            url = "/user?limit=0"
            res = yield self._sub_query_account(uid, url, email, name, filter_status, session)
            get_data = get_data + res

        raise gen.Return(get_data)

    @gen.coroutine
    def _sub_query_account(self, uid=None, url=None, email=None, name=None,
                           filter_status=False, session=None):
        # logging.warning("--- email {} \n --- name {} \n ---- session {} \n".format(email, name, session))
        resp = yield self._request_metadata_api(
            method='GET', key=url, session=session)
        mdReturn = self._request_return_format(resp).get('data')
        if not mdReturn:
            raise gen.Return([])
        if not isinstance(mdReturn, list):
            mdReturn = [mdReturn]
        if filter_status and mdReturn:
            mdReturn = [user for user in mdReturn if user.get('status') == 'enabled']

        user = json.loads(json_util.dumps(mdReturn, default=json_util.default))
        # print("--------------return ", user, "--------------return")
        raise gen.Return(user)

    @gen.coroutine
    def filter_users(self, user_ids=None, session=None):
        """过滤用户id,只保留状态为enabled状态的用户id"""
        filtered_users = []

        if isinstance(user_ids, list):
            for uid in user_ids:
                one_user_info = yield self._query_account(
                    uid=uid, filter_status=True, session=session)
                one_user_info = one_user_info[0] if one_user_info else {}
                filtered_users.append(one_user_info)

        if isinstance(user_ids, str):
            one_user_info = yield self._query_account(
                uid=user_ids, filter_status=True, session=session)
            one_user_info = one_user_info[0] if one_user_info else {}
            filtered_users.append(one_user_info)

        raise gen.Return(filtered_users)

    # ----------------------------------------------------------------------------------------------
    # resolve group
    # ----------------------------------------------------------------------------------------------

    @gen.coroutine
    def _query_resolvegroup_info(self, doc=None, limit=0, page=1, session=None):
        doc = doc if doc else {}
        res = yield AppServiceGlobal.db.resolvegroup.find(
            doc,
            {
                '_id': False
            },
            skip=(page - 1) * limit,
            limit=limit
        ).to_list(None)
        if not res:
            raise gen.Return([])
        # 取数据一次
        allUserInfo = yield self._query_account(session=session)

        for rg in res:
            rc_spec = {'resolvegroup': rg['id']}
            sla_spec = {'id': rg['id']}
            filters = {'_id': False}

            rg_rootcause = yield AppServiceGlobal.db.rootcause.find(rc_spec, filters).to_list(None)
            rg.update({'rootcause': rg_rootcause})
            rg_sla = yield AppServiceGlobal.db.sla.find(sla_spec, filters).to_list(None)

            if rg_sla:
                rg_sla = rg_sla[0]
                escalation = rg_sla.get('escalation')
                escalation_users = []
                if isinstance(escalation, list):
                    for es in escalation:
                        es_users = []
                        if isinstance(es, list):
                            for e in es:
                                userinfo = filter(lambda x: x.get(u'id') == e, allUserInfo)
                                es_users.append(userinfo[0] if userinfo else e)
                            escalation_users.append(es_users)

                rg.update(
                    {
                        'sla': rg_sla.get('sla'),
                        'customize': rg_sla.get('customize'),
                        'escalation': rg_sla.get('escalation'),
                        'escalation_users': escalation_users
                    }
                )
        result = json.loads(json_util.dumps(res, default=json_util.default))
        raise gen.Return(result)

    @gen.coroutine
    def _query_resolvegroup_totalcount(self, doc={}):
        res = yield AppServiceGlobal.db.resolvegroup.find(
            doc,
            {
                '_id': False
            }
        ).count()
        raise gen.Return(res)

    @gen.coroutine
    def _query_order_info(self, doc):
        if not doc:
            raise gen.Return([])
        res = yield AppServiceGlobal.db.order.find(
            doc,
            {
                '_id': False
            }
        ).to_list(None)
        if not res:
            res = False
        raise gen.Return(res)

    @gen.coroutine
    def _check_resolvegroup_is_in_scheduler(self, doc=None):
        doc = doc if doc else {}
        res = yield AppServiceGlobal.db.schedule.find(
            doc,
            {
                '_id': False
            }
        ).to_list(None)
        if not res:
            raise gen.Return(True)
        raise gen.Return(False)

    @gen.coroutine
    def _update_resolvegroup_user_order(self, spec, setting):
        doc = {
            '$set': setting
        }
        res = yield AppServiceGlobal.db.resolvegroup.update(
            spec,
            doc,
            w=AppServiceGlobal.config.mongo_db_copy
        )
        if not res:
            # logging.error("AppServiceUtil._update_resolvegroup_user_order(): update user failed")
            raise gen.Return(False)
        raise gen.Return(True)

    # ----------------------------------------------------------------------------------------------
    # hostname --> department  --> cti  -->  resolvegroup  --> scheduler --> oncall user
    # hostname --> resolvegroup --> scheduler --> oncall user
    # ----------------------------------------------------------------------------------------------

    @gen.coroutine
    def get_cti_resolvegroup_oncall_user(self, endpoint, description,
                                         cti=None,
                                         resolvegroup=None,
                                         oncall_user=None):
        # 获取所有的cti中的name
        # depart = self.filter_baseinfo(endpoint)
        res = yield self._query_cti_node()
        # 过滤所有叶子节点，获取所有和resolvegroup挂钩的cti
        leafNode = self.get_all_leaf_node(res)
        not_leaf_node = self.get_all_not_leaf_node(res, leafNode)
        in_resolvegroup_node = yield self.get_cti_in_resolvegroup(leafNode)
        res = not_leaf_node + in_resolvegroup_node
        # 生成对应的表
        (ctiInfo, T_INFO) = self.generate_string(res)
        name_ratio = self.calu_ratio(endpoint, ctiInfo)
        return_name_info = self.get_weight_and_cti_id(name_ratio, T_INFO)
        # -----------------------------------------------
        desc_ratio = self.calu_ratio(description, ctiInfo)
        return_desc_info = self.get_weight_and_cti_id(desc_ratio, T_INFO)
        # 设置name匹配权重高于desc，所以在进行求和计算之后，先将name的权重都乘以5，然后在进行求和对比权重
        merge_cti_result = self.get_ratio_by_name_desc(return_name_info, return_desc_info)
        logging.warning('AppServiceUtil.get_cti_resolvegroup_oncall_user.cti ratio result is {0}'.format(merge_cti_result))
        # Find the resolvegroup
        # -----------------------------------------------
        resolvegroup_res = yield self._query_resolvegroup_info()
        RG_INFO = {rgOne.get('id'): rgOne.get('name') for rgOne in resolvegroup_res if rgOne}
        rgInfo = [rgOne.get('name') for rgOne in resolvegroup_res if rgOne]
        # logging.debug('-------{0} --- {1}'.format(RG_INFO, rgInfo))

        name_rg_ratio = self.calu_ratio(endpoint, rgInfo)
        return_name_rg_info = self.get_weight_and_cti_id(name_rg_ratio, RG_INFO)
        # logging.warning('----{0}'.format(return_name_rg_info))
        desc_rg_ratio = self.calu_ratio(description, rgInfo)
        return_desc_rg_info = self.get_weight_and_cti_id(desc_rg_ratio, RG_INFO)
        # logging.warning('-----{0}'.format(return_desc_rg_info))
        # 设置name匹配权重高于desc，所以在进行求和计算之后，先将name的权重都乘以5，然后在进行求和对比权重
        merge_rg_result = self.get_ratio_by_name_desc(return_name_rg_info, return_desc_rg_info)
        logging.warning('AppServiceUtil.get_cti_resolvegroup_oncall_user.resolvegroup ratio result is {0}'.format(merge_rg_result))
        highest_cti_and_rg = yield self.get_highest_group(merge_cti_result, merge_rg_result)
        # logging.warning('Total Result is ---{0}'.format(highest_cti_and_rg))
        raise gen.Return(highest_cti_and_rg)

    @gen.coroutine
    def get_highest_group(self, merge_cti_result, merge_rg_result):
        weight = 0
        total_list = ()
        for cti_id in merge_cti_result:
            for rg_id in merge_rg_result:
                isReleated = yield self.check_cti_resolvegroup_is_related(cti_id, rg_id)
                if isReleated:
                    # 如果cti和resolvegroup有关系，则计算总权重，并加入到列表中
                    link_cti_rg = self.get_cti_with_resolvegroup_weight(cti_id, rg_id, merge_cti_result, merge_rg_result)
                    cti_rg_weight = link_cti_rg.values()[0][1]
                    if cti_rg_weight > weight:
                        total_list = (link_cti_rg.keys()[0], link_cti_rg.values()[0][0])
                        weight = cti_rg_weight
        raise gen.Return(total_list)

    def get_cti_with_resolvegroup_weight(self, cti_id, rg_id, merge_cti_result, merge_rg_result):
        get_weight_dict = {}
        cti_weight = merge_cti_result[cti_id][1]
        rg_weight = merge_rg_result[rg_id][1]
        total_weight = cti_weight + rg_weight
        get_weight_dict[cti_id] = (rg_id, total_weight, merge_cti_result[cti_id], merge_rg_result[rg_id])
        return get_weight_dict

    @gen.coroutine
    def check_cti_resolvegroup_is_related(self, cti_id, rg_id):
        doc = {
            'id': rg_id,
            'ctis': {
                '$in': [cti_id]
            }
        }
        res = yield self._query_resolvegroup_info(doc)
        flag = True if res else False
        raise gen.Return(flag)

    def get_ratio_by_name_desc(self, name_ratio, desc_ratio):
        temp_dict = {}
        for nameKey, nameValue in name_ratio.iteritems():
            if nameKey in desc_ratio:
                temp_dict[nameKey] = (nameValue[0], nameValue[1] * 5 + desc_ratio[nameKey][1])
            else:
                temp_dict[nameKey] = nameValue
        return temp_dict

    def get_weight_and_cti_id(self, ratios, base_info):
        import copy
        temp = copy.deepcopy(base_info)
        for ratio in ratios:
            for k, v in temp.iteritems():
                if ratio[0] == v:
                    temp[k] = ratio
        return temp

    def calu_ratio(self, input_value, base_value):
        depart = self.filter_baseinfo(input_value)
        return_ratio = process.extract(depart, base_value, scorer=fuzz.token_sort_ratio, limit=5)
        return return_ratio

    @gen.coroutine
    def endpoint_to_cti_ratio(self, endpoint):
        # 获取所有的cti中的name
        depart = self.filter_baseinfo(endpoint)
        # 找出所有的cti
        res = yield self._query_cti_node()
        # 过滤所有叶子节点，获取所有和resolvegroup挂钩的cti
        # 和resolvegroup没有关系的cti丢弃
        leafNode = self.get_all_leaf_node(res)
        not_leaf_node = self.get_all_not_leaf_node(res, leafNode)
        in_resolvegroup_node = yield self.get_cti_in_resolvegroup(leafNode)
        res = not_leaf_node + in_resolvegroup_node
        # 生成对应的表
        (ctiInfo, T_INFO) = self.generate_string(res)

        name_ratio = process.extract(depart, ctiInfo, scorer=fuzz.token_sort_ratio, limit=5)
        # 根据cti找出前面10个，根据resolvegroup找出前面10个
        name_high_ratio = process.extractOne(depart, ctiInfo, scorer=fuzz.token_sort_ratio)
        resolvegroup_id = [k for k, v in T_INFO.iteritems() if name_high_ratio[0] == v]
        # 根据resolvegroup id去查找schedu，找寻当前值班人员，如果没有找到，则使用resolve中权重最高的人员
        oncall_user = yield self.get_oncall_user_by_resolvegroup_id(resolvegroup_id)
        raise gen.Return(oncall_user)

    @gen.coroutine
    def get_oncall_user_by_resolvegroup_id(self, resolvegroup_id):
        current_time = datetime.utcnow()
        onduty_user = yield self._get_the_schedule(current_time, resolvegroup_id)
        if not onduty_user:
            doc = {
                'id': resolvegroup_id
            }
            resolvegroup_res = yield AppServiceGlobal.db.resolvegroup.find(
                doc,
                {
                    '_id': False
                }
            ).to_list(None)
            if not resolvegroup_res:
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
            user_list = resolvegroup_res[0].get('users')
            # 以后如果将权重加入到resolvegroup中之后，需要修改，使用权重最大的
            onduty_user = user_list[0]
        raise gen.Return(onduty_user)

    @gen.coroutine
    def get_cti_in_resolvegroup(self, leafNode):
        in_rg_leafNode = []
        for node in leafNode:
            doc = {'ctis': {'$in': [node.get('id')]}}
            res = yield self._query_resolvegroup_info(doc)
            if res:
                in_rg_leafNode.append(node)
        raise gen.Return(in_rg_leafNode)

    def get_all_not_leaf_node(self, data, leafNode):
        # not_leaf_node = set(data) ^ set(leafNode)
        # return not_leaf_node
        all_not_leaf_node = []
        for node in data:
            res = self.check_not_is_leaf_node(node, data)
            if res:
                all_not_leaf_node.append(res)
        return all_not_leaf_node

    def check_not_is_leaf_node(self, node, data):
        for n in data:
            if node.get('id') == n.get('parent'):
                return node
        return None

    def get_all_leaf_node(self, data):
        all_leaf_node = []
        for node in data:
            res = self.check_is_leaf_node(node, data)
            if res:
                all_leaf_node.append(res)
        return all_leaf_node

    def check_is_leaf_node(self, node, data):
        for n in data:
            if node.get('id') == n.get('parent'):
                return None
        return node

    def filter_baseinfo(self, endpoint):
        import re
        comp = re.compile('([a-zA-Z0-9\-]+).host.dataengine.com')
        e = comp.match(endpoint)
        if e:
            return e.group(1)
        else:
            return endpoint

    def generate_string(self, DATA):
        ROOT_CTI = {}
        for cti in DATA:
            if cti.get('parent') is None:
                ROOT_CTI[cti.get('id')] = cti.get('name')

        SECOND_CTI = {}
        for cti in DATA:
            if cti.get('parent') in ROOT_CTI:
                SECOND_CTI[cti.get('id')] = ROOT_CTI.get(cti.get('parent')) + "-" + cti.get('name')

        THIRD_CTI = {}
        for cti in DATA:
            if cti.get('parent') in SECOND_CTI:
                THIRD_CTI[cti.get('id')] = SECOND_CTI.get(cti.get('parent')) + "-" + cti.get('name')
        # print json.dumps(THIRD_CTI)
        cti_list = []
        for k in THIRD_CTI:
            cti_list.append(THIRD_CTI.get(k))
        return (cti_list, THIRD_CTI)

    # ----------------------------------------------------------------------------------------------
    # Object Storage
    # ----------------------------------------------------------------------------------------------
    @gen.coroutine
    def request_obj(self, token, bucket_name, object_name, method, body=None,
                    headers=None, params=None):
        """
        :type   token      : Dict {"access_key": "UCCOICB1EONUJ2BOZ7FW",
                                "secret_key": "IvMh3W839EDNG0xA2dAQJAriUKlqaMzPEpKVOibN"}
        :param  token      : RCOS Token.
        :type   bucket_name: String
        :param  bucket_name: Bucket name. (Name must lowercase or -)
        :type   object_name: String
        :param  object_name: Object name.
        :type   method     : String
        :param  method     : HTTP method.
        :type   body       : HTTPServerRequest.body
        :param  body       : Request content.
        :type   headers    : Dict
        :param  headers    : Http request headers.
        :type   params     : Dict
        :param  params     : Http request params.

        :raise  RCOSException
        :return json object.
        """
        http_client = AsyncHTTPClient()
        url = "http://%s:%d/%s/%s" % (AppServiceGlobal.config.rcos_host,
                                      AppServiceGlobal.config.rcos_port,
                                      bucket_name, object_name)

        try:
            request = HTTPRequest(url=url, method=method, headers=headers, body=body)
            resp = yield http_client.fetch(request)
        except HTTPError as e:
            raise RCOSException(e)
        except Exception as e:
            raise Exception(e)
        raise gen.Return(resp)

    def request_get_obj(self, token, bucket_name, object_name,
                        headers=None, params=None):
        """
        :type   token      : Dict {"access_key": "UCCOICB1EONUJ2BOZ7FW",
                                "secret_key": "IvMh3W839EDNG0xA2dAQJAriUKlqaMzPEpKVOibN"}
        :param  token      : RCOS Token.
        :type   bucket_name: String
        :param  bucket_name: Bucket name. (Name must lowercase or -)
        :type   object_name: String
        :param  object_name: Object name.
        :type   method     : String
        :param  method     : HTTP method.
        :type   headers    : Dict
        :param  headers    : Http request headers.
        :type   params     : Dict
        :param  params     : Http request params.

        :raise  RCOSException
        :return requests.Response.content
        """
        url = "http://%s:%d/%s/%s" % (AppServiceGlobal.config.rcos_host,
                                      AppServiceGlobal.config.rcos_port,
                                      bucket_name, object_name)

        try:
            resp = requests.get(url=url, params=params, headers=headers)
        except HTTPError as e:
            raise RCOSException(e)
        except Exception as e:
            raise Exception(e)
        return resp

    @gen.coroutine
    def process_req_delete(self, request, token, bucket_name, object_name):
        """ Process object request DELETE http method.

        :type   request    : HTTPServerRequest class
        :param  request    : Request content.
        :type   token      : Dict {"access_key": "UCCOICB1EONUJ2BOZ7FW",
                                "secret_key": "IvMh3W839EDNG0xA2dAQJAriUKlqaMzPEpKVOibN"}
        :param  token      : RCOS Token.
        :type   bucket_name: String
        :param  bucket_name: Bucket name. (Name must lowercase or -)
        :type   object_name: String
        :param  object_name: Object name.

        :raise  RCOSException
        :return json object.
        """
        logging.info("AppServiceUtil.process_req_delete() %s begin action." % object_name)
        try:
            resp = yield self.request_obj(token=token, bucket_name=bucket_name,
                                          object_name=object_name, method="DELETE")
            if resp.code != 200:
                resp_body = resp.body if resp.body else None
                logging.error("AppServiceUtil.process_req_delete() RCOS response \
                              code and error : %s-%s" % (resp.code, resp_body))
                raise APIError(status_code=resp.code, message=resp_body)
        except RCOSException as e:
                logging.error("AppServiceUtil.process_req_delete() RCOSException error: %s" % e)
                raise RCOSException(e)
        except Exception as e:
                logging.error("AppServiceUtil.process_req_delete() Exception error: %s" % e)
                raise APIError(400, flag='APS.HANDLER.RCOS.ERROR')

        logging.info("AppServiceUtil.process_req_delete() %s success." % object_name)

    def process_req_get(self, request, token, bucket_name, object_name):
        """ Process object request GET http method.

        :type   request    : HTTPServerRequest class
        :param  request    : Request content.
        :type   token      : Dict {"access_key": "UCCOICB1EONUJ2BOZ7FW",
                                "secret_key": "IvMh3W839EDNG0xA2dAQJAriUKlqaMzPEpKVOibN"}
        :param  token      : RCOS Token.
        :type   bucket_name: String
        :param  bucket_name: Bucket name. (Name must lowercase or -)
        :type   object_name: String
        :param  object_name: Object name.

        :raise  RCOSException, Exception
        :return requests.Response.content
        """
        logging.info("AppServiceUtil.process_req_get() download %s begin action." % object_name)
        try:
            resp = self.request_get_obj(token=token, bucket_name=bucket_name,
                                        object_name=object_name)
            if resp.status_code != 200:
                resp_body = resp.body if resp.body else None
                logging.error("AppServiceUtil.process_req_get() RCOS response \
                              code and error : %s-%s" % (resp.status_code, resp_body))
                raise APIError(status_code=resp.status_code, message=resp_body)
        except RCOSException as e:
                logging.error("AppServiceUtil.process_req_get() RCOSException error: %s" % e)
                raise RCOSException(e)
        except Exception as e:
                logging.error("AppServiceUtil.process_req_get() Exception error: %s" % e)
                raise APIError(400, flag='APS.HANDLER.RCOS.ERROR')

        logging.info("AppServiceUtil.process_req_get() download %s success." % object_name)

        return resp.content

    @gen.coroutine
    def process_req_put(self, request, token, bucket_name, dir_name=None, reset_size=False):
        """ Process object request PUT http method.

        :type   request    : HTTPServerRequest class
        :param  request    : Request content.
        :type   token      : Dict {"access_key": "UCCOICB1EONUJ2BOZ7FW",
                                "secret_key": "IvMh3W839EDNG0xA2dAQJAriUKlqaMzPEpKVOibN"}
        :param  token      : RCOS Token.
        :type   bucket_name: String
        :param  bucket_name: Bucket name. (Name must lowercase or -)
        :type   dir_name   : String
        :param  dir_name   : Object directory name.
        :type   reset_size : Boolean
        :param  reset_size : Enable image crop, default is disable.

        :raise  RCOSException
        :return dict object.
        """
        _ret = []

        logging.info("AppServiceUtil.process_req_put() upload begin action.")
        try:
            for f in request.files:
                file_object = request.files[f]
                for k in file_object:
                    file_name = k['filename']

                    if reset_size:
                        body = self.resize_img(io.BytesIO(k['body']), IMGSIZE)
                    else:
                        body = k['body']
                    content_type = k['content_type']
                    key = self._get_file_md5(body)

                    key_suffix = file_name.split('.')[-1] \
                        if file_name.find('.') != -1 else None

                    if dir_name and key_suffix:
                        object_name = dir_name + '/' + key + '.' + key_suffix
                    else:
                        if key_suffix:
                            object_name = key + '.' + file_name.split('.')[-1]
                        else:
                            object_name = key

                    resp = yield self.request_obj(token=token, bucket_name=bucket_name,
                                                  object_name=object_name, method="PUT",
                                                  body=body)

                    if resp.code != 200:
                        resp_body = resp.body if resp.body else None
                        logging.error("AppServiceUtil.process_req_put() RCOS response code\
                                    and error : %s-%s" % (resp.code, resp_body))
                        raise APIError(status_code=resp.code, message=resp_body)

                    _ret.append({"file_name": file_name, "key": object_name})

        except RCOSException as e:
                logging.error("AppServiceUtil.process_req_put() RCOSException error: %s" % e)
                raise RCOSException(e)
        except Exception as e:
                logging.error("AppServiceUtil.process_req_put() Exception error: %s" % e)
                raise APIError(400, flag='APS.HANDLER.RCOS.ERROR')

        logging.info("AppServiceUtil.process_req_put()  upload %s success." % object_name)

        raise gen.Return({'body': _ret})

    def resize_img(self, file_content, size, img_format="JPEG"):

        file_bytes = io.BytesIO()

        try:
            img = Image.open(file_content)
            img_format = img.format if img.format else img_format
            img.thumbnail(size, Image.ANTIALIAS)
            img.save(file_bytes, img.format)
        except IOError as e:
            raise RCOSException("Open resize file error.")

        return file_bytes.getvalue()

    def get_buffer_mime(self, fp):
        try:
            if isinstance(fp, str):
                prefix = io.BytesIO(fp).read(1024)
            else:
                prefix = fp.read(1024)

            return magic.from_buffer(prefix, mime=True)
        except Exception as e:
            logging.error("AppServiceUtil.get_buffer_mime Get mime error: %s" % e)
            raise RCOSException("Get mime error.")

    def _get_file_md5(self, file_content):
        hash_md5 = hashlib.md5()
        hash_md5.update(file_content)

        return hash_md5.hexdigest()

    # ----------------------------------------------------------------------------------------------
    # NetDisk
    # ----------------------------------------------------------------------------------------------

    @gen.coroutine
    def get_id_by_email(self, email):
        """ Get ObjectId by email

        :type   email    : string
        :param  email    : email

        :return string.
        """
        id = yield AppServiceGlobal.db.user.find_one(
            {
                "email": email
            },
            {
                "_id": True
            })

        raise gen.Return(str(id["_id"]))

    @gen.coroutine
    def get_userid_by_id(self, id):
        """ Get user_id by ObjectId

        :type   id    : string
        :param  id    : email

        :return string.
        """
        user_id = yield AppServiceGlobal.db.ceph.find_one(
            {
                "user_id": id
            },
            {
                "user_id": True
            })

        raise gen.Return(user_id["user_id"])

    @gen.coroutine
    def find_user_dir(self, id, path, file_type="dir"):
        """ Get user dir name by ObjectId

        :type   id           : string
        :param  id           : ObjectId
        :type   path         : string
        :param  path         : path
        :type   file_type    : string
        :param  file_type    : file type

        :return [string].
        """

        ret_name = []

        ret = yield AppServiceGlobal.db.files.find(
            {
                "user_id": id,
                "type": file_type,
                "path": path
            },
            {
                "name": True,
                "_id": False
            }).to_list(None)

        if ret:
            for i in ret:
                ret_name.append(i.get('name'))
        else:
            ret_name = None

        raise gen.Return(ret_name)

    @gen.coroutine
    def find_user_path(self, id, file_type="dir"):
        """ Get user path by ObjectId

        :type   id           : string
        :param  id           : ObjectId
        :type   file_type    : string
        :param  file_type    : file type

        :return [string].
        """

        ret_path = []

        if file_type != FILE_TYPE['DIR']:
            spec = {
                "user_id": id,
                "type": {"$ne": FILE_TYPE['DIR']},
            }
        else:
            spec = {
                "user_id": id,
                "type": file_type,
            }
        ret = yield AppServiceGlobal.db.files.find(
            spec,
            {
                "path": True,
                "_id": False
            }).to_list(None)

        if ret:
            for i in ret:
                ret_path.append(i.get('path'))
        else:
            ret_path = None

        raise gen.Return(ret_path)

    @gen.coroutine
    def _get_bucket_name(self, id, bucket=None):

        ret = yield AppServiceGlobal.db.ceph.find_one(
            {
                "user_id": str(id)
            },
            {
                "bucket": True
            })
        if ret:
            if bucket is None:
                raise gen.Return(ret["bucket"][0].get("bucket_name"))
            else:
                for b in ret["bucket"]:
                    if b.get("bucket_name") == bucket:
                        raise gen.Return(b.get("bucket_name"))
        else:
            raise Exception("_get_bucket_name %s not found bucket." % id)

    def is_valid_time(self, encode_string, valid_time=24, key="RgoN"):
        valid_time_s = valid_time * 60 * 60

        if encode_string.startswith(key):
            encode_timestamp = encode_string.replace(key, "")
        else:
            return False

        timestamp = base64.urlsafe_b64decode(encode_timestamp)
        now_time = time.time()
        if now_time - float(timestamp) > valid_time_s:
            return False

        return True

    def codeing_valid_time(self, key="RgoN"):
        encode_time = base64.urlsafe_b64encode(str(time.time()))
        encode = key + encode_time

        return encode

    @gen.coroutine
    def _refresh_metadata_token(self, refresh_token):
        url = "{}?client_id={}&grant_type=refresh_token&refresh_token={}&&client_secret={}".format(
            AppServiceGlobal.config.md_oauth_token,
            AppServiceGlobal.config.md_client_id,
            refresh_token,
            AppServiceGlobal.config.md_secret)
        try:
            # 获取到token, refresh_token
            token_resp = requests.get(url=url)
            if token_resp.status_code == 401:
                logging.error("AppServiceUtils._refresh_metadata_token authorization failed")
                raise APIError(401, message="metadata authorize failed")
            token = token_resp.json()
            return token
        except Exception as err:
            logging.error("OAuth.OAuthHandler.code() oauth exception {}".format(err))
            raise APIError(400, message="OAuth server exception")

    @gen.coroutine
    def _request_metadata_api(self, key=None, method='GET', filters=None,
                              data=None, headers=None, session=None, appToken=None):
        '''
            @key: /url
            @method: GET/DELETE/POST/PUT
            @filters: parameters
            @data: body
        '''

        if key and key.startswith('/'):
            key = "/v1{}".format(key)
        else:
            key = "/v1/{}".format(key)
        if not headers:
            headers = {}
        if "Authorization" not in headers:
            if session:
                now = int(time.time())
                if not session.expires_at or int(session.expires_at) < now:
                    if not session.refresh_token:
                        logging.error("AppUtils.AppUtils.requestmetadataapi authorization failed")
                        raise APIError(401, message="ms auth failed")
                    # refresh token
                    token = yield self._refresh_metadata_token(session.refresh_token)
                    session.access_token = token.get("access_token")
                    session.refresh_token = token.get("refresh_token")
                    session.expires_at = token.get("expires_in") + now
                    session.token_type = token.get("token_type")
                    self._update_session(session)
                headers["Authorization"] = "{} {}".format(session.token_type, session.access_token)
            if appToken is None and session is None:
                from Forge.Common.AppToken import AppMSToken
                appMsToken = AppMSToken()
                session = yield appMsToken.get_token()
                headers["Authorization"] = "{} {}".format(session.get("token_type"),
                                                          session.get("access_token"))

        try:
            client = AsyncHTTPClient()

            if method and (method.lower() in ['get', 'delete']):
                url = '{0}{1}'.format(AppServiceGlobal.config.metadata_url, key)
                resp = yield gen.Task(client.fetch,
                                      url,
                                      method=method,
                                      headers=headers)

            elif method and (method.lower() in ['post', 'put', 'patch']):
                url = '{0}{1}'.format(AppServiceGlobal.config.metadata_url, key)
                resp = yield gen.Task(client.fetch,
                                      url,
                                      method=method,
                                      headers=headers,
                                      body=json.dumps(data))

            else:
                logging.error("AppServiceUtil.request_ms method error")
                raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')

            if resp.code == 401:
                logging.error("---- ms url : {}".format(url))
                logging.error("---- ms token: {}".format(headers))

                from Forge.Common.AppToken import AppMSToken
                appMsToken = AppMSToken()
                yield appMsToken.get_token(reset=True)
                logging.error("AppServiceUtil._request_metadata_api() Metadata auth failed")
                raise APIError(401, message="metadata auth failed")

            if resp.code == 400:
                logging.error("---- ms url : {}".format(url))
                logging.error("---- ms token: {}".format(headers))

                logging.error("AppServiceUtil._request_metadata_api() ms response code error")
                raise APIError(400, message="ms response code error")
        except APIError as api_err:
            raise api_err
        except Exception as err:
            logging.error("---- ms url : {}".format(url))
            logging.error("---- ms token: {}".format(headers))

            logging.info('AppServiceUtil._request_metadata_api() result {0}'.format(err))
            raise APIError(400, message="Metadata exception")
        raise gen.Return(resp)

    def _request_params(self, params=None, *args, **kwargs):
        '''
            Return string include the parameters that is a dictionary
        '''
        if isinstance(params, dict):
            _tmp = ''
            for k, v in params.iteritems():
                if v and isinstance(v, basestring):
                    _tmp = '{param}{key}={value}&'.format(param=_tmp or '?', key=k, value=v)
                if v and isinstance(v, list):
                    for oneValue in v:
                        _tmp = '{param}{key}={value}&'.format(
                            param=_tmp or '?', key=k, value=oneValue)
            params = _tmp
        elif isinstance(params, str):
            pass
        else:
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        return params

    def _request_return_format(self, response, *args, **kwargs):
        '''
            Return HTTP Return Result. Including HTTP Status and HTTP Return text [json]
        '''
        api_return = {'code': response.code}
        try:
            if 200 <= response.code < 400:
                resp = response.body
                api_return = json.loads(resp)
        except Exception as err:
            logging.error('AppServiceUtil._request_return_format() Return Error {0}'.format(err))
        return api_return

    @gen.coroutine
    def _filter_valid_user(self, data, filter_key, before, way='list', *args, **kwargs):
        '''
            @param data: 需要过滤的数据,仅接受列表和字典
            @param filter_key: 过滤关键字,采用 a.b.c 的方式
            @param before: 关键字的上一级,仅仅支持列表
            @param way: 过滤最后字段是放在列表中(list),还是字典中(dict)
            @param args: 其它参数
        '''

        try:
            for tdata in data:
                remove_data = []
                for t in before.split('.'):
                    tdata = tdata.get(t)
                for td in tdata:
                    if isinstance(tdata[0], basestring):
                        pass
                    if isinstance(tdata[0], dict):
                        if not (yield self._check_userId_is_exist(td.get(filter_key))):
                            remove_data.append(td)
                try:
                    if remove_data and isinstance(remove_data, list):
                        for rd in remove_data:
                            tdata.remove(rd)
                except ValueError as e:
                    logging.error("AppServiceUtil() {}".format(e))
        except Exception as err:
            logging.error("AppServiceUtil._filter_valid_user() params is error {}".format(err))
            raise APIError(400, message="Filter params is error")
        raise gen.Return(data)

    @gen.coroutine
    def _get(self, url, **kwargs):
        try:
            resp = yield self._request("GET", url, **kwargs)
        except Exception as err:
            logging.error("AppServiceUtil.AppServiceUtil._get() {}".format(err))
            raise APIError(400, message="http request error ")
        raise gen.Return(resp)

    @gen.coroutine
    def _post(self, url, **kwargs):
        try:
            resp = yield self._request("POST", url, **kwargs)
        except Exception as err:
            logging.error("AppServiceUtil.AppServiceUtil._post() {}".format(err))
            raise APIError(400, message="http request error ")
        raise gen.Return(resp)

    @gen.coroutine
    def _put(self, url, **kwargs):
        try:
            resp = yield self._request("PUT", url, **kwargs)
        except Exception as err:
            logging.error("AppServiceUtil.AppServiceUtil._put() {}".format(err))
            raise APIError(400, message="http request error ")
        raise gen.Return(resp)

    @gen.coroutine
    def _delete(self, url, **kwargs):
        try:
            resp = yield self._request("DELETE", url, **kwargs)
        except Exception as err:
            logging.error("AppServiceUtil.AppServiceUtil._delete() {}".format(err))
            raise APIError(400, message="http request error ")
        raise gen.Return(resp)

    @gen.coroutine
    def _request(self, method, url, **kwargs):
        try:
            client = AsyncHTTPClient()
            resp = yield gen.Task(client.fetch,
                                  url,
                                  method=method,
                                  **kwargs)
        except Exception as err:
            logging.error("AppServiceUtil.AppServiceUtil._request() {}".format(err))
            raise APIError(400, message="http request error ")

        raise gen.Return(resp)

    def _ipa_input_except(self, body, input_str="", input_type="POST"):
        res = ""
        try:
            res = body.get(input_str)
        except Exception:
            pass
        finally:
            try:
                if not res.strip():
                    res = res.lower().strip().encode('utf-8')
            except AttributeError:
                logging.warning("AppServiceUtil.AppServiceUtil.get() is None")
        return res

    @gen.coroutine
    def _create_app_access(self, app_id=None, grant_type=None, code=None):
        '''
        @param tid: token id
        @param client_id: app token id
        @param grant_type: app type
        @param client_secret: app client secret
        '''
        doc = {
            'access_token': code,
            'state': 'login',
            'expired_time': str(int(SEC_PER_HOUR)),
        }
        base = {
            'app_id': app_id,
            'grant_type': grant_type,
            'expired_time': str(int(SEC_PER_HOUR)),
            'create_at': int(time.time()),
            'update_at': int(time.time()),
        }
        base.update(doc)
        res = yield AppServiceGlobal.db.access_token.insert(base)
        if not res:
            logging.error("AppServiceUtil._create_app_access() db failed")
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        raise gen.Return(doc)

    @gen.coroutine
    def _check_app_access(self, app_id=None):
        '''
        @param app_id: app token id
        '''
        doc = {
            'app_id': app_id,
        }
        res = yield AppServiceGlobal.db.access_token.find_one(doc)
        status = True if res else False
        raise gen.Return(status)

    @gen.coroutine
    def _update_app_access(self, app_id=None, code=None):
        '''
        @param tid: token id
        @param client_id: app token id
        @param grant_type: app type
        @param client_secret: app client secret
        '''
        param = {
            'app_id': app_id,
        }
        doc = {
            'access_token': code,
            'state': 'login',
            'expired_time': str(int(SEC_PER_HOUR)),
        }
        base = {
            'app_id': app_id,
            "update_at": int(time.time())
        }
        base.update(doc)
        setting = {
            "$set": base
        }
        res = yield AppServiceGlobal.db.access_token.update(param, setting)
        if not res:
            logging.error("AppServiceUtil._update_app_access() db failed")
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_DB')

        raise gen.Return(doc)

    @gen.coroutine
    def _check_app_access_token(self, access_token=None):
        '''
        @param app_id: app token id
        '''
        doc = {
            'access_token': access_token,
        }
        res = yield AppServiceGlobal.db.access_token.find_one(doc)
        status = True if res else False
        raise gen.Return(status)

    @gen.coroutine
    def _check_app(self, client_id=None, grant_type=None, client_secret=None):
        '''
        @param client_id: app token id
        @param grant_type: app type
        @param client_secret: app client secret
        '''
        appId = yield AppServiceGlobal.db.app.find_one(
            {
                'client_id': client_id,
                'grant_type': grant_type,
                'client_secret': client_secret
            },
            {
                "id": True,
                "_id": False
            })

        aId = appId["id"] if appId else False
        raise gen.Return(aId)
