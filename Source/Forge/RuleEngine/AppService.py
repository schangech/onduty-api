#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:31:19
# @Updated Date: 2016-09-12 11:57:48

"""
AppService application instance class.
"""

import os
import sys
import signal
import errno
import redis
import logging
import logging.handlers
from optparse import Option
from motor import MotorClient
from tornado import web, ioloop, autoreload
from tornado.httpclient import AsyncHTTPClient


from Forge.Common.Routes import get_routes
from Forge.Common.Session import RedisSessionManager
from Forge.Common.Global import Default
from Forge.Common.Utils import ignored
from Runtime.Config import ConfigException
from Runtime.Log import LogFormatter
from Runtime.ScribeHandler import ScribeHandler
from Runtime.Command import LocalCommand
from Forge.AppService.AppServiceGlobal import AppServiceGlobal, AppServiceConfig


# -----------------------------------------------------
#  Exception
# -----------------------------------------------------
class AppServiceException(Exception):
    """A simple exception class used for AppService exceptions"""
    pass


class AppService():

    def __init__(self, foreground=True):

        self.__foreground = foreground

        # init logger
        try:
            level = logging.getLevelName(AppServiceGlobal.config.log_level)
            logging.getLogger().setLevel(level)
            logger = logging.getLogger()
            if self.__foreground:
                # Set up color if we are in a tty and curses is installed
                fh = logging.StreamHandler()
                formatter = LogFormatter(console=True)
                fh.setFormatter(formatter)
                logger.addHandler(fh)
        except Exception as error:
            raise AppServiceException(error)

    def start(self):
        """
            Start tornado application.
        """
        try:
            import Handlers
            handlers = get_routes(Handlers)

            logging.info("AppService routes: {0}".format(handlers))

            settings = {
                'debug': True,
                'gzip': True,
                'static_path': AppServiceGlobal.config.attachment_dir
            }
            self.application = web.Application(handlers, **settings)
            self.application.listen(int(AppServiceGlobal.config.service_port))

            logging.info("AppService is starting.")
            ioloop.IOLoop.instance().start()
        except Exception as error:
            raise AppServiceException(error)


if __name__ == '__main__':
    usage = "[-c] [-f] [-d] [-p]"
    version = "0.1"
    cmd = LocalCommand(usage, version, sys.argv)
    cmd.add_option(Option(
        "-c", "--conf", action="store", type="string", dest="conf",
        help="""
            The location of the AppService configuration file. If not
            specified, it will try to search under $INSTANT_HOME/conf/instant.cfg
        """))
    cmd.add_option(Option(
        "-f", "--fg", action="store_true", dest="foreground",
        help="""
            AppService logs to foreground. Default is background
        """))
    cmd.add_option(Option(
        "-d", "--debug", action="store_true", dest="debug",
        help="""
            Run AppService in debug mode. Default is Not
        """))
    cmd.add_option(Option(
        "-p", "--port", action="store", dest="port",
        help="""AppService port"""))
    cmd.parse_options()
    if not cmd.opt.foreground:
        cmd.opt.debug = False

    # Check if a daemon is already running
    if cmd.opt.foreground:
        pidfile = Default.Forge.AppService.pidfile
        if os.path.exists(pidfile):
            pf = file(pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()

            try:
                os.kill(pid, signal.SIG_DFL)
            except OSError, (err, msg):
                if err == errno.ESRCH:
                    # Pidfile is stale. Remove it.
                    os.remove(pidfile)
                else:
                    msg = "Unexpected error when checking pid file '{0}'.\n{1}\n".format(pidfile, msg)
                    sys.stderr.write(msg)
                    sys.exit(1)
            else:
                msg = "AppService is already running (pid {0})\n".format(pid)
                sys.stderr.write(msg)
                sys.exit(1)
        else:
            if not os.path.exists(os.path.dirname(Default.Forge.AppService.pidfile)):
                os.mkdir(os.path.dirname(Default.Forge.AppService.pidfile))
            file(Default.Forge.AppService.pidfile, 'w+').write("%i\n" % os.getpid())

    # load the config and init
    try:
        if cmd.opt.conf is None:
            if "INSTANT_HOME" in os.environ:
                cmd.opt.conf = os.environ["INSTANT_HOME"] + "/conf/instant.cfg"
            else:
                cmd.opt.conf = "instant.cfg"
        if not os.path.exists(cmd.opt.conf):
            cmd.error("Missing configuration file %s" % cmd.opt.conf)

        AppServiceGlobal.debug = cmd.opt.debug if cmd.opt.debug is not None else False
        AppServiceGlobal.config = AppServiceConfig(cmd.opt.conf)
        if AppServiceGlobal.debug:
            AppServiceGlobal.config.log_level = 'DEBUG'
        if cmd.opt.port:
            AppServiceGlobal.config.service_port = int(cmd.opt.port)

        AppServiceGlobal.session_manager = RedisSessionManager(
            hostname=AppServiceGlobal.config.redis_host,
            port=AppServiceGlobal.config.redis_port,
            db=AppServiceGlobal.config.redis_db,
        )

        AppServiceGlobal.db = MotorClient(AppServiceGlobal.config.mongo_url)[AppServiceGlobal.config.mongo_db]
        AppServiceGlobal.redis_db = redis.Redis(host=AppServiceGlobal.config.redis_host,
                                                port=AppServiceGlobal.config.redis_port,
                                                db=AppServiceGlobal.config.redis_db)

        # log : connection to scribe server
        for i in range(3):
            with ignored(Exception):
                level = logging.getLevelName(AppServiceGlobal.config.log_level)
                formatter = logging.Formatter("%(asctime)s-%(name)s-%(levelname)s-%(message)s")
                if AppServiceGlobal.config.scribe_enable:
                    handler = ScribeHandler(
                        host=AppServiceGlobal.config.scribe_host,
                        port=AppServiceGlobal.config.scribe_port,
                        category='%(hostname)s-OnDuty.AppService'
                    )
                else:
                    log_file = AppServiceGlobal.config.log_dir + '/RuleEngine_' + os.uname()[1] + '.log'
                    handler = logging.handlers.RotatingFileHandler(log_file, mode='a', maxBytes=(1024*1024*10))
                handler.setFormatter(formatter)
                logger = logging.getLogger()
                logger.removeHandler(handler)
                logger.addHandler(handler)
                logger.setLevel(level)
                break
        else:
            logging.error('AppService: cannot connect to log service')
            sys.exit(255)

        AppServiceGlobal.http_client = AsyncHTTPClient(max_clients=1000)

    except KeyError, msg:
        print >> sys.stderr, "Missing environment setting:" + str(msg)
        sys.exit(1)
    except ConfigException, msg:
        print >> sys.stderr, "Cannot start AppService: {0}".format(msg)
        sys.exit(1)

    # start
    rs = AppService(cmd.opt.foreground)
    rs.start()
