#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
# File: ReEvent.py
# Time: 2:07:13 PM
# Desc:
# Author: sendal
# Email: <schangech@gmail.com>
# Version: 0.0.1
'''

import logging
import json
import time
import copy
import jinja2
from tornado import gen
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from Runtime.Constants import (
    MIN_LENGTH,
    LOGIN_SHELL,
    DEFAULT_LOCALE)
from Forge.Common.Result import Result
from Forge.Common.Utils import is_email, validate_base64
from Forge.Common.Schema import validate
from Forge.Common.Gen import coroutine
from Forge.Common.Email import Email
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil
from Forge.Common.Utils import ignored, parse_department


class ReEvent(object):

    @staticmethod
    def __format(agent, **kwargs):
        try:
            agent = agent.lower()
            template = copy.deepcopy(ReEvent._data.get(agent))

            env = jinja2.Environment()
            return env.from_string(template).render(**kwargs)
        except Exception:
            return False

    __data = {
        'zabbix': '''
{
    "event_type": "{{event_type}}",
    "service_key": "{{service_key}}",
    "description": "{{description}}",
    "details": {
        "status": "{{status}}",
        "priority": "{{priority}}",
        "endpoint": "{{hostname}}",
        "event_id": "{{event_id}}",
        "metric": "{{metric}}",
        "content": "{{content}}",
        "ip": "{{ip}}",
        "severity": "{{severity}}",
        "name": "{{name}}"
    },
    "agent": {
        "queued_by": "{{queued_by}}",
        "queued_at": "{{queued_at}}",
        "agent_id": "{{agent_id}}"
    }
}
''',

        'falcon': '''
{
    "event_type": "{{event_type}}",
    "service_key": "{{service_key}}",
    "description": "{{description}}",
    "details": {
        "status": "{{status}}",
        "priority": "{{priority}}",
        "endpoint": "{{endpoint}}",
        "event_id": "{{event_id}}",
        "metric": "{{metric}}",
        "content": "{{content}}",
        "ip": "{{ip}}",
        "severity": "{{severity}}",
        "name": "{{name}}"
    },
    "agent": {
        "queued_by": "{{queued_by}}",
        "queued_at": "{{queued_at}}",
        "agent_id": "{{agent_id}}"
    }
}
'''
    }


class ReEventUtilsHandler(APIHandler, AppServiceUtil):
    pass


class ReEventHandler(ReEventUtilsHandler):
    @validate(
        output_schema={
            "type": "object"
        }
    )
    @coroutine
    def post(self):
        body = json.loads(self.request.body)
        source = "auto"
        event = body.get('event_type', None)
        if not event:
            logging.error('ReEvent.ReEventHandler.post() param error {0}'.format(body))
            raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        try:
            agent = body.get('agent').get('queued_by').split('-')[-1]
        except Exception as e:
            logging.warning('ReEvent.ReEventHandler.post() param error {0}'.format(e))
            # raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        try:
            if agent.lower() == 'zabbix':
                temp = ReEvent().__format(agent, **body)
            elif agent.lower() == 'falcon':
                temp = ReEvent().__format(agent, **body)
            else:
                hostname = ''
                source = "manual"
        except Exception:
            pass
            # raise APIError(400, flag='COMMON.GLOBAL.ERROR_PARAM')
        agent_id = body.get('agent').get(temp.get('agent').get('agent_id')).strip().encode('utf-8')
        service_type = body.get('agent').get(temp.get('agent').get('queued_by')).strip().encode('utf-8')
        queued_at = body.get('agent').get(temp.get('agent').get('queued_at')).strip().encode('utf-8')

        service_key = body.get(temp.get('service_key')).strip().encode('utf-8')
        description = body.get(temp.get('description')).strip().encode('utf-8')
        event_type = body.get(temp.get('event_type')).strip().encode('utf-8')

        event_id = body.get('details').get(temp.get('details').get('event_id')).strip().encode('utf-8')
        status = body.get('details').get(temp.get('details').get('status')).strip().lower().encode('utf-8')
        endpoint = body.get('details').get(temp.get('details').get('endpoint')).strip().lower().encode('utf-8')
        metric = body.get('details').get(temp.get('details').get('metric')).strip().lower().encode('utf-8')
        content = body.get('details').get(temp.get('details').get('content')).strip().lower().encode('utf-8')
        severity = body.get('details').get(temp.get('details').get('severity')).strip().lower().encode('utf-8')
        priority = body.get('details').get(temp.get('details').get('priority'), 3)
        ip = body.get('details').get(temp.get('details').get('ip')).strip().lower().encode('utf-8')
        name = body.get('details').get(temp.get('details').get('name')).strip().lower().encode('utf-8')

        (org, department) = parse_department(endpoint)
        logging.debug('ReEvent.ReEventHandler.post() info {0} {1}'.format(org, department))
        # 检查该事件次数，判断是否做次数报警升级，默认是没有次数报警升级
#                 info = yield AppServiceGlobal.db.ruleengine.find(
#                     {
#                         'metric': department,
#                         'source': source
#                      },
#                     {
#                         '_id': False
#                     }
#                 ).to_list(None)
        rules = {
            # str(uuid.uuid4()).replace('-','')
            "id": "74c9f75871c04df7953d1f8abd717e18",
            "metric": "devops",
            "source": "auto",
            "type": "single",
            "receiver": "suqirui",
            "add_time": int(time.time()),
            "update_time": int(time.time())
        }
        if rules.get('metric') == department and rules.get('source') == source:
            raise gen.Return({"data": rules.get('receiver')})
        else:
            raise gen.Return({"data": "not found the receivers"})


















