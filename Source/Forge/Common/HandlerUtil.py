#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2015-11-19 09:26:03
# @Updated Date: 2015-12-21 17:11:22

"""
NotFoundHandler.
"""

from tornado import web, ioloop

class NotFoundHandler(web.RequestHandler):

    def get(self):
        raise web.HTTPError(404)
