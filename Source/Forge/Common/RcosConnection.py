#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Alex <lisuo@rongcapital.cn>
# Date: 2017-03-17  16:24

"""
RcosConnection API
"""

import re
import json
import io
import hashlib
import tornado
from PIL import Image
import logging

from tornado import gen
from tornado.httpclient import HTTPRequest, AsyncHTTPClient
from tornado.simple_httpclient import SimpleAsyncHTTPClient
from tornado.httpclient import HTTPError

from Runtime.Constants import (BUCKETNAME, SERVICETOKEN, IMGSIZE)

class RcosException(HTTPError):
    pass

class BucketError(RcosException):
    pass

class ObjectError(RcosException):
    pass

class ServiceError(RcosException):
    pass


class RcosConnection():

    def __init__(self, host, port, service_id, bucket=None):
        self._valid_bucket(bucket)
        self.service_id = service_id
        self.host = host
        self.port = port
        self.bucket_default = bucket
        self.bucket_list = []
        self.token = None
        self.access_key = None
        self.secret_key = None
        self.service = None

    @gen.coroutine
    def _mexe(self, request):
        #client = AsyncHTTPClient()
        client = SimpleAsyncHTTPClient(
            max_body_size=1000*1024*1024,
            max_buffer_size=1000*1024*1024)

        resp = yield client.fetch(request)
        raise gen.Return(resp)

    @gen.coroutine
    def make_request(self, method, bucket=None, key=None, headers=None,
                     data=None, query_arg=None, admin_api=False):

        if headers is None:
            headers = {}

        if not admin_api:
            headers['Authorization'] = json.dumps(
                {"access_key": self.access_key,
                 "secret_key": self.secret_key})
            if bucket is None:
                bucket = self.bucket_default
            if key:
                endpoint = "%s/%s" % (bucket, key)
            else:
                endpoint = "%s" % (bucket)
        else:
            #headers['Token'] = self.token
            endpoint = "Service"

        if query_arg:
            endpoint += "?" + query_arg

        url = "http://%s:%s/%s" % (self.host, self.port, endpoint)
        logging.debug("url: %s\nmethod: %s\nheaders: %s" % (
            url, method, headers))

        request = HTTPRequest(
            url=url, method=method, headers=headers,
            body=data, allow_nonstandard_methods=True,
            request_timeout=60.0)

        resp = yield self._mexe(request)
        raise gen.Return(resp)

    @gen.coroutine
    def _create_service(self):

        headers = {}
        body = {}

        if self.service:
            raise gen.Return()

        if (yield self._get_service()) is not None:
            raise gen.Return()

        if self.token:
            headers = {"Token": self.token}

        body['ms_service'] = False
        body['service_id'] = self.service_id
        body['display_name'] = self.service_id

        if self.access_key and self.secret_key:
            body['generate_key'] = False
            body['access_key'] = self.access_key
            body['secret_key'] = self.secret_key

        body = json.dumps(body)

        try:
            resp  = yield self.make_request(
                method='PUT', headers=headers, data=body, admin_api=True)
        except HTTPError as e:
                raise ServiceError(e.code, message=e.message)
        
        resp = json.loads(resp.body)
        self.service = resp
        self.access_key = resp['keys'][0]['access_key']
        self.secret_key = resp['keys'][0]['secret_key']


    @gen.coroutine
    def _get_service(self):
        headers = {}
        body = {}

        if self.service:
            raise gen.Return()
        if self.token:
            headers = {"Token": self.token}

        query_arg = "service_id=%s" % self.service_id
        try:
            resp = yield self.make_request(
                method='GET', headers=headers, query_arg=query_arg,
                admin_api=True)
        except HTTPError as e:
            if e.code == 404:
                raise gen.Return(None)

        ret = json.loads(resp.body)
        self.service = ret
        self.access_key = ret['keys'][0]['access_key']
        self.secret_key = ret['keys'][0]['secret_key']

        raise gen.Return(True)

    def delete_service(self):
        pass 

    def _valid_bucket(self, name):
        name = re.compile("[a-z\-]")
        if not name:
            raise BucketError("Bucket name error.")

    @gen.coroutine
    def _create_bucket(self, name, headers=None, para=None):

        self._valid_bucket(name)

        if self.service is None:
            yield self._create_service()

        if name in self.bucket_list:
            raise gen.Return()

        try:
            resp  =  yield self.make_request(
            method='PUT', bucket=name, headers=headers)
        except HTTPError as e:
            if e.code == 409:
                pass
            else:
                raise BucketError(e.code, message=e.message)

        self.bucket_list.append(name)

    def _check_bucket(self, name):
        if name not in self.bucket_list:
            raise  BucketError("Can't found bucket %s" % bucket_name)

    def delete_bucket(self, name):
        pass

    def _get_file_md5(self, file_content):
        hash_md5 = hashlib.md5()
        hash_md5.update(file_content)

        return hash_md5.hexdigest()

    def resize_img(self, file_content, size, img_format="JPEG"):

        file_bytes = io.BytesIO()

        try:
            img = Image.open(file_content)
            img_format = img.format if img.format else  img_format
            img.thumbnail(size, Image.ANTIALIAS)
            img.save(file_bytes, img.format)
        except IOError as e:
            raise IOError("Open resize file error.")

        return file_bytes.getvalue()

    @gen.coroutine
    def upload_object(self, key, data, bucket_name=None):
        """
        Upload object

        :type   key                     : string
        :param  key                     : object name
        :type   bucket_name             : string
        :param  bucket_name             : bucket name
        :type   data                    : io.Bytes
        :param  data                    : object body

        return
        """
        if bucket_name and bucket_name not in self.bucket_list:
            yield self._create_bucket(bucket_name)

        elif self.bucket_default not in self.bucket_list:
            yield self._create_bucket(self.bucket_default)

        try:
            resp = yield self.make_request(
                method="PUT", bucket=bucket_name, key=key, 
                data=data)
        except HTTPError as e:
            raise ObjectError(e.code, message=e.message)

    @gen.coroutine
    def upload_request_files(self, request, reset_size=False,
                             bucket_name=None, dir_name=None):
        """
        Process object request.

        :type   request    : HTTPServerRequest class
        :param  request    : Request content.
        :type   bucket_name: String
        :param  bucket_name: Bucket name. (Name must lowercase or -)
        :type   reset_size : Boolean
        :param  reset_size : Enable image crop, default is disable.
        :type   dir_name   : String
        :param  dir_name   : Specific key name.

        :raise  RCOSException
        :return dict object.
        """
        if bucket_name:
            self._check_bucket(bucket_name)

        _ret = []
        for f in request.files:
            file_object = request.files[f]
            for k in file_object:
                file_name = k['filename']

                if reset_size:
                    body = self.resize_img(io.BytesIO(k['body']), IMGSIZE)
                else:
                    body = k['body']
                content_type = k['content_type']
                key = self._get_file_md5(body)

                key_suffix = file_name.split('.')[-1] \
                    if file_name.find('.') != -1  and \
                    file_name.split('.')[-1] else None

                if  key_suffix and dir_name:
                    object_name = dir_name + '/' + key + '.' + key_suffix
                else:
                    object_name = key

                logging.debug("key name: %s" % object_name)
                yield self.upload_object(
                    key=object_name, data=body)
                if k.has_key('path'):
                    _ret.append({
                        "file_name": file_name, 
                        "key": object_name, 
                        "path": k['path']
                    })
                else:
                    _ret.append({
                        "file_name": file_name, 
                        "key": object_name, 
                    })


        raise gen.Return(_ret)

    @gen.coroutine
    def download_object(self, key, bucket_name=None):
        """
        Download object

        :type   key                     : string
        :param  key                     : object name
        :type   bucket_name             : string
        :param  bucket_name             : bucket name

        return
        """
        if bucket_name:
            self._check_bucket(bucket_name)

        if self.service is None:
            yield self._create_service()

        try:
            resp = yield self.make_request(
                method="GET", bucket=bucket_name, key=key)
        except HTTPError as e:
            raise ObjectError(e.code, message=e.message)
        raise gen.Return(resp.body)



    @gen.coroutine
    def delete_object(self, key, bucket_name=None):
        """
        Delete object

        :type   key                     : string
        :param  key                     : object name
        :type   bucket_name             : string
        :param  bucket_name             : bucket name

        return
        """
        if bucket_name:
            self._check_bucket(bucket_name)

        if self.service is None:
            yield self._create_service()

        resp = yield self.make_request(
            method="DELETE", bucket=bucket_name, key=key)

        if resp.code != 200:
            raise ObjectError(resp.code, message=resp.reason)
