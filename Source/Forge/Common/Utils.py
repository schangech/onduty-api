#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-08-20 18:27:35
# @Updated Date: 2016-09-21 10:55:16

import logging
import os
import re
import zipfile
import types
import pyclbr
import smtplib
import time
import uuid
import base64
import md5
import string
import inspect
from hashlib import sha256
from hmac import HMAC
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.utils import COMMASPACE
from functools import wraps
from contextlib import contextmanager
import signal
import functools
from Forge.Common.Email import Email
from Forge.Common.Global import Default
from Runtime.Constants import VALIDATION_CODES


def validate_base64(key):
    # validate and complete the base64 encode
    remain_num = len(key)%4
    if remain_num != 0:
        logging.info("validate_base64(): not completed base64 encode {0}".format(key))
        key += (4-remain_num)*'='

    return key


def is_email(email):
    """
        RFC822 Email Address Regex
        --------------------------

        Originally written by Cal Henderson
        c.f. http://iamcal.com/publish/articles/php/parsing_email/

        Translated to Python by Tim Fletcher, with changes suggested by Dan Kubb.

        Licensed under a Creative Commons Attribution-ShareAlike 2.5 License
        http://creativecommons.org/licenses/by-sa/2.5/
    """

    qtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]'
    dtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]'
    atom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+'
    quoted_pair = '\\x5c[\\x00-\\x7f]'
    domain_literal = "\\x5b(?:%s|%s)*\\x5d" % (dtext, quoted_pair)
    quoted_string = "\\x22(?:%s|%s)*\\x22" % (qtext, quoted_pair)
    domain_ref = atom
    sub_domain = "(?:%s|%s)" % (domain_ref, domain_literal)
    word = "(?:%s|%s)" % (atom, quoted_string)
    domain = "%s(?:\\x2e%s)*" % (sub_domain, sub_domain)
    local_part = "%s(?:\\x2e%s)*" % (word, word)
    addr_spec = "%s\\x40%s" % (local_part, domain)

    email_address = re.compile('\A%s\Z' % addr_spec)

    try:
        if not email_address.match(email):
            return False
        return True
    except Exception as error:
        logging.error("is_email(): email {0} - exception {1}".format(email, str(error)))
        return False


def is_phonenumbers(phone_numbers, territory_id='CN'):
    """
        Use phonenumbers package to parse, check and format phone numbers.
        Default is China phone numbers.
        (https://github.com/daviddrysdale/python-phonenumbers)
    """

    import phonenumbers

    try:
        phone_numbers = str(phone_numbers)
        numbers = phonenumbers.parse(phone_numbers, territory_id)
        is_valid = phonenumbers.is_valid_number(numbers)
        if not is_valid:
            return False

        return phonenumbers.format_number(numbers, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
    except Exception as error:
        logging.error("is_phonenumbers(): check phone numbers {0} exception \n{1}".format(str(phone_numbers), str(error)))
        return False


def is_username(username):
    try:
        if not re.match("^[a-zA-Z0-9_]+$", username):
            return False

        return True
    except Exception, e:
        logging.error("is_username(): {0} - invalid format username".format(username))
        return False


def container(dec):
    """Meta-decorator (for decorating decorators)

    Keeps around original decorated function as a property ``orig_func``

    :param dec: Decorator to decorate
    :type  dec: function
    :returns: Decorated decorator
    """
    # Credits: http://stackoverflow.com/a/1167248/1798683
    @wraps(dec)
    def meta_decorator(f):
        decorator = dec(f)
        decorator.orig_func = f
        return decorator
    return meta_decorator


def extract_method(wrapped_method):
    """Gets original method if wrapped_method was decorated

    :rtype: any([types.FunctionType, types.MethodType])
    """
    # If method was decorated with validate, the original method
    #   is available as orig_func thanks to our container decorator
    return wrapped_method.orig_func if \
        hasattr(wrapped_method, "orig_func") else wrapped_method


def is_method(method):
    method = extract_method(method)
    # Can be either a method or a function
    return type(method) in [types.MethodType, types.FunctionType]


def is_handler_subclass(cls, classnames=("ViewHandler", "APIHandler")):
    """
        Determines if ``cls`` is indeed a subclass of ``classnames``
        This function should only be used with ``cls`` from ``pyclbr.readmodule``
    """
    if isinstance(cls, pyclbr.Class):
        return is_handler_subclass(cls.super)
    elif isinstance(cls, list):
        return any(is_handler_subclass(s) for s in cls)
    elif isinstance(cls, str):
        return cls in classnames
    else:
        raise TypeError(
            "Unexpected pyclbr.Class.super type `{}` for class `{}`".format(
                type(cls),
                cls
            )
        )


def zipdir(src_dir, dst_zip, is_relative=True, ignore_list=[]):
    """
        Zip compress source directory into zip file dst_zip.
        Default getting the relative path in the archive with setting is_relative as True.
        Ignore files in the ingore_list if setted.
    """
    with zipfile.ZipFile(dst_zip, 'w') as ziph:
        for root, dirs, files in os.walk(src_dir):
            for file in files:  # compress files
                file_path = os.path.join(root, file)
                if file_path in ignore_list:
                    continue
                if is_relative:
                    ziph.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), os.path.join(src_dir, '..')), \
                        zipfile.ZIP_DEFLATED)
                else:
                    ziph.write(os.path.join(root, file))

            for dir in dirs:    # compress directories
                if is_relative:
                    ziph.write(os.path.join(root, dir), os.path.relpath(os.path.join(root, dir), os.path.join(src_dir, '..')), \
                        zipfile.ZIP_STORED)
                else:
                    ziph.write(os.path.join(root, dir))


def format_params(params_string):
    """
        Format get request params.
        Input params string format:
            param1_name=<param1_value>&param2_name=<param2_value>&...
        Output format:
            {
                param1: [p1_v1, p1_v2, ...],
                param2: [p2_v1, p2_v2, ...]
                ...
            }
    """
    if not isinstance(params_string, basestring) or not params_string:
        return

    params = {}
    for param_str in params_string.strip().split('&'):
        if not param_str or '=' not in param_str:
            continue

        param_list = param_str.strip().split('=')
        if len(param_list) < 2:
            continue

        if param_list[0] not in params:
            params[param_list[0]] = []
        params[param_list[0]].append(param_list[-1])

    return params


@contextmanager
def ignored(*exceptions):
    """
        Ignore raised exception.
    """
    try:
        yield
    except exceptions:
        pass


def send_email(email_from, email_to, code, format, smtp_host, smtp_port, smtp_username, smtp_pwd, **kwargs):
    try:
        msg = MIMEMultipart()
        msg['From'] = email_from
        msg['To'] = COMMASPACE.join(email_to)

        the_email = Email.format(code, **kwargs)
        msg['Subject'] = the_email['Subject']

        if format == 'text':
            msg.attach(MIMEText(the_email['Body'].encode('utf-8'), 'plain', 'UTF-8'))
        else:   # html
            msg.attach(MIMEText(the_email['Body'].encode('utf-8'), 'html', 'UTF-8'))

        # add attachments
        if kwargs.get('files'):
            for f in kwargs['files']:
                display_filename = os.path.basename(f)
                with open(f, "rb") as fh:
                    msg.attach(MIMEApplication(
                        fh.read(),
                        Content_Disposition='attachment; filename="%s"' % display_filename,
                        Name=display_filename
                    ))

        logging.info(u"send_email(): {0} - {1} - {2}".encode('utf-8').format(email_from, email_to, format))

        smtp = smtplib.SMTP_SSL(smtp_host, smtp_port, timeout=15)
        smtp.login(smtp_username, smtp_pwd)
        smtp.sendmail(email_from, email_to, msg.as_string())
    except Exception as error:
        logging.error("send_email(): {0}".format(str(error)))
        msg = 'Send email from {0} to {1} exception, \n{2}'.format(email_from, str(email_to), str(error))
        raise Exception(msg)


class TimeoutError(Exception):
    pass


def timeout(seconds, err_msg='Function call timed out'):
    """
        timeout deorater.
        Restrict function exec in special seconds or throw TimeoutError exception.
    """
    def decorated(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(err_msg)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result
        return functools.wraps(func)(wrapper)
    return decorated


def verification_code(size=(160, 40), length=4, img_type='png', mode="RGB", font_size=20, font_type=None,
    draw_line=True, n_line=(1, 2), draw_points=True, point_chance=2):
    import random
    from PIL import Image, ImageDraw, ImageFont, ImageFilter

    def get_chars(length):
        """
            verification code string.
        """
        return random.sample(VALIDATION_CODES, length)

    def backgroup_color(): # white
        """
            backgroup color
        """
        return (255, 255, 255)

    def txt_color(): # blue
        """verification code string color"""
        return (0, 0, 255)

    def create_strs(length):
        """draw verification code string"""
        c_chars = get_chars(length)
        strs = ' %s ' % ' '.join(c_chars)

        font = ImageFont.truetype(font_type, font_size)
        font_width, font_height = font.getsize(strs)

        draw.text(((width - font_width) / 3, (height - font_height) / 3),
                    strs, font=font, fill=txt_color())

        return ''.join(c_chars)

    def create_lines():
        """create `n_line` interferential lines"""
        line_num = random.randint(*n_line)

        for i in range(line_num):
            # 起始点
            begin = (random.randint(0, size[0]), random.randint(0, size[1]))
            #结束点
            end = (random.randint(0, size[0]), random.randint(0, size[1]))
            draw.line([begin, end], fill=(0, 0, 0))

    def create_points():
        """create `point_chance` interferential points"""
        chance = min(100, max(0, int(point_chance)))

        for w in xrange(width):
            for h in xrange(height):
                tmp = random.randint(0, 100)
                if tmp > 100 - chance:
                    draw.point((w, h), fill=(0, 0, 0))

    # create image
    width, height = size
    image = Image.new(mode, size, backgroup_color())
    # create font
    font = ImageFont.truetype(font_type, font_size)
    draw = ImageDraw.Draw(image)
    # create lines
    if draw_line:
        create_lines()

    # create points
    if draw_points:
        create_points()

    # create verification string
    strs = create_strs(length)

    # 图形扭曲参数
    params = [1 - float(random.randint(1, 2)) / 100,
              0,
              0,
              0,
              1 - float(random.randint(1, 10)) / 100,
              float(random.randint(1, 2)) / 500,
              0.001,
              float(random.randint(1, 2)) / 500
              ]
    image = image.transform(size, Image.PERSPECTIVE, params) # 创建扭曲

    image = image.filter(ImageFilter.EDGE_ENHANCE_MORE) # 滤镜，边界加强（阈值更大）

    return image, strs


# get next weekday from the given `datetime`
# weekday: 0 == Monday, 1 == Tuesday, 2 == Wednesday...
def special_weekday(datetime, weekday, flag='next'):
    from datetime import timedelta
    days_ahead = weekday - datetime.weekday()
    if days_ahead <= 0:     # Target day already happened this week
        days_ahead += 7

    the_date = datetime + timedelta(days_ahead)
    if flag == 'last':  # return last weekday
        return the_date - timedelta(7)

    # default return the next weekday
    return the_date


def makeSessionId(st):
    # create a unique web session id
        m = md5.new()
        m.update('this is a test of the emergency broadcasting system')
        m.update(str(time.time()))
        m.update(str(st))
        return string.replace(base64.encodestring(m.digest())[:-3], '/', '$')


def parse_department(hostname):
    # hostname: [location]-[company]-[department]-[business]-[v|p]-[test|online]-[index].host.dataengine.com
    # hostname: [location]-[company]-[department]-users
    # 屏蔽那些传过来admin,ipausers等信息
    group = re.compile('^(\w+)-(\w+)-(\w+)-.*$')
    department = None
    org_department = None
    with ignored(Exception):
        groups = group.match(hostname.strip())
        org_department = groups.groups()[0] + "-" + groups.groups()[1] + "-" + groups.groups()[2]
        department = groups.groups()[2]
    return (org_department, department)


def generate_user_info(email, password=None, telephone=None, title=None, wechat=None, \
                        superior=[], username=None, lastname=None, firstname=None, \
                        status="enabled", avatar=None, ctiIds=[], masters=[], keys=None):
    # Generate the IPA user info.
    params = dict()
    current_time = int(time.time())
    params['id'] = str(uuid.uuid4()).split('-')[-1]
    params['username'] = username if username else email.split('@')[0]
    params['email'] = email
    params['password'] = password
    params['lastname'] = lastname
    params['firstname'] = firstname
    params['telephone'] = telephone
    params['title'] = title
    params['wechat'] = wechat
    params['superior'] = superior
    params['status'] = status
    params['avatar'] = avatar
    params['add_time'] = current_time
    params['update_time'] = current_time
    params['cti_ids'] = ctiIds
    params['masters'] = masters
    params['rand_seed'] = keys
    return params


def encrypt_password(password, salt=None):
    if salt is None:
        salt = os.urandom(8)

#     assert 8 == len(salt)
#     assert isinstance(salt, str)

    if isinstance(password, unicode):
        password = password.encode('UTF-8')

#     assert isinstance(password, str)

    result = password
    for i in xrange(10):
        result = HMAC(result, salt, sha256).digest()

    return base64.b64encode(salt + result)


def is_valid_date(date):
    try:
        if ':' in date:
            time.strptime(date, "%H:%M:%S")
        else:
            time.strptime(date, "%H-%M-%S")
        return True
    except Exception:
        return False


def ipa_user_info_format_joint(total_info, org_id=None, regions_id=None, depart_id=None, staff_id=None, info_list=None, **kwargs):
    '''
        total_info   已经存在表
        org_id       组织/公司简写
        regions_id   地域/地区简写
        depart_id    部门简写
        staffs_list  部门员工列表
    '''
    staff_list = list()
    if staff_id:
        staff = dict()
        staff = info_list
        staff_list.append(staff)

    department_list = list()
    if depart_id:
        details = dict()
        details['id'] = depart_id
        if not staff_list:
            staff_list = info_list
        details['staffs'] = staff_list
        department_list.append(details)

    regions_list = list()
    if regions_id:
        regions = dict()
        regions['id'] = regions_id
        if not department_list:
            department_list = info_list
        regions['departments'] = department_list
        regions_list.append(regions)

    if org_id:
        info = dict()
        info['id'] = org_id
        if not regions_list:
            regions_list = info_list
        info['regions'] = regions_list

    if len(total_info):
        org_status = regions_status = dep_status = staff_status = False
        if org_id:
            for _info in total_info:
                if _info['id'] == org_id:
                    org_status = True

                    if regions_id:
                        for _info_regions in _info['regions']:
                            if _info_regions['id'] == regions_id:
                                regions_status = True

                                if depart_id:
                                    for _info_regions_department in _info_regions['departments']:
                                        if _info_regions_department['id'] == depart_id:
                                            dep_status = True

                                            if staff_id:
                                                for _info_regions_department_staff in _info_regions_department['staffs']:
                                                    if _info_regions_department_staff['username'] == staff_id:
                                                        staff_status = True
                                                        dict_info = _info_regions_department_staff
                                                        map(lambda x, y:dict_info.setdefault(x, y), staff.keys(), staff.values())

                                                if not staff_status:
                                                    _info_regions_department['staffs'].append(staff)
                                            else:
                                                _info_regions_department['staffs'] = list(set(_info_regions_department['staffs'] + info_list))

                                    if not dep_status:
                                        _info_regions['departments'].append(details)
                                else:
                                    _info_regions['departments'] = list(set(_info_regions['departments'] + info_list))

                        if not regions_status:
                            _info['regions'].append(regions)
                    else:
                        _info['regions'] = list(set(_info['regions'] + info_list))

            if not org_status:
                total_info.append(info)

    else:
        total_info.append(info)
    return total_info

def get_cls_name(cls, msg):
    return "%s.%s(): %s" % (
        cls.__class__.__name__,
        inspect.stack()[1][3],
        msg)


if __name__ == "__main__":

    # @timeout(5)
    # def slowfunc(sleep_time):
    #     import time
    #     time.sleep(sleep_time)

    # print("sleep 3s")
    # slowfunc(3)
    # print("success sleep 3s")

    # print("sleep 6s")
    # slowfunc(6)
    # print("success sleep 6s")

    code_img, strs = verification_code()
    code_img.save("validate.jpg", "JPEG")
