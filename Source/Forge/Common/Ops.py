#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-31 19:29:19
# @Updated Date: 2016-09-06 16:09:59

"""
Operation states of all modules.
"""

# ---- Account ----
OPS_ACCOUNT_CREATE = 'APS.Account.Create'
OPS_ACCOUNT_ACTIVE = 'APS.Account.Active'
OPS_ACCOUNT_REJECT = 'APS.Account.Reject'
OPS_ACCOUNT_RESET_PASSWORD = 'APS.Account.ResetPassword'
OPS_ACCOUNT_UPDATE_PASSWORD = 'APS.Account.UpdatePassword'

# ---- Ticket ----
OPS_TICKET_UPDATE = 'APS.Ticket.Update'
OPS_TICKET_CREATE = 'APS.Ticket.Create'

# ---- Scheduler ----
OPS_SCHEDULER_ALERT = 'Duty.Scheduler.Alert'

# ---- Scheduler ----
OPS_ACTIVITY_TRIGGER = 'Trigger'
OPS_ACTIVITY_OPEN = 'Open'
OPS_ACTIVITY_ACKNOWLEDGE = 'Acknowledge'
OPS_ACTIVITY_UNACKNOWLEDGE = 'Unacknowledge'
OPS_ACTIVITY_ASSIGN = 'Assign'
OPS_ACTIVITY_UPDATE = 'Update'
OPS_ACTIVITY_NOTIFY = 'Notify'
OPS_ACTIVITY_RESOLVE = 'Resolve'
OPS_ACTIVITY_CLOSE = 'Close'
OPS_SCHEDULE_REMINDER = 'Duty.Scheduler.Remind'
