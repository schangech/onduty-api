#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-08-20 18:27:35
# @Updated Date: 2015-12-21 17:11:08

"""
Wrapper of `tornado.gen.coroutine`.
"""

import inspect
from tornado import gen
from tornado import version_info as tornado_version_info

(TORNADO_MAJOR,
 TORNADO_MINOR,
 TORNADO_PATCH) = tornado_version_info[:3]


def coroutine(func, replace_callback=True):
    """
    Tornado-JSON compatible wrapper for ``tornado.gen.coroutine``
    Annotates original argspec.args of ``func`` as attribute ``__argspec_args``
    """
    # gen.coroutine in tornado 3.x.x has a different signature from 4.x.x
    if TORNADO_MAJOR == 3:
        wrapper = gen.coroutine(func)
    else:
        wrapper = gen.coroutine(func, replace_callback)
    wrapper.__argspec_args = inspect.getargspec(func).args
    return wrapper
