#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-08-26 17:39:19
# @Updated Date: 2017-01-16 14:51:21

import logging
import redis
import json

from Runtime.Constants import SESSION_EXPIRED
from Runtime.Constants import SECS_PER_MINUTE, MINUTES_PER_HOUR, HOURS_PER_DAY
from Runtime.UUID import uuidgen
from Forge.Common.Utils import ignored


class SessionException(Exception):
    """A simple exception class used for Session exceptions"""
    pass


class ExpiredException(Exception):
    """A simple exception class used for Session expired"""
    pass


class Session(object):

    def __init__(self, username, email, id=None, **kwargs):
        self.id = id if id else uuidgen()
        self.username = username
        self.email = email
        for k, v in kwargs.iteritems():
            if k not in vars(self):
                setattr(self, k, v)


class RedisSessionManager(object):

    def __init__(self, hostname, port=6379, db=0, password=None, socket_timeout=5):
        try:
            # redis 5秒之内没有链接成功，则失败
            self.__redis = redis.Redis(host=hostname, port=port, db=db, password=password, socket_timeout=socket_timeout)
            if self.__redis is None:
                raise SessionException()
        except Exception:
            logging.error("Common.RedisSessionManager.__init__(): {0} - {1} - {2} - cannot connect with redis".format(hostname, port, db))
            raise SessionException("Cannot connect with redis")

    def create(self, session):
        try:
            s = {}
            for k, v in vars(session).iteritems():
                s[k] = v if isinstance(v, basestring) else json.dumps(v)

            if 'email' not in s or 'id' not in s:
                raise SessionException("not found email, id in session")

            if not self.__redis.hmset(s['id'], s):
                raise SessionException("cannot create session of {0}/{1}".format(session.email, session.id))

            self.__redis.expire(s['id'], SESSION_EXPIRED)
            return session.id
        except Exception as error:
            logging.error("Common.RedisSessionManager.create(): {0} - {1}, \n{2}".format(session.email, session.id, str(error)))
            raise SessionException("create session {0}/{1} exception".format(session.email, session.id))

    def delete(self, session_id):
        try:
            if self.__redis.hget(session_id, 'id') != session_id:
                raise SessionException("invalid {0}".format(session_id))

            if not self.__redis.delete(session_id):
                raise SessionException("cannot delete session of {0}".format(session_id))
        except Exception as error:
            logging.error("Common.RedisSessionManager.delete(): {0}, \n{１}".format(session_id, str(error)))
            raise SessionException("delete session {0} exception".format(session_id))

    def update(self, session):
        try:
            s = {}
            for k, v in vars(session).iteritems():
                s[k] = v

            if 'email' not in s or 'id' not in s:
                raise SessionException("not found email, id in session")

            if not self.__redis.hmset(session.id, s):
                raise SessionException("cannot update session of {0}/{1}".format(session.email, session.id))
            # update expire(for debug)
            self.__redis.expire(session.id, SESSION_EXPIRED)
        except Exception as error:
            logging.error("Common.RedisSessionManager.update(): {0}/{1}, \n{2}".format(session.email, session.id, str(error)))
            raise SessionException("update session {0}/{1} exception".format(session.email, session.id))

    def get(self, session_id):
        session = None
        with ignored(Exception):
            session = self.__redis.hgetall(session_id)
        print(session, "==========================")
        if not session:
            logging.error("Common.RedisSessionManager.get(): {0}, not found session".format(session_id))
            raise ExpiredException("expired session {0}".format(session_id))

        try:
            # update expire(for debug)
            self.__redis.expire(session_id, SESSION_EXPIRED)

            # deserialize json
            for k, v in session.iteritems():
                with ignored(Exception):
                    session[k] = v

            return Session(
                id=session.pop('id'),
                email=session.pop('email'),
                **session
            )
        except Exception as e:
            logging.error("Common.RedisSessionManager.get(): {0}, \n{1}".format(session_id, str(e)))
            raise SessionException("get user {0} session exception".format(session_id))

    def verify(self, session_id):
        session = self.get(session_id)
        if session_id != session.id:
            logging.error("Common.RedisSessionManager.verify(), invalid session {1}".format(session_id))
            raise SessionException("invalid session {0}".format(session_id))

        return session

    def get_os_session(self, key='app-token'):

        with ignored(Exception):
            session = self.__redis.hgetall(key)
        if not session:
            logging.info("Common.RedisSessionManager.get(): {0}, not found session".format(key))

        # deserialize json
        _session = {}
        for k, v in session.iteritems():
            with ignored(Exception):
                k = str(k)
                v = str(v)
            _session[k] = v
        session = _session

        return session

    def update_os_session(self, token=None, key='openstack', SESSION_EXPIRED=None, value=None):

        if SESSION_EXPIRED is None:
            SESSION_EXPIRED = (MINUTES_PER_HOUR - 1) * SECS_PER_MINUTE

        if token:
            value = {
                'token': token
            }
        try:
            if not self.__redis.hmset(key, value):
                raise SessionException("cannot update session of {0}".format(key))

            # openstack token过期时间是1小时,设置redis提前一分钟过期
            self.__redis.expire(key, SESSION_EXPIRED)
        except Exception as error:
            logging.error("Common.RedisSessionManager.update(): {0}/{1}".format(key, str(error)))
            raise SessionException("update session {0} exception".format(key))

