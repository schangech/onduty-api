#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2015-12-16 11:51:25
# @Updated Date: 2017-01-17 13:59:59

import logging
import copy
from Forge.Common.Utils import ignored


class ResultException(Exception):
    """A simple exception class used for Result exceptions"""
    pass


class Result(Exception):

    _result = {
        'common': {
            'global': {
                'idx': 100,
                'ok': {
                    'en': '{0}',
                    'cn': '{0}'
                },
                'error_param': {
                    'en': 'Invalid API Parameters',
                    'cn': '输入参数错误'
                },
                'null_param': {
                    'en': 'Input API parameter cannot be null',
                    'cn': '输入参数不能为空'
                },
                'error_output': {
                    'en': 'Invalid output data',
                    'cn': '输出数据错误'
                },
                'error_session': {
                    'en': 'Session expired',
                    'cn': '会话超时'
                },
                'error_db': {
                    'en': 'DB operation failed',
                    'cn': '数据库操作失败'
                },
                'error_request': {
                    'en': 'Bad request format',
                    'cn': '请求格式错误'
                },
                'incomplete_request': {
                    'en': 'Incomplete request data',
                    'cn': '请求格式不完整'
                },
                'error_sso': {
                    'en': 'SSO request failed',
                    'cn': '用户系统服务异常'
                },
                'error_external': {
                    'en': 'Request service external error',
                    'cn': '服务请求出现异常'
                },
                'error_rcos': {
                    'en': 'Request service rcos error',
                    'cn': '服务请求RCOS出现异常'
                },
                'error_unknown': {
                    'en': 'Sorry, we are suffering from some technical issue, please try again later',
                    'cn': '对不起，我们的服务目前不可用，请稍后再试。'
                },
                'error_email': {
                    'en': 'Sent email failed',
                    'cn': '发送邮件失败'
                }
            },
            'session': {
                'create_failed': {
                    'en': 'Create user {0} session failed',
                    'cn': '创建用户 {0} 会话失败'
                },
                'destroy_failed': {
                    'en': 'Destroy user {0} session {1} failed',
                    'cn': '销毁用户 {0} 会话 {1} 失败'
                },
                'update_failed': {
                    'en': 'Update user {0} session {1} failed',
                    'cn': '更新用户 {0} 会话 {1} 失败'
                },
                'verify_failed': {
                    'en': 'Verify user {0} session {1} failed',
                    'cn': '验证用户 {0} 会话 {1} 失败'
                },
                'get_failed': {
                    'en': 'Get user {0} session {1} failed',
                    'cn': '获取用户 {0} 会话失败'
                },
                'invalid_session': {
                    'en': 'Invalid user session',
                    'cn': '用户会话超时'
                },
                'is_logged': {
                    'en': 'Your account has logged in',
                    'cn': '该帐号正在使用'
                }
            },
            'schema': {
                'not_json': {
                    'en': "Input params isn't json format",
                    'cn': '输入数据非JSON格式'
                },
                'no_resource': {
                    'en': "Can not find the resource",
                    'cn': "找不到资源"
                }
            },
            'account': {
                'limit_authority': {
                    'en': 'user limits of authority',
                    'cn': '普通用户没有更新数据权限'
                },
                'user_exist': {
                    'en': 'user is exist',
                    'cn': '用户已经存在'
                },
                'user_invalid': {
                    'en': 'user is invalid',
                    'cn': '用户数据错误'
                }
            }
        },
        'aps': {
            'handler': {
                'account': {
                    'idx': 110,
                    'invalid_email': {
                        'en': 'Invalid email format',
                        'cn': '输入的邮件格式错误'
                    },
                    'repeated_email': {
                        'en': 'Repeated email {0}',
                        'cn': '重复的Email {0}'
                    },
                    'invalid_phone': {
                        'en': 'Invalid telephone format',
                        'cn': '输入的电话格式错误'
                    },
                    'repeated_user': {
                        'en': 'Repeated user {0}',
                        'cn': '此邮箱已注册，请您用其他邮箱再次尝试。'
                    },
                    'no_user': {
                        'en': 'Not existed user {0}',
                        'cn': '用户 {0} 不存在'
                    },
                    'no_attribute': {
                        'en': 'User {0} missed attribute {1}',
                        'cn': '用户 {0} 缺少属性 {1}'
                    },
                    'inactive_user': {
                        'en': 'inactive',
                        'cn': '请先激活用户'
                    },
                    'blocked_user': {
                        'en': 'blocked',
                        'cn': '用户被锁定，请联系客服'
                    },
                    'removed_user': {
                        'en': 'removed',
                        'cn': '用户已经删除'
                    }
                },
                'account_password': {
                    'idx': 120,
                    'post_failed': {
                        'en': 'The reset-password request failed',
                        'cn': '重置密码请求失败'
                    },
                    'post_success': {
                        'en': 'The reset-password email have sent',
                        'cn': '重置密码邮件发送成功'
                    },
                    'put_success': {
                        'en': 'User {0} update password successfully',
                        'cn': '用户 {0} 重置密码成功'
                    },
                    'invalid_password': {
                        'en': 'Input wrong password',
                        'cn': '输入密码错误'
                    },
                    'error_key': {
                        'en': 'Error key',
                        'cn': 'key格式错误'
                    },
                    'expired_key': {
                        'en': 'key is expired',
                        'cn': 'key 已经失效'
                    },
                    'no_match': {
                        'en': 'password not match',
                        'cn': "密码不匹配"
                    },
                    'password_short': {
                        'en': 'password is too short',
                        'cn': '密码长度太小'
                    }
                },
                'account_inviter': {
                    'idx': 130
                },
                'account_activation': {
                    'invalid_key': {
                        'en': 'Error key',
                        'cn': '输入的key错误'
                    },
                    'activate_failed': {
                        'en': 'Active user {0} failed',
                        'cn': '激活用户 {0} 失败'
                    },
                    'activate_success': {
                        'en': 'Active user {0} successfully',
                        'cn': '成功激活用户 {0}'
                    }
                },
                'agent': {
                    'idx': 140,
                    'invalid_type': {
                        'en': 'Not support agent type {0}',
                        'cn': '不支持的探针类型{0}'
                    },
                    'over_limit': {
                        'en': 'Over agent number threshold',
                        'cn': '超过探针允许创建的最大值'
                    },
                    'update_success': {
                        'en': 'Update uesr {0} agent {1} info successfully',
                        'cn': '更新用户 {0} 探针 {1} 配置成功'
                    },
                    'invalid_agent': {
                        'en': 'Invalid input agent id {0}',
                        'cn': '输入的探针id {0} 错误'
                    },
                    'invalid_version': {
                        'en': 'Invalid agent version {0}',
                        'cn': '指定的探针版本 {0} 错误'
                    },
                    'repeated_name': {
                        'en': 'Repeated agent name {0}',
                        'cn': '重复的探针名 {0}'
                    }
                },
                'agent_group': {
                    'add_failed': {
                        'en': 'add new agent group failed',
                        'cn': '生成探针组失败'
                    }
                },
                'agent_package': {
                    'idx': 150,
                    'no_template': {
                        'en': 'Agent template diretory not existed',
                        'cn': '探针模板目录不存在'
                    },
                    'not_exist': {
                        'en': 'Agent package {0} not existed',
                        'cn': '探针包 {0} 不存在'
                    },
                    'compress_failed': {
                        'en': 'Compress agent package failed',
                        'cn': '压缩探针包失败'
                    },
                    'invalid_type': {
                        'en': 'not supported agent type {0}',
                        'cn': '目前不支持探针类型 {0}'
                    },
                    'invalid_arch': {
                        'en': 'not supported agent type {0} arch {1}',
                        'cn': '目前不支持{0}探针类型的{1}架构'
                    }
                },
                'agent_config': {
                    'not_exist': {
                        'en': 'Agent configuration {0} not exist',
                        'cn': '探针配置 {0} 不存在'
                    }
                },
                'alert': {
                    'idx': 160,
                    'put_success': {
                        'en': 'update alert {0} success',
                        'cn': '更新告警 {0} 配置成功'
                    }
                },
                'event': {
                    'idx': 170,
                    'no_stat': {
                        'en': 'no input the stat type',
                        'cn': '没有指定统计类型'
                    },
                    'no_spantime': {
                        'en': 'no input span time',
                        'cn': '没有指定扫描时间'
                    },
                    'invalid_index': {
                        'en': 'invalid input event index {0}',
                        'cn': '输入的事件编号{0}错误'
                    },
                    'bad_request': {
                        'en': 'stat request failed',
                        'cn': '统计请求失败'
                    },
                    'invalid_type': {
                        'en': 'invalid stat type {0}',
                        'cn': '目前不支持的统计类型 {0}'
                    }
                },
                'event_config': {
                    'idx': 210,
                    'invalid_status': {
                        'en': 'invalid input event config status {0}',
                        'cn': '输入的事件配置状态 {0} 错误'
                    },
                    'invalid_config': {
                        'en': 'invalid input event config id {0}',
                        'cn': '输入的事件配置编号 {0} 错误'
                    },
                    'error_config': {
                        'en': 'input event config {0} error',
                        'cn': '输入的事件配置 {0} 错误'
                    },
                    'invalid_operation': {
                        'en': 'invalid input event config operation {0}',
                        'cn': '输入的事件配置操作 {0} 错误'
                    }
                },
                'rule': {
                    'idx': 180,
                    'invalid_rule': {
                        'en': 'Invalid rule id {0}',
                        'cn': '输入的规则编号{0}错误'
                    }
                },
                'scheduler': {
                    'idx': 190,
                    'add_failed': {
                        'en': 'add scheduler job external error',
                        'cn': '添加扫描任务失败'
                    },
                    'update_failed': {
                        'en': 'update scheduler job external error',
                        'cn': '更新扫描任务失败'
                    }
                },
                'session': {
                    'idx': 220,
                    'delete_success': {
                        'en': 'User {0} has logged out',
                        'cn': '用户 {0} 注销成功'
                    },
                    'invalid_code': {
                        'en': 'Invalid validation code {0}',
                        'cn': '错误的验证码 {0}'
                    }
                },
                'verification': {
                    'idx': 230,
                    'over_limit': {
                        'en': 'You have retried too much times and please retry {0} minutes later',
                        'cn': '您尝试次数太多，请 {0} 分钟后再试'
                    },
                    'over_load': {
                        'en': 'Sorry, our service over loading and please retry later',
                        'cn': '对不起，我们的服务器超过负载，请稍后再试'
                    }
                },
                'email': {
                    'idx': 240,
                    'post_success': {
                        'en': 'Update email server configuration successfully',
                        'cn': '配置Email服务器参数成功'
                    },
                    'no_config': {
                        'en': 'Email server configuration not setted',
                        'cn': 'Email服务器参数还没有设置'
                    },
                    'no_email': {
                        'en': 'Email address not setted',
                        'cn': '用户Email地址还没有设置'
                    }
                },
                'report': {
                    'idx': 250,
                    'put_success': {
                        'en': 'Update report {0} config successfully',
                        'cn': '更新报告{0}配置成功'
                    },
                    'delete_success': {
                        'en': 'Delete report {0} config successfully',
                        'cn': '删除报告{0}配置成功'
                    }
                },
                'report_file': {
                    'idx': 260,
                    'not_exist': {
                        'en': 'Report file {0} not existed',
                        'cn': '威胁报告文件 {0} 不存在'
                    }
                },
                'cti': {
                    'idx': 270,
                    'exist': {
                        'en': 'CTI is existed',
                        'cn': 'CTI 已经存在'
                    }
                },
                'resolvegroup': {
                    'idx': 280,
                    'exist': {
                        'en': 'CTI is existed',
                        'cn': 'CTI 已经存在'
                    },
                    'user': {
                        'en': 'admin not allow to delete',
                        'cn': '管理员用户禁止删除'
                    },
                    'rgschedu': {
                        'en': '{0} had the scheduler',
                        'cn': '支持组{0}存在排班'
                    }
                },
                'portal': {
                    'idx': 290,
                    'exist': {
                        'en': '{0} is exist',
                        'cn': '{0} 已经存在'
                    },
                    'miss': {
                        'en': '{0} is not found',
                        'cn': '{0} 没有找到'
                    }
                },
                'netdisk': {
                    'error': {
                        'en': 'Request rcos error.',
                        'cn': '请求rcos异常.'
                    },
                    'dirinvalid':{
                        'en': 'Directory name is not exists', 
                        'cn': '目录名不存在.'
                    },
                    'pathinvalid':{
                        'en': 'Path name is not exists', 
                        'cn': '路径名不存在.'
                    },
                    'fileinvalid':{
                        'en': 'files is not exists', 
                        'cn': '文件不存在.'
                    },
                    'direxists':{
                        'en': 'Directory name is exists', 
                        'cn': '目录名已经存在.'
                    },
                    'dirnoempty':{
                        'en': 'Directory is not empty', 
                        'cn': '目录名已经存在.'
                    },
                    'fileexists':{
                        'en': 'Files is exists', 
                        'cn': '还有文件存在.'
                    },
                    'idinvalid':{
                        'en': 'email is invalid.', 
                        'cn': 'email 无效.'
                    },
                }
            }
        },
        'us': {
            'handler': {
                'account': {
                    'invalid_email': {
                        'en': 'Invalid email format',
                        'cn': '输入的邮件格式错误'
                    },
                    'invalid_phone': {
                        'en': 'Invalid telephone format',
                        'cn': '输入的电话格式错误'
                    },
                    'repeated_user': {
                        'en': 'Repeated user {0}',
                        'cn': '此邮箱已注册，请您用其他邮箱再次尝试。'
                    },
                    'no_user': {
                        'en': 'Not existed user {0}',
                        'cn': '用户 {0} 不存在'
                    },
                    'no_attribute': {
                        'en': 'User {0} missed attribute {1}',
                        'cn': '用户 {0} 缺少属性 {1}'
                    },
                    'inactive_user': {
                        'en': 'User {0} not is inactive',
                        'cn': '请先激活用户 {0}'
                    },
                    'blocked_user': {
                        'en': 'User {0} has been blocked',
                        'cn': '用户 {0} 被锁定，请联系客服'
                    },
                    'removed_user': {
                        'en': 'User {0} has been removed',
                        'cn': '用户 {0} 已经删除'
                    }
                },
            }
        },
        'duty': {
            'handler': {
                'scheduler': {
                    'no_ticket': {
                        'en': 'Can not find the opened ticket {0}',
                        'cn': '查找新增任务 {0} 失败'
                    },
                    'add_failed': {
                        'en': 'Add new scheduler job {0} failed',
                        'cn': '增加新的定时任务 {0} 失败'
                    },
                    'add_success': {
                        'en': 'Add new scheduler job {0} successfully',
                        'cn': '成功增加新的定时任务 {0}'
                    },
                    'update_failed': {
                        'en': 'Update scheduler job {0} failed',
                        'cn': '更新定时任务 {0} 失败'
                    },
                    'update_success': {
                        'en': 'Update scheduler job {0} successfully',
                        'cn': '成功更新定时任务 {0}'
                    },
                    'delete_failed': {
                        'en': 'Delete scheduler job {0} failed',
                        'cn': '删除定时任务 {0} 失败'
                    },
                    'delete_success': {
                        'en': 'Delete scheduler job {0} successfully',
                        'cn': '成功删除定时任务 {0}'
                    }
                }
            }
        }
    }

    def __init__(self, flag, *args, **kwargs):
        self.flag = flag
        self.locale = kwargs.get('locale')
        self.args = args

    @property
    def result(self):
        return self.__format()

    def __format(self):
        try:
            # check flag
            flag = self.flag.encode('utf-8').lower()
            if not flag:
                logging.error("Common.Result.__format(): Invalid result flag {0}".format(flag))
                raise ResultException("Invalid result flag")

            code_list = flag.split('.')
            if len(code_list) < 3:
                logging.error("Common.Result.__format(): Invalid result flag {0}".format(flag))
                raise ResultException("Invalid result flag format")

            # get return code
            msg = None
            with ignored(Exception):
                result = self._result

                for i in xrange(len(code_list)):
                    result = result[code_list[i]]

                msg = copy.deepcopy(result.get(self.locale or 'en'))

            if not msg:
                logging.error("Common.Result.__format(): Not supported flag {0}".format(flag))
                raise ResultException("Not supported result flag")

            if self.args:
                msg = unicode(msg, 'utf8').format(*self.args)

            return msg
        except Exception as error:
            logging.error("Common.Result.__format(): {0}/{1}, exception \n{2}".format(self.flag, str(self.args), str(error)))
            result = self._result['common']['global']['error_unknown']
            return result.get(self.locale or 'en')
