#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
@Class:
@Date: 2017-09-07
@Module: Handlers.AppToken
@Author: sendal <schangech@gmail.com>
@Version:
'''

import logging

from tornado import gen

from Forge.Common.Gen import coroutine
from Forge.Common.Schema import validate
from Forge.Common.RequestHandlers import APIError
from Forge.AppService.AppServiceGlobal import AppServiceGlobal
from Forge.AppService.AppServiceUtil import AppServiceUtil
from Runtime.Constants import SECS_PER_MINUTE, MINUTES_PER_HOUR, HOURS_PER_DAY


class AppMSToken(AppServiceUtil):

    @gen.coroutine
    def __request_ms_app_key(self):
        # 针对非用户秘钥方式认证接口处理
        # http://10.200.0.217:8083/v1/oauth2/access_token?client_id=xxxx&grant_type=client_credentials&client_secret=xxxx
        url = "{}?client_id={}&grant_type=client_credentials&client_secret={}".format(
            AppServiceGlobal.config.md_oauth_token,
            AppServiceGlobal.config.md_client_id,
            AppServiceGlobal.config.md_secret)

        try:
            util = AppServiceUtil()
            appToken = yield util._get(url)
        except Exception as err:
            logging.error("AppToken.AppMSToken.ms_app_key() metadata link failed {}".format(err))
            raise APIError(code=400, title="message", description="metadata link failed")
        raise gen.Return(appToken)

    @gen.coroutine
    def __get_app_ms_token(self):
        # 获取元数据应用token
        tokenInfo = yield self.__request_ms_app_key()
        # Todo: Redis
        try:
            import json
            token = json.loads(tokenInfo.body)
        except Exception as err:
            logging.error("AppToken.get_app_ms_token() token failed {}".format(err))
            raise APIError(code=400, title="message", description="token failed")

        raise gen.Return(token)

    @gen.coroutine
    def get_token(self, reset=False):
        # 获取当前应用token的方式
        # 当reset为True时，表示强制重置redis中token信息

        try:
            token = AppServiceGlobal.session_manager.get_os_session(key="app-onduty-token")

            if not token or reset is True:
                token = yield self.__get_app_ms_token()
                # 提前一个小时session过期
                SESSION_EXPIRED = SECS_PER_MINUTE * MINUTES_PER_HOUR * (HOURS_PER_DAY - 1)
                AppServiceGlobal.session_manager.update_os_session(
                    key="app-onduty-token", SESSION_EXPIRED=SESSION_EXPIRED, value=token)
                logging.debug("AppToken.get_app_token save {} to redis".format(token))
        except Exception as err:
            logging.error("AppToken.get_app_ms_token() get token failed {}".format(err))
            raise APIError(401, message="metadata auth failed")

        logging.debug("AppToken.get_app_token get token {} ".format(token))
        raise gen.Return(token)
