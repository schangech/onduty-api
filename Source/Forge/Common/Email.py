#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-12 10:27:49
# @Updated Date: 2016-09-06 16:50:54

"""
Eamil formater.
"""

import copy
import jinja2
import os


# -------------------------------------------------------------------------------------
class EmailException(Exception):
    """A simple exception class used for Email exceptions"""
    pass


# -------------------------------------------------------------------------------------
class Email(object):

    @staticmethod
    def format(code, **kwargs):
        """
        Format email with input kwargs.
        """
        try:
            c_list = code.split('.')		# Forge.Guest.Invite
            email = copy.deepcopy(Email._data[c_list[1]][c_list[2]])

            env = jinja2.Environment()
            email['Subject'] = env.from_string(email['Subject'].decode('utf-8')).render(**kwargs)
            email['Body'] = env.from_string(email['Body'].decode('utf-8')).render(**kwargs)

            return email
        except Exception as error:
            raise EmailException(error)

    templatePath = (os.environ['PYTHONPATH'] if os.path.isdir(os.environ['PYTHONPATH']) else "/rc/local/api/Source") + "/Templates/"
    _data = {
        'Account': {
            'Create': {
                'Subject': "新 OnDuty 帐号注册申请等待您的审核",
                'Body': open( templatePath + 'audit.html', 'rb').read()
            },

            'Active': {
                'Subject': 'Welcome to OnDuty',
                'Body': open( templatePath + 'welcome.html', 'rb').read()
            },

            'Reject': {
                'Subject': '您的 OnDuty 帐号注册已被驳回',
                'Body': open( templatePath + 'reject.html', 'rb').read()
            },

            'ResetPassword': {
                'Subject': '重置您的 OnDuty 密码',
                'Body': open( templatePath + 'reset.html', 'rb').read()
            },

            'UpdatePassword': {
                'Subject': '您的 OnDuty 帐号密码重置成功',
                'Body': open( templatePath + 'reset-success.html', 'rb').read()
            }
        },

        'Scheduler': {
            'Alert': {
                'Subject': '您有报障尚未处理: {{ticket_title}}',
                'Body': open(templatePath + 'no-response.html', 'rb').read()
            },

            "Report": {
                'Subject': '',
                'Body': """ """
            },
            "Remind": {
                'Subject': 'OnDuty 排班提醒',
                'Body': open(templatePath + 'schedule_reminder.html', 'rb').read()
            }
        },

        'Ticket': {
            'Update': {
                'Subject': 'OnDuty 报障已更新: {{ticket_title}}',
                'Body': open(templatePath + 'changed.html', 'rb').read()
            },
            'Create': {
                'Subject': '您有新的报障需要处理: {{ticket_title}}',
                'Body': open(templatePath + 'created.html', 'rb').read()
            }
        }
    }
