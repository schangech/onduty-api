#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-08-26 17:50:34
# @Updated Date: 2016-08-16 17:08:01
#
# Copyright (c) 2013 Hamza Faran(https://github.com/hfaran/Tornado-JSON.git)
#

import logging
import json
import time
from tornado.web import RequestHandler, HTTPError
from jsonschema import ValidationError
from Jsend import JSendMixin
from Forge.Common.DBMongo import conn_db
from Forge.Common.Result import Result
from Runtime.Constants import DEFAULT_LOCALE
from Forge.AppService.AppServiceGlobal import AppServiceGlobal


class APIError(HTTPError):
    """Equivalent to ``RequestHandler.HTTPError`` except for in name"""

    def __init__(self, status_code, *args, **kwargs):
        self.status_code = status_code
        self.args = []
        self.reason = None

        if kwargs.get('message'):
            self.log_message = kwargs['message'].encode('utf-8')
        if kwargs.get('flag'):
            # generate log message
            result = Result(kwargs['flag'], *args, locale=DEFAULT_LOCALE)
            self.log_message = result.result


def api_assert(condition, *args, **kwargs):
    """Assertion to fail with if not ``condition``

    Asserts that ``condition`` is ``True``, else raises an ``APIError``
    with the provided ``args`` and ``kwargs``

    :type  condition: bool
    """
    if not condition:
        raise APIError(*args, **kwargs)


class BaseHandler(RequestHandler):
    """BaseHandler for all other RequestHandlers"""

    __url_names__ = ["__self__"]
    __urls__ = []


class ViewHandler(BaseHandler):
    """Handler for views"""

    def initialize(self):
        """
        - Set Content-type for HTML
        """
        self.set_header("Content-Type", "text/html")


class APIHandler(BaseHandler, JSendMixin):
    """RequestHandler for API calls

    - Sets header as ``application/json``
    - Provides custom write_error that writes error back as JSON \
    rather than as the standard HTML template
    """

    def initialize(self):
        """
        - Set Content-type for JSON
        """
        self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin", "*")

    def write_error(self, status_code, **kwargs):
        """Override of RequestHandler.write_error

        Calls ``error()`` or ``fail()`` from JSendMixin depending on which
        exception was raised with provided reason and status code.

        :type  status_code: int
        :param status_code: HTTP status code
        """
        def get_exc_message(exception):
            return exception.log_message if \
                hasattr(exception, "log_message") else str(exception)

        self.clear()
        self.set_status(status_code)
        self.set_header("Access-Control-Allow-Origin", "*")

        # Any APIError exceptions raised will result in a JSend fail written
        # back with the log_message as data. Hence, log_message should NEVER
        # expose internals. Since log_message is proprietary to HTTPError
        # class exceptions, all exceptions without it will return their
        # __str__ representation.
        # All other exceptions result in a JSend error being written back,
        # with log_message only written if debug mode is enabled
        exception = kwargs["exc_info"][1]
        if any(isinstance(exception, c) for c in [APIError, ValidationError]):
            # ValidationError is always due to a malformed request
            if isinstance(exception, ValidationError):
                self.set_status(400)
            self.fail(get_exc_message(exception))
        else:
            self.error(
                message=self._reason,
                data=get_exc_message(exception) if self.settings.get("debug")
                else None,
                code=status_code
            )

    # def check_token(self):
    #     auth = self.request.headers.get('Authorization')

    #     if auth:
    #         try:
    #             auth = json.loads(auth)
    #         except Exception as e:
    #             raise APIError(401, flag='COMMON.GLOBAL.ERROR_REQUEST')

    #         token = auth.get('token')
    #         email = auth.get('email')
    #         # need both session id and email
    #         if not token or not email:
    #             raise APIError(401, flag='COMMON.GLOBAL.INCOMPLETE_REQUEST')

    #         # verify session
    #         try:
    #             return AppServiceGlobal.token_manager.verify(email, token)
    #         except Exception as e:
    #             raise APIError(401, flag='COMMON.TOKEN.INVALID_TOKEN')

    def check_authorization(self, session_manager, request):
        # 验证headers信息是否授权
        try:
            auth = request.headers.get('Authorization')
            auth = json.loads(auth)
        except Exception:
            raise APIError(401, flag='COMMON.GLOBAL.ERROR_REQUEST')

        if not auth:
            raise APIError(401, flag='COMMON.GLOBAL.ERROR_REQUEST')

        session_id = auth.get('session')
        access_token = auth.get('access_token')
        if session_id and not access_token:
            email = auth.get('email')
            # need both session id and email
            if not session_id or not email:
                raise APIError(401, flag='COMMON.GLOBAL.INCOMPLETE_REQUEST')

            # verify session
            try:
                print("start redis")
                session = session_manager.verify(session_id)
                print("end redis")
            except Exception:
                logging.warning("Common.RequestHandlers.check_authorization(): {0}/{1}, invalid \
                 user email and session".format(email, session_id))
                raise APIError(401, flag='COMMON.SESSION.INVALID_SESSION')

            # check account authority
            if hasattr(session, 'role') and session.role == 'auditor' and request.method != 'GET':
                if request.uri not in ['/session/', '/account/password/']:
                    logging.warning("Common.RequestHandlers.check_authorization(): {0}/{1}, user \
                     limits of authority".format(email, session_id))
                    raise APIError(401, flag='COMMON.ACCOUNT.LIMIT_AUTHORITY')

            return session
        elif not session_id and access_token:
            req_method = request.method
            req_uri = self._get_url_path(request.uri)
            mdb = conn_db(AppServiceGlobal.config.mongo_url,
                          AppServiceGlobal.config.mongo_db)
            # 第一步，校验access_token是否有效
            appId = self._check_app_access_token(mdb, access_token)
            if not appId:
                logging.error("RequestHandler.check_authorization() access invalid")
                raise APIError(401, flag='COMMON.ACCOUNT.LIMIT_AUTHORITY')
            # 第一步，校验是否有请求当前接口的权限
            isPermission = self._check_app_uri_method(
                 mdb, method=req_method, uri=req_uri, appId=appId)

            if not isPermission:
                logging.error("RequestHandler.check_authorization() uri/method invalid")
                raise APIError(401, flag='COMMON.ACCOUNT.LIMIT_AUTHORITY')
            print(isPermission, appId, req_method, req_uri, "---------------------")
        else:
            logging.error("RequestHandler.check_authorization() request error")
            raise APIError(401, flag='COMMON.GLOBAL.ERROR_REQUEST')

    def _get_url_path(self, url):
        '''
        @param url: request url
        '''
        try:
            # http://localhost:8080/v1/api/index?asdjkljflaf
            from urllib.parse import urlparse
        except Exception:
            from urlparse import urlparse
        o = urlparse(url)
        path = o.path
        return path

    def _check_app_access_token(self, mdb, access_token=None):
        '''
        @param app_id: app token id
        '''
        now = int(time.time())
        doc = {
            'access_token': access_token,
        }
        res = mdb.access_token.find_one(doc)
        #
        if not res:
            logging.error("RequestHandler.app_access_token() not found access_token")
            raise APIError(401, flag='COMMON.ACCOUNT.LIMIT_AUTHORITY')

        if res.get("expired_time") and res.get("update_at") and \
                now - int(res.get("update_at")) < int(res.get("expired_time")):
            status = res.get("app_id") if res else False
        else:
            status = False
        return status

    def _check_app_uri_method(self, mdb, method=None, uri=None, appId=None):
        '''
        @param app_id: app token id
        '''
        doc = {
            'id': appId,
            "scopes.uri": uri,
            "scopes.method": {"$in": [method]}
        }
        appInfo = mdb.app.find_one(doc)
        # 校验
        status = True if appInfo else False
        return status
