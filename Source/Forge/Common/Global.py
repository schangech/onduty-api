#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Eamil:  zuow11@gmail.com
# @Created Date: 2015-08-20 18:27:35
# @Updated Date: 2016-09-12 14:15:32

import logging.handlers
from Runtime.Constants import *


class Default(object):

    # redis
    redis_db_session = 0
    redis_db_token = 1

    # ---------------------------------------------------------------------------------------------
    class Forge(object):
        class General(object):
            scribe_enable = False
            scribe_host = 'localhost'
            scribe_port = logging.handlers.DEFAULT_TCP_LOGGING_PORT
            redis_host = '127.0.0.1'
            redis_port = 8285
            redis_password = None
            redis_db_session = 0
            redis_db_token = 1
            #redis_agent_info   = 1
            #redis_monitor_rule = 2
            #redis_agent_key    = 3
            #redis_monitor_cache= 4
            #redis_monitor_alert= 5
            #redis_user         = 6
            # redis_socket_timeout = None
            # zookeeper_host = 'localhost'
            # zookeeper_port = 8288
            mongodb_url = '127.0.0.1:8290'
            mongodb_timeout = 5
            mongodb_db_copy = 1
            mongodb_db = 'rasp'
            domain = 'onduty.in.dataengine.com'
            scheduler_host = 'localhost'
            scheduler_port = 8960
            attachment_dir = '/rc/data/static/'
            # rcos
            rcos_host = "127.0.0.1"
            rcos_port = 8701

        class AppService(object):
            stdout = "/dev/null"
            stdin = "/dev/null"
            stderr = "/dev/null"
            pidfile = "/rc/lock/AppService.lock"
            log_dir = "/rc/log/"
            log_level = LOG_INFO
            log_rotate = 3
            log_size = 100000000
            port = 8700
            max_search_level = 5

        class Scheduler(object):
            stdout = "/dev/null"
            stdin = "/dev/null"
            stderr = "/dev/null"
            pidfile = "/rc/lock/Scheduler.lock"
            log_dir = "/rc/log/"
            log_level = LOG_DEBUG
            log_rotate = 3
            log_size = 100000000
            port = 8960
            remove_outdate_data = True
            alert_outdate_day_shreshold = 30

        class OnCall(object):
            stdout = "/dev/null"
            stdin = "/dev/null"
            stderr = "/dev/null"
            pidfile = "/rc/lock/OnCall.lock"
            log_dir = "/rc/log/"
            log_level = LOG_DEBUG
            log_rotate = 3
            log_size = 100000000
            port = 8410
    # ---------------------------------------------------------------------------------------------
