#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
@Class:
@Date: 2017-09-27
@Module: Source.Forge.Common.DBMongo
@Author: sendal <schangech@gmail.com>
@Version:
'''


from pymongo import Connection


def conn_db(url, db):
    db = Connection(url)[db]
    return db
