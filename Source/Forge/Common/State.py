#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-08-21 15:47:49
# @Updated Date: 2016-09-01 14:35:06

"""
Global states for all modules.
"""


# ---- ticket ----
TICKET_STATE_OPENED = "open"
TICKET_STATE_PROCESSING = "processing"
TICKET_STATE_PENDING = 'pending'
TICKET_STATE_DONE = "resolved"
TICKET_STATE_CLOSED = "closed"

# ---- alarm ----
ALARM_STATE_OPENED = "opened"
ALARM_STATE_CLOSED = "closed"

# ---- agent ----
AGENT_STATE_INACTIVE = "inactive"
AGENT_STATE_ACTIVE = "active"
AGENT_STATE_DISCONNECTED = "disconnected"
AGENT_STATE_REMOVED = "removed"

# ---- schedule ----
SCHEDULE_STATE_ACTIVE = "active"
SCHEDULE_STATE_REMOVED = "removed"
