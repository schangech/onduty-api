#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Alex <lisuo@rongcapital.cn>
# Date: 2017-01-20  13:01

"""
Manage RCOS bucket.
"""

import re
import logging

from Forge.Common.Utils import get_cls_name
from tornado import gen
from tornado.httpclient import HTTPRequest, HTTPClient
from tornado.httpclient import HTTPError

class BucketException(Exception):
    pass

class Bucket(object):
    """
        Bucket create 
    """

    def __init__(self, rcos_host, rcos_port, token):
        """ Init Bucket class.

        :type   rcos_host: String
        :param  rcos_host: RCOS host.
        :type   rcos_port: Int
        :param  rcos_port: RCOS host port.
        :type   token    : Dict {"access_key": "UCCOICB1EONUJ2BOZ7FW",
                                "secret_key": "IvMh3W839EDNG0xA2dAQJAriUKlqaMzPEpKVOibN"}
        :param  token    : RCOS Token.

        :return
        """
        self.rcos_host = rcos_host
        self.rcos_port = rcos_port
        self.token = token

    def create(self, bucket_name, headers=None):
        """ Create bucket.

        :type   bucket_name: String
        :param  bucket_name: Bucket name. (Name must lowercase or -)
        :type   headers    : Dict
        :param  headers    : Http request headers.


        :raise BucketException
        :return
        """
        name = re.compile("[a-z\-]+")
        if name.match(bucket_name):
            try:
                self._request_bucket(bucket_name, 'PUT', headers=headers)
            except BucketException as e:
                logging.error(get_cls_name(self, "create bucket error %s" % e))
                raise BucketException(e)
        else:
            raise BucketException("Bucket name error.")

    def delete(self, bucket_name):
        """ Delete bucket.

        :type   bucket_name: String
        :param  bucket_name: Bucket name.


        :raise BucketException
        :return
        """
        try:
            self._request_bucket(bucket_name, 'DELETE')
        except BucketException as e:
                logging.error(get_cls_name(self, "delete bucket error %s" % e))
                raise BucketException(e)

    def _request_bucket(self, bucket_name, method, body=None, headers=None):

        http_client = HTTPClient()
        url = "http://%s:%d/%s" % (self.rcos_host, self.rcos_port, bucket_name)
        logging.debug(get_cls_name(self, "request bucket url %s" % url))
        try:
            # TODO 请求加Token, 目前服务端RCOS 不支持.
            request = HTTPRequest(url=url, method=method, headers=headers, body=body, 
                                  allow_nonstandard_methods=True)
            resp = http_client.fetch(request)
        except (Exception, HTTPError) as e:
            raise BucketException(e)

        if resp.code != 200:
            raise BucketException(resp.code, resp.body)
