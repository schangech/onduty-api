#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: liuzhenwu

"""
Wechat formater.
"""

import copy
import jinja2
import sys


reload(sys)
sys.setdefaultencoding('utf-8')


# -------------------------------------------------------------------------------------
class WechatException(Exception):
    """A simple exception class used for Wechat exceptions"""
    pass


# -------------------------------------------------------------------------------------
class Wechat(object):

    @staticmethod
    def format(type, **kwargs):
        """
        Format wechat with input kwargs.
        """
        try:
            wechat = copy.deepcopy(Wechat._data[type])

            env = jinja2.Environment()
            wechat = env.from_string(wechat.decode('utf-8')).render(**kwargs)

            return wechat
        except Exception as error:
            raise WechatException(error)

    _data = {
        'account':
            "用户{{mail_pre}}申请注册成为部门的成员\n\n"
            "用户名：{{cn}}\n\n"
            "所属部门：{{department}}\n\n"
            "联系电话：{{telephonenumber}}\n\n"
            "邮箱地址：{{mail}}\n\n"
            "部门负责人：{{cc_list}}\n\n"
            "全名：{{fullname}}\n\n"
            "登录指令：{{loginshell}}\n\n"
            "链接：{{url}}"
        ,

        'bug':
            "报障{{id}}尚未处理：\n\n"
            "报障内容：{{description}}\n\n"
            "创建时间：{{created_time}}\n\n"
            "报障状态：{{status}}\n\n"
            "严重等级：{{severity}}\n\n"
            "主机名：{{hostname}}\n\n"
            "IP：{{ip}}\n\n"
            "链接：{{url}}"
        ,

        'resolve':
            "报障{{id}}已解决：\n\n"
            "报障内容：{{description}}\n\n"
            "创建时间：{{created_time}}\n\n"
            "报障状态：{{status}}\n\n"
            "严重等级：{{severity}}\n\n"
            "主机名：{{hostname}}\n\n"
            "IP：{{ip}}\n\n"
            "链接：{{url}}"
    }
