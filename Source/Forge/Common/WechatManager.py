#!/usr/bin/env python
# -*- coding: utf-8 -*-

# __author__ = 'liuzhenwu'
"""
微信企业号工具类，用于添加用户，编辑部门，创建群聊等
新的企业微信号已经不能创建群聊了，发消息只能通过message接口来发送
"""

import time
import json
import logging
import requests
from pymongo import MongoClient
from requests.packages.urllib3.exceptions import InsecureRequestWarning


class WechatManagerException(Exception):
    pass


class WechatManager(object):
    def __init__(self, mongo_url, mongo_db, wechat_surl, wechat_scorpsecret, wechat_scorpid, wechat_qychatsecret):
        logging.getLogger("requests").setLevel(logging.WARNING)
        self.db = MongoClient(mongo_url)[mongo_db]

        self.wechat_surl = wechat_surl
        self.wechat_scorpsecret = wechat_scorpsecret
        self.wechat_scorpid = wechat_scorpid
        self.wechat_qychatsecret = wechat_qychatsecret

        self.wechat_url = "{0}/cgi-bin/gettoken?corpid={1}&corpsecret={2}".format(self.wechat_surl, self.wechat_scorpid,
                                                                                  self.wechat_scorpsecret)
        self.wechat_dialog_url = "{0}/cgi-bin/gettoken?corpid={1}&corpsecret={2}".format(self.wechat_surl,
                                                                                         self.wechat_scorpid,
                                                                                         self.wechat_qychatsecret)

    @staticmethod
    def _request(url, method, data=None):
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        if method == 'GET':
            response = requests.get(url, verify=False)
        else:
            response = requests.post(url, data=json.dumps(data, ensure_ascii=False).encode('utf-8'), verify=False)

        response = response.json()
        return response

    def _create_token(self, url, source):
        """创建token"""
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        response = requests.get(url, verify=False)
        token_info = response.json()
        token_info['time'] = int(time.time())
        token_info['source'] = source
        self.db.token.insert(token_info)
        return token_info

    def get_token(self):
        """获取token"""
        token_info = list(self.db.token.find({"source": "message"}).sort("time", -1).limit(1))
        if not token_info:
            token_info = self._create_token(self.wechat_url, "message")
        else:
            token_info = token_info[0]
            if int(time.time()) - token_info['time'] >= 7200:
                token_info = self._create_token(self.wechat_url, "message")
        return token_info['access_token']

    # def get_dialog_token(self):
    #     token_info = self.db.token.find({"source": "dialog"}).sort("time", -1).limit(1)
    #     if not token_info:
    #         token_info = self._create_token(self.wechat_dialog_url, "dialog")
    #     else:
    #         token_info = token_info[0]
    #         if int(time.time()) - token_info['time'] >= 7200:
    #             token_info = self._create_token(self.wechat_dialog_url, "dialog")
    #     return token_info['access_token']

    def get_department_list(self):
        """获取部门列表"""
        get_department_url = '{0}/cgi-bin/department/list?access_token={1}'.format(self.wechat_surl, self.get_token())
        res = self._request(get_department_url, 'GET')
        return res.get('department')

    def get_department_by_name(self, department_name):
        for department in self.get_department_list():
            if department.get('name') == department_name:
                return department

    def get_department_id_by_name(self, department_name):
        """根据部门名称获取部门id号"""
        for department in self.get_department_list():
            if department.get('name') == department_name:
                return department.get('id')

    def get_department_user(self, department_name):
        """获取部门成员"""
        dp_id = self.get_department_id_by_name(department_name)
        if dp_id:
            get_dp_user_url = '{0}/cgi-bin/user/simplelist?access_token={1}&department_id={2}' \
                              '&fetch_child=0&status=0'.format(self.wechat_surl, self.get_token(), dp_id)
            res = self._request(get_dp_user_url, 'GET')
            return res.get('userlist')

    def get_user(self, userid):
        """获取成员详情"""
        get_user_url = '{0}/cgi-bin/user/get?access_token={1}&userid={2}'.format(self.wechat_surl, self.get_token(),
                                                                                 userid)
        res = self._request(get_user_url, 'GET')
        return res

    def update_user(self, userid, weixinid=None, name=None, department=None):
        """update user info"""
        update_user_url = '{0}/cgi-bin/user/update?access_token={1}&userid={2}'.format(self.wechat_surl,
                                                                                       self.get_token(), userid)

        post_data = {'userid': userid}
        if weixinid is not None:
            post_data['weixinid'] = weixinid
        if name is not None:
            post_data['name'] = name
        if department is not None:
            post_data['department'] = department

        res = self._request(update_user_url, 'POST', post_data)
        logging.debug('WechatManager update_user(): {0}, result: {1}'.format(userid, res))
        result = bool(res.get('errcode') == 0)
        return result

    def delete_user(self, userid):
        """删除成员"""
        del_user_url = '{0}/cgi-bin/user/delete?access_token={1}&userid={2}'.format(self.wechat_surl,
                                                                                    self.get_token(), userid)

        res = self._request(del_user_url, 'GET')
        logging.debug('WechatManager delete_user(): {0} result: {1}'.format(userid, res))

        result = bool(res.get('errcode') == 0)
        return result

    def create_department(self, department_name):
        """创建部门"""
        create_department_url = '{0}/cgi-bin/department/create?access_token={1}'.format(
            self.wechat_surl, self.get_token())
        post_data = {'name': department_name, 'parentid': 1}
        res = self._request(create_department_url, 'POST', post_data)
        logging.debug('WechatManager create_department(): {0}, result: {1}'.format(department_name, res))
        result = bool(res.get('errcode') == 0)
        return result

    def check_user_exits(self, userid):
        """判断用户是否存在"""
        get_user_detail_url = '{0}/cgi-bin/user/get?access_token={1}&userid={2}'.format(
            self.wechat_surl, self.get_token(), userid)
        res = self._request(get_user_detail_url, 'GET')
        result = bool(res.get('errcode') == 0)
        return result

    def check_user_follow(self, userid):
        """
        判断用户是否已经关注
        :param userid: 
        :return: 
        """
        get_user_detail_url = '{0}/cgi-bin/user/get?access_token={1}&userid={2}'.format(
            self.wechat_surl, self.get_token(), userid)
        res = self._request(get_user_detail_url, 'GET')
        if res.get('errcode') == 0:
            if res.get('status') == 1:  # 关注状态: 1=已关注，2=已禁用，4=未关注
                return True
        return False

    def create_user(self, userid, name, department, weixinid, **kwargs):
        """
        创建用户
        参数            必须 说明
        access_token	是	调用接口凭证
        userid	        是	成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
        name	        是	成员名称。长度为1~64个字节
        department	    是	成员所属部门id列表,不超过20个(这里的参数用的是部门名称，比如devops)
        position	    否	职位信息。长度为0~64个字节
        mobile	        否	手机号码。企业内必须唯一，mobile/weixinid/email三者不能同时为空
        gender	        否	性别。1表示男性，2表示女性
        email	        否	邮箱。长度为0~64个字节。企业内必须唯一
        weixinid	    否	微信号。企业内必须唯一。（注意：是微信号，不是微信的名字）
        avatar_mediaid	否	成员头像的mediaid，通过多媒体接口上传图片获得的mediaid
        extattr	        否	扩展属性。扩展属性需要在WEB管理端创建后才生效，否则忽略未知属性的赋值
        """
        logging.info('WechatManager create_user(): userid: {0}, name: {1}, department: {2}, weixinid: {3}, {4}'.format(
                userid, name, department, weixinid, kwargs))
        dp_name_list = department if isinstance(department, list) else [department]
        dp_id_list = [self.get_department_id_by_name(dp) for dp in dp_name_list if self.get_department_id_by_name(dp)]
        post_data = {
            'userid': userid,
            'name': name,
            'department': dp_id_list,
            'weixinid': weixinid
        }
        post_data.update(**kwargs)
        create_user_url = '{0}/cgi-bin/user/create?access_token={1}'.format(self.wechat_surl, self.get_token())
        res = self._request(create_user_url, 'POST', post_data)

        result = bool(res.get('errcode') == 0)
        # if result:
        #     # 创建成员后，查看是否已经创建群聊，如果已经创建，则将用户添加到群聊中，
        #     # 如果没有创建，则判断部门人数是否大于三人，如果大于三人，则创建群聊（创建群聊要求聊天人数大于等于3）
        #     for dp in dp_name_list:
        #         if self.get_chat_info(dp):
        #             self.add_user_to_chat(dp, userid)
        #             logging.info('Add {0} to {1} chat'.format(userid, dp))
        #         else:
        #             userlist = self.get_department_user(dp)
        #             if userlist and len(userlist) >= 3:
        #                 logging.info('department: {0} user length: {1}, create chat'.format(dp, len(userlist)))
        #                 self.create_chat(dp, [user.get('userid') for user in userlist])

        return result

    def create_chat(self, department_name, userid_list):
        """创建群聊"""
        create_chat_url = '{0}/cgi-bin/chat/create?access_token={1}'.format(self.wechat_surl, self.get_dialog_token())
        post_data = {
           "chatid": department_name,
           "name": department_name,
           "owner": 'robot',    # 群聊管理员：robot
           "userlist": userid_list
        }
        res = self._request(create_chat_url, 'POST', post_data)
        result = bool(res.get('errcode') == 0)
        return result

    def get_chat_info(self, chatid):
        """获取群聊信息"""
        get_session_url = '{0}/cgi-bin/chat/get?access_token={1}&chatid={2}'.format(self.wechat_surl,
                                                                                    self.get_dialog_token(), chatid)
        res = self._request(get_session_url, 'GET')
        return res.get('chat_info')

    def add_user_to_chat(self, chatid, userid):
        """将用户添加到群聊中"""
        update_chat_url = '{0}/cgi-bin/chat/update?access_token={1}'.format(self.wechat_surl, self.get_dialog_token())
        post_data = {
            'chatid': chatid,
            'op_user': 'robot',  # 操作人: robot
            'add_user_list': [userid]
        }
        res = self._request(update_chat_url, 'POST', post_data)
        result = bool(res.get('errcode') == 0)
        return result

    # ======================================================== #
    #                   新的企业微信号                            #
    # ======================================================== #

    def send_message(self, touser=None, department_name=None, toparty=None, content=''):
        """
        发送消息，可以发送给指定的user,也可以发送给指定的部门（这里的部门是微信里的部门）
        
        touser	否	成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
        toparty	否	部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
        totag	否	标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
        msgtype	是	消息类型，此时固定为：text （支持消息型应用跟主页型应用）
        agentid	是	企业应用的id，整型。可在应用的设置页面查看
        content	是	消息内容，最长不超过2048个字节，注意：主页型应用推送的文本消息在微信端最多只显示20个字（包含中英文）
        safe	否	表示是否是保密消息，0表示否，1表示是，默认0
        """
        send_message_url = '{0}/cgi-bin/message/send?access_token={1}'.format(self.wechat_surl, self.get_token())
        # toparty应该为部门的id号，而不是名称，如果传入的时部门名称，现根据名称获取对应的ID
        if department_name is not None:
            content = content.format(receiver=department_name)
            toparty = self.get_department_id_by_name(department_name)
            toparty = str(toparty) if toparty else ''

        if isinstance(touser, list):
            content = content.format(receiver=' @'.join(touser))
            touser = '|'.join(touser)
        elif isinstance(touser, basestring):
            content = content.format(receiver=touser)

        logging.info(
            'Send message: touser: {0}, department: {1}, toparty: {2}'.format(touser, department_name, toparty))

        data = {
           "touser": touser,
           "toparty": toparty,
           "msgtype": "text",
           "agentid": 1,
           "text": {
               "content": content
           },
           "safe": 0
        }
        logging.debug('Request data: %s' % data)
        res = self._request(send_message_url, 'POST', data)
        logging.debug('Send result: {0}'.format(res))
        result = bool(res.get('errcode') == 0)
        return result
