#!/usr/bin/env python
# -*- coding: utf-8 -*-

# __author__ = 'liuzhenwu'

"""
WeChat handler interfaces.
"""

from tornado import gen

from Forge.Common.Gen import coroutine
from Forge.Common.Schema import validate
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.OnCall.WeChat.WeChatSender import WeChatSender
from Forge.OnCall.OnCallGlobal import OnCallGlobal


class WeChatHandler(APIHandler):
    """
        endpoint: /wechat/
    """
    we_sender = WeChatSender()

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "sender": {"type": "string"},
                "receiver": {"type": "string"},
                "type": {"enum": ["single", "group"]},
                "content": {"type": "string"}
            },
            "required": ["receiver", "type", "content"]
        }
    )
    @coroutine
    def post(self):
        sender = self.body.get('sender') or OnCallGlobal.config.default_wechat_sender
        receiver = self.body.get('receiver')
        type = self.body.get('type')
        content = self.body.get('content')
        try:
            yield self.we_sender.send_message(sender=sender, receiver=receiver, type=type, content=content)
        except Exception as error:
            raise APIError(status_code=400, message=str(error))
        raise gen.Return(dict(message='The wechat has been sent.'))
