#!/usr/bin/env python
# -*- coding: utf-8 -*-

# __author__ = 'liuzhenwu'

"""
Email handler interfaces.
"""

import smtplib
import logging

from tornado import gen
from email.utils import COMMASPACE
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from Forge.Common.Gen import coroutine
from Forge.Common.Schema import validate
from Forge.Common.RequestHandlers import APIHandler, APIError
from Forge.OnCall.OnCallGlobal import OnCallGlobal


class EmailHandler(APIHandler):
    """
        endpoint: /email/
    """

    @validate(
        input_schema={
            "type": "object",
            "properties": {
                "sender": {"format": "email"},
                "receiver": {
                    "type": "array",
                    "minItems": 1,
                    "items": {"format": "email"}
                },
                "subject": {"type": "string"},
                "content": {"type": "string"},
                "format": {"enum": ["text", "html"]}
            },
            "required": ["receiver", "subject", "content", "format"]
        }
    )
    @coroutine
    def post(self):
        sender = self.body.get('sender')
        receiver = self.body.get('receiver')
        subject = self.body.get('subject')
        content = self.body.get('content')
        c_format = self.body.get('format')

        # 如果没有指定发送人，默认是RongCapital<support@rongcapital>账号
        if sender is None:
            sender = '{0}<{1}>'.format(OnCallGlobal.config.smtp_from_name, OnCallGlobal.config.smtp_username)

        try:
            msg = MIMEMultipart()
            msg['From'] = sender    # 在这里指定发送人，邮件就会显示是这人发的
            msg['To'] = COMMASPACE.join(receiver)
            msg['Subject'] = subject
            subtype = c_format if c_format == 'html' else 'plain'
            msg.attach(MIMEText(content.encode('utf-8'), subtype, 'UTF-8'))
            logging.info(u"Send email: \n\tSender:{0}\n\tReceiver:{1}\n\tSubject:{2}".format(
                sender, COMMASPACE.join(receiver), subject))

            smtp = smtplib.SMTP_SSL(OnCallGlobal.config.smtp_host, OnCallGlobal.config.smtp_port, timeout=15)
            smtp.login(OnCallGlobal.config.smtp_username, OnCallGlobal.config.smtp_pwd)
            # 这里的from_addr应该和登录时的邮箱地址一样
            smtp.sendmail(from_addr=OnCallGlobal.config.smtp_username, to_addrs=msg['To'], msg=msg.as_string())
            smtp.quit()
            logging.info('The email has been sent.')
        except Exception as error:
            logging.exception(error)
            raise APIError(status_code=400, message=str(error))

        raise gen.Return({'message': 'The email has been sent.'})
