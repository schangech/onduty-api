#!/usr/bin/env python
# -*- coding: utf-8 -*-

# __author__ = 'liuzhenwu'

"""
OnCall application instance class.
"""

import os
import sys
import signal
import errno
import logging
import logging.handlers
from optparse import Option
from motor import MotorClient
from tornado import web, ioloop, autoreload
from tornado.httpclient import AsyncHTTPClient


from Forge.Common.Routes import get_routes
from Runtime.Log import LogFormatter
from Forge.Common.Session import RedisSessionManager
from Forge.Common.Utils import ignored
from Forge.Common.Global import Default
from Runtime.Command import LocalCommand
from Runtime.Config import ConfigException
from Runtime.ScribeHandler import ScribeHandler
from Forge.OnCall.OnCallGlobal import OnCallGlobal, OnCallConfig


# -----------------------------------------------------
#  Exception
# -----------------------------------------------------
class OnCallException(Exception):
    """A simple exception class used for OnCall exceptions"""
    pass


class OnCall(object):

    def __init__(self, foreground=True):

        self.__foreground = foreground

        # init logger
        try:
            level = logging.getLevelName(OnCallGlobal.config.log_level)
            logging.getLogger().setLevel(level)
            logger = logging.getLogger()
            if self.__foreground:
                # Set up color if we are in a tty and curses is installed
                fh = logging.StreamHandler()
                formatter = LogFormatter(console=True)
                fh.setFormatter(formatter)
                logger.addHandler(fh)
        except Exception as error:
            raise OnCallException(error)

    def start(self):
        """
            Start tornado application.
        """
        try:
            import Handlers
            handlers = get_routes(Handlers)

            logging.info("OnCall routes: {0}".format(handlers))

            settings = {
                'debug': True,
                # 'xsrf_cookies': True,
                'gzip': True,
                'static_path': OnCallGlobal.config.attachment_dir
            }
            self.application = web.Application(handlers, **settings)
            self.application.listen(int(OnCallGlobal.config.service_port))

            logging.info("OnCall is starting.")
            ioloop.IOLoop.instance().start()
        except Exception as error:
            raise OnCallException(error)


if __name__ == '__main__':
    usage = "[-c] [-f] [-d] [-p]"
    version = "0.1"
    cmd = LocalCommand(usage, version, sys.argv)
    cmd.add_option(Option(
        "-c", "--conf", action="store", type="string", dest="conf",
        help="""
            The location of the OnCall configuration file. If not
            specified, it will try to search under $INSTANT_HOME/conf/instant.cfg
        """))
    cmd.add_option(Option(
        "-f", "--fg", action="store_true", dest="foreground",
        help="""
            OnCall logs to foreground. Default is background
        """))
    cmd.add_option(Option(
        "-d", "--debug", action="store_true", dest="debug",
        help="""
            Run OnCall in debug mode. Default is Not
        """))
    cmd.add_option(Option(
        "-p", "--port", action="store", dest="port",
        help="""OnCall port"""))
    cmd.parse_options()
    if not cmd.opt.foreground:
        cmd.opt.debug = False

    # Check if a daemon is already running
    if cmd.opt.foreground:
        pidfile = Default.Forge.AppService.pidfile
        if os.path.exists(pidfile):
            pf = file(pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()

            try:
                os.kill(pid, signal.SIG_DFL)
            except OSError, (err, msg):
                if err == errno.ESRCH:
                    # Pidfile is stale. Remove it.
                    os.remove(pidfile)
                else:
                    msg = "Unexpected error when checking pid file '{0}'.\n{1}\n".format(pidfile, msg)
                    sys.stderr.write(msg)
                    sys.exit(1)
            else:
                msg = "OnCall is already running (pid {0})\n".format(pid)
                sys.stderr.write(msg)
                sys.exit(1)
        else:
            if not os.path.exists(os.path.dirname(Default.Forge.AppService.pidfile)):
                os.mkdir(os.path.dirname(Default.Forge.AppService.pidfile))
            file(Default.Forge.AppService.pidfile, 'w+').write("%i\n" % os.getpid())

    # load the config and init
    try:
        if cmd.opt.conf is None:
            if "INSTANT_HOME" in os.environ:
                cmd.opt.conf = os.environ["INSTANT_HOME"] + "/conf/instant.cfg"
            else:
                cmd.opt.conf = "instant.cfg"
        if not os.path.exists(cmd.opt.conf):
            cmd.error("Missing configuration file %s" % cmd.opt.conf)

        OnCallGlobal.debug = cmd.opt.debug if cmd.opt.debug is not None else False
        OnCallGlobal.config = OnCallConfig(cmd.opt.conf)
        if OnCallGlobal.debug:
            OnCallGlobal.config.log_level = 'DEBUG'
        if cmd.opt.port:
            OnCallGlobal.config.service_port = int(cmd.opt.port)

        OnCallGlobal.session_manager = RedisSessionManager(
            hostname=OnCallGlobal.config.redis_host,
            port=OnCallGlobal.config.redis_port,
            db=OnCallGlobal.config.redis_db,
            # password    = OnCallGlobal.config.redis_password,
            # socket_timeout  = OnCallGlobal.config.redis_socket_timeout
        )

        OnCallGlobal.db = MotorClient(OnCallGlobal.config.mongo_url)[OnCallGlobal.config.mongo_db]

        # log : connection to scribe server
        for i in range(3):
            with ignored(Exception):
                level = logging.getLevelName(OnCallGlobal.config.log_level)
                formatter = logging.Formatter("%(asctime)s-%(name)s-%(levelname)s-%(message)s")
                if OnCallGlobal.config.scribe_enable:
                    handler = ScribeHandler(
                        host=OnCallGlobal.config.scribe_host,
                        port=OnCallGlobal.config.scribe_port,
                        category='%(hostname)s-OnDuty.OnCall'
                    )
                else:
                    log_file = os.environ["INSTANT_HOME"] + '/log/OnDuty_oncall.log'
                    handler = logging.handlers.RotatingFileHandler(log_file, mode='a', maxBytes=(1024*1024*10))
                handler.setFormatter(formatter)
                logger = logging.getLogger()
                logger.removeHandler(handler)
                logger.addHandler(handler)
                logger.setLevel(level)
                break
        else:
            logging.error('OnCall: cannot connect to log service')
            sys.exit(255)

        OnCallGlobal.http_client = AsyncHTTPClient(max_clients=1000)

    except KeyError, msg:
        print >> sys.stderr, "Missing environment setting:" + str(msg)
        sys.exit(1)
    except ConfigException, msg:
        print >> sys.stderr, "Cannot start OnCall: {0}".format(msg)
        sys.exit(1)

    # start
    rs = OnCall(cmd.opt.foreground)
    rs.start()
