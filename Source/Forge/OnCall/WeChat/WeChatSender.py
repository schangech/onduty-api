#!/usr/bin/env python
# -*- coding: utf-8 -*-

# __author__ = 'liuzhenwu'

import sys
import time

import logging
import json
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

from Forge.Common.Gen import coroutine, gen
from Forge.OnCall.OnCallGlobal import OnCallGlobal
from Runtime.Constants import WeChat_sURL, WeChat_sCorpID, WeChat_sCorpSecret, WeChat_qyChatSecret

reload(sys)
sys.setdefaultencoding('utf-8')


class WeChatSender(object):

    def __init__(self):
        self.url = "{0}/cgi-bin/gettoken?corpid={1}&corpsecret={2}".format(
            WeChat_sURL, WeChat_sCorpID, WeChat_sCorpSecret)
        self.dialog_url = "{0}/cgi-bin/gettoken?corpid={1}&corpsecret={2}".format(
            WeChat_sURL, WeChat_sCorpID, WeChat_qyChatSecret)

    @coroutine
    def _create_token(self, url, source):
        """创建token"""
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        response = requests.get(url, verify=False)
        token_info = response.json()
        token_info['time'] = int(time.time())
        token_info['source'] = source
        yield OnCallGlobal.db.token.insert(token_info)
        raise gen.Return(token_info)

    @coroutine
    def get_token(self):
        """获取token"""
        token_info = yield OnCallGlobal.db.token.find({"source": "message"}).sort("time", -1).limit(1).to_list(None)
        token_info = token_info[0]
        if not token_info:
            token_info = self._create_token(self.url, "message")
        else:
            if int(time.time()) - token_info['time'] >= 7200:
                token_info = self._create_token(self.url, "message")
        raise gen.Return(token_info)

    @coroutine
    def get_dialog_token(self):
        token_info = yield OnCallGlobal.db.token.find({"source": "dialog"}).sort("time", -1).limit(1).to_list(None)
        if not token_info:
            token_info = yield self._create_token(self.dialog_url, "dialog")
        else:
            token_info = token_info[0]
            if int(time.time()) - token_info['time'] >= 7200:
                token_info = yield self._create_token(self.dialog_url, "dialog")
        raise gen.Return(token_info)

    @coroutine
    def send_message(self, sender, receiver, type, content):
        """
            发送微信消息
            type:
                single: 单人发送
                group： 群发
        """
        body = {
            "receiver": {"type": type, "id": receiver},
            "sender": sender,
            "msgtype": "text",
            "text": {"content": content}
        }
        token = yield self.get_dialog_token()
        url = "{0}/cgi-bin/chat/send?access_token={1}".format(WeChat_sURL, token['access_token'])
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        response = requests.post(url, data=json.dumps(body, ensure_ascii=False).encode('utf-8'), verify=False)
        if response.status_code != 200:
            logging.error('Send wechat from {0} to {1} failed'.format(sender, receiver))
            raise Exception('Send wechat from {0} to {1} failed'.format(sender, receiver))
        msg = response.json()
        response.close()
        raise gen.Return(msg)
