#!/usr/bin/env python
# -*- coding: utf-8 -*-

# __author__ = 'liuzhenwu'

import os
import ast
import time
import json
import redis
import urllib
import smtplib
import requests
import logging
import logging.handlers

from pymongo import MongoClient
from email.utils import COMMASPACE
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from Forge.OnCall.OnCallGlobal import OnCallConfig
from Forge.Common.WechatManager import WechatManager
from Runtime.Constants import (
    DEFAULT_RETRY_TIMES,
    TIME_FORMAT,
    ONCALL_EMAIL,
    ONCALL_WECHAT,
    ONCALL_SLACK,
    ONCALL_FAILURE_FILE,
    DEFAULT_SUPPORT_USER,
    ONCALL_SMS,
    SMS_URL,
    SMS_METHOD,
    SMS_FORMAT,
    SMS_ACCESS_TOKEN,
    SMS_CHANNEL_CODE,
    SMS_RC_CODE)

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


class OnCallRedis(object):
    """
    利用redis的消息队列实现oncall模块的调用
    """

    def __init__(self):
        self.config = self.read_config()
        self.get_log()
        self.db = MongoClient(self.config.mongo_url)[self.config.mongo_db]
        self.redis = self._redis_connect()
        self.we_mg = WechatManager(
            self.config.mongo_url,
            self.config.mongo_db,
            self.config.wechat_surl,
            self.config.wechat_scorpsecret,
            self.config.wechat_scorpid,
            self.config.wechat_qychatsecret)

    @staticmethod
    def read_config():
        if "INSTANT_HOME" in os.environ:
            conf = os.environ["INSTANT_HOME"] + "/conf/instant.cfg"
        else:
            current_path = os.path.dirname(os.path.abspath(__file__))
            conf = "{}/instant.cfg".format(current_path)

        if not os.path.exists(conf):
            raise Exception("Missing configuration file %s" % conf)
        return OnCallConfig(conf)

    def get_log(self):
        """
        获取日志
        """
        level = logging.getLevelName(self.config.log_level)
        formatter = logging.Formatter(
            fmt="%(asctime)s-%(name)s-%(levelname)s-%(message)s",
            datefmt="%Y-%m-%d %H:%M:%S"
        )
        if "INSTANT_HOME" in os.environ:
            log_file = os.environ["INSTANT_HOME"] + '/log/OnDuty_oncall.log'
        else:
            log_file = '/rc/log/OnDuty_oncall.log'
        handler = logging.handlers.RotatingFileHandler(
            log_file, mode='a', maxBytes=(1024 * 1024 * 10))
        handler.setFormatter(formatter)
        logger = logging.getLogger()
        logger.removeHandler(handler)
        logger.addHandler(handler)
        logger.setLevel(level)

    def _redis_connect(self):
        logging.info("// start to connect redis")
        return redis.Redis(
            self.config.redis_host,
            self.config.redis_port,
            db=self.config.redis_db
        )

    def start(self):
        """
        消息队列循环
        """

        while True:
            if not self.redis.ping():
                self.redis = self._redis_connect()

            keys = [ONCALL_EMAIL, ONCALL_WECHAT, ONCALL_SLACK, ONCALL_SMS]

            try:
                # 格式: ('email/wechat', 'data')
                message_type, message_content = self.redis.brpop(keys, timeout=0)
            except redis.ConnectionError:
                continue

            print message_type, message_content
            self.send_oncall(message_type, message_content)

    def send_oncall(self, message_type, message_content):
        """
        消息处理
        """
        # 这里的content是str，先转换成dict
        message_dict = ast.literal_eval(message_content)
        error = None
        for i in range(DEFAULT_RETRY_TIMES):
            try:
                if message_type == ONCALL_EMAIL:
                    self.email_sender(message_dict)
                    logging.info(
                        'OnCallRedis.OnCallRedis.send_oncall: Send email OK on redis')
                elif message_type == ONCALL_SMS:
                    self.sms_sender(message_dict)
                    logging.info(
                        'OnCallRedis.OnCallRedis.send_oncall: Send sms OK on redis')
                elif message_type == ONCALL_WECHAT:
                    self.wechat_sender(message_dict)
                    logging.info(
                        'OnCallRedis.OnCallRedis.send_oncall: Send wechat OK on redis')
                elif message_type == ONCALL_SLACK:
                    self.slack_sender(message_dict)
                break
            except Exception as error:
                logging.error(
                    'OnCallRedis.OnCallRedis.send_oncall: '
                    'retry after {0} times failure!：{1}'.format(
                        i + 1, error))
        else:
            self.on_send_fail(message_type, message_dict, error)

    def on_send_fail(self, fail_type, message_dict, reason):
        """
        重试3次后仍然失败，用另外一种方式通知devops组，并把发送失败的消息记录到文件中
        """
        def send_notify_email():
            """微信发送失败，用邮件通知devops"""
            email_dict = {
                'receiver': [
                    DEFAULT_SUPPORT_USER,
                    'liuzhenwu@rongcapital.cn'],
                'subject': 'OnCall WeChat Send Fail',
                'content': 'Send wechat to {0} failed: {1}'.format(
                    message_dict.get('receiver'),
                    reason),
                'format': 'text'}
            self.email_sender(email_dict)

        def send_notify_wechat():
            """邮件发送失败，用微信通知devops"""
            wechat_dict = {
                'receiver': '12',   # devops
                'type': 'group',
                'content': 'Send email to {0} failed: {1}'.format(message_dict.get('receiver'), reason)
            }
            self.wechat_sender(wechat_dict)

        logging.info('OnCallRedis.OnCallRedis.on_send_fail: begin action')
        try:
            if fail_type == ONCALL_WECHAT:
                send_notify_email()
            elif fail_type == ONCALL_EMAIL:
                send_notify_wechat()
        except Exception as error:
            logging.exception(
                'OnCallRedis.OnCallRedis.on_send_fail: Notify devops fail:{0}'.format(error))
        try:
            if not os.path.exists(os.path.dirname(ONCALL_FAILURE_FILE)):
                # 如果'/rc/data'不存在，先创建
                os.makedirs(os.path.dirname(ONCALL_FAILURE_FILE))
            with open(ONCALL_FAILURE_FILE, 'ab+') as f:
                content = ''
                content += time.strftime(TIME_FORMAT, time.localtime())
                for k, v in message_dict.items():       # 确保中文不出现乱码
                    content += '\n\t{0}: {1}'.format(k, v)
                content += '\n'
                f.write(content)
                logging.info('OnCallRedis.OnCallRedis.on_send_fail: log file "{0}" write ok'.format(
                    ONCALL_FAILURE_FILE))
        except Exception as error:
            logging.exception(
                'OnCallRedis.OnCallRedis.on_send_fail: Record failure message fail:{0}'.format(error))

    def email_sender(self, email_dict):
        """邮件发送"""
        sender = email_dict.get('sender')
        receiver = email_dict.get('receiver')
        cc = email_dict.get('cc')
        subject = email_dict.get('subject').decode('utf-8')
        content = email_dict.get('content').decode('utf-8')
        c_format = email_dict.get('format')

        # 如果没有指定发送人，默认是RongCapital<support@rongcapital>账号
        if sender is None:
            sender = '{0}<{1}>'.format(
                self.config.smtp_from_name,
                self.config.smtp_username)
        # 抄送人也需要添加到收件人列表，否则收不到邮件
        if cc is not None:
            receiver.extend(cc)
        # 收件人去重
        receiver = list(set(receiver))

        logging.info(
            u'OnCallRedis.OnCallRedis.email_sender: Sender:{0}, Receiver:{1}, Subject:{2}, Format:{3}' .format(
                sender,
                COMMASPACE.join(receiver),
                subject,
                c_format))

        msg = MIMEMultipart()
        msg['From'] = sender  # 在这里指定发送人，邮件就会显示是这人发的
        msg['To'] = COMMASPACE.join(receiver).strip()
        if cc is not None:
            msg['Cc'] = COMMASPACE.join(cc).strip()
        msg['Subject'] = subject
        subtype = c_format if c_format == 'html' else 'plain'
        msg.attach(MIMEText(content.encode('utf-8'), subtype, 'UTF-8'))

        smtp = smtplib.SMTP_SSL(
            self.config.smtp_host,
            self.config.smtp_port,
            timeout=15)
        smtp.login(self.config.smtp_username, self.config.smtp_pwd)
        # 这里的from_addr应该和登录时的邮箱地址一样
        smtp.sendmail(
            from_addr=self.config.smtp_username,
            to_addrs=receiver,
            msg=msg.as_string())
        smtp.quit()

    def wechat_sender(self, wechat_dict):
        """
            发送微信消息
            type:
                single: 单人发送
                group： 群发
        """
        sender = wechat_dict.get('sender') or self.config.default_wechat_sender
        receiver = wechat_dict.get('receiver')
        type = wechat_dict.get('type')
        content = wechat_dict.get('content').decode('utf-8')

        if type == "single":
            result = self.we_mg.send_message(touser=receiver, content=content)
        else:
            result = self.we_mg.send_message(toparty=receiver, content=content)

        logging.info('Send result: {0}'.format(result))
        return result

    def slack_sender(self, slack_dict):
        """
        send slack message
        """
        text = slack_dict.get('text')
        attachment = slack_dict.get('attachment')
        at_list = slack_dict.get('at', [])

        if at_list and isinstance(at_list, list):

            text = ''.join(map(lambda x: "<@%s> " % x, at_list)) + text
        data = {
            "text": text
        }
        if attachment:
            data['attachment'] = attachment

        header = {
            "Content-Type": "application/json"
        }

        result = requests.post(
            url=self.config.slack_url,
            headers=header,
            data=json.dumps(data),
            timeout=10)

        return result.text

    def sms_sender(self, sms_dict):
        """
        send sms message
        """
        mobile_list = sms_dict.get('mobile')
        mobiles = ', '.join(mobile_list)
        content = sms_dict.get('content').decode('unicode-escape')
        logging.info(
            'Oncall SMS receiver: {}, content: {}'.format(
                mobiles, content))

        content_url = urllib.urlencode({'content': content})
        url = "{}?method={}&access_token={}&format={}&channel_code={}&mobile={}&append_code={}&{}".format(
            SMS_URL, SMS_METHOD, SMS_ACCESS_TOKEN, SMS_FORMAT, SMS_CHANNEL_CODE, mobiles, SMS_RC_CODE, content_url)

        resp = requests.get(url=url, verify=False, timeout=10.0)
        logging.info(
            'Send sms result: {} {}'.format(
                resp.status_code,
                resp.text))


if __name__ == '__main__':
    oncall = OnCallRedis()
    oncall.start()
