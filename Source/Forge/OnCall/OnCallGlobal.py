#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: MichaelCho
# @Email:  zuow11@gmail.com
# @Created Date: 2015-10-09 15:31:19
# @Updated Date: 2016-09-12 11:58:08

"""
AppService global variabs which from config file.
"""

import os

from Runtime.Constants import TYPE_STRING, TYPE_INT, TYPE_FLOAT, TYPE_BOOLEAN
from Runtime.Constants import LOG_DEBUG, LOG_INFO, LOG_WARNING, LOG_ERROR, LOG_CRITICAL
from Runtime.Config import Config, ConfigSection, ConfigOption, ConfigException
from Forge.Common.Global import Default


class OnCallConfig(Config):
    """ OnCall config class
    """
    # ------------------------------ General -----------------------------------
    general = ConfigSection(
        "General",
        required=True,
        doc=""
    )
    general.options = [
        ConfigOption(
            name="scribe_host",
            getter="scribe_host",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="scribe_port",
            getter="scribe_port",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.General.scribe_port
        ),
        ConfigOption(
            name="scribe_enable",
            getter="scribe_enable",
            type=TYPE_BOOLEAN,
            required=False,
            default=Default.Forge.General.scribe_enable
        ),
        ConfigOption(
            name="redis_host",
            getter="redis_host",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="redis_port",
            getter="redis_port",
            type=TYPE_INT,
            required=True,
            default=Default.Forge.General.redis_port
        ),
        ConfigOption(
            name="redis_db",
            getter="redis_db",
            type=TYPE_INT,
            required=True
        ),
        ConfigOption(
            name="redis_password",
            getter="redis_password",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.General.redis_password
        ),
        ConfigOption(
            name="mongodb_url",
            getter="mongodb_url",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="mongodb_timeout",
            getter="mongodb_timeout",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.General.mongodb_timeout
        ),
        ConfigOption(
            name="mongodb_db_copy",
            getter="mongodb_db_copy",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.General.mongodb_db_copy
        ),
        ConfigOption(
            name="mongodb_db",
            getter="mongodb_db",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.General.mongodb_db
        ),
        ConfigOption(
            name="domain",
            getter="domain",
            type=TYPE_STRING,
            required=True,
            default=Default.Forge.General.domain
        ),
        ConfigOption(
            name="encrypt_key",
            getter="encrypt_key",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="encrypt_user",
            getter="encrypt_user",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="oncall_url",
            getter="oncall_url",
            type=TYPE_STRING,
            required=False
        ),
        ConfigOption(
            name="img_dir",
            getter="attachment_dir",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.General.attachment_dir
        ),
        ConfigOption(
            name="wechat_surl",
            getter="wechat_surl",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="wechat_scorpsecret",
            getter="wechat_scorpsecret",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="wechat_scorpid",
            getter="wechat_scorpid",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="wechat_qychatsecret",
            getter="wechat_qychatsecret",
            type=TYPE_STRING,
            required=True
        )
    ]

    # ------------------------------ OnCall -----------------------------------
    oncall = ConfigSection(
        "OnCall",
        required=True,
        doc=""
    )
    oncall.options = [
        ConfigOption(
            name="log_dir",
            getter="log_dir",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.OnCall.log_dir
        ),
        ConfigOption(
            name="log_level",
            getter="log_level",
            type=TYPE_STRING,
            required=False,
            default=Default.Forge.OnCall.log_level,
            valid=[LOG_DEBUG, LOG_INFO, LOG_WARNING, LOG_ERROR, LOG_CRITICAL]
        ),
        ConfigOption(
            name="log_rotate",
            getter="log_rotate",
            type=TYPE_INT,
            required=False,
            default=Default.Forge.OnCall.log_rotate
        ),
        ConfigOption(
            name="log_size",
            getter="log_size",
            type=TYPE_FLOAT,
            required=False,
            default=Default.Forge.OnCall.log_size
        ),
        ConfigOption(
            name="port",
            getter="port",
            type=TYPE_INT,
            required=True,
            default=Default.Forge.OnCall.port
        ),
        ConfigOption(
            name="default_wechat_sender",
            getter="default_wechat_sender",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="smtp_host",
            getter="smtp_host",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="smtp_port",
            getter="smtp_port",
            type=TYPE_INT,
            required=True
        ),
        ConfigOption(
            name="smtp_username",
            getter="smtp_username",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="smtp_pwd",
            getter="smtp_pwd",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="smtp_from_name",
            getter="smtp_from_name",
            type=TYPE_STRING,
            required=True
        ),
        ConfigOption(
            name="slack_url",
            getter="slack_url",
            type=TYPE_STRING,
            required=False
        )
    ]

    sections = [general, oncall]

    def __init__(self, cfgfile):
        try:
            Config.__init__(self, cfgfile, self.sections)
            # log
            self.log_level = self.get("log_level")
            self.log_dir = self.get("log_dir")
            self.log_rotate = self.get("log_rotate")
            self.log_file = self.log_dir + '/OnCall.log'
            self.log_size = self.get("log_size")
            self.stdin = Default.Forge.OnCall.stdin
            self.stdout = Default.Forge.OnCall.stdout
            self.stderr = Default.Forge.OnCall.stderr
            self.domain = self.get("domain")
            self.attachment_dir = self.get("attachment_dir")
            # service port
            self.service_port = self.get("port")
            # scribed
            self.scribe_port = self.get("scribe_port")
            self.scribe_host = self.get("scribe_host")
            self.scribe_enable = self.get("scribe_enable")
             # redis
            self.redis_host = self.get("redis_host")
            self.redis_port = self.get("redis_port")
            self.redis_db = self.get("redis_db")
            # mongo
            self.mongo_url = self.get("mongodb_url")
            self.mongo_timeout = self.get("mongodb_timeout")
            self.mongo_db_copy = self.get("mongodb_db_copy")
            self.mongo_db = self.get("mongodb_db")
            # encrypt
            self.aes_key = self.get("encrypt_key")
            self.aes_user = self.get("encrypt_user")
            # email
            self.smtp_host = self.get("smtp_host")
            self.smtp_port = self.get("smtp_port")
            self.smtp_username = self.get("smtp_username")
            self.smtp_pwd = self.get("smtp_pwd")
            self.smtp_from_name = self.get("smtp_from_name")
            # oncall
            self.default_wechat_sender = self.get("default_wechat_sender")
            self.oncall_url = self.get('oncall_url') or 'oncall.{0}'.format(self.domain)
            # wechat
            self.wechat_surl = self.get('wechat_surl')
            self.wechat_scorpsecret = self.get('wechat_scorpsecret')
            self.wechat_scorpid = self.get('wechat_scorpid')
            self.wechat_qychatsecret = self.get('wechat_qychatsecret')
            # slack
            self.slack_url = self.get('slack_url')

            self.__check()
        except KeyError as error:
            raise ConfigException("missing configuration parameter {0}".format(error))
        except Exception as error:
            raise ConfigException(error)

    def __check(self):
        if not os.path.exists(self.log_dir):
            try:
                os.mkdirs(self.log_dir, 0755)
            except Exception:
                raise ConfigException("the log directory <{0}> does not exist".format(self.log_dir))

    def get_attr(self, attr):
        return self.attrs[attr]

    def get_attrs(self):
        return self.attrs.keys()


class OnCallGlobal(object):
    debug = False
    config = None
    db = None
    http_client = None
    es_client = None
    session_manager = None
