#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : QiRui.Su <schangech@gmail.com>
# @Desc    :
# @license : Copyright(C), ***.Inc
# @Contact : QiRui.Su <schangech@gmail.com>

import sys
import json
import redis


redis_client = redis.Redis("redis.in.dataengine.com", "8285", 1)

if not redis_client.ping():
    print("connect redis failed")
    sys.exit(1)

data = {
    "receiver": "suqirui",
    "type": "single",
    "content": "我是一条测试信息2"
}

# r = redis_client.lpush(
#     'wechat',
#     json.dumps(
#         data,
#         ensure_ascii=False,
#         encoding='UTF-8'))
#
# print(r)

data = {
    'receiver': ["suqirui@rongcapital.cn"],
    'subject': "subject",
    'content': "content text",
    # 'format': "text"
}

r = redis_client.lpush(
    'email',
    json.dumps(
        data,
        ensure_ascii=False,
        encoding='UTF-8'))

print(r)