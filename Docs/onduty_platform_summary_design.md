# OnDuty platform

## 1. Summary
 - Agent
 - Adaptor
 - RuleEngine
 - Ticket
 - Scheduler
 - Activity

 ![OnDuty Architecture Diagram](/Users/zuowei/Dropbox/RCapital/picture/on_duty_function_modules.jpeg "OnDuty Function Module Diagram")


## 2. System Dataflow
![OnDuty Activity Diagram](/Users/zuowei/Dropbox/RCapital/picture/onduty_state_diagram.png)


## 3. API

### 3.1 Overview

#### 3.1.1 Schema
RESTful
https
send/receive data as json

#### 3.1.2 Parameters
uri/?`p1`=`v1`&`p2`=`v2`&`p3`=`v3`

#### 3.1.3 Domain
https://api.onduty.rc.dataengine.com

#### 3.1.4 Version
https://api.onduty.rc.dataengine.com/`v1`

#### 3.1.5 HTTP Verbs

Verb  | Description
:----------|:----------
GET | Used for retrieving resources
POST | Used for creating resources
PATCH | Used for updating resources with partial JSON data
DELETE | Used for deleting resources

#### 3.1.6 Status Codes

Status Code | Description
:----------|:----------
200 | OK
400 | Bad request syntax
401 | Unauthorized. The request requires user authentication.
403 | Forbidden.
404 | Not found.
408 | Request timeout.
500 | Internal server error.

#### 3.1.7 Authentication
1. agent
    service key
2. user
    username/password
    username/session id

#### 3.1.8 User agent
OnDuty-API-client

### 3.2 Modules

#### 3.2.1 Handlers

##### 3.2.1.1 Activity

1. GET: Query all the activities with ticket `ticket_id`.

    * endpoint

    ```
    /activity/?ticketId=<ticket_id>&responser=<responser>&from=<started_time_string>&to=<ended_time_string>
    ```

    * params
        - ticketId: string
        - responser: string
        - from: datetime string
        - to: datetime string

    * output

    ```
        {
            'activity': <activity list>
        }
    ```

2. POST: Create a new activity to ticket `ticket_id`

    * endpoint

    ```
    /activity/
    ```

    * params
        - ticket_id: string(required)
        - action: string(required), ['trigger', 'open', 'acknowledge', 'unacknowledge', 'assign', 'notify', 'note', 'resolve', 'close']
        - detail: dict(required)
            - event_description: string
            - event_url: string
            - requester: email
            - responser: email
            - notify_type: string
            - notify_target: email
            - note: string
            - files: array

    * output

    ```
        {
            'activity_id': activity1,
            'ticket_id': string,
            'create_time': datetime,
            'details': [<detail>],
            'created_time': datetime
        }
    ```

##### 3.2.1.2 Agent

1. POST: Add a new agent;

   * endpoint

   ```
   /agent/
   ```

   * params
        - agent_id: string(required)
        - agent_version: string(required)
        - system_info: dict

   * output

   ```
   {
       "message": "Handlered agent agent-1 heartbeat"
   }
   ```

2. GET: List all the agents;

   * endpoint

   ```
   /agent/?agentId=<agent_id>
   ```

   * params
        - agentId: string

   * output

   ```
   {"data": [<agent1>,<agent2>...]
   ```


##### 3.2.1.3 Event

1. POST: Handler new alarm event;

   * endpoint

   ```
   /event/
   ```

   * params
        - agent: dict(required)
            - agent_id: string
            - queued_at: datetime string
            - queued_by: string
        - details: dict(required)
            - event_id: string(required)
            - status: string(required)
            - severity: string(required)
            - ...
        - service_key: string(required)
        - event_type: string(required)
        - incident_key: string(required)
        - description: string

   * output

   ```
   {"message": "......"}
   ```

2. GET: List all or special event;

   * endpoint

   ```
   /event/?agentId=<agent_id>&agentType=<agent_type>&eventType=<event_type>&eventId=<event_id>&severity=<severity>&page=page&limit=limit&from=<started_time_string>&to=<ended_time_string>
   ```

   * params
        - agentId: string
        - agentType: string
        - eventType: string
        - eventId: string
        - severity: string
        - from: datetime string
        - to: datetime string
        - page: string, specify returned page number
        - limit: string, event count of each page

   * output

   ```
   {"data": [<event1>...], "total": <event_count>}
   ```


##### 3.2.1.4 Schedule

1. POST: Add a new schedule;

    * endpoint

    ```
    /schedule/
    ```

    * params
        - since: datetime string(required)
        - until: datetime string(required)
        - resolvegroup: string(required)
        - users: array(required)
        - rotation_type: string(required), [weekly, daily]
        - backups: array
        - is_preview: boolean(required)

    * output

    ```
    schedule object
    ```

2. PUT: Update the special scheduler `schedule_id`;

    * endpoint

    ```
    /schedule/
    ```

    * params
        - schedule_id: string
        - since: datetime string
        - until: datetime string
        - users: array
        - rotation_type: string
        - backups: array
        - is_preview: boolean

    * output

    ```
    schedule object
    ```

3. GET: Get the schedule info;

    * endpint

    ```
    /schedule/?scheduleId=<schedule_id>&from=<started_time_string>&to=<ended_time_string>
    ```

    * params
        - scheduleId: string
        - from: datetime string
        - to: datetime string

    * output

    ```
        {
            'data': <schedule list>
        }
    ```

4. DELETE: Remove special schedule;

    * endpoint

    ```
    /schedule/?scheduleId=<schedule_id>
    ```

    * params
        - scheduleId: string

    * output

    ```
    {"message": "....."}
    ```

5. PATCH: Update special entry of the scheduler `schedule_id`;

    * endpoint

    ```
    /schedule/
    ```

    * params
        - schedule_id: string
        - since: datetime string
        - until: datetime string
        - user: email

    * output

    ```
    schedule object
    ```

##### 3.2.1.5 Session module

1. POST: Create a new account session.

    * endpoint

    ```
    /session/
    ```

    * params
        - email: string(required)
        - password: string(required)

    * output

    ```
        {
            'session_id': string,
            'email': string,
            'department': string,
            'state': string,
            'telephone': string
        }
    ```

2. DELETE: User logout

    * endpoint

    ```
    /session/
    ```

    * params
        - email: string
        - session_id: string

    * output

    ```
        {
            'message': string
        }
    ```

##### 3.2.1.6 Ticket

1. POST: Add a new ticket
    * endpoint

    ```
    /ticket/
    ```

    * params
        - type: string(required), [bug, authorization, workflow]
        - severity: int(required), incident level [1-5]
        - description: string(required), description about the incident
        - requester: string(required), user who report the ticket/on duty(user id)
        - responser: string(required), user who handler the incident(user id)
        - details: dict, detail information abount the ticket
        - ctiId: string, the department about the ticket

    * output

    ```
        {
            ticket_id: string
        }
    ```


2. PUT: Update the special ticket `ticket_id`

    * endpoint

    ```
    /ticket/
    ```

    * params
        - ticket_id: string(required), the ticket which will be updated
        - severity: int, incident level [1-5]
        - description: string, description about the incident
        - responser: string, user who handler the incident(user id)
        - status: string, ticket status
        - cc_list: array
        - details: dict
        - ctiId: string
        - resolvegroupId: string

    * output

    ```
        {
            ticket_id: string,
        }
    ```

3. GET: Get all tickets or the special ticket `ticket_id`

    * endpoint

    ```
    /ticket/?ticketId=<ticket_id>&requester=<requester>&responser=<responser>&severity=<severity>&status=<status>&from=<started_time_string>&to=<ended_time_string>&page=page&limit=limit&department=<department>&resolvegroupId=<resolvegroupId>
    ```

    * params
        - ticketId: string
        - severity: string
        - requester: string
        - responser: string
        - ctiId: string
        - resolvegroupId: string
        - from: start datetime string
        - to: end datetime string
        - page: return page number
        - limit: count in each page

    * output

    ```
        {
            'tickets': <ticket list>
        }
    ```

4. Post: Search Ticket message

	* endpoint
		/Handlers/ticker/search

	```
		Search Ticket by Ticket_ID/Ticket_Description(support Fuzzy query)
	```

	* params
		- id: string
		- description: string

	* output

	```
		{"data": "..."}
	```

##### 3.2.1.7 Category

1. POST: Add a new category of the specified department.

    * endpoint

    ```
    /category/
    ```

    * params
        - type: string(required), the category type
        - name: string(required), the category name
        - description: string, the category description

    * output

    ```
        {
            'data': <category_object>
        }
    ```

2. PUT: Update the category of the specifed department.

    * endpoint

    ```
    /category/
    ```

    * params
        - type: string(required), the category type
        - id: string(required), the category id which will be updated
        - name: string, the new category name
        - description: string, the new category description

    * output

    ```
        {
            'message': 'update category <category_id> successfully'
        }
    ```


3. GET: List all or the specifed department categories.

    * endpoint

    ```
    /category/?type=<type>
    ```

    * params
        - type: string, the specified category type

    * output

    ```
        {
            'data': [<department_categories>]
        }
    ```

4. DELETE: Delete specified categories.

    * endpoint

    ```
    /category/?type=<type>&id=<category_id>
    ```

    * params
        - type: string(required), the specified category type
        - id: string(required), the specified category id

    * output

    ```
        {
            'message': 'remove category <category_id> successfully'
        }
    ```

#### 3.2.2 IPA

##### 3.2.2.1 Account Module

##### 3.2.2.2 Host Module
Host group and host module;

##### 3.2.2.3 Service Module
Service group and service module;

##### 3.2.2.4 Policy
Authentication policy modules;

###### 3.2.2.4.1 PW
Password policy;

###### 3.2.2.4.2 HBAC
HBAC policy;

###### 3.2.2.5 Automember
Auto discovery the Host;

#### 3.2.3 OnCall

#### 3.2.4 CTI

##### 3.2.4.1 CTI
Category-Type-Item management handlers;

1. POST: Create a category/type/item node
    * endpoint

    ```
        /cti/
    ```

    * params

    ```
        - name: string(required), the cti name
        - description: string(required), the cti description string
        - type: string, the cti type(catogory|org|region|department|business|others)
        - parent: string, the parent node id or null if root node
    ```

    * output

    ```
        the cti node data.
    ```

2. PUT: Update the specified cti node
    * endpoint

    ```
        /cti/
    ```

    * params

    ```
        - id: string(required), the cit id
        - name: string
        - description: string
        - type: string
        - parent: string
    ```

    * output

    ```
        the updated cti node.
    ```

3. GET: Get all specified CTI nodes
    * endpoint

    ```
        /cti/?id=<cti_id>
    ```

    * params

    ```
        - id: string(required), the specified cti id
    ```

    * output

    ```
        the specified cti nodes data.
    ```

4. DELETE: Delete the specified cti node
    * endpoint

    ```
        /cti/?id=<cti_id>
    ```

    * params

    ```
        - id: string(required), the specified cti id

    ```

    * output

    ```
        status code and message

    ```

5. Batch Insert CTI

    * endpoint

    ```
        /Handlers/cti/ctibatch
    ```

    * params

    ```
        - cti: array(required)
        - category: string(required) id/name
        - type: string(required) id/name
        - item: string(required) id/name
			example
			{
				"cti": [
					{
						"category": "batch_category_01",
						"type": "batch_type_01",
						"item": "batch_item_01"
					},
					{
						"category": "batch_category_02",
						"type": "batch_type_02",
						"item": "batch_item_02"
					}
				]
			}
    ```

    * output

    ```
        {"message": "success"}
    ```

##### 3.2.4.2 CTI Group

1. GET: Get the specified cti tree
    * endpoint

    ```
        /cti/group/?id=<cti_id>&direction=<direction>
    ```

    *params

    ```
        - id: string, the specified cti id, generate the whole tree if no cti id specified
        - direction: string, `up` or `down`, default is down
    ```

    * output

    ```
        the cti node trees data json.
    ```

##### 3.2.4.3 CTI Admin

1. POST: Add CTI Admin
    * endpoint

    ```
        /cti/admin
    ```

    * params

    ```
        - ctiId: string(required) resolvegrpup id
        - uid: array
        - flag: add/remove
    ```

    * output

    ```
        {"message": "successfully/failed"}
    ```

##### 3.2.4.4 CTI User

1. POST: Add Cti User
    * endpoint

    ```
        /cti/user
    ```

    * params

    ```
        - id: string(required) resolvegrpup id
        - uid: array
        - flag: add/remove
    ```

    * output

    ```
        {"message": "successfully/failed"}
    ```

2. GET: Get the Cti user, if title=admin, only query the cti admin
    * endpoint

    ```
        /cti/user?ctiId=<id>&title=admin
    ```

    * params

    ```
        - ctiId: string
        - title: admin
    ```

    * output

    ```
        the cti users/admins
    ```

#### 3.2.5 Falcon

##### 3.2.5.1 Host
The host handler in CMDB.

1. GET: Get the specified host info
    * endpoint

    ```
        /falcon/host/?hostname=<hostname>
    ```

    * params

    ```
        - hostname: string, the specified hostname
    ```

    * output

    ```
        the host information.
    ```

##### 3.2.5.2 RuleEngine

1. POST: Attach hosts to specified cti
    * endpoint

    ```
        /falcon/rulengine/
    ```

    * params

    ```
        - cti_id: string(required)
        - hosts: array(required), the specified hostnames
    ```

    * output

    ```
        status code and message.
    ```

2. PUT: Attach hosts to another cti or update hosts
    * endpoint

    ```
        /falcon/rulengine/
    ```

    * params

    ```
        - id: string(required), the specified rule engine id
        - cti_id: string, new attached cti id
        - hosts: array, the specified hostnames
    ```

    * output

    ```
        status code and message.
    ```

3. GET: Get the host tree
    * endpoint

    ```
        /falcon/rulengine/?id=<rule_engine_id>&ctId=<cti_id>
    ```

    * params

    ```
        - id: string, the specified rule engine id
        - ctId: string, the specified cti id
    ```

    * output

    ```
        the cti and hosts tree.
    ```

4. DELETE: Deattach hosts to the specified cti
    * endpoint

    ```
        /falcon/rulengine/?id=<rule_engine_id>&ctId=<cti_id>
    ```

    * params

    ```
        - id: string(required), the specified rule engine id
    ```


    * output

    ```
        status code and message.
    ```

#### 3.2.6 Falcon

##### 3.2.6.1 ResolveGroup

1. POST: Add ResolveGroup
    * endpoint

    ```
        /resolvegroup
    ```

    * params

    ```
        - name: string(required)
        - description: string(required)
        - masters: array(required)
        - users: array
        - ctis: array
        - rootcause: array
        - sla: array
        - escalation: array
        - customize: boolean
    ```

    ```{
    "masters":[
        {
            "uid":"450ff95b4017",
            "order":10
        },
        {
            "uid":"6fb389c3045c",
            "order":9
        }
    ],
    "users":[
        {
            "uid":"450ff95b4017",
            "order":10
        },
        {
            "uid":"6fb389c3045c",
            "order":9
        }
    ],
    "name":"test-rg-by-houshoushuai",
    "description":"Description of resolvegroup.",
    "ctis":[
        "f4e495a8bae0"
    ],
    "rootcause":[
        {
            "description":"计划内的人为事件，如计划内关机、重启、网站升级、网站维护等。",
            "name":"系统维护"
        },
        {
            "description":"网络原因导致的故障，如主干网络发生的故障。",
            "name":"网络故障"
        },
        {
            "description":"服务器硬件导致的故障，如磁盘故障、内存故障、网卡故障等。",
            "name":"硬件故障"
        },
        {
            "description":"软件原因导致的故障，如操作系统故障、Web服务器运行故障、应用程序BUG、软件服务容量超载等。",
            "name":"软件故障"
        },
        {
            "description":"基础服务平台故障，比如内部IPA、DNS、VPN、git、Jenkins、wiki、jira等。",
            "name":"基础服务故障"
        }
    ],
    "customize":false,
    "sla":[
        {
            "processing_time":360,
            "severity":1,
            "response_time":30
        },
        {
            "processing_time":720,
            "severity":2,
            "response_time":120
        },
        {
            "processing_time":1440,
            "severity":3,
            "response_time":480
        },
        {
            "processing_time":2880,
            "severity":4,
            "response_time":1440
        },
        {
            "processing_time":5760,
            "severity":5,
            "response_time":2880
        }
    ],
    "escalation":[
        [
            "450ff95b4017",
            "c41fd18bc8d9"
        ]
    ]
    }
    ```


    * output

    ```
        {"message": "successfully/failed"}
    ```

2. PUT: update resolvegroup
    * endpoint

    ```
        /resolvegroup
    ```

    * params

    ```
        - id: string(required), the specified id
        - name: string
        - description: string
        - users: array
        - masters: array
        - ctis: array
    ```

    * output

    ```
        {"message": "successfully/failed"}
    ```

3. GET: Get the host tree
    * endpoint

    ```
        /resolvegroup?id=<ID>&uid=<ID>
    ```

    * params

    ```
        - id: string, the specified resolvegroup id
        - uid: string, the specified user id
    ```

    * output

    ```
        {"data": ...}
    ```

4. DELETE:
    * endpoint
    ```
        /resolvegroup?id=<ID>
    ```

    * params

    ```
        - id: string(required), the specified resolvegroup id
    ```


    * output

    ```
        status message.
    ```

5. Get:

	* endpoint
	'''
	/Handlers/resolvegroup/search?keyword=<key>
	'''

   * params
   '''
   	- keyword: string
   '''
   
   * output
   '''
   	{"data": "result"}
   '''
    
##### 3.2.6.2 ResolveGroup Admin

1. POST: Add ResolveGroup Admin
    * endpoint

    ```
        /resolvegroup/admin
    ```

    * params

    ```
        - id: string(required) resolvegrpup id
        - uid: array
        - flag: add/remove
    ```

    * output

    ```
        {"message": "successfully/failed"}
    ```

##### 3.2.6.3 ResolveGroup User

1. POST: Add ResolveGroup User
    * endpoint

    ```
        /resolvegroup/user
    ```

    * params

    ```
        - id: string(required) resolvegrpup id
        - uid: array
        - flag: add/remove
    ```

    * output

    ```
        {"message": "successfully/failed"}
    ```


2. GET: Get ResolveGroup User

    * endpoint

    ```
        /resolvegroup/user?rgId=<resolvegroupId>
    ```

    * params

    ```
        - rgId: string
    ```

    * output

    ```
        {"data": "resolvegroup user info"}
    ```

##### 3.2.6.4 ResolveGroup Cti

1. POST: Add ResolveGroup Cti
    * endpoint

    ```
        /resolvegroup/cti
    ```

    * params

    ```
        - id: string(required) resolvegrpup id
        - ctiId: array
        - flag: add/remove
    ```

    * output

    ```
        {"message": "successfully/failed"}
    ```

2. GET:
    * endpoint
    ```
        /resolvegroup/cti
    ```

    * params

    ```
        - ctiId: string(required)
    ```

    * output
    ```
        {"data": ""}
    ```

##### 3.2.6.5 ResolveGroup Order

1. POST: Add ResolveGroup Order
    * endpoint
    ```
		/resolvegroup/order
    ```

	* params
	```
		- rgId: String(required)
		- ctiId: String(required)
		- order: array
    ```

	* output
	```
		{"message": "[success/failed]"}
    ```

2. GET:
    * endpoint
    ```
        /resolvegroup/order
    ```

    * params

    ```
        - rgId: string(required)
        - ctiId: String(required)
    ```

    * output
    ```
        {"data": "[user list]"}
    ```

##### 3.2.6.6 Ticket Notify API

1.GET:
    * endpoint

    ```
        /Handlers/ticket/notify
    ```

    * params
        - ticketId: string

    * output
    ```
        {"data": "[notify list]"}
    ```

2.POST:

	* endpoint:

	```
		/Handlers/ticket/notify
	```

    * params
        - ticketId: string (required)
        - description: string
        - user: arraya
        - email: array
        - wechat: array
        - sms: array

    * output
    ```
        {"message": "..."}
    ```

3.PUT:

	* endpoint:

	```
		/Handlers/ticket/notify
	```

    * params
    	- id: string (required)
        - ticketId: string
        - description: string
        - user: array
        - email: array
        - wechat: array
        - sms: array
        - other: array

    * output
    ```
        {"message": "..."}
    ```

4.DELETE:

	* endpoint:
	```
		/Handlers/ticket/notify?ticketId=<>&wechat=<>...
	```

	* params
	    - ticketId: string(required)
	    - email: string
	    - wechat: string
	    - user: string
	    - sms: string

    * output
    ```
        {"message": "..."}
    ```	

##### 3.2.6.7 Ticket Comment API

1. GET:

   - endpoint

     ```
     /Handlers/ticket/comment
     ```

   - params

     - ticketId: string
     - commentId: string

   - output:

     ```json
     {
       "data": [
         {
           "content": "Hello world",  // 评论内容
           "user": {                    // 用户
               "username": "lianghao",
               "uid": "45090ce312dc",
               "email": "lianghao@rongcaptial.cn"
           },
           "created_time": {          // 创建时间
             "$date": 1494836910556
           },
           "id": "cmt-7f1e8a753c2d",  // 评论 ID
           "ticket_id": "case-9e45f209ac5d", // 报障 ID
           "comments": [               // 评论的回复列表
             {
               "reply_tid": "cmt-7f1e8a753c2d",
               "content": "回复 cmt-7f1e8a753c2d",
               "user": {                    // 用户
                   "username": "lianghao",
                   "uid": "45090ce312dc",
                   "email": "lianghao@rongcaptial.cn"
               },
               "created_time": {
                 "$date": 1494836998339
               },
               "id": "cmt-1e3b31d116c8",
               "ticket_id": "case-9e45f209ac5d"
             },
             {
              "reply_tid": "cmt-7f1e8a753c2d", // 所在 Thread 的评论 ID
               "reply_cid": "cmt-1e3b31d116c8", // 指定回复的 comment ID
              "reply_uid": "6fb389c3045c",     // 指定回复的 comment 的用户 ID
               "content": "回复 cmt-1e3b31d116c8, 应该有reply_uid",
               "user": {                    // 用户
                   "username": "lianghao",
                   "uid": "45090ce312dc",
                   "email": "lianghao@rongcaptial.cn"
               },
               "created_time": {
                 "$date": 1494837205994
               },
               "id": "cmt-2eabf6249143",
               "ticket_id": "case-9e45f209ac5d"
             }
           ],
         }
       ]
     }
     ```

2. POST

   - endpoint

     ```
     /Handlers/ticket/comment
     ```

   - params

     ```json
     {
        "ticket_id": "case-xxx",      // 要回复的报障 ID，required
        "content": "Hello world",     // 评论内容, required
        "reply_tid": "cmt-7f1e8a753c2d", // 评论所属 Thread, optional
        "reply_cid": "cmt-1e3b31d116c8"  // 评论所回复的 comment, optional
     }
     ```

   - output

     ```json
     {
       "message": "add comment for ticket case-9e45f209ac5d successfully"
     }
     ```

3. DELETE

   - endpoint

     ```
     /Handlers/ticket/comment?commentId=<comment_id>
     ```

   - params:

     - commentId: string, 评论的 ID

   - output

     ```json
     {
       "message": "Delete comment xxxxxxx successfully"
     }
     ```

#### 3.2.6.8 Ticket attachment

1. POST: Add a ticket attachment
    * endpoint
    ```
        /Handlers/ticket/attachment
    ```

    * params
        - ticket_id: string
        - filename: string
        - filepath: string
        - description: string

    * output
    ```
        {
            'message': <result>
        }
    ```

#### 3.2.6.9 Related ticket

1. POST: Add a related ticket
    * endpoint
    ```
        /Handlers/ticket/related
    ```

    * params
        - ticket_id: string
        - related_id: string (ticket id or ticket url)

    * output
    ```
        {
            'message': <result>
        }
    ```

2. DELETE: Delete a related ticket
    * endpoint
    ```
        /Handlers/ticket/related
    ```

    * params
        - ticket_id: string
        - related_id: string (ticket id or ticket url)

    * output
    ```
        {
            'message': <result>
        }
    ```

#### 3.2.6.10 Duplicated ticket

1. POST: Add a Duplicated ticket
    * endpoint
    ```
        /Handlers/ticket/duplicated
    ```

    * params
        - ticket_id: string
        - duplicated_id: string (ticket id or ticket url)

    * output
    ```
        {
            'message': <result>
        }
    ```

2. DELETE: Delete a duplicated ticket
    * endpoint
    ```
        /Handlers/ticket/related
    ```

    * params
        - ticket_id: string
        - duplicated_id: string (ticket id or ticket url)

    * output
    ```
        {
            'message': <result>
        }
    ```

#### 3.2.6.11 Ticket count

2. DELETE: Delete a duplicated ticket
    * endpoint
    ```
        /Handlers/ticket/count?status=open&status=closed&resolvegroupId=rg-9ec68cb92bb9&resolvegroupId=rg-637e50a75ded
    ```

    * params
        - status: string
        - resolvegroupId: string (ticket id or ticket url)

    * output
    ```
        {
          "data": [
            {
              "responser": 80       # 分配给我的
            },
            {
              "cc_list": 15         # 关注列表
            },
            {
              "requester": 11       # 我创建的
            },
            {
              "group": 120          # 分配给我的/关注列表/我创建的
            }
          ]
        }
    ```

## 4. Data Structure

1. agent

    Field Name  | Type         | Desc
    :----------:| :----------: | :----------
    id          | string       | agent id
    hostname    | string       | instance hostname of agent in
    ip          | array        | instance ips
    status      | string       | agent status [inactive, active, disconnected, removed]

2. event

    Field Name  | Type         | Desc
    :----------:| :----------: | :----------
    agent_id    | string       | agent id of the alarm from
    service_key | string       | service key
    description | string       | event description
    event_type  | string       | [trigger, resolve]
    incident_key| string       | <trigger_id>-<hostname>
    report_by   | string       | [zabbix]
    report_at   | datetime     | event reported time
    details     | dict         | event detail information, status, event id, severity and so on

    zabbix severity | definition | colour
    :----------:| :----------: | :----------
    Not classified | Unknown severity | Grey
    Information  | For information purposes | Light green
    Warning  | Be warned | Yellow
    Average | Average problem | Orange
    High | Something important has happened | Red
    Disaster | Disaster. Financial losses, etc | Bright red

3. ticket

    Field Name  | Type         | Desc
    :----------:| :----------: | :----------
    id          | string       | incident case id
    type        | string       | [bug, account, host, service, policy]
    severity    | string       | [critical, urgent, high, normal, low]
    report_date | datetime     |
    update_date | datetime     |
    duration    | string       |
    subject     | string       |
    description | string       |
    status      | string       | [opened, processing, resolved, closed]
    location    | string       |
    category    | string       |
    requester   | string       |
    responser   | string       |
    cc_list     | array        | carbon copy list
    note        | dict         |
    -- tag      | string       | [not fix, duplicated, not issue, by design, not recur]
    -- reason   | string       |

    Severity | First-Response Time | Description / Support Plan
    :----------:| :----------: | :----------
    low         | 24 hours  | You have a general development question or want to request a feature.
    normal      | 12 hours | You can work around the problem. Non-critical functions of your application are behaving abnormally. You have a time-sensitive development question.
    high        | 4 hours | You can't work around the problem. Critical functions of your application are impaired or degraded.
    urgent      | 1 hour | You can't work around the problem, and your organization is significantly impacted. Important functions of your application are unavailable.
    critical    | 30 minutes | You can't work around the problem, and your organization is at risk. Critical functions of your application are unavailable.

4. activity

    Field Name  | Type         | Desc
    :----------:| :----------: | :----------
    id          | string       |
    action      | string       | [trigger, open, acknowledge, unacknowledge, reassign, update, notify, close]
    ticket_id   | string       |
    responser   | string       |
    datetime    | datetime     |
    description | string       |
    files       | list         |

5. severity

    Field Name  | Type         | Desc
    :----------:| :----------: | :----------
    id          | string       | 'default' or rg_id
    severity    | string       | [Critical, Urgent, High, Normal, Low]
    priority    | int          | [1, 2, 3, 4, 5]
    respond     | int          | unit: minutes

6. service

    Field Name  | Type         | Desc
    :----------:| :----------: | :----------
    key         | string       |
    type        | string       |
    agents      | list         |

7. category

    Field Name  | Type         | Desc
    :----------:| :----------: | :----------
    type        | string       | general or department name
    name        | string       |
    description | string       |

    type | name | description
    :----------:|:----------:|:----------
    general | system maintenance/系统维护 | Planned human events, such as planned shutdown, restart, website upgrades, website maintenance, etc./计划内的人为事件，如计划内关机、重启、网站升级、网站维护等。
    general | network fault/网络故障 | Network causes of failure, such as the failure of the backbone network./网络原因导致的故障，如主干网络发生的故障。
    general | hardware failure/硬件故障 | Server hardware led to the failure, such as disk failure, memory failure, network card failure, etc../服务器硬件导致的故障，如磁盘故障、内存故障、网卡故障等。
    general | software failure/软件故障 | Software causes of failure, such as operating system failure, Web server operation failure, application BUG, software service capacity overload, etc../软件原因导致的故障，如操作系统故障、Web服务器运行故障、应用程序BUG、软件服务容量超载等。
    general | public service failure/基础服务故障 | Basic service platform failure, such as internal IPA, DNS, VPN, GIT, Jenkins, Wiki, JIRA, etc../基础服务平台故障，比如内部IPA、DNS、VPN、git、Jenkins、wiki、jira等。

8. schedule

    Field Name  | Type          | Desc
    :----------:| :-----------: | :----------
    id          | string        |
    start       | datetime      |
    end         | datetime      |
    rotation_type | string      | [daily, weekly]
    department  | string        |
    status      | string        |
    users       | array         |
    entries     | array         | [{"start": start_time, "end": end_time, "user": user}]
    backups     | array         |

9. cti

    Field Name  |   Type        |   Desc
    :----------:| :-----------: | :----------
    id          | string        |
    name        | string        |
    description | string        |
    type        | string        | ['org', 'region', 'department', 'business', '']
    parent      | string        | the parent CTI node id

10. account

    Field Name  |   Type        |   Desc
    :----------:| :-----------: | :----------
    id          | string        | uid
    username    | string        | username(the prefix of email)
    email       | string        | email
    firstname   | string        |
    lastname    | string        |
    telephone   | string        |
    wechat      | string        |
    avatar      | string        |
    title       | string        |
    status      | string        | ['enabled', 'disabled', 'removed']
    create_time | long          |
    update_time | long          |
    cti_ids     | array         | cti ids and map to the org/region/departments and so on
    masters     | array         | cti ids user mastered
    rand_seed   | string        |

11. rule: rules in rule engine

    Field Name  |   Type        |   Desc
    :----------:| :-----------: | :----------
    id          | string        | the rule engine id
    hosts       | array         | the all hostnames attached
    cti_id      | string        | the attached cti id

12. host: all managed hosts

    Field Name  |   Type        |   Desc
    :----------:| :-----------: | :----------
    hostname    | string        | host name string
    type        | string        | virtual or physical machine, 'a' -> aliyun, 'v' -> 'openstack', 'p' -> physical machine
    location    | string        | beijing, dalian and so on
    cpu         | dict          | cpu info
    -- cores    | int           | core number
    -- model_name | string      | cpu model name
    memory      | dict          | memory info
    -- total    | int           | total size
    -- free     | int           |
    -- buffers  | int           |
    disk        | array         | dick info
    -- mountpoint | string      |
    -- filesystem | string      |
    -- total      | int         |
    -- used       | int         |
    -- free       | int         |
    -- free_percent | float     |
    -- inode_total  | float     |
    -- inode_used   | float     |
    -- inode_free   | float     |
    -- inode_free_percent | float |
    os          | dict          | operation system info
    -- type     | string        | windows, centos, ubuntu
    -- kernel   | string        |
    -- version  | string        |
    agent_version | string      |

13. resolvegroup

    Field Name  |   Type        |   Desc
    :----------:| :-----------: | :----------
    id          | string        |
    name        | string        |
    description | string        |
    users       | array         |
    masters     | array         |
    ctis        | array         |
    add_time    | int           |
    update_time | int           |
