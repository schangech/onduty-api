###  Data schema
1. ceph

	```
	{
    	"user_id": "ObjectId"
    	"access_key": "String",
    	"secret_key": "String",
    	"bucket": [
        	{
        		"bucket_name": "String"
        		"bucket_max_size": "int",
        		"bucket_max_objects": "int"
        	},
    	],
    	"user_caps": "String",
    	"max_buckets": "int",
    	"suspended": "Boolean",
    	"user_max_objects": "int",
    	"user_max_size": "int",
	}
	```
	
2. files

	```
	{
		"user_id": "ObjectId",
		"name": "String",
		"key": "String",
		"path": "String",
		"size":	"int",
		"create_time": "Timestamp",
		"update_time": "Timestamp",
		"type": "String",
		"secret"： “String”
	}
	```

### API

1. POST: 创建目录
    * endpoint
	
    ```
        /NetDisk/directory
    ```

    * params
        - type name		: string
        - param name	: 目录名
        - type path		: string
        - param path	: 路径

    * output
    
    ```
        {
            'email': lisuo@rongcapital.cn,
            'body': [
            	{
					"dir": ["doc", "test"]
            	
           	 	}
            ]
        }
    ```
    
2.  DELETE: 删除目录
    * endpoint
	
    ```
        /NetDisk/directory/path/name
    ```
    
    
3.  PUT: 修改目录名
    * endpoint
	
    ```
        /NetDisk/directory
    ```
    
    
    * params
    	
    	- type new_name		: string
    	- param new_name	: 新目录名称
    	- type old_name		: string
    	- param odl_name	: 要修改的目录名称
    	- type path			: string
        - param path		: 路径
    	

    * output
    
    ```
        {
            'email': lisuo@rongcapital.cn,
            'new_name': newdoc,
            'old_name': olddoc
        }
    ```  

4. GET: 查看指定目录文件列表

    * endpoint
	
	```
		/NetDisk/directory
	```
	 * params
	 
    	- type path		: string
        - param path	: 路径（可以不指定）

    * output
    
    ```
        {
            'email': lisuo@rongcapital.cn,
            'body': [
      			{
            		"name": test.txt,
            		"create_time": 2017年1月8日,
            		"update_time": 2017年1月8日,
            		"key": test/aaaaaa.txt,
     				"path": test,
            		"size": 1KB,
            		"type": txt         	
            	}
            ]
        }
    ```  

5. POST: 上传文件对象

    * endpoint
	
	```
		/NetDisk/files
	```

    * output
    
    ```
        {
            'email': lisuo@rongcapital.cn,
           	'body':[
           		{
           			"file_name": test.txt,
           			"key": adfafafafafaeeee.txt
           		}
           	]
        }
    ```  

6. GET: 下载文件对象

	* endpoint
	
	```
		/NetDisk/files/path/file_name
	```
	
	
7. DELETE: 删除文件对象

	* endpoint
	
	```
		/NetDisk/files/path/file_name
	```
	
8.	PUT: 修改文件对象名称

	* endpoint
	
	```
		/NetDisk/files
	```
	
	 * params
	 
	 	- type path					: string
	 	- param path				: 路径名称
	 	- type new_file_name		: string
    	- param new_file_name		: 新文件名称
    	- type old_file_name		: string
    	- param old_file_name		: 要修改的文件名称

    * output
    
    ```
        {
            "email": lisuo@rongcapital.cn,
            "new_file_name": test.txt,
            "old_file_name": old_test.txt,    
        }
    ```  
