# OnDuty platform 3rd

## 1. Summary
 - 合并OnDuty和OmniSential agent
    * 合并agent
    * 定制zabbix监控event模板，并将event发到OmniSential agent
 - 规则引擎：
    * 集成 OmniSential portal并重新规划设计
    * judge模块适配不同监控event，并能应用新规则
    * scheduler调度规则并持久化调度任务
 - API
    * rule
    * oder
    * sla
    * user ssh key管理
    * 用户审核失败时删除mongodb数据并发送通知邮件
 - 接入ELK日志报警
 - 小文件对象存储服务及相应调整
 - 其它:
    * @metion通知功能
    * 微信号流程和界面优化
    * 排班UI优化

URL地址列表更新
[('/Handlers/activity/?', <class 'Handlers.Activity.ActivityHandler'>),
 ('/Handlers/agent/?', <class 'Handlers.Agent.AgentHandler'>),
 ('/Handlers/agent/(?P<agentId>[a-zA-Z0-9-_()+&=\\.%@]*$)', <class 'Handlers.Agent.AgentHandler'>),
 ('/Handlers/attachment/?', <class 'Handlers.Attachment.AttachmentHandler'>),
 ('/Handlers/category/?', <class 'Handlers.Category.CategoryHandler'>),
 ('/Handlers/cti/group/?', <class 'Handlers.Cti.GroupHandler'>),
 ('/Handlers/cti/?', <class 'Handlers.Cti.CtiHandler'>),
 ('/Handlers/cti/admin/?', <class 'Handlers.Cti.AdminHandler'>),
 ('/Handlers/cti/user/?', <class 'Handlers.Cti.UserHandler'>),
 ('/Handlers/event/?', <class 'Handlers.Event.EventHandler'>),
 ('/Handlers/event/statistic/?', <class 'Handlers.Event.StatisticHandler'>),
 ('/Handlers/falcon/host/?', <class 'Handlers.Falcon.HostHandler'>),
 ('/Handlers/falcon/rulengine/?', <class 'Handlers.Falcon.RulengineHandler'>),
 ('/Handlers/resolvegroup/order/?', <class 'Handlers.ResolveGroup.OrderHandler'>),
 ('/Handlers/resolvegroup/user/?', <class 'Handlers.ResolveGroup.UserHandler'>),
 ('/Handlers/resolvegroup/?', <class 'Handlers.ResolveGroup.ResolveGroupHandler'>),
 ('/Handlers/resolvegroup/admin/?', <class 'Handlers.ResolveGroup.AdminHandler'>),
 ('/Handlers/resolvegroup/cti/?', <class 'Handlers.ResolveGroup.CtiHandler'>),
 ('/Handlers/sla/?', <class 'Handlers.SLA.SLAHandler'>),
 ('/Handlers/schedule/duty/?', <class 'Handlers.Schedule.DutyHandler'>),
 ('/Handlers/schedule/?', <class 'Handlers.Schedule.ScheduleHandler'>),
 ('/Handlers/session/?', <class 'Handlers.Session.SessionHandler'>),
 ('/Handlers/ticket/watcher/?', <class 'Handlers.Ticket.WatcherHandler'>),
 ('/Handlers/ticket/?', <class 'Handlers.Ticket.TicketHandler'>),
 ('/Handlers/ticket/statistic/?', <class 'Handlers.Ticket.StatisticHandler'>),
 ('/IPA/account/active/?', <class 'IPA.Account.ActiveHandler'>),
 ('/IPA/account/icon/?', <class 'IPA.Account.IconHandler'>),
 ('/IPA/account/password/?', <class 'IPA.Account.PasswordHandler'>),
 ('/IPA/account/passexpiration/?', <class 'IPA.Account.PassexpirationHandler'>),
 ('/IPA/account/group/?', <class 'IPA.Account.GroupHandler'>),
 ('/IPA/account/?', <class 'IPA.Account.AccountHandler'>),
 ('/IPA/authentication/?', <class 'IPA.Authentication.AuthenticationHandler'>),
 ('/IPA/automember/host/?', <class 'IPA.Automember.HostHandler'>),
 ('/IPA/host/group/?', <class 'IPA.Host.GroupHandler'>),
 ('/IPA/host/?', <class 'IPA.Host.HostHandler'>),
 ('/IPA/permission/?', <class 'IPA.Permission.PermissionHandler'>),
 ('/IPA/policy/passwd/?', <class 'IPA.Policy.PasswdHandler'>),
 ('/IPA/policy/hbac/?', <class 'IPA.Policy.HbacHandler'>),
 ('/IPA/service/?', <class 'IPA.Service.ServiceHandler'>),
 ('/IPA/service/group/?', <class 'IPA.Service.GroupHandler'>),
 ('/Falcon/host/?', <class 'Falcon.Host.HostHandler'>)]

## 2. System Architecture

 ![OnDuty 3rd Architecture Diagram](/Users/zuowei/Dropbox/RCapital/picture/onduty_design_3rd.png "OnDuty 3rd Architecture Diagram")

## 3. Modules

### 3.1 Falcon

#### 3.1.1 Host

1. GET: Query all the hosts or in the special hostgroup.
    * endpoint
    ```
        /host/?groupId=<group_id>&limit=[10|20|100]
    ```

    * params
        - groupId: string, hostgroup id
        - limit: integer

    * output
    ```
        host list
    ```

2. POST: Add hosts into the hostgroup.
    * endpoint
    ```
        /host/
    ```

    * params
        - groupId: string(required)
        - hosts: array(required), host ids

    * output
    ```
        message
    ```

3. DELETE: Remove the special host from the hostgroup.
    * endpoint
    ```
        /host/?groupId=<group_id>&hosts=[...]
    ```

    * params
        - groupId: string(required)
        - hosts: array(required)

    * output
    ```
        message
    ```


#### 3.1.2 hostgroup

1. POST: Create a hostgroup
    * endpoint
    ```
        /hostgroup/
    ```

    * params
        - name: string(required)
        - hosts: array, host ids in the group
        - templates: array, template ids with the hostgroup
        - clusters: array, aggregator ids with the hostgroup

    * output
    ```
    {
        id: <group_id>,
        name: <group_name>,
        creator: <username>,
        create_at: <create_time>,
        hosts: [<host_id>],
        templates: [<template_id>],
        clusters: [<aggregator_id>]
    }
    ```

2. PUT: Update the special hostgroup
    * endpoint
    ```
        /hostgroup/
    ```

    * params
        - id: string(required), the hostgroup id
        - name: string
        - hosts: array, host ids in the group
        - templates: array, template ids with the hostgroup
        - clusters: array, aggregator ids with the hostgroup

    * output
    ```
        message
    ```

3. DELETE: Remove the special hostgroup
    * endpoint
    ```
        /hostgroup/?id=<hostgroup_id>
    ```

    * params
        - id: string(required)

    * output
    ```
        message
    ```

4. GET: Get the special hostgroup
    * endpoint
    ```
        /hostgroup/?id=<hostgroup_id>
    ```

    * params
        - id: string(required)

    * output
    ```
    [
        {
            id: <group_id>,
            name: <group_name>,
            creator: <username>,
            create_at: <create_time>,
            hosts: [<host_id>],
            templates: [<template_id>],
            clusters: [<aggregator_id>]
        }
    ]
    ```

#### 3.1.3 template

1. POST: create a new template
    * endpoint
    ```
        /template/
    ```

    * params
        - name: string(required)
        - parent: string, the parent template id
        - strategies: array, the strategy ids binded with the template

    * output
    ```
        message
    ```

2. PUT: update the special template
    * endpoint
    ```
        /template/
    ```

    * params
        - id: string(required)
        - name: string
        - parent: string, the parent template id
        - strategies: array, the strategy ids binded with the template

    * output
    ```
        message
    ```

3. DELETE: Remove the special template
    * endpoint
    ```
        /template/?id=<template_id>
    ```

    * params
        - id: string(required)

    * output
    ```
        message
    ```

4. GET: List the special templates
    * endpoint
    ```
        /template/?id=<template_id>
    ```

    * params
        - id: string

    * output
    ```
    [
        {
            id: <string>,
            name: <string>,
            parent_id: <string>,
            create_user: <user_name>,
            create_at: <datetime>,
            strategies: [<strategy_id>]
        }
    ]
    ```


#### 3.1.4 strategy

1. POST: create a new strategy
    * endpoint
    ```
        /strategy/
    ```

    * params
        - metric: string(required)
        - function: string(required)
        - right_value: float(required)
        - max_step: int(required)
        - priority: int(required)
        - tags: string
        - note: string
        - run_begin: string
        - run_end: string

    * output
    ```
    {
        id: <strategy_id>
    }
    ```

2. PUT: update the special strategy
    ```
        /strategy/
    ```

    * params
        - id: string(required),
        - metric: string
        - function: string
        - right_value: float(requir
        - max_step: int
        - priority: int
        - tags: string
        - note: string
        - run_begin: string
        - run_end: string

    * output
    ```
        message
    ```

3. DELETE: remove the special strategy, warning when binded with the hostgroup
    ```
        /strategy/?id=<strategy_id>
    ```

    * params
        - id: string(required)

    * output
    ```
        message
    ```

4. GET
    ```
        /strategy/?id=<strategy_id>
    ```

    * params
        - id: string(required)

    * output
    ```
    [<strategy>]
    ```

#### 3.1.5 aggregator

1. POST: create a new aggregator
    * endpoint
    ```
        /aggregator/
    ```

    * params
        - numerator: string(required)
        - denominator: string(required)
        - metric: string(required)
        - endpoint: string(required)
        - tags: string(required)
        - ds_type: string(required)
        - step: int(required)

    * output
    ```
    {
        id: <aggregator_id>
    }
    ```


2. PUT: update the special aggregator
    * endpoint
    ```
        /aggregator/
    ```

    * params
        - id: string(required)
        - numerator: string
        - denominator: string
        - metric: string
        - endpoint: string
        - tags: string
        - ds_type: string
        - step: int

    * output
    ```
        message
    ```

3. DELETE: remove the special aggregator
    * endpoint
    ```
        /aggregator/?id=<aggregator_id>
    ```

    * params
        id: string(required)

    * output
    ```
        message
    ```

4. GET: list all aggregators or the special ones
    * endpoint
    ```
        /aggregator/?id=<aggregator_id>
    ```

    * params
        - id: string(required)

    * output
    ```
    [<aggregator>]
    ```

#### 3.1.6 nodata

1. POST: create a new nodata item
    * endpoint
    ```
        /nodata/
    ```

    * params
        - name: string(required)
        - object: string(required)
        - type: string(requried)
        - ds_type: string(required)
        - step: int(required)
        - tags: string

    * output
    ```
    {
        id: <nodata_id>
    }
    ```


2. PUT: update the special nodata item
    * endpoint
    ```
        /nodata/
    ```

    * params
        - id: string(required)
        - name: string
        - object: string
        - type: string
        - ds_type: string
        - step: int
        - tags: string

    * output
    ```
        message
    ```

3. DELETE: remove the special nodata item
    * endpoint
    ```
        /nodate/?id=<nodata_id>
    ```

    * params
        - id: string(required)

    * output
    ```
        message
    ```

4. GET: list all nodata items or the special ones
    * endpoint
    ```
        /nodate/?id=<nodata_id>
    ```

    * params
        - id: string

    * output
    ```
    [<nodate_item>]
    ```


#### 3.1.7 expression

1. POST: create a new expression
    * endpoint
    ```
        /expression/
    ```

    * params
        - expression: string(required)
        - function: string(required)
        - operator: string(required)
        - right_value: float(required)
        - max_step: int(required)
        - priority: int(required)
        - note: string

    * output
    ```
    {
        id: <exp_id>
    }
    ```

2. PUT: update the special expression
    * endpoint
    ```
        /expression/
    ```

    * params
        - id: string(required)
        - expression: string
        - function: string
        - operator: string
        - right_value: float
        - max_step: int
        - priority: int
        - note: string

    * output
    ```
        message
    ```

3. DELETE: remove the special expression
    * endpoint
    ```
        /expression/?id=<exp_id>
    ```

    * params
        - id: string(required)

    * output
    ```
        message
    ```

4. GET: list all expressions or the special ones
    * endpoint
    ```
        /expression/?id=<exp_id>
    ```

    * params
        - id: string(required)

    * output
    ```
    [<expression_item>]
    ```

#### 3.1.8 sla

1. POST: create a new sla
    * endpoint
    ```
        /sla/
    ```

    * params
        - id: string(required)
        - sla: array
        --- severity : int
        --- response_time: int
        --- processing_time: int
        --- description : string
        - escalation: array
        - customize: bool

    * output
    ```
    {
        message: [successfully/failed]
    }
    ```

2. PUT: update the special sla
    * endpoint
    ```
        /sla/
    ```

    * params
        - id: string(required)
        - sla: array
        --- severity: int
        --- response_time: int
        --- processing_time: int
        --- description: string
        - escalation: array
        - customize: bool

    * output
    ```
        message
    ```

3. DELETE: remove the special sla
    * endpoint
    ```
        /sla/?id=<rg_id>
    ```

    * params
        - id: string(required)

    * output
    ```
        message
    ```

4. GET: list all expressions or the special ones
    * endpoint
    ```
        /sla/?id=<rg_id>
    ```

    * params
        - id: string(required)

    * output
    ```
    {"data":}
    ```

#### 3.1.9 account ssh key

1. GET: Get the current user's ssh key
    * endpoint
    ```
        /ipa/account/sshkey
    ```

    * params
        - sshPublicKey: array(string array, added)

    * output
    ```
        {
            'data': <sshkey>
        }
    ```

2. PUT: Update the current user's ssh key
    * endpoint
    ```
        /ipa/account/sshkey
    ```

    * params
        - sshPublicKey: array(string array, added)

    * output
    ```
        {
            'message': <result>
        }
    ```

#### 3.1.10 account activeness management

1. PUT: update account activeness
    * endpoint
    ```
        /ipa/account/active/
    ```

    * note
    ```
        delete the user from mongodb and send user email about the reason.
    ```


#### 3.1.11 root cause

1. GET: Find root cause by resolve group Id or by root cause Id
    * endpoint
    ```
        /Handlers/rootcause/?resolvegroupId=<resolvegroupId>&rootcauseId=<rootcauseId>
    ```

    * params
        - resolvegroupId: string
        - rootcauseId: string

    * output
    ```
        {
            'data': <result>
        }
    ```

2. POST: Create a root cause
    * endpoint
    ```
        /Handlers/rootcause
    ```

    * params
        - resolvegroup_id: string
        - name: string
        - description: string

    * output
    ```
        {
            'message': <result>
        }
    ```

3. PUT: Update root cause
    * endpoint
    ```
        /Handlers/rootcause
    ```

    * params
        - rootcause_id: string
        - name: string
        - description: string

    * output
    ```
        {
            'message': <result>
        }
    ```

4. DELETE: Delete root cause (Cancel the associated with resolvegroup)
    * endpoint
    ```
        /Handlers/rootcause
    ```

    * params
        - rootcauseId: string

    * output
    ```
        {
            'message': <result>
        }
    ```

#### 3.1.12 wechat

1. GET: Get if the user followed qy wechat
    * endpoint
    ```
        /Handlers/account/?email=<email>
    ```

    * params
        - email: string

    * output
    ```
        {
            'data': <true or false>
        }
    ```


#### 3.1.13 ticket comments

1. GET: Get a ticket's all comments
    * endpoint
    ```
        /Handlers/comment
    ```

    * params
        - ticketId: string

    * output
    ```
        {
            'data': <result>
        }
    ```

2. POST: Add a comment to a ticket
    * endpoint
    ```
        /Handlers/comment
    ```

    * params
        - ticket_id: string (required)
        - content: string (required)
        - user: string (required) (commit user id)
        - at: array (at user id list)
        - reply_to: string (reply to user id)


    * output
    ```
        {
            'message': <result>
        }
    ```

3. DELETE: Delete a comment
    * endpoint
    ```
        /Handlers/comment
    ```

    * params
        - commentId: string

    * output
    ```
        {
            'message': <result>
        }
    ```

## 4. Data Structure

### 4.1 resolvegroup

   Field Name   |   Type        |   Desc
   :----------: | :-----------: | :----------
   id           | string        |
   name         | string        |
   description  | string        |
   users        | array         |
   --user_id    | string        |
   --order      | int           | [0, 100], default 60
   masters      | array         |
   -- uid       | string        |
   -- order     | int           |
   ctis         | array         |
   add_time     | int           |
   update_time  | int           |


### 4.2 sla

   Field Name   |   Type        |   Desc
   :----------: | :-----------: | :----------
   id           | string        | 'default' or rg_id
   sla          | array          |
   --severity     | int           | severity reported by monitor, eg:[1, 5]
   --priority     | int           | severity defined by rg, [1, 5]
   --interval     | int           | unit: minute
   --description  | string        |
   escalation   | bool          | true: upgrade the priority when traverse the rg; false: not upgrade the priority; default false
   leadergroup  | bool          | true: report to leadergroup; false: report to leader; default false

### 4.3 job

### 4.4 zabbix event模板
