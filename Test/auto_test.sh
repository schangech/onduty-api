#! /bin/sh
#
# test.sh
# Copyright (C) 2017 Alex <lisuo@rongcapital.cn>
#
# Distributed under terms of the Desired License license.
#

RunTest='Forge.AppService.NetDisk.RunTest'
DirUnitTestPrefix='Forge.AppService.NetDisk.DirectoryTest.DirectoryTestCase'
FileUnitTestPrefix='Forge.AppService.NetDisk.FilesTest.FilesTestCase'


# Directory api test
TestDir(){
    python -m $RunTest "$DirUnitTestPrefix.test_get_directory"
    #python -m $RunTest "$DirUnitTestPrefix.test_create_directory"
    #python -m $RunTest "$DirUnitTestPrefix.test_get_name_directory"
    #python -m $RunTest "$DirUnitTestPrefix.test_change_directory"
}

TestDelDir(){
    python -m $RunTest "$DirUnitTestPrefix.test_delete_directory"
}

# Files api test
TestFile(){
    python -m $RunTest "$FileUnitTestPrefix.test_upload_file"
    #python -m $RunTest "$FileUnitTestPrefix.test_download_file"
    #python -m $RunTest "$FileUnitTestPrefix.test_change_file"
    #python -m $RunTest "$FileUnitTestPrefix.test_delete_file"
}

TestDir
TestFile
#TestDelDir
