#!/usr/bin/env python
# -*- coding: utf-8 -*-

# __author__ = 'liuzhenwu'

import logging
import json
from tornado.httpclient import HTTPError
from tornado.testing import AsyncTestCase
from tornado.testing import AsyncHTTPClient, gen_test, LogTrapTestCase
from Forge.OnCall.Config import Config, login


class WeChatTestCase(AsyncTestCase, LogTrapTestCase):

    url = Config.handlers['wechat']
    # session = login(Config.data['email'], Config.data['password'])
    # auth = "{\"email\": \"liuzhenwu@rongcapital.cn\",\"session\": \"%s\"}" % session
    header = {"Content-Type": "application/json; indent=4"}

    @gen_test
    def test_send_wechat(self):
        data = {
            # "sender": "",
            "receiver": "liuzhenwu",
            "type": "single",
            "content": "好高兴啊"
        }
        body = json.dumps(data)

        try:
            client = AsyncHTTPClient(self.io_loop)
            response = yield client.fetch(self.url, method='POST', headers=self.header, body=body)
            self.assertEqual(response.code, 200)
            logging.info("test_get_account_all(): get() ut passed")
        except Exception, e:
            self.assertEqual(e.code, 400)
            logging.info("test_get_account_all(): get ut failed {0}".format(e))
            raise e
