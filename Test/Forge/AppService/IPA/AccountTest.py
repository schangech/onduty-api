#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Alex <lisuo@rongcapital.cn>
# Date: 2017-02-14  14:37

"""
AccountTestCase api
"""

import logging
import json
import requests
from StringIO import StringIO
from tornado.httpclient import HTTPError
from tornado.testing import AsyncTestCase
from tornado.testing import AsyncHTTPClient, gen_test, LogTrapTestCase
from Forge.AppService.Config import  auth_header
from Forge.AppService.Config import  Config


TESTCONTEND = """
ttttttttttttttttttttttt
ttttttttttttttttttttttt
"""

TESTKEY= 'attachment/8c6845011ec8596491bee1b1346ee8c5.testfile'

class IconTestCase(AsyncTestCase, LogTrapTestCase):

    auth = auth_header()
    url = Config.service_url

    file_name = "testfile"

    @gen_test
    def test_upload_file(self):

        url = "%s/%s" % (self.url, "IPA/account/icon")

        body = {
            "test": (self.file_name, StringIO(TESTCONTEND)),
        }

        headers = self.auth
        try:
            response = requests.put(url=url, files=body, headers=headers)
            self.assertEqual(response.status_code, 200)
            logging.info("IconTestCase.test_upload_file(): success. %s" % response.content)
        except Exception, e:
            print("ERROR: %s" % e)

    @gen_test
    def test_download_file(self):

        url = "%s/%s/%s" % (self.url, "IPA/account/icon", TESTKEY)

        body = None

        headers = self.auth
        try:
            response = requests.get(url=url, files=body, headers=headers)
            self.assertEqual(response.status_code, 200)
            logging.info("IconTestCase.test_download_file(): success. %s" % response.content)
        except Exception, e:
            print("ERROR: %s" % e)
