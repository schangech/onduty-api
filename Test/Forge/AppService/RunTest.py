#!/usr/bin/env python
# -*- coding: utf-8 -*-

# __author__ = 'liuzhenwu'

"""
run all aps uts.
"""

from __future__ import absolute_import, division, print_function, with_statement
import sys
import logging
import textwrap
import tornado.testing
from tornado.options import add_parse_callback
from tornado.test.util import unittest

TEST_MODULES = [
    'Forge.AppService.Handlers.AttachmentTest',
    'Forge.AppService.IPA.AccountTest',
    'Forge.AppService.Handlers.Schedule'
]


def all():
    return unittest.defaultTestLoader.loadTestsFromNames(TEST_MODULES)


class TornadoTextTestRunner(unittest.TextTestRunner):
    def run(self, test):
        result = super(TornadoTextTestRunner, self).run(test)
        if result.skipped:
            skip_reasons = set(reason for (test, reason) in result.skipped)
            self.stream.write(textwrap.fill(
                "Some tests were skipped because: %s" %
                ", ".join(sorted(skip_reasons))))
            self.stream.write("\n")
        return result


class LogCounter(logging.Filter):
    """Counts the number of WARNING or higher log records."""
    def __init__(self, *args, **kwargs):
        # Can't use super() because logging.
        # Filter is an old-style class in py26
        logging.Filter.__init__(self, *args, **kwargs)
        self.warning_count = self.error_count = 0

    def filter(self, record):
        if record.levelno >= logging.ERROR:
            self.error_count += 1
        elif record.levelno >= logging.WARNING:
            self.warning_count += 1
        return True


def main():
    log_counter = LogCounter()
    add_parse_callback(
        lambda: logging.getLogger().handlers[0].addFilter(log_counter))

    kwargs = {}
    if sys.version_info >= (3, 2):
        # HACK:  unittest.main will make its own changes to the warning
        # configuration, which may conflict with the settings above
        # or command-line flags like -bb.  Passing warnings=False
        # suppresses this behavior, although this looks like an implementation
        # detail.  http://bugs.python.org/issue15626
        kwargs['warnings'] = False
    kwargs['testRunner'] = TornadoTextTestRunner

    # try:
    tornado.testing.main(**kwargs)
    # finally:
    #     sys.exit(1)

if __name__ == '__main__':
    main()
