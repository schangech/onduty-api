#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @Author: zuowei
# @Eamil:  zuow11@gmail.com
# @Created Date: 2015-12-16 22:09:37
# @Updated Date: 2015-12-17 17:12:53

"""
AppService config module.
"""

import sys
import os
import logging
import json
from datetime import datetime, date, timedelta
# from pymongo import Connection
from requests import request


class Config(object):

    service_url = 'http://127.0.0.1:8700'

    mongo_url = '10.200.0.123:8290'

    headers = {
       'Accept': 'application/json',
    }

    handlers = {
        'email': '{0}/email/'.format(service_url),
        'wechat': '{0}/wechat'.format(service_url),
        'session': "%s/Handlers/session" % (service_url),
        'schedule': '{0}/Handlers/schedule'.format(service_url)
    }

    # test data
    data = {
        'email': 'lisuo@rongcapital.cn',
        'password': '20110506!@#js',
    }

    session = None
    alert_id = None

    # conn = Connection(mongo_url)
    # db = conn['rasp']

    # init log
    date_str = date.today().__str__()
    log_filename = 'log/onerasp_aps_ut_{0}.log'.format(date_str)
    log_filename = os.path.join(os.environ["INSTANT_HOME"], log_filename)

    handler = logging.handlers.RotatingFileHandler(log_filename, mode='a', maxBytes=(1024*1024*10))
    formatter = logging.Formatter("%(asctime)s-%(levelname)s-%(message)s")
    handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.removeHandler(handler)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)


def login(email, password):
    data = json.dumps({
        'email': email,
        'password': password
    })
    resp = request('POST', Config.handlers['session'], headers=Config.headers, data=data)
    if resp.status_code != 200:
        return False
    result = resp.json()
    #return result['session']
    return result
    # return json.loads(resp.text)


def auth_header():

    email = Config.data['email']
    password = Config.data['password']

    session = Config.session or login(email, password)
    Config.session = session

    # add auth in header
    auth = json.dumps({
        'email': session['email'],
        'session': session['session']
    })

    return {
        'Accept': 'application/json',
        'Authorization': auth
    }


def list_agents(user_id):

    agents = Config.db.agent.find(
        {
            'user_id': user_id
        }
    )

    return [agent['id'] for agent in agents if 'id' in agent and agent['id']]


def list_alerts(user_id):

    alerts = Config.db.alert.find(
        {
            'user_id': user_id
        }
    )

    return [alert['id'] for alert in alerts if 'id' in alert and alert['id']]


def clean_test_data():

    user = Config.db.user.find_one({'email': Config.data['email']})
    if user:
        # remove relative agents, alerts first
        Config.db.agent.remove({'user_id': user['id']})
        Config.db.agent.remove({'user_id': user['id']})

        # remove account info at last
        Config.db.user.remove({'email': Config.data['email']})


def ipa_auth():
    pass
