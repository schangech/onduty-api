#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Alex <lisuo@rongcapital.cn>
# Date: 2017-02-09  17:18

"""
Directory Test Case.
"""

import logging
import json
from tornado.httpclient import HTTPError
from tornado.testing import AsyncTestCase
from tornado.testing import AsyncHTTPClient, gen_test, LogTrapTestCase
from Forge.AppService.NetDisk.Config import  auth_header
from Forge.AppService.NetDisk.Config import  Config


class DirectoryTestCase(AsyncTestCase, LogTrapTestCase):

    auth = auth_header()
    url = Config.service_url

    test_dir = "doc"
    path = '/'
    change_dir = "new_doc"


    @gen_test
    def test_create_directory(self):

        url = "%s/%s" % (self.url, "NetDisk/directory")

        body = json.dumps({
            "name": self.test_dir,
            "path": self.path
        })

        headers = self.auth
        try:
            client = AsyncHTTPClient(self.io_loop)
            response = yield client.fetch(url, method='POST', headers=headers, body=body)
            self.assertEqual(response.code, 200)
            logging.info("DirectoryTestCase.test_create_directory(): success. %s" % response.body)
        except Exception, e:
            logging.error("DirectoryTestCase.test_create_directory(): failure")
            print ("ERROR: %s" % e)

    @gen_test
    def test_delete_directory(self):

        url = "%s/%s/%s" % (self.url, "NetDisk/directory", self.change_dir)

        body = None

        headers = self.auth
        print headers
        print url

        try:
            client = AsyncHTTPClient(self.io_loop)
            response = yield client.fetch(url, method='DELETE', headers=headers, body=body)
            self.assertEqual(response.code, 200)
            logging.info("DirectoryTestCase.test_delete_directory(): success." )
        except Exception, e:
            logging.error("DirectoryTestCase.test_delete_directory(): failure")
            print ("ERROR: %s" % e)

    @gen_test
    def test_change_directory(self):
        url = "%s/%s" % (self.url, "NetDisk/directory")

        body = json.dumps({
            "old_name": self.test_dir,
            "new_name": self.change_dir,
            "path": self.path
        })

        headers = self.auth
        print headers
        print url

        try:
            client = AsyncHTTPClient(self.io_loop)
            response = yield client.fetch(url, method='PUT', headers=headers, body=body)
            self.assertEqual(response.code, 200)
            logging.info("DirectoryTestCase.test_change_directory(): success. %s" % response.body)
        except Exception, e:
            logging.error("DirectoryTestCase.test_change_directory(): failure")
            print e

    @gen_test
    def test_get_directory(self):

        url = "%s/%s" % (self.url, "NetDisk/directory")

        body = None

        headers = self.auth

        try:
            client = AsyncHTTPClient(self.io_loop)
            response = yield client.fetch(url, method='GET', headers=headers, body=body)
            self.assertEqual(response.code, 200)
            logging.info("DirectoryTestCase.test_get_directory(): success. %s" % response.body)
        except Exception, e:
            logging.error("DirectoryTestCase.test_get_directory(): failure")
            print e
        pass

    @gen_test
    def test_get_name_directory(self):

        url = "%s/%s?name=%s" % (self.url, "NetDisk/directory", self.test_dir)

        body = None

        headers = self.auth

        try:
            client = AsyncHTTPClient(self.io_loop)
            response = yield client.fetch(url, method='GET', headers=headers, body=body)
            self.assertEqual(response.code, 200)
            logging.info("DirectoryTestCase.test_get_name_directory(): success. %s" % response.body)
        except Exception, e:
            logging.error("DirectoryTestCase.test_get_name_directory(): failure")
            print e
        pass
