#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Alex <lisuo@rongcapital.cn>
# Date: 2017-02-09  17:18

"""
FilesHandler Test Case
"""

import logging
import json
from  StringIO import StringIO
import requests

from  DirectoryTest import DirectoryTestCase
from tornado.httpclient import HTTPError
from tornado.testing import AsyncTestCase
from tornado.testing import AsyncHTTPClient, gen_test, LogTrapTestCase
from Forge.AppService.NetDisk.Config import  auth_header
from Forge.AppService.NetDisk.Config import  Config


TESTCONTEND = """
ttttttttttttttttttttttt
ttttttttttttttttttttttt
"""

TESTKEY='%s/8c6845011ec8596491bee1b1346ee8c5.testfile' % DirectoryTestCase.change_dir

class FilesTestCase(AsyncTestCase, LogTrapTestCase):

    auth = auth_header()
    url = Config.service_url

    key = None
    file_name = "testfile"
    change_file_name = "newfile"
    path = DirectoryTestCase.path

    change_dir = DirectoryTestCase.change_dir

    @gen_test
    def test_upload_file(self):

        url = "%s/%s" % (self.url, "NetDisk/files")

        body = {
            "test": (self.file_name, StringIO(TESTCONTEND)),
            "name": DirectoryTestCase.change_dir,
        }

        headers = self.auth
        try:
            response = requests.post(url=url, files=body, headers=headers)
            self.assertEqual(response.status_code, 200)
            self.key = response.json()["body"][0]["key"]
            logging.info("DirectoryTestCase.test_upload_file(): success. %s" % response.content)
        except Exception, e:
            logging.error("DirectoryTestCase.test_upload_files(): failure")
            print("ERROR: %s" % e)

    @gen_test
    def test_download_file(self):
        url = "%s/%s/%s" % (self.url, "NetDisk/files", TESTKEY)

        headers = self.auth
        try:
            response = requests.get(url=url, headers=headers)
            self.assertEqual(response.status_code, 200)
            logging.info("DirectoryTestCase.test_download_file(): success.")
        except Exception, e:
            logging.error("DirectoryTestCase.test_download_files(): failure")
            print("ERROR: %s" % e)

    @gen_test
    def test_delete_file(self):

        url = "%s/%s/%s" % (self.url, "NetDisk/files", TESTKEY)

        headers = self.auth
        try:
            response = requests.delete(url=url, headers=headers)
            self.assertEqual(response.status_code, 200)
            logging.info("DirectoryTestCase.test_delete_file(): success. %s" % response.content)
        except Exception, e:
            logging.error("DirectoryTestCase.test_delete_files(): failure")
            print("ERROR: %s" % e)

    @gen_test
    def test_change_file(self):

        url = "%s/%s" % (self.url, "NetDisk/files")

        body = json.dumps({
            "new_file_name": self.change_file_name,
            "old_file_name": self.file_name,
            "path": self.path + self.change_dir
        })

        headers = self.auth
        try:
            response = requests.put(url=url, data=body, headers=headers)
            self.assertEqual(response.status_code, 200)
            logging.info("DirectoryTestCase.test_change_file(): success. %s" % response.content)
        except Exception, e:
            logging.error("DirectoryTestCase.test_change_files(): failure")
            print("ERROR: %s" % e)
