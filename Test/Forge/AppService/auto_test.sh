#! /bin/sh
#
# auto_test.sh
# Copyright (C) 2017 Alex <lisuo@rongcapital.cn>
#
# Distributed under terms of the Desired License license.
#


python -m Forge.AppService.RunTest Forge.AppService.Handlers.AttachmentTest.AttachmentTestCase
python -m Forge.AppService.RunTest Forge.AppService.IPA.AccountTest.IconTestCase
