#!/usr/bin/env python
# -*- coding: utf-8 -*-

# __author__ = 'liuzhenwu'

import logging
import json
from tornado.httpclient import HTTPError
from tornado.testing import AsyncTestCase
from tornado.testing import AsyncHTTPClient, gen_test, LogTrapTestCase
from Forge.AppService.Config import Config, login


class ScheduleTestCase(AsyncTestCase, LogTrapTestCase):

    url = Config.handlers['schedule']
    session = login(Config.data['email'], Config.data['password'])
    auth = "{\"email\": \"liuzhenwu@rongcapital.cn\",\"session\": \"%s\"}" % session
    header = {"Content-Type": "application/json; indent=4"}

    @gen_test
    def test_create_schedule(self):
        data = {
            "since": "2017-02-28T08:00:00Z",
            "resolvegroup": "rg-637e50a75ded",
            "users": [
                "liuzhenwu@rongcapital.cn",
                "houshoushuai@rongcapital.cn"
            ],
            "rotation_type": "weekly",
            "is_preview": True,         # 预览模式
            "handoff_day": 0,           # (0 - 6)对应周一到周天
            "handoff_time": [18, 0, 0]
        }
        body = json.dumps(data)

        try:
            client = AsyncHTTPClient(self.io_loop)
            response = yield client.fetch(self.url, method='POST', headers=self.header, body=body)
            self.assertEqual(response.code, 200)
            logging.info("test_create_schedule(): get() ut passed")
        except Exception, e:
            self.assertEqual(e.code, 400)
            logging.info("test_create_schedule(): get ut failed {0}".format(e))
            raise e

    @gen_test
    def test_update_schedule(self):
        data = {
            "since": "2017-02-28T08:00:00Z",
            "resolvegroup": "rg-637e50a75ded",
            "users": [
                "liuzhenwu@rongcapital.cn",
                "houshoushuai@rongcapital.cn"
            ],
            "schedule_id": "sched-21dc8001",
            "rotation_type": "weekly",
            "is_preview": True,  # 预览模式
            "handoff_day": 0,  # (0 - 6)对应周一到周天
            "handoff_time": [18, 0, 0]
        }
        body = json.dumps(data)

        try:
            client = AsyncHTTPClient(self.io_loop)
            response = yield client.fetch(self.url, method='PUT', headers=self.header, body=body)
            self.assertEqual(response.code, 200)
            logging.info("test_update_schedule(): get() ut passed")
        except Exception, e:
            self.assertEqual(e.code, 400)
            logging.info("test_update_schedule(): get ut failed {0}".format(e))
            raise e
